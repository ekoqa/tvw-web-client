*** Settings ***
Library    SeleniumLibrary
Library    Collections

*** Variables ***

${filePath}    /opt/robotframework/tests/main/resources
${downloadFilePath}    /opt/robotframework/tests/CheckDownloads

# ${filePath}    /Users/tanawootchawapon/Desktop/EkoQA/main/resources
# ${filePath}    C:/Amity2/EkoQA/main/resources
# ${filePath}    /Users/eko/Desktop/EkoQA/main/resources



#-------------------------------BROWSER-----------------------------------------------

# ${BROWSER}       headlesschrome
${BROWSER}    chrome
# ${BROWSER}      headlessfirefox
# ${BROWSER}      firefox
#${BROWSER}      safari

${timeout}    35s
${speed}    0.7s
#------------------------------Staging------------------------------------------------

${AdminURL}          https://sea-staging-admin.ekoapp.com
${AdminLibraryiframe}    "https://sea-staging-admin.km.dev.ekoapp.com/?lang=en"
${AdminQuiziframe}    "https://sea-staging-admin.km.dev.ekoapp.com/quiz?lang=en"
${AdminWorkflowiframe}    "https://sea-staging-workflow-admin.ekoapp.com/oauth2/wf-staging"

# ${ClientURL}    https://eko-dev.ekoapp.com/?regionCode=sea
${ClientURL}    https://eko-dev.ekoapp.com
${PhotoURL}    https://www.japan-guide.com/thumb/destination_tokyo.jpg
${EkoURL}    https://eko-dev.ekoapp.com
${ClientLibraryiframe}    "https://sea-staging.km.dev.ekoapp.com?appName=Eko&lang=en"
${NewLandingPageURL}    https://sea-staging-register.ekoapp.com/ 
${LibraryURL}    https://eko-dev.ekoapp.com?redirect_path=doc%2F5ffeb651882ace001ffbd514&eko_action=open_library
${GoogleURL}    https://google.com
${ConfelenceURL}    https://eko-dev.ekoapp.com/conferences
${EkogreenURL}    https://ekogreen.ekoapp.com
${SSOURL}    https://onedrive.live.com/about/th-th/signin/
${TVWLoginURL}    https://auth-ekoidp-staging-staging.ekoapp.com/sso/login
${10MinURL}    https://10minutemail.com
${TempmailninjaURL}    https://tempmail.ninja/
${YahooURL}    https://login.yahoo.com
${TempmailURL}    https://temp-mail.org/en/
${mainUsername}     try11111    #normal network
${mainYahooUsername}     test99email
${mainPassword}     password
${mainYahooPassword}     123456789gG$
${mainUserDisplayName}  main staging
${Email}     ekoqa91@gmail.com
${emailPassword}     password123456gG

# -------------------------- testrail ------------------------

${testRailServer}           https://ekoapp20.testrail.io/
${testRailUser}             ratchadaporn@ekoapp.com
${testRailPassword}         Ekoqa12345
${testRailRun}              392
${testRailProtocol}         https