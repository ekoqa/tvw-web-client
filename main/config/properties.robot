*** Settings ***
Library    SeleniumLibrary


*** Variables ***

${longText}  There are a lot of upsides to working from home as a parent: you’re closer to the kids, you save costs on child care, and you have more time to spend with them now that you don’t have to commute. But needless to say, this setup also comes with its own set of unique challenges. According to a survey conducted by Regus, 48% of respondents reported that children or family demanding attention was the number one issue when working from home. So if you’re a parent out there who finds themself in the situation of having to juggle both work and child care while you’re at home, here are our top tips for succeeding in both
${longJapanText}  犬は人間社会のさまざまな役割を果たしており、しばしば働く犬として訓練されています。伝統的な仕事のない犬のために、幅広い犬のスポーツが自然なスキルを発揮する機会を提供しています.12,000年以上にわたって、犬は狩猟の仲間、保護者、友人として人間と一緒に住んでいたと推定されています。犬は世界で最も人気のあるペットの1つであり、「人間の親友」と呼ばれています。貧しい人も金持ちも、犬はあなたに忠実で忠実で、愛する人になります。ペットの犬は、家族の生活や環境に簡単に馴染むので、他のペットと同じように世話をする必要があります。病気のときは、獣医の診察が必要です。犬は愛情で繁栄し、愛と注意を浴びせると喜んで尾を振ります。犬はまた、何か悪いことをしたと言われた場合、座って不機嫌になります。動物を虐待することは非常に悪いことであるだけでなく、自分の防御に噛みつく人もいるので、いつでも犬を虐待してはいけません。田園地帯や地元の公園やレクリエーション場に連れて行くことができます。ここでは、走り回ったりゲームをプレイしたり、健康に保つために必要な運動をしたりできます。小さな枝、スティック、またはフリズビーを投げれば、犬にアイテムを取りに行き、持ち帰るように教えることができます。犬は、ポケットハンカチのサイズとほぼ同じ面積に2億2千万個近くの匂いに敏感な細胞を持っています（人間の切手のサイズと比べて500万個と比べて）。いくつかの品種は、香りの検出において卓越するために選択的に飼育されています。
${linkText}    https://www.ekoapp.com/
${ekoLink}    www.ekoapp.com
${ekoDescription}    Turning workplaces into places where all employees can thrive.  #The virtual workspace helping teams stay engaged, productive and connected while working remotely.
${facebookDescription}    Create an account or log into Facebook. Connect with friends, family and other people you know. Share photos and videos, send messages and get updates.
${yahooDescription}    Go beyond news and email. Get finance, lifestyle and other fresh content daily. See now.
${createGroupevent}    main staging created chat
${renameGCEvent}    chatevent1 web renamed this chat to EditCloseGroup
${addMemberEvent}    chatevent1 web added chatevent3 web to this chat
${removeMemberEvent}    chatevent1 web removed chatevent2 web
${removeMemberEventInChat}    chat1 web removed chat2 web
${createTopicEvent}    chatevent1 web created topic newTopic
${changeCoverPhotoEvent}    chatevent1 web updated cover picture
${deleteUserIcon}    /bedcfbaa22ba66a8d5292f21ca0c9a8d.png
# ${deleteUserIcon}    /deab721cacef44f9b7d3b435ae787cd7
${longTranslate}    Dogs play a variety of roles in human society and are often trained as working dogs
${email}    Email
${phoneNumber}    Phone Number
${portalDescription}    Enjoy movie form around the world
${editString}    Edited
${todayString}    Today at
${invitorEmail}    ekoqa92+
${invitorEmailDomain}    @outlook.com
${qaEmail}    ekoqa92@outlook.com
${startCallEvent}    web started a call
${cancelCallEvent}    Call canceled
${CTRL_OR_COMMAND}    ${EMPTY}

#-------------------- fileName ------------------------------------

${imageInChatFile}    chat_image.jpg
${videoInChatFile}    chat_video.mp4
${coverProfile}    coverProfile.jpg
${pdfFile}    workflow_file.pdf


${TRUEVWORLDJSON}    {"url": "https://vroom.truevirtualworld.com","displayCreateMenu": false}
${JITSIJSON}    {"url": "https://meet.jit.si/","displayCreateMenu": false}

