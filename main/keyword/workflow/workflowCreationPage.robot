*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${WORKFLOWTEMPLATESEARCHFIELD}    //*[@data-qa-anchor="search-box"]
${FIRSTWORKFLOWTEMPLATELIST}    //div[contains(@class, "InfiniteScroll")]/div[contains(@class, "ListItem")][1]
${SEARCHFORWORKFLOWUSERFIELD}    //div[@class="Select-input"]/input
${CONFIRMSELECTUSERINWORKFLOWBTN}    //div[contains(@class, "FooterControl")]/button[2]
${SUBMITWORKFLOWBTN}    //div[contains(@class, "FooterActions")]/button[2]
${FIRSTDIRECTORYGROUPINRECEIVERLIST}    //div[contains(@class, "Scrollable")]/div[2]/div[contains(@class, "PickerRowContainer")][1]   
# ${CONFIRMDATEBTN}    //a[@class="ant-calendar-ok-btn"]   
${CONFIRMDATEBTN}    //*[contains(@class,"calendar-ok-btn")]  
# ${CURRENTHOURSELECTEDLIST}    //div[@class="ant-time-picker-panel-select"][1]/ul/li[contains(@class, "selected")]
${CURRENTHOURSELECTEDLIST}    //div[@class="antd3-time-picker-panel-combobox"]/div[1]//li[contains(@class, "selected")]
${CLEARTIMEBTN}    //a[@class="ant-time-picker-panel-clear-btn"]
${CONFIRMACTIONBTN}    //div[@class="ant-confirm-btns"]/button[2]
${DUEDATETEXTFIELDINWORKFLOW}    //label[@for="dueDate"]
${PRIORITYFIELDINWORKFLOW}    //label[@for="priority"]
${SCHEDULEFIELDINWORKFLOW}    //label[@for="scheduleWorkflow"]
${WORKFLOWTEMPLATETITLETEXTFIELD}    //div[@class="antd3-modal-title"]
${CLOSEWORKFLOWCREATIONPAGEBTN}    //button[@class="antd3-modal-close"]

*** Keywords ***

inputWorkflowTemplateKeyword
    [Arguments]    ${text}
    Wait until element is enabled    ${WORKFLOWTEMPLATESEARCHFIELD}   timeout=${timeout}
    Input text    ${WORKFLOWTEMPLATESEARCHFIELD}    ${text}

selectFirstWorkflowTemplateInList
    Wait until element is enabled    ${FIRSTWORKFLOWTEMPLATELIST}    timeout=${timeout}
    Click element    ${FIRSTWORKFLOWTEMPLATELIST}

inputTextInCustomField
    [Arguments]    ${field}    ${text}
    # Wait until element is enabled    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//input    timeout=${timeout}
    # Input text    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//input    ${text}

    Wait until element is enabled    //span[contains(text(),"${field}")]//ancestor::div[contains(@class, "antd3-form-item-label")]//following-sibling::div[contains(@class, "antd3-form-item-control")]/div/span/input    timeout=${timeout}
    Input text    //span[contains(text(),"${field}")]//ancestor::div[contains(@class, "antd3-form-item-label")]//following-sibling::div[contains(@class, "antd3-form-item-control")]/div/span/input    ${text}

inputTextInTextAreaField
    [Arguments]    ${field}    ${text}
    # Wait until element is enabled    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//textarea    timeout=${timeout}
    # Input text    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//textarea    ${text}

    Wait until element is enabled    //span[contains(text(),"${field}")]//ancestor::div[contains(@class, "antd3-form-item-label")]//following-sibling::div[contains(@class, "antd3-form-item-control")]/div/span/textarea    timeout=${timeout}
    Input text    //span[contains(text(),"${field}")]//ancestor::div[contains(@class, "antd3-form-item-label")]//following-sibling::div[contains(@class, "antd3-form-item-control")]/div/span/textarea    ${text}

clearTextInCustomField
    [Arguments]    ${field}
    ${value}=     Get Element Attribute   //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//input      value
    ${backspacesCount}=    Get Length      ${value}
    Run Keyword If    """${value}""" != ''    
    ...     Repeat Keyword  ${backspacesCount}  Press Keys  //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//input   BACKSPACE

selectMultipleChoiceInCustomField
    [Arguments]    ${field}    @{index}
    :FOR  ${element}  IN  @{index}
    \    Wait until element is enabled    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//label[${element}]//input    timeout=${timeout}
    \    Click element    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//label[${element}]//input

selectSingleChoiceInCustomField
    [Arguments]    ${field}    ${index}
    # Wait until element is enabled    //label[@title="${field}"]/parent::*/following-sibling::div//label[${index}]//input    timeout=${timeout}
    # Click element    //label[@title="${field}"]/parent::*/following-sibling::div//label[${index}]//input
    Wait until element is enabled    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//label[${index}]    timeout=${timeout}
    Click element    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//label[${index}]

uploadImageInCustomField
    [Arguments]    ${field}
    Wait until element is enabled    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//input    timeout=${timeout}
    Choose file    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//input    ${filePath}/${imageInChatFile}

  uploadAttachmentInCustomField
    [Arguments]    ${field}
    Wait until element is enabled    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//input    timeout=${timeout}
    Choose file    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//input    ${filePath}/${pdfFile}

clickOpenUserListInField
    [Arguments]    ${field}
    # Wait until element is enabled    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//button[contains(@class, "Open")]    timeout=${timeout}
    # Click element    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//button[contains(@class, "Open")]

    Wait until element is enabled    //span[contains(text(),"${field}")]//ancestor::div[contains(@class, "antd3-form-item-label")]//following-sibling::div[contains(@class, "antd3-form-item-control-wrapper")]//descendant::button[contains(@class, "UnstyledButton")]    timeout=${timeout}
    Click element    //span[contains(text(),"${field}")]//ancestor::div[contains(@class, "antd3-form-item-label")]//following-sibling::div[contains(@class, "antd3-form-item-control-wrapper")]//descendant::button[contains(@class, "UnstyledButton")]

searchForWorkflowReceiver
    [Arguments]    ${text}
    Wait until element is enabled    ${SEARCHFORWORKFLOWUSERFIELD}    timeout=${timeout}
    Input text    ${SEARCHFORWORKFLOWUSERFIELD}    ${text}

selectUserToAddInWorkflow
    [Arguments]    ${text}
    Wait until element is enabled    //h4//*[text()="${text}"]    timeout=${timeout}
    Click element    //h4//*[text()="${text}"]

clickConfirmToSelectReceiver
    Wait until element is enabled    ${CONFIRMSELECTUSERINWORKFLOWBTN}    timeout=${timeout}
    Click element    ${CONFIRMSELECTUSERINWORKFLOWBTN}

clickSubmitWorkflow
    Wait until element is enabled    ${SUBMITWORKFLOWBTN}    timeout=${timeout}
    Click element    ${SUBMITWORKFLOWBTN}

selectFirstDirectoryGroupReceiver
    Wait until element is enabled    ${FIRSTDIRECTORYGROUPINRECEIVERLIST}    timeout=${timeout}
    Click element    ${FIRSTDIRECTORYGROUPINRECEIVERLIST}

selectWorkflowReceiverByIndex
    [Arguments]    ${index}
    Wait until element is enabled    //div[text()="Contacts"]/following-sibling::div[${index}]    timeout=${timeout}
    Click element    //div[text()="Contacts"]/following-sibling::div[${index}]

clickOpenCalendarDate
    [Arguments]    ${field}
    Wait until element is visible    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//*[@data-icon="calendar-alt"]   timeout=${timeout}
    Click element    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//*[@data-icon="calendar-alt"]

# clickOpenCalendarDate
#     Wait until element is visible    //*[contains(@class,"calendar-picker")]   timeout=${timeout}
#     Click element    //*[contains(@class,"calendar-picker")]

clickOpenTimeSetUpInWorkflow
    [Arguments]    ${field}
    Scroll element into view    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//span[contains(@class, "time-picker-icon")]
    Wait until element is visible    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//span[contains(@class, "time-picker-icon")]   timeout=${timeout}
    Click element    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//span[contains(@class, "time-picker-icon")]

clickConfirmSelectedDate
    Wait until element is enabled    ${CONFIRMDATEBTN}    timeout=${timeout}
    Click element    ${CONFIRMDATEBTN}

selectCurrentHourInTimeField
    Wait until element is enabled    ${CURRENTHOURSELECTEDLIST}    timeout=${timeout}
    Click element    ${CURRENTHOURSELECTEDLIST}

closeHourSelectionInWorkflow
    [Arguments]    ${field}
    Wait until element is visible    //label[@title][contains(text(),"${field}")]    timeout=${timeout}
    Click element    //label[@title][contains(text(),"${field}")]

clickToggleButtonInCustomField
    [Arguments]    ${field}
    Wait until element is enabled    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//span[@id]    timeout=${timeout}
    Click element    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//span[@id]

# clickToggleButtonInCustomField
#     [Arguments]    ${field}
#     Wait until element is enabled    //*[contains(@class,"calendar-ok-btn")]      timeout=${timeout}
#     Click element    //*[contains(@class,"calendar-ok-btn")]  

clickActionButtonInWorkflow
    [Arguments]    ${index}=1
    Wait until element is enabled    //div[contains(@class, "ButtonContainer")]/button[${index}]    timeout=${timeout}
    Click element    //div[contains(@class, "ButtonContainer")]/button[${index}]

clickConfirmActionInWorkflow
    Wait until element is enabled    ${CONFIRMACTIONBTN}    timeout=${timeout}
    Click element    ${CONFIRMACTIONBTN}

removeUserFromCustomField
    [Arguments]    ${field}    ${user}
    Wait until element is enabled    //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//div[contains(text(), "${user}")]/following-sibling::button    timeout=${timeout}
    Click element     //label[@title][contains(text(),"${field}")]/parent::*/following-sibling::div//div[contains(text(), "${user}")]/following-sibling::button

clickCloseWorkflowCreationPage
    Wait until element is enabled    ${CLOSEWORKFLOWCREATIONPAGEBTN}    timeout=${timeout}
    Click element    ${CLOSEWORKFLOWCREATIONPAGEBTN}

inputTextInCustomNumberField
    [Arguments]    ${field}    ${text}
    Wait until element is enabled    //span[contains(text(),"${field}")]//ancestor::div[contains(@class, "styles__StyledFormItem")]//following-sibling::div[contains(@class, "antd3-input-number-input-wrap")]/input    timeout=${timeout}
    Input text    //span[contains(text(),"${field}")]//ancestor::div[contains(@class, "styles__StyledFormItem")]//following-sibling::div[contains(@class, "antd3-input-number-input-wrap")]/input    ${text}

selectSingleChoiceInButtonField
    [Arguments]    ${field}    ${index}
    Wait until element is enabled    //span[contains(text(),"${field}")]//ancestor::div[contains(@class, "styles__StyledFormItem")]//descendant::span[contains(@class, "antd3-form-item-children")]/div/label[${index}]      timeout=${timeout}
    Click element    //span[contains(text(),"${field}")]//ancestor::div[contains(@class, "styles__StyledFormItem")]//descendant::span[contains(@class, "antd3-form-item-children")]/div/label[${index}]
#----------------- verify ----------------------------------------

isDueDateFieldDisplay
    Wait until element is visible    ${DUEDATETEXTFIELDINWORKFLOW}    timeout=${timeout}
    
isPriorityFieldDisplay
    Wait until element is visible    ${PRIORITYFIELDINWORKFLOW}    timeout=${timeout}

isScheduleFieldDisplay
    Wait until element is visible    ${SCHEDULEFIELDINWORKFLOW}    timeout=${timeout}

isDueDateFieldNotDisplay
    Wait until element is not visible    ${DUEDATETEXTFIELDINWORKFLOW}    timeout=${timeout}

isScheduleFieldNotDisplay
    Wait until element is not visible    ${SCHEDULEFIELDINWORKFLOW}    timeout=${timeout}

isAppRedirectToCorrectWorkflowTemplate
    [Arguments]    ${text}
    Wait until element is visible    ${WORKFLOWTEMPLATETITLETEXTFIELD}    timeout=${timeout}
    Element Should Contain    ${WORKFLOWTEMPLATETITLETEXTFIELD}    ${text}
    