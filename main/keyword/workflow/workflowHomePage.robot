*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${WORKFLOWLIST}    //div[contains(@class, "ListItem")]
${CREATEWORKFLOWBTN}    //div[contains(@class, "FlexHeaderRight")]/button
${FILTERWORKFLOWICON}    //div[contains(@class, "Filter")]/button
${FILTERAWAITINGLIST}    //ul[contains(@class, "dropdown-menu")]/li[text()="Awaiting your Action"]
${ADVANCESEARCHICON}    //div[contains(@class, "AdvanceSearch")]/button
${APPLYADVANCEFILTERBTN}    //div[contains(@class, "FormFooter")]/button[2]
${WORKFLOWSEARCHFIELD}    //input[@type="search"]
${WORKFLOWUNREADBADGE}    //a[@data-qa-anchor="appnav-menu-workflows"]//sup[@data-show="true"]//p[@class="current"]
${WORKFLOWCOMMENTBTN}    //*[contains(@class, 'styles__WrapRight')]/button
${WORKFLOWCOMMENTCLOSEBTN}    //*[contains(@class, 'styles__CloseButton')]
${WORKFLOWBLUEHIGHLIGHT}    //div[contains(@class, "is-unread")]

*** Keywords ***

clickCreateNewWorkflow
    WAit until element is enabled    ${CREATEWORKFLOWBTN}    timeout=${timeout}
    Click element    ${CREATEWORKFLOWBTN}

selectWorkflowByTitle
    [Arguments]    ${text}
    Wait until element is enabled    //h4[contains(@class, "Title")]/span[text()="${text}"]    timeout=${timeout}
    Click element    //h4[contains(@class, "Title")]/span[text()="${text}"]

selectWorkflowByIndex
    [Arguments]    ${index}
    Wait until element is enabled    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ListItem")][${index}]    timeout=${timeout}
    Click element    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ListItem")][${index}]

filterAwaitingWorkflow
    Wait until element is enabled    ${FILTERWORKFLOWICON}    timeout=${timeout}
    Click element    ${FILTERWORKFLOWICON}
    Wait until element is enabled    ${FILTERAWAITINGLIST}    timeout=${timeout}
    Click element    ${FILTERAWAITINGLIST}

clickAdvanceSearchIcon
    Wait until element is enabled   ${ADVANCESEARCHICON}    timeout=${timeout}
    Click element    ${ADVANCESEARCHICON}

clickOpenAdvanceSearchListInType
    [Arguments]    ${text}
    Wait until element is enabled    //label[@title="${text}"]/parent::*/following-sibling::div    timeout=${timeout}
    Click element    //label[@title="${text}"]/parent::*/following-sibling::div

clickSelectFilterWorkflowPriority
    [Arguments]    ${text}
    Wait until element is enabled    //ul/li[text()="${text}"]    timeout=${timeout}
    Click element    //ul/li[text()="${text}"]

clickApplyAdvanceFilterWorkflow
    Wait until element is enabled    ${APPLYADVANCEFILTERBTN}    timeout=${timeout}
    Click element    ${APPLYADVANCEFILTERBTN}

searchWorkflowByKeyword
    [Arguments]    ${text}
    Wait until element is enabled    ${WORKFLOWSEARCHFIELD}    timeout=${timeout}
    Input text    ${WORKFLOWSEARCHFIELD}    ${text}

clickWorkflowComment
    Wait until element is enabled    ${WORKFLOWCOMMENTBTN}    timeout=${timeout}
    Click element    ${WORKFLOWCOMMENTBTN}

closeWorkflowComment
    Wait until element is enabled    ${WORKFLOWCOMMENTCLOSEBTN}    timeout=${timeout}
    Click element    ${WORKFLOWCOMMENTCLOSEBTN}


#---------------------- verify ------------------------------------------------

isWorkflowStatusComplete
    [Arguments]    ${text}
    Wait until element is visible    //div[@title="${text}"]//div[@color="#29cb72"]    timeout=${timeout}

isOnlyAwaitingWorkflowDisplay
    ${count}    Get element count    ${WORKFLOWLIST}
    :FOR  ${index}  IN RANGE  ${count}
    \    Wait until element is visible    //div[contains(@class, "ListItem")][${index}+1]//span[text()="Awaiting your Action"]    timeout=${timeout}

isAppDisplayOnlyWorkflowTemplateName
    [Arguments]    ${text}
    ${count}    Get element count    ${WORKFLOWLIST}
    :FOR  ${index}  IN RANGE  ${count}
    \    Wait until element is visible    //div[contains(@class, "ListItem")][${index}+1]//p[contains(text(), "${text}")]    timeout=${timeout} 

isAppDisplayOnlyHighWorkflowPriority
    ${count}    Get element count    ${WORKFLOWLIST}
    :FOR  ${index}  IN RANGE  ${count}
    \    Wait until element is visible    //div[contains(@class, "ListItem")][${index}+1]//*[@color="#e71d36"]    timeout=${timeout}

isSearchResultCorrect
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "ListItem")][1]//span[text()="${text}"]    timeout=${timeout}

isWorkflowUnreadBadgeDisplay
    [Arguments]    ${index}=1
    ${count}    Convert To String    ${index}
    Wait until element is visible    ${WORKFLOWUNREADBADGE}    timeout=${timeout}
    Element should contain    ${WORKFLOWUNREADBADGE}  ${count}

isUnreadWorkflowHasBlueHighlight
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "is-unread")][@title="${text}"]    timeout=${timeout}

isWorkflowUnreadBadgeNotDisplay
    Wait until element is not visible    ${WORKFLOWUNREADBADGE}    timeout=${timeout}

isBlueHighlightNotDisplay
    [Arguments]    ${text}
    Wait until element is not visible    //div[contains(@class, "is-unread")][@title="${text}"]    timeout=${timeout}

isWorkflowSent
    [Arguments]    ${text}
    Wait until element is visible    //div[@data-qa-anchor="wrap-workflows-list"]//div[@title="${text}"]    timeout=${timeout}