*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${RESPONDERNAMEINWORKFLOWDETAIL}    //span[contains(@class, "Responder")]
${CLOSEWORKFLOWSTAGEDETAILPOPUP}    //button[@aria-label="Close"]   
${COMMENTICONINWORKFLOW}    //button[contains(@class, "ToolButton")]


*** Keywords ***

openCommentInWorkflow
    Wait until element is enabled    ${COMMENTICONINWORKFLOW}    timeout=${timeout}
    Click element    ${COMMENTICONINWORKFLOW}

#-------------------------- verify ----------------------------

isUserCannotSeeRecipientNameInStage
    [Arguments]    ${stageName}
    ${user}    Get text    //span[contains(@class, "Title")][text()="${stageName}"]/parent::*/parent::*/following-sibling::div//span[contains(@class, "UserName")]
    Click element    //span[contains(@class, "Title")][text()="${stageName}"]/parent::*
    Wait until element is visible    //div[@class="antd3-modal-title"][text()="${stageName}"]    timeout=${timeout}
    ${responder}    Get element attribute    ${RESPONDERNAMEINWORKFLOWDETAIL}    title
    Should Be Equal    ${user}    ${responder}
    Should Be Equal    ${user}    Recipient
    Click element    ${CLOSEWORKFLOWSTAGEDETAILPOPUP}

isUserCanSeeTheNameWhoHasDoneTheStage
    [Arguments]    ${stageName}
    ${user}    Get text    //span[contains(@class, "Title")][text()="${stageName}"]/parent::*/parent::*/following-sibling::div//span[contains(@class, "UserName")]
    Click element    //span[contains(@class, "Title")][text()="${stageName}"]/parent::*
    Wait until element is visible    //div[@class="antd3-modal-title"][text()="${stageName}"]    timeout=${timeout}
    ${responder}    Get element attribute    ${RESPONDERNAMEINWORKFLOWDETAIL}    title
    Should Be Equal    ${user}    ${responder}
    Should not Be Equal    ${user}    Recipient
    Click element    ${CLOSEWORKFLOWSTAGEDETAILPOPUP}

isCreatorCanSeeTheNameWhoHasDoneTheStage
    [Arguments]    ${stageName}
    ${user}    Get text    //span[contains(@class, "Title")][text()="${stageName}"]/parent::*/parent::*/following-sibling::div//span[contains(@class, "UserName")]
    Click element    //span[contains(@class, "Title")][text()="${stageName}"]/parent::*
    Wait until element is visible    //div[@class="antd3-modal-title"][text()="${stageName}"]    timeout=${timeout}
    ${responder}    Get element attribute    ${RESPONDERNAMEINWORKFLOWDETAIL}    title
    Should Be Equal    ${user}    ${responder}
    Should not Be Equal    ${user}    Recipient
    Click element    ${CLOSEWORKFLOWSTAGEDETAILPOPUP}

isCreatorCannotSeeRecipientNameInStage
    [Arguments]    ${stageName}
    ${user}    Get text    //span[contains(@class, "Title")][text()="${stageName}"]/parent::*/parent::*/following-sibling::div//span[contains(@class, "UserName")]
    Click element    //span[contains(@class, "Title")][text()="${stageName}"]/parent::*
    Wait until element is visible    //div[@class="antd3-modal-title"][text()="${stageName}"]    timeout=${timeout}
    ${responder}    Get element attribute    ${RESPONDERNAMEINWORKFLOWDETAIL}    title
    Should Be Equal    ${user}    ${responder}
    Should Be Equal    ${user}    Recipient
    Click element    ${CLOSEWORKFLOWSTAGEDETAILPOPUP}