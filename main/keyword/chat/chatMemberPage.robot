*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${ADDMEMBERTOGCBTN}    //div[contains(@class, "UserAdd")]
${REMOVEMEMBERBTN}    //a[@data-qa-anchor="user-item-card-remove-btn"]
${MODERATORROLEBTN}    //a[@data-qa-anchor="user-item-card-make-as-moderator-btn"]
${REMOVEMODERATORROLEBTN}    //a[@data-qa-anchor="user-item-card-remove-from-moderators-btn"]
${CONFIRMREMOVEBTN}    //span[text()="Remove"]/parent::*
${CONFIRMADDMODERATORBTN}    //div[@class="ant-confirm-btns"]/button[2]
${THREEDOTINCHATMEMBERPAGE}    //a[@data-qa-anchor="user-card-list-item"]//div[@data-qa-anchor="user-card-3dot"]
${CLOSECHATTYPEMSG}    //span[text()="Only you will be allowed to add or remove users and edit the details of the chat."]
${OPENCHATTYPEMSG}    //span[text()="Anyone will be able to add or remove users and edit the details of the chat"]
${MODERATORTITLETEXT}    //div[contains(@class, "GridContainer")]//following-sibling::span[text()="Moderator"]


*** Keywords ***

addMemberToGC
    Wait until element is enabled    ${ADDMEMBERTOGCBTN}    timeout=${timeout}
    Click element    ${ADDMEMBERTOGCBTN}

removeMemberFromGC
    [Arguments]    ${text}
    Wait until element is enabled    //h6[contains(text(), "${text}")]/parent::*/parent::*/preceding-sibling::div[2]    timeout=${timeout}
    sleep    3s
    Click element    //h6[contains(text(), "${text}")]/parent::*/parent::*/preceding-sibling::div[2]
    Click element    ${REMOVEMEMBERBTN}
    Wait until element is enabled    ${CONFIRMREMOVEBTN}    timeout=${timeout}
    Click element    ${CONFIRMREMOVEBTN}

assignMemberTobeModerator
    [Arguments]    ${text}
    Wait until element is enabled    //h6[contains(text(), "${text}")]/parent::*/parent::*/preceding-sibling::div[2]    timeout=${timeout}
    sleep    3s
    Click element    //h6[contains(text(), "${text}")]/parent::*/parent::*/preceding-sibling::div[2]
    Click element    ${MODERATORROLEBTN}
    Wait until element is enabled   ${CONFIRMADDMODERATORBTN}    timeout=${timeout}
    Click element    ${CONFIRMADDMODERATORBTN}

unassignMemberTobeModerator
    [Arguments]    ${text}
    Wait until element is enabled    //h6[contains(text(), "${text}")]/parent::*/parent::*/preceding-sibling::div[2]    timeout=${timeout}
    sleep    3s
    Click element    //h6[contains(text(), "${text}")]/parent::*/parent::*/preceding-sibling::div[2]
    Click element    ${REMOVEMODERATORROLEBTN}
    Wait until element is enabled   ${CONFIRMADDMODERATORBTN}    timeout=${timeout}
    Click element    ${CONFIRMADDMODERATORBTN}

#--------------------------------------------------------------------------------------------------

isUserIsModerator
    [Arguments]    ${text}
    Wait until element is visible    //h6[contains(@class, "UserName")][text()="${text}"]/following-sibling::span[text()="Moderator"]    timeout=${timeout}

isUserNotBeModerator
    [Arguments]    ${text}
    Page should not contain element    //h6[contains(@class, "UserName")][text()="${text}"]/following-sibling::span[text()="Moderator"]

isUserRemovedFromChat
    [Arguments]    ${text}
    Wait until element is not visible    //h6[contains(@class, "UserName")][text()="${text}"]

isUserInGroupChat
    [Arguments]    @{text}
    :FOR  ${element}  IN  @{text}
    \  Wait until element is visible    //h6[contains(@class, "UserName")][contains(text(), "${element}")]  timeout=${timeout}

isThreeDotDisappear
    Wait until element is not visible    ${THREEDOTINCHATMEMBERPAGE}    timeout=${timeout}

isCloseChatType
    Wait until element is visible    ${CLOSECHATTYPEMSG}    timeout=${timeout}

isOpenChatType
    Wait until element is visible    ${OPENCHATTYPEMSG}    timeout=${timeout}

isAddMemberDisplay
    Wait until element is enabled    ${ADDMEMBERTOGCBTN}    timeout=${timeout}

isAddMemberNotDisplay
    Wait until element is not visible    ${ADDMEMBERTOGCBTN}    timeout=${timeout}

isNoAnyUserBeModerator
    Wait until element is not visible    ${MODERATORTITLETEXT}    timeout=${timeout}

isThreeDotDisplayCorrect
    [Arguments]    @{text}
    FOR  ${element}  IN  @{text}
        Wait until element is visible    //h6[contains(@class, "UserName")][text()="${element}"]/parent::*/parent::*/preceding-sibling::div[2]/div[@data-qa-anchor="user-card-3dot"]  timeout=${timeout}
    END