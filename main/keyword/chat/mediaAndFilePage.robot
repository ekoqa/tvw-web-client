*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${MEDIALOCATOR}    //div[contains(@class, "styles__Grid")]


*** Keywords ***

isMediaDisplayCorrectly
    Wait until element is visible    ${MEDIALOCATOR}    timeout=${timeout}