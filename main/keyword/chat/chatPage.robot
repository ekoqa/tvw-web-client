*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${TASKICON}    //button[@data-qa-anchor="btn-send-task"]
${MESSAGECONTAINER}    //div[contains(@class, "MessageContainer")]
${MOREBTN}    //span[contains(@class, "CreateCall")]/following-sibling::span/button
${CHATSETTINGBTN}    //span[@data-qa-anchor="group-action-menu-settings"]
${LEAVECHATBUTTON}    //span[@data-qa-anchor="group-action-menu-leave"]
${ARCHIVECHATBTN}    //span[@data-qa-anchor="group-action-menu-archive-chat"]
${MARKASREADINCHATBTN}    //span[@data-qa-anchor="group-action-menu-mark-as-read"]
${DISABLEDMARKASREADBTN}    //li[@aria-disabled="true"]/span[@data-qa-anchor="group-action-menu-mark-as-read"]
${CONFIRMBUTTONFORLEAVEANDARCHIVE}    //div[@class="ant-confirm-body-wrapper"]//button[2]
${FIRSTMESSAGE}    //div[contains(@class, "MessageContainer")][@id="1"]
${CHATTAB}    //div[@role="tab"][1]
${TOPICTAB}    //div[@role="tab"][2]
${MEMBERTAB}    //div[@role="tab"][3]
${MEDIAFILESTABINGC}    //div[@role="tab"][4]
${MEDIAFILESTABINDC}    //div[@role="tab"][3]
${SAVEGCSETTINGBTN}    //span[text()="Save"]/parent::*
${UNDOBTN}    //a[@href="#undo"]
${SENDCARDBTN}    //button[@data-qa-anchor="btn-send-card"]
${SENDFORMBTN}    //button[@data-qa-anchor="btn-send-workflow"]
${THREEDOTBTNINTOPICCHAT}    //div[contains(@class, "UpperRight")]/button[1]
${ARCHIVETOPICBTN}    //li[@role="menuitem"][text()="Archive Topic"]
${TEXTMESSAGEFIELD}    message
${STICKERICON}    //button[@data-qa-anchor="btn-sticker-toggle"]
${FIRSTSTICKERITEM}    //div[contains(@class, "StickerItem")][1]
${INPUTIMAGEINCHAT}    //input[@accept="image/png,image/jpg,image/jpeg"]
${CONFIRMSENDMEDIAINCHATBTN}    //div[contains(@class, "Footer")]/button[2]
${IMAGECAPTIONFIELD}    //div[contains(@class, "Caption")]//input
${INPUTVIDEOINCHAT}    //input[@accept="video/mp4,video/x-m4v,video/*"]
${INPUTFILEINCHAT}    //div[contains(@class, "Options")]/span[1]//input[@type="file"]
${UPLOADMEDIAINDICATOR}    //button[contains(@class, "UploadIndicator")]
${THUMBUPICON}    //button[@data-qa-anchor="btn-send-thumbsup"]
${FIRSTCOMMENDATIONLIST}    //div[@id="type"]/label[1]
${COMMENDATIONTEXTFIELD}    //div[@class="ant-form-item-control "]/textarea
${CONFIRMSENDCOMMENDATIONBTN}    //div[@class="ant-form-item-control "]/button[2]
${IMAGEPREVIEWINCHAT}    //div[contains(@class, "swiper-slide-active")]//div/img[contains(@class, "ImgCentered")]
# ${DETAILACKBTN}    //ul[@data-qa-anchor="chat-message-popup-menu"]/li[2]
${DETAILACKBTN}    //span[text()="Detail Ack"]
${REPLYBTN}    //li[@data-qa-anchor="chat-message-reply-to"]
${TRANSLATEBTN}    //*[@data-qa-anchor="chat-message-translate"]
${EDITMESSAGEBTN}    //li[@data-qa-anchor="chat-message-edit"]
${DELETEMESSAGEBTN}    //li[@data-qa-anchor="chat-message-delete"]
${FORWARDBTN}    //li/span[text()="Forward"]
${OPTIONMESSAGEBTN}    //li[@role="menuitem"]/span
${CONFIRMFORWARDMESSAGEBTN}    //div[@class="antd3-modal-footer"]/button[2]  
${MENTIONLIST}    //*[@data-qa-anchor="mention-item"]
${FIRSTMENTIONLIST}    //*[@data-qa-anchor="mention-item"][1]  
${ACKBTNINTHREEDOT}    //li[@role="menuitem"]/i
${EDITMESSAGEFIELD}    data
# ${OLDMESSAGEFIELD}    data
${CONFIRMEDITMESSAGE}    //div[@class="antd3-modal-footer"]/button[2]
${CONFIRMLEAVECHATBTN}    //div[@class="ant-confirm-btns"]/button[2]
${CANCELLEAVECHATBTN}    //div[@class="ant-confirm-btns"]/button[1]
${CLOSEDETAILACKPOPUP}    //button[@class="antd3-modal-close"]
${READMOREBTN}    //*[contains(@class, "styles__TranslatedMessage-")][last()]//*[contains(@class, "styles__ReadmoreButton")]
${LONGTRANSLATEMESSAGE}    //div[contains(@class, "Readmore")]//p[contains(@class, "TextMessageBody")]
${OPENCHATSTATUS}    //span[contains(@class, "ant-switch-checked")]
${CLOSECHATSTATUS}     //span[@class="ant-switch"]
${MARKALLASREADBTN}        //li[text()="Mark all as read"]
${CLOSEINPHOTO}    //*[contains(@class, 'styles__WrapInner')]/button
${VERIFYOLDTEXTMESSAGE}    //*[contains(@class, 'styles__TextItemContainer')]//*[contains(text(),'Test')] 
${VERIFYOLDEDITEDTEXTMESSAGE}    //*[contains(@class, 'styles__TextItemContainer')]//*[contains(text(),'Edited')]
${VERIFYOLDDELETEDTEXTMESSAGE}    //div[contains(@class, "DeletedMessage")]
${VERIFYOLDSTICKER}    //*[contains(@class, 'styles__StickerContainer')]
${VERIFYOLDFILE}    //*[contains(@class, 'styles__FileMessage')]
${VERIFYOLDPHOTO}    //*[contains(@class, 'styles__ImageContainer')]
${VERIFYOLDVIDEO}    //*[contains(@class, 'lnr lnr-play-circle')]
${LASTPROFILEUSER}    //*[contains(@class, "styles__MessageContainer")][last()]//*[contains(@class, "styles__UserBlock")]
${JITSIICONINCHATROOM}    //button//*[@data-icon="video-plus"]
${CONFIRMCREATEJITSIBTN}    //div[@class="ant-confirm-btns"]/button[2]
${LATESTJOINCONFERENCEBTN}    //div[contains(@class, "MessageContainer")][last()]//a[text()="Join Call"]
${JOINCONFERENCEBTN}    //div[contains(@class, "MessageContainer")]//a[text()="Join Call"]
${QUCIKREPLYCONTAINER}    quickReplyContainer
${QUICKREPLYICON}    //div[@id="quickReplyContainer"]//*[contains(@class, "ImageIcon")]
${SEENEXTQUICKREPLYLIST}    //div[@id="quickReplyContainer"]//*[@data-icon="angle-right"]
${LASTTHREEDOT}    //*[contains(@class, "styles__MessageContainer")][last()]//*[contains(@class, "IconButton__StyledButton")]
${UNREADBADGEINTOPICTAB}    //span[text()="Topics"]/parent::*/*[@class="ant-scroll-number ant-badge-dot"]
${TOPICCHATUNREADBADGE}    //div[@data-qa-anchor="topic-item"]//span[contains(@class, "styles__Badge")]
${QUICKREPLYWORKFLOWTEMPLATELIST}    //ul[@id="quickReplyInnerTray"]/li
${CAROUSELITEM}    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "CarouselContainer")]/div/div
${NEXTCAROUSELBTN}    //div[contains(@class, "MessageContainer")][last()]//button[contains(@class, "NextButton")]
${BOTTITLEANDDESCRIPTION}    //div[contains(@class, "MessageContainer")][last()]//h4[contains(@class, "Title")][text()="title"]/following-sibling::p[text()="select things below"]
${BOTBUTTONIMAGECONTAINER}    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ImageContainer")]
${BOTBUTTONLIST}    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ButtonContainer")]/button[@data-qa-anchor="bot-button"]/span[text()="Leave Request"]







*** Keywords ***

selectChatTab
    Wait until element is enabled    ${CHATTAB}    timeout=${timeout}
    click element    ${CHATTAB}

selectTopicTab
    Wait until element is enabled    ${TOPICTAB}    timeout=${timeout}
    click element    ${TOPICTAB}

selectMemberTab
    Wait until element is enabled    ${MEMBERTAB}    timeout=${timeout}
    click element    ${MEMBERTAB}

selectMediaAndFileTabInDC
    Wait until element is enabled    ${MEDIAFILESTABINDC}    timeout=${timeout}
    click element    ${MEDIAFILESTABINDC}
    
selectMediaAndFileTabInGC
    Wait until element is enabled    ${MEDIAFILESTABINGC}    timeout=${timeout}
    click element    ${MEDIAFILESTABINGC}

createTaskInChat
    Wait until element is enabled    ${TASKICON}    timeout=${timeout}
    Click element    ${TASKICON}

leaveChatroom
    Wait until element is enabled    ${MOREBTN}    timeout=${timeout}
    Click element    ${MOREBTN}
    Click element    ${LEAVECHATBUTTON}
    Wait until element is enabled    ${CONFIRMBUTTONFORLEAVEANDARCHIVE}
    Click element    ${CONFIRMBUTTONFORLEAVEANDARCHIVE}

seeMentionUserList
    Wait until element is enabled    ${TEXTMESSAGEFIELD}    timeout=${timeout}
    Press keys    ${TEXTMESSAGEFIELD}    @

sendMentionMessage
    [Arguments]    @{text}
    Wait until element is enabled    ${TEXTMESSAGEFIELD}    timeout=${timeout}
    :FOR  ${element}  IN  @{text}
    \    Press keys    ${TEXTMESSAGEFIELD}    @
    \    Press keys    ${TEXTMESSAGEFIELD}    ${element}
    \    Wait until element is enabled   ${FIRSTMENTIONLIST}    timeout=${timeout}
    \    Click element    ${FIRSTMENTIONLIST}
    Press keys    ${TEXTMESSAGEFIELD}    RETURN

keyMentionButNotSendMessage
    [Arguments]    @{text}
    Wait until element is enabled    ${TEXTMESSAGEFIELD}    timeout=${timeout}
    :FOR  ${element}  IN  @{text}
    \    Press keys    ${TEXTMESSAGEFIELD}    @
    \    Press keys    ${TEXTMESSAGEFIELD}    ${element}
    \    Wait until element is enabled   ${FIRSTMENTIONLIST}    timeout=${timeout}
    \    Click element    ${FIRSTMENTIONLIST}

selectMentionUser
    [Arguments]    @{text}
    Wait until element is enabled    ${TEXTMESSAGEFIELD}    timeout=${timeout}
    :FOR  ${element}  IN  @{text}
    \    Press keys    ${TEXTMESSAGEFIELD}    @
    \    Press keys    ${TEXTMESSAGEFIELD}    ${element}
    \    Wait until element is enabled   ${FIRSTMENTIONLIST}    timeout=${timeout}
    \    Click element    ${FIRSTMENTIONLIST}

clickOnMentionUserMessage
    [Arguments]    ${text}
    Wait until element is enabled    //div[contains(@class, "MessageContainer")][last()]//a[contains(@class, "Mention")][text()="${text}"]    timeout=${timeout}
    Click element    //div[contains(@class, "MessageContainer")][last()]//a[contains(@class, "Mention")][text()="${text}"]

clickOnMessage
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//span[text()="${text}"]    timeout=${timeout}
    Click element    //div[contains(@class, "MessageContainer")][last()]//span[text()="${text}"]

sendMessage
    [Arguments]    ${text}
    Wait until element is enabled    ${TEXTMESSAGEFIELD}    timeout=${timeout}
    Press keys    ${TEXTMESSAGEFIELD}    ${text}    
    Press keys    ${TEXTMESSAGEFIELD}    RETURN

# clickThreeDotOnLatestMessage
#     Click element    //div[contains(@class, "MessageContainer")][last()]//button

clickThreeDotOnLatestMessage
    Wait until element is visible    ${LASTTHREEDOT}    timeout=${timeout}
    Click element    ${LASTTHREEDOT}

clickSettingInChatroom
    Wait until element is enabled    ${MOREBTN}    timeout=${timeout}
    Click element    ${MOREBTN}
    Click element    ${CHATSETTINGBTN}

clickChangeToCloseGC
    ${status}    Run Keyword And Return Status    Wait until element is enabled    ${OPENCHATSTATUS}    timeout=${timeout}
    Run Keyword If    ${status}    Click element    ${OPENCHATSTATUS}

clickChangeToOpenGC
    ${status}    Run Keyword And Return Status    Wait until element is enabled    ${CLOSECHATSTATUS}    timeout=${timeout}
    Run Keyword If    ${status}    Click element    ${CLOSECHATSTATUS}

clickSaveGCSetting
    Click element    ${SAVEGCSETTINGBTN}

clickLeaveChatroom
    Wait until element is enabled    ${MOREBTN}    timeout=${timeout}
    Click element    ${MOREBTN}
    Wait until element is enabled    ${LEAVECHATBUTTON}    timeout=${timeout}
    Click element   ${LEAVECHATBUTTON}

clickConfirmLeaveChatroom
    Wait until element is enabled    ${CONFIRMLEAVECHATBTN}    timeout=${timeout}
    Click element    ${CONFIRMLEAVECHATBTN}

clickCancelLeaveChatroom
    Wait until element is enabled    ${CANCELLEAVECHATBTN}    timeout=${timeout}
    Click element    ${CANCELLEAVECHATBTN}

clickMarkAsReadInChatroom
    Wait until element is enabled    ${MOREBTN}    timeout=${timeout}
    Click element    ${MOREBTN}
    Click element    ${MARKASREADINCHATBTN}

clickArchiveInChatroom
    Wait until element is enabled    ${MOREBTN}    timeout=${timeout}
    Click element    ${MOREBTN}
    Click element    ${ARCHIVECHATBTN}

cancelArchiveChat
    Wait until element is visible    ${UNDOBTN}    timeout=${timeout}
    Click element    ${UNDOBTN}

clickArchiveInTopicChat
    Wait until element is enabled    ${THREEDOTBTNINTOPICCHAT}    timeout=${timeout}
    Click element    ${THREEDOTBTNINTOPICCHAT}
    Wait until element is visible    ${ARCHIVETOPICBTN}    timeout=${timeout}
    Click element    ${ARCHIVETOPICBTN}

clickSendCard
    Click element    ${SENDCARDBTN}

clickSendFormInChat
    Click element    ${SENDFORMBTN}
    
clickOpenSticker
    Wait until element is enabled    ${STICKERICON}    timeout=${timeout}
    Click element    ${STICKERICON}

clickReadMore
    Wait until element is enabled    ${READMOREBTN}    timeout=${timeout}
    Click element    ${READMOREBTN}

selectStickerToSend
    Wait until element is enabled    ${FIRSTSTICKERITEM}    timeout=${timeout}
    Click element    ${FIRSTSTICKERITEM}

sendImageFile
    Wait until element is enabled    ${INPUTIMAGEINCHAT}    timeout=${timeout}
    Choose file    ${INPUTIMAGEINCHAT}    ${filePath}/${imageInChatFile}

sendVideoInChat
    Wait until element is enabled    ${INPUTVIDEOINCHAT}    timeout=${timeout}
    Choose file    ${INPUTVIDEOINCHAT}    ${filePath}/${videoInChatFile}

sendFileInChat
    Wait until element is enabled    ${INPUTFILEINCHAT}    timeout=${timeout}
    Choose file    ${INPUTFILEINCHAT}    ${filePath}/${pdfFile}

addImageCaption
    [Arguments]    ${text}
    Wait until element is enabled    ${IMAGECAPTIONFIELD}    timeout=${timeout}
    Input text    ${IMAGECAPTIONFIELD}    ${text}

clickConfirmSendMedia
    Wait until element is enabled    ${CONFIRMSENDMEDIAINCHATBTN}    timeout=${timeout}
    Click element    ${CONFIRMSENDMEDIAINCHATBTN}

clickOpenCommendation
    Wait until element is enabled    ${THUMBUPICON}    timeout=${timeout}
    Click element    ${THUMBUPICON}
    
selectCommendation
    Wait until element is enabled    ${FIRSTCOMMENDATIONLIST}    timeout=${timeout}
    Click element    ${FIRSTCOMMENDATIONLIST}

addDescriptionForThumbUps
    [Arguments]    ${text}
    Wait until element is enabled    ${COMMENDATIONTEXTFIELD}    timeout=${timeout}
    Input text    ${COMMENDATIONTEXTFIELD}    ${text}

confirmSendCommendation
    Wait until element is enabled    ${CONFIRMSENDCOMMENDATIONBTN}    timeout=${timeout}
    Click element    ${CONFIRMSENDCOMMENDATIONBTN}

waitForMediaUpload
    Wait until element is not visible    ${UPLOADMEDIAINDICATOR}    timeout=60s

clickOnLastSentImage
    ${countMessage}    Get element count    ${MESSAGECONTAINER}
    ${id}    Get element attribute    //div[contains(@class, "MessageContainer")][${countMessage}-1]    id
    Click element    //div[contains(@class, "MessageContainer")][@id="${id}"]//div[contains(@class, "ImageContainer")]

clickAckMessage
    [Arguments]    ${text}
    Wait until element is enabled    //span[text()="${text}"]/parent::*/following-sibling::div    timeout=${timeout}
    Click element    //span[text()="${text}"]/parent::*/following-sibling::div

clickAckMessageFromOptionList
    Wait until element is enabled    ${ACKBTNINTHREEDOT}    timeout=${timeout}
    Click element    ${ACKBTNINTHREEDOT}

clickThreeDotOnSpecificMessage
    [Arguments]    ${text}
    Wait until element is enabled    //span[text()="${text}"]/following-sibling::span//button    timeout=${timeout}
    Click element    //span[text()="${text}"]/following-sibling::span//button

openDetailAck
    Wait until element is enabled    ${DETAILACKBTN}    timeout=${timeout}
    Click element    ${DETAILACKBTN}

clickReplyToButton
    Wait until element is enabled    ${REPLYBTN}    timeout=${timeout}
    Click element    ${REPLYBTN}

clickTranslateButton
    Wait until element is enabled    ${TRANSLATEBTN}    timeout=${timeout}
    Click element    ${TRANSLATEBTN}

clickForwardButton
    Wait until element is enabled    ${FORWARDBTN}    timeout=${timeout}
    Click element    ${FORWARDBTN}

selectRecentChatroomToForwardMessage
    [Arguments]    ${text}
    Wait until element is enabled    //div[contains(@class, "ListItem")]//h4[text()="${text}"]    timeout=${timeout}
    Click element    //div[contains(@class, "ListItem")]//h4[text()="${text}"]

selectRecentTopicToForwardMessage
    [Arguments]    ${text}
    Wait until element is enabled    //div[contains(@class, "ListItem")]//h4[text()="${text}"]    timeout=${timeout}
    Click element    //div[contains(@class, "ListItem")]//h4[text()="${text}"]

clickConfirmForwardMessage
    Wait until element is enabled    ${CONFIRMFORWARDMESSAGEBTN}    timeout=${timeout}
    Click element    ${CONFIRMFORWARDMESSAGEBTN}

clickEditMessageButton
    Wait until element is enabled    ${EDITMESSAGEBTN}    timeout=${timeout}
    Click element    ${EDITMESSAGEBTN}

editMessageByAddText
    [Arguments]    ${text}
    Wait until element is enabled    ${EDITMESSAGEFIELD}    timeout=${timeout}
    Press keys    ${EDITMESSAGEFIELD}    \ ${text}

editMessagByClearOldText
    [Arguments]    ${text}
    # Wait until element is visible     ${OLDMESSAGEFIELD}    timeout=${timeout}  
    # Input Text    ${OLDMESSAGEFIELD}    ${EMPTY}
    Wait until element is enabled    ${EDITMESSAGEFIELD}    timeout=${timeout}
    # Clear element text    ${EDITMESSAGEFIELD}
    ${value}=     Get text   ${EDITMESSAGEFIELD}
    ${backspaces count}=    Get Length      ${value}
    Run Keyword If    """${value}""" != ''    # if there's no current value - no need to slow down by an extra SE call
    ...     Repeat Keyword  ${backspaces count +1}  Press Keys  ${EDITMESSAGEFIELD}   BACKSPACE
    Wait until element is enabled    ${EDITMESSAGEFIELD}    timeout=${timeout}
    Press keys    ${EDITMESSAGEFIELD}    ${text}

clickConfirmEditMessage
    Wait until element is enabled    ${CONFIRMEDITMESSAGE}    timeout=${timeout}
    Click element    ${CONFIRMEDITMESSAGE}

clickDeleteMessage
    Wait until element is enabled    ${DELETEMESSAGEBTN}    timeout=${timeout}
    Click element    ${DELETEMESSAGEBTN}

clickCloseDetailAckPopup
    Wait until element is enabled    ${CLOSEDETAILACKPOPUP}    timeout=${timeout}
    Click element    ${CLOSEDETAILACKPOPUP}

clickCloseReadMorePopup
    Wait until element is enabled    ${CLOSEDETAILACKPOPUP}    timeout=${timeout}
    Click element    ${CLOSEDETAILACKPOPUP}

clickUserInLastMessage
    Wait until element is enabled    ${LASTPROFILEUSER}    timeout=${timeout}
    Click element    ${LASTPROFILEUSER}

clickLastPhoto
    Wait until element is enabled    ${LASTPHOTO}    timeout=${timeout}
    Click element    ${LASTPHOTO}

clickCreateJitsiInChat
    Wait until element is enabled    ${JITSIICONINCHATROOM}    timeout=${timeout}
    Click element    ${JITSIICONINCHATROOM}
    Wait until element is enabled    ${CONFIRMCREATEJITSIBTN}    timeout=${timeout}
    Click element    ${CONFIRMCREATEJITSIBTN}

clickJoinConferenceFromLatestMessage
    Wait until element is enabled    ${LATESTJOINCONFERENCEBTN}    timeout=${timeout}
    Click element    ${LATESTJOINCONFERENCEBTN}

clickOnLinkPreview
    Wait until element is enabled    //div[contains(@class, "MessageContainer")][last()]//div[@data-qa-anchor="preview-link-item"]    timeout=${timeout}
    Click element    //div[contains(@class, "MessageContainer")][last()]//div[@data-qa-anchor="preview-link-item"]

clickToSeeMoreQuickReplyList
    Wait until element is enabled    ${SEENEXTQUICKREPLYLIST}    timeout=${timeout}
    Click element    ${SEENEXTQUICKREPLYLIST}

tapOnQuickReplyButton
    [Arguments]    ${text}
    Wait until element is visible    //span[text()="${text}"]/parent::button    timeout=${timeout}
    Click element    //span[text()="${text}"]/parent::button

tapOnCenterLeftOfImageMap
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ImageMapItemContainer")]    timeout=${timeout}
    sleep     3s
    # Click Element At Coordinates  //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ImageMapItemContainer")]  20  20
    Click element    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ImageMapItemContainer")]/a[1]

tapOnCenterButtomOfImageMap
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ImageMapItemContainer")]    timeout=${timeout}
    sleep     3s
    Click element    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ImageMapItemContainer")]/a[5]

clickMoreButtonInChatroom
    Wait until element is enabled    ${MOREBTN}    timeout=${timeout}
    Click element    ${MOREBTN}

mouseOverLastMessage
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "HoverMask")]    timeout=${timeout}
    Mouse Over    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "HoverMask")]

clickUserInActDetail
    [Arguments]    ${text}
    Wait until element is visible    //h4[contains(@class, "styles__Title")][contains(text(), "${text}")]     timeout=${timeout}
    Click element    //h4[contains(@class, "styles__Title")][contains(text(), "${text}")] 


clickOnBotButtonByText
    [Arguments]    ${text}
    ${last}    Get element count    //div[@data-qa-anchor="wrap-chats-content"]/div/div[2]/div[contains(@class, "MessageContainer")]
    ${id}    Get element attribute    //div[@data-qa-anchor="wrap-chats-content"]/div/div[2]/div[contains(@class, "MessageContainer")][${last}]    id
    Wait until element is visible    //div[contains(@class, "MessageContainer")][@id=${id}+1]//div[contains(@class, "CarouselContainer")]/div/div    timeout=${timeout}
    Click element    //div[contains(@class, "MessageContainer")][@id=${id}+1]//span[text()="${text}"]/parent::*    #button[@data-qa-anchor="bot-button"]/

#-------------------------------------------------------------------------

isNewChatroom
    page should not contain element    ${FIRSTMESSAGE}

isChatroomDisplay
    [Arguments]    ${text}
    Wait until element is visible    //h4[contains(@class, "Title")]/span[text()="${text}"]    timeout=${timeout}    

isSearchResultForChatDisplay
    [Arguments]    ${text}
    Wait until element is visible    //span[contains(@class, "Highlight")]/*[text()="${text}"]    timeout=${timeout}
    
isMessageDisplay
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "MessageContainer")]//span[text()="${text}"]    timeout=${timeout}

isMessageSent
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//span[text()="${text}"]    timeout=${timeout}

isBotReplyCorrect
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//span[text()="${text}"]    timeout=${timeout}

isMessageTranslate
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "Translate")]/p[text()="${text}"]    timeout=${timeout}

isSpaceMessageTranslate
    [Arguments]    ${text}
    Wait until element is visible    //*[contains(@class, "Translate")][last()]//*[contains(text(), '${text}')]    timeout=${timeout}

isMessageTranslateInReadMorePopUp
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "Readmore")]/p[contains(@class, "TextMessageBody")][contains(text(), "${text}")]    timeout=${timeout}

isLongTextSent
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//a[@data-qa-anchor="chat-readmore"]
    
isLinkTextSent
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[@data-qa-anchor="preview-link-item"]

isStickerSent
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "StickerContainer")]

isImageSent
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ImageContainer")]    timeout=${timeout}

isVideoSent
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "HoverMask")]    timeout=${timeout}
    Mouse over    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "HoverMask")]
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//span[contains(@class, "lnr lnr-play-circle")]    timeout=${timeout}

isVoiceSent
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "VoiceMessage")]    timeout=${timeout}

isCommendationSent
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ThumbsUpTable")]    timeout=${timeout}

isImagePreviewOpen
    Wait until element is visible    ${IMAGEPREVIEWINCHAT}    timeout=${timeout}

isTaskSent
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "TaskContent")]//span[text()="${text}"]    timeout=${timeout}

isCardSent
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//a/span[text()="View Card"]    timeout=${timeout}

isFormSent
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//a/span[text()="View Workflow"]    timeout=${timeout}

isEventMessageCorrect
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//p[contains(@class, "EventContent")]    timeout=${timeout}
    Element should contain    //div[contains(@class, "MessageContainer")][last()]//p[contains(@class, "EventContent")]    ${text}    

isUserReply
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//a[contains(@class, "UserBlock")]    timeout=${timeout}

isDeleteAvatarDisplayInChatPage
    ${reload}    Run keyword and return status    Wait until element is visible    //div[contains(@class, "GroupAvatar")][contains(@style, "/bedcfbaa22ba66a8d5292f21ca0c9a8d.png")]    timeout=${timeout}
    Run keyword Unless    ${reload}    Reload page
    Wait until element is visible    //div[contains(@class, "GroupAvatar")][contains(@style, "/bedcfbaa22ba66a8d5292f21ca0c9a8d.png")]    timeout=${timeout}

isFullnameDisplayWithDeleteTitle
    [Arguments]    ${text}
    Element should contain    //h4[text()="${text}"]/following-sibling::span    Deleted user

isDeleteAvatarDisplayInDetailAck
    [Arguments]   ${text}
    Wait until element is visible    //h4[text()="${text}"]/parent::*/preceding-sibling::div/div[contains(@style, "${deleteUserIcon}")]

isReplyToTextSuccess
    [Arguments]    ${text}
    Element should contain    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ReplyBubble")]//div[contains(@class, "Preview")]    ${text}    

isReplyToStickerSuccess
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ReplyBubble")]//div[contains(@class, "Sticker")]    timeout=${timeout}

isReplyToImageSuccess
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ReplyBubble")]//div[contains(@class, "Image")]    timeout=${timeout}

isReplyToVideoSuccess
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ReplyBubble")]//div[contains(@class, "Preview")]//span[text()="Video"]    timeout=${timeout}

isReplyToFileSuccess
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ReplyBubble")]//div[contains(@class, "Preview")]/span[@class="lnr-paperclip"]    timeout=${timeout}
    
isMentionListDisplay
    [Arguments]    ${index}
    ${count}    Get element count  ${MENTIONLIST}
    Run Keyword If    ${index}!=${count}    fail    msg=Wrong total of mention user list

isMentionSent
    [Arguments]    ${index}
    ${count}    Get element count    //div[contains(@class, "MessageContainer")][last()]//a[contains(@class, "Mention")]
    Run Keyword If    ${index}!=${count}    fail    msg=Send mention failed
    
isMentionListNotDisplay
    Wait until element is not visible    ${MENTIONLIST}    timeout=${timeout}

isMentionListCanFilterByCharacter
    [Arguments]    ${first}    ${second}
    Press keys    ${TEXTMESSAGEFIELD}    ${first}
    ${count}    Get element count  ${MENTIONLIST}
    Press keys    ${TEXTMESSAGEFIELD}    ${second}
    ${compare}    Get element count  ${MENTIONLIST}
    Run Keyword If    ${compare}>${count}    fail    msg=Send mention failed

isLastMessageAcked
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "AckIndicator")]    timeout=${timeout}

isMessageDisplayAsNormalText
    Wait until element is not visible    //div[contains(@class, "MessageContainer")][last()]//a[contains(@class, "Mention")]    timeout=${timeout}

isMessageDeleted
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "Delete")]    timeout=${timeout}

isLinkTextContainsDescription
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//*[contains(text(), "${text}")]    timeout=${timeout}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//*[contains(@class, "styles__ImagePreview")]    timeout=${timeout}

isMessageDisplayAfterEditLink
    [Arguments]    ${editText}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[@data-qa-anchor="preview-link-item"]    timeout=${timeout}
    ${text}    Get text    //div[contains(@class, "MessageContainer")][last()]//p/span[1]
    Run keyword If    '${editText}'!='${text}'    fail    msg=app display ${text}

isOldTextMessageDisplay
    Wait until element is visible    ${VERIFYOLDTEXTMESSAGE}    timeout=${timeout}

isOldEditedTextMessageDisplay
    Wait until element is visible    ${VERIFYOLDEDITEDTEXTMESSAGE}    timeout=${timeout}

isOldDeletedTextMessageDisplay
    Wait until element is visible    ${VERIFYOLDDELETEDTEXTMESSAGE}    timeout=${timeout}

isOldStickerDisplay
    Wait until element is visible    ${VERIFYOLDSTICKER}    timeout=${timeout}

isOldFileDisplay
    Wait until element is visible    ${VERIFYOLDFILE}    timeout=${timeout}

isOldPhotoDisplay
    Wait until element is visible    ${VERIFYOLDPHOTO}    timeout=${timeout}

isOldVideoDisplay
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "HoverMask")]    timeout=${timeout}
    Mouse over    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "HoverMask")]
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//span[contains(@class, "lnr lnr-play-circle")]    timeout=${timeout}

isJitsiIconDisplay
    Wait until element is visible    ${JITSIICONINCHATROOM}    timeout=${timeout}

isUserAbleToGenerateJitsiCall
    Wait until element is visible    ${LATESTJOINCONFERENCEBTN}    timeout=${timeout}

isNewAddedUserCanSeeJitsiJoinButton
    Wait until element is visible    ${JOINCONFERENCEBTN}    timeout=${timeout}

isEditedTimeStampDisplayCorrect
    [Arguments]    ${time}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//span[contains(@class, "Edited")][contains(text(),"${time}")]    timeout=${timeout}

isDeletedMessageTimeStampDisplayCorrect
    [Arguments]    ${time}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "Delete")]//span[contains(@class, "Time")][contains(text(), "${time}")]    timeout=${timeout}

isSendMessageTimeStampDisplayCorrect
    [Arguments]    ${time}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//span[contains(@class, "message-timestamp")][contains(text(), "${time}")]    timeout=${timeout}

isSendMessageDateStampDisplayCorrect
    Wait until element is visible    //div[contains(@class, "styles__DateSeparator")][contains(text(), "Today")]    timeout=${timeout}

isQuickReplyListDisplay
    Wait until element is visible    ${QUCIKREPLYCONTAINER}    timeout=${timeout}

isIconDisplayOnQuickReplyList
    Wait until element is visible    ${QUICKREPLYICON}    timeout=${timeout}

isQucikReplyButtonDisplay
    [Arguments]    ${text}
    Wait until element is visible    //button[@data-qa-anchor="quick-reply-button"]/span[text()="${text}"]    timeout=${timeout}

isQuickReplyButtonDisappear
    Wait until element is not visible    ${QUCIKREPLYCONTAINER}    timeout=${timeout}

isImageMapSent
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "ImageMapItemContainer")]    timeout=${timeout}

isUnreadBadgeInTopicTabDisappear
    Wait until element is not visible    ${UNREADBADGEINTOPICTAB}    timeout=${timeout}
    Wait until element is not visible    ${TOPICCHATUNREADBADGE}    timeout=${timeout}

isMarkAsReadInChatroomIsDisabled
    Wait until element is visible    ${DISABLEDMARKASREADBTN}    timeout=${timeout}

isAllWorkflowTemplateResultContainKeyword
    [Arguments]    ${text}
    Wait until element is visible    ${QUICKREPLYWORKFLOWTEMPLATELIST}    timeout=${timeout}
    ${count}    Get Element Count    ${QUICKREPLYWORKFLOWTEMPLATELIST}
    :For    ${index}    IN RANGE    ${count}
    \    Element Should Contain    //ul[@id="quickReplyInnerTray"]/li[${index}+1]//span  ${text}
    
isOnlyForwardButtonDisplay
    ${count}    Get element count    ${OPTIONMESSAGEBTN}
    Run keyword if    '${count}'!='2'    fail
    Wait until element is enabled    ${FORWARDBTN}    timeout=${timeout}

isResultContainKeywordNotDisplay
    [Arguments]    ${text}
    Wait until element is not visible    //ul[@id="quickReplyInnerTray"]/li[text()="${text}"]    timeout=${timeout}

isDeleterAvatarDisplay
    # [Arguments]    ${text}
    # Wait until element is not visible     //div[contains(@class, "styles__UserAvatar")]//descendant::span[contains(@class, "styles__UserLabel")][contains(text(), "${text}")]
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "UserAvatar")][contains(@style, "bedcfbaa22ba66a8d5292f21ca0c9a8d.png")]    timeout=${timeout}

isNameDisplayOnChatroom
    [Arguments]    ${text}
    Wait until element is visible    //span[contains(@class, "styles__GroupName")][text()="${text}"]    timeout=${timeout}

isDeleteUserTitleDisplay
    [Arguments]    ${text}
    Wait until element is visible    //span[contains(@class, "styles__UserLabel")][contains(text(), "${text}")]//following-sibling::span[contains(@class, "styles__UserPosition")][contains(string(), 'Deleted user')]    timeout=${timeout}

isQuickReplyButtonDisplay
    [Arguments]    ${text}
    Wait until element is visible    //span[text()="${text}"]/parent::button    timeout=${timeout}

isLeaveChatButtonDisappear
    Wait until element is not visible    ${LEAVECHATBUTTON}    timeout=${timeout}

isVoiceSentFromBot
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//span[contains(@class, "UserLabel")][text()="bossbot"]    timeout=${timeout}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "VoiceMessage")]    timeout=${timeout}

isAppCanDisplayAll10Carousel
    Wait until element is visible    ${CAROUSELITEM}    timeout=${timeout}
    Mouse over    ${CAROUSELITEM}
    Click element    ${NEXTCAROUSELBTN}
    Click element    ${NEXTCAROUSELBTN}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//div[contains(@class, "CarouselContainer")]/div/div[10]

isBotButtonDisplayCorrect
    Wait until element is visible    ${BOTTITLEANDDESCRIPTION}    timeout=${timeout}
    Wait until element is visible    ${BOTBUTTONIMAGECONTAINER}    timeout=${timeout}
    Wait until element is visible    ${BOTBUTTONLIST}    timeout=${timeout}

isAppReturnTextMessageWhenClickOnBotButton
    [Arguments]    ${text}
    # Wait until element is visible    //div[contains(@class, "MessageContainer")][last()]//span[contains(text(), "not recognized")]    timeout=${timeout}
    Wait until element is visible    //div[contains(@class, "MessageContainer")][last()-1]//span[text()="${text}"]    timeout=${timeout}

isAppRedirectToCorrectURLFromChatPage
    [Arguments]    ${URL}
    @{list}    get window handles
    FOR  ${window}  IN  @{list}
       Select window    ${window}
    END
    ${location}    get location    
    Run Keyword if     '${location}'!='${URL}'    fail
