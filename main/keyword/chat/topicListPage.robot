*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${INPUTTOPICNAMEFIELD}    //div[contains(@class, "CreateTopic")]//input
${CREATETOPICBTN}    //div[contains(@class, "CreateTopic")]//button
${FIRSTTOPICLIST}    //div[contains(@class, "TopicItem")][1]


*** Keywords ***

createNewTopicInChat
    [Arguments]    ${text}
    Wait until element is enabled    ${INPUTTOPICNAMEFIELD}    timeout=${timeout}
    Input text    ${INPUTTOPICNAMEFIELD}    ${text}
    Click element    ${CREATETOPICBTN}

selectTopic
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "TopicItem")][@title="${text}"]    timeout=${timeout}
    Click element    //div[contains(@class, "TopicItem")][@title="${text}"]

selectFirstTopic
    Wait until element is enabled    ${FIRSTTOPICLIST}    timeout=${timeout}
    Click element    ${FIRSTTOPICLIST}

#--------------------------------- Verify ---------------------------------------

isTopicDisappear
   [Arguments]    ${text}
   Wait until element is not visible    //div[contains(@class, "TopicItem")][@title="${text}"]    timeout=${timeout}
    
isTopicAppear
   [Arguments]    ${text}
   Wait until element is visible    //div[contains(@class, "TopicItem")][@title="${text}"]    timeout=${timeout}