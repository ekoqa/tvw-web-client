*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${TITLEFIELD}    //input[@data-qa-anchor="title"]
${SELECTASSIGNEE}    //div[contains(@class, "CustomArrow")]
${ASSIGNEELISTCONTAINER}    //div[@class="Select-menu-outer"]
${SUBMITBTN}    //button[@data-qa-anchor="btn-submit"]
${ASSIGNEESEARCHFIELD}    //input[@data-qa-anchor="select-input"]

*** Keywords ***

setTaskTiTleFromChatPage
    [Arguments]    ${title}
    Wait until element is visible    ${TITLEFIELD}    timeout=${timeout}
    Input text    ${TITLEFIELD}    ${title}

openTaskAssigneeListInChat
    Wait until element is enabled    ${SELECTASSIGNEE}    timeout=${timeout}
    Click element    ${SELECTASSIGNEE}

selectTaskAssigneeInChat
    [Arguments]    ${text}
    Wait until element is visible    //div[@class="Select-menu-outer"]//*[contains(text(), "${text}")]    timeout=${timeout}
    Click element    //div[@class="Select-menu-outer"]//*[contains(text(), "${text}")]

confirmCreateTask
    Wait until element is enabled    ${SUBMITBTN}    timeout=${timeout}
    Click element    ${SUBMITBTN}
    Wait until element is not visible    ${SUBMITBTN}    timeout=${timeout}

searchForAddAssigneeInTask
    [Arguments]    ${text}
    Wait until element is enabled    ${ASSIGNEESEARCHFIELD}    timeout=${timeout}
    Input text    ${ASSIGNEESEARCHFIELD}    ${text}



    