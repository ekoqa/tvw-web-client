*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${ADDNEWPORTALBTN}    //a[@href="/webview/create"]
${REMOVESELECTEDPORTALBTN}    webview-remove-selected
${CONFIRMREMOVEPORTALBTN}    //button[contains(@class, "confirm-delete")]

*** Keywords ***

clickAddNewPortal
    Wait until element is enabled    ${ADDNEWPORTALBTN}    timeout=${timeout}
    Click element    ${ADDNEWPORTALBTN}

selectPortalToRemove
    [Arguments]    ${text}
    Wait until element is enabled    //span[text()="${text}"]/parent::*/preceding-sibling::div//input    timeout=${timeout}
    Click element    //span[text()="${text}"]/parent::*/preceding-sibling::div//input

clickRemoveSelectedPortal
    Wait until element is enabled    ${REMOVESELECTEDPORTALBTN}    timeout=${timeout}
    Click element    ${REMOVESELECTEDPORTALBTN}

confirmRemovePortal
    Wait until element is enabled    ${CONFIRMREMOVEPORTALBTN}    timeout=${timeout}
    Click element    ${CONFIRMREMOVEPORTALBTN}