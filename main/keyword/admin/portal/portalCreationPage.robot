*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${PORTALTITLETEXTFIELD}    title
${PORTALURLFIELD}    url
${PORTALPERMISSIONSEARCHFIELD}    //li[contains(@class, "search")]/input
${SAVEPORTALBTN}    webviewCreateFormSubmit

*** Keywords ***

inputPortalName
    [Arguments]    ${text}
    Wait until element is enabled    ${PORTALTITLETEXTFIELD}    timeout=${timeout}
    Input text    ${PORTALTITLETEXTFIELD}    ${text}

inputPortalURL
    [Arguments]    ${text}=https://www.netflix.com/browse
    Wait until element is enabled    ${PORTALURLFIELD}    timeout=${timeout}
    Input text    ${PORTALURLFIELD}    ${text}

searchDirectoryForPortalPermission
    [Arguments]    ${text}
    Wait until element is enabled    ${PORTALPERMISSIONSEARCHFIELD}    timeout=${timeout}
    Input text    ${PORTALPERMISSIONSEARCHFIELD}    ${text}

selectDirectoryToAddPortalPermission
    [Arguments]    ${text}
    Wait until element is enabled    //li[contains(@class, "result")]//strong[text()="${text}"]    timeout=${timeout}
    Click element    //li[contains(@class, "result")]//strong[text()="${text}"]

savePortalCreation
    Wait until element is enabled    ${SAVEPORTALBTN}    timeout=${timeout}
    Click element    ${SAVEPORTALBTN}