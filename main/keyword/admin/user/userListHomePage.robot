*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${USERSLISTTITLEPAGE}    //li[@class="active"][@title="Users"]
${ADDUSERINNETWORKICON}    //i[@class="material-icons"][text()="add"]/parent::*
${ADDSINGLEUSERINNETWORKBTN}    //a[@data-original-title="Add single user"]
${ORDERUSERINUSERADMINPAGE}    //select[@class="users-index-order-by"]



*** Keywords ***

clickCreateSingleUser
    Wait until element is enabled    ${ADDUSERINNETWORKICON}    timeout=${timeout}
    Mouse over    ${ADDUSERINNETWORKICON}
    Wait until element is enabled    ${ADDSINGLEUSERINNETWORKBTN}    timeout=${timeout}
    Click element    ${ADDSINGLEUSERINNETWORKBTN}

orderUserByName
    Select from list by value    ${ORDERUSERINUSERADMINPAGE}    fullName

openUserProfileFromUserHomePage
    [Arguments]    ${username}
    Wait until element is visible        //div[@title="${username}"]/preceding-sibling::div/a    timeout=${timeout}
    Click element    //div[@title="${username}"]/preceding-sibling::div/a