*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${SETUSERNAMETEXTFIELD}    username
${PASSWORDSETTING}    sendEmail
${SETPASSWORDTEXTFIELD}    password
${SETFIRSTNAMETEXTFIELD}    firstname
${CREATEUSERBTN}    createUserButton

*** Keywords ***

inputUsername
    [Arguments]    ${text}
    Wait until element is enabled    ${SETUSERNAMETEXTFIELD}    timeout=${timeout}
    Input text    ${SETUSERNAMETEXTFIELD}    ${text}

selectToSpecifyPassword
    Select radio button    ${PASSWORDSETTING}    false

specifyUserPassword
    [Arguments]    ${password}=password
    Wait until element is enabled    ${SETPASSWORDTEXTFIELD}    timeout=${timeout}
    Input text    ${SETPASSWORDTEXTFIELD}    ${password}

setFirstname
    [Arguments]    ${firstname}
    Wait until element is enabled    ${SETFIRSTNAMETEXTFIELD}    timeout=${timeout}
    Input text    ${SETFIRSTNAMETEXTFIELD}    ${firstname}

confirmToCreateUser
    [Arguments]    ${firstname}
    Wait until element is enabled    ${CREATEUSERBTN}    timeout=${timeout}
    Click element    ${CREATEUSERBTN}
    Wait until element is visible    //li[@title="${firstname}"]    timeout=${timeout}