*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${DELETEUSERFROMNETWORKBTN}    //a[@data-method="DELETE"]
${LOGINDEVICEDATA}    //div[@class="profile-list-autoscroll"]//*[contains(text(), "a few seconds ago")]



*** Keywords ***

deleteUserFromTheNetwork
    Wait until element is enabled    ${DELETEUSERFROMNETWORKBTN}    timeout=${timeout}
    Click element    ${DELETEUSERFROMNETWORKBTN}
    Alert should be present
    Wait until page contains element    ${USERSLISTTITLEPAGE}    timeout=${timeout}

isUserLogInDeviceIsRecorded
    Wait until element is visible    ${LOGINDEVICEDATA}    timeout=${timeout}