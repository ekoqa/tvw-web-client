*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${MASTERSEARCHFIELD}    //form[@class="master-search"]//input



*** Keywords ***

searchUserInAdminFromMasterSearch
    [Arguments]    ${firstname}
    Wait until element is enabled    ${MASTERSEARCHFIELD}    timeout=${timeout}
    Input text    ${MASTERSEARCHFIELD}    ${firstname}

selectTheSearchUser
    [Arguments]    ${firstname}
    Wait until element is enabled    //div[contains(@class, "type-search-suggestion")]/div[@class="media-body"]/strong[contains(text(), "${firstname}")]    timeout=${timeout}
    Click element        //div[contains(@class, "type-search-suggestion")]/div[@class="media-body"]/strong[contains(text(), "${firstname}")]