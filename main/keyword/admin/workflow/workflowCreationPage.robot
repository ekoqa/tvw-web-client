*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${WORKFLOWTEMPLATEPENCILICON}    //div[@title]/*[@data-icon="pen"]
${WORKFLOWTEMPLATETITLEEDITTEXTFIELD}    //input[@placeholder="Workflow\ Title"]
${WORKFLOWTEMPLATEDESCRIPTIONEDITTEXTFIELD}    //input[@placeholder="Workflow\ Description"]
${SAVEWORKFLOWBTN}    //div[@class="sc-dfVpRl eDFGxQ"]/button[@type="button"][3]
${SAVEWORKFLOWLOADING}    //div[@class="sc-lhVmIH fRKAc"]/button[@type="button"][3][contains(@class, "loading")]
${WORKFLOWBACKARROW}    //a[@href="/"]
${CONFIRMBACKTOWIRKFLOWHOMEPAGEBTN}    //div[@class="ant-popover-message-title"]/parent::*/following-sibling::div/button[2]
${WORKFLOWBUILDERMENU}    //div[text()="Builder"]
${EDITSTAGEDETAILBTN}    //span[text()="Stage Details"]/parent::*
${STAGETITLETEXTEDITFIELD}    //input[@placeholder="Stage Title"]
${STAGEDESCRIPTIONEDITTEXTFIELD}    //input[@placeholder="Stage Description"]
${PARALLELSTAGEEDITTEXTFIELD}    //input[@placeholder="Parallel Title"]
${SETTINGTABMENU}    //div[@role="tab"][2]
${RECIPIENTSTABMENU}    //div[@role="tab"][1]
${FORMDESIGNSTABMENU}    //div[@role="tab"][2]
${ACTIONSTABMENU}    //div[@role="tab"][3]
${ADMINDEFINERADIOTYPE}    //input[@value="preselected"]
${USERDEFINERADIOTYPE}    //input[@value="state"]    
${PRIVACYBOX}    //div[text()="Privacy"]/following-sibling::div[1]//div[contains(@class, "selection__rendered")]
${EVERYONEPRIVACYSELECTION}    //ul[@role="listbox"]/li[@role="option"][text()="Everyone"]
${CREATORPRIVACYSELECTION}    //ul[@role="listbox"]/li[@role="option"][text()="Creator"]
${NOONEPRIVACYSELECTION}    //ul[@role="listbox"]/li[@role="option"][text()="No One"]
${ASSIGNEENUMBEREDITTEXT}    //div[text()="Assignees"]/following-sibling::div[1]/input
${APPROVALNUMBEREDITTEXT}    //div[text()="Approvals"]/following-sibling::div[1]/input
${ADDANOTHERACTIONBTN}    //div[@class="sc-dEoRIm cfWjNb"]/button[@type="button"]
${ENDACTIONLISTBTN}    //div[@class="ant-popover-inner-content"]//div[text()="End"]
${WORKFLOWNOTIFICATIONCHECKBOX}    //*[contains(text(), "template notification")]/preceding-sibling::span/input
${WORKFLOWDUEDATECHECKBOX}    //*[contains(text(), "Due date")]/preceding-sibling::span/input



*** Keywords ***


clickEditWorkflowTemplateTitle
    Wait until element is enabled    ${WORKFLOWTEMPLATEPENCILICON}    timeout=${timeout}
    Click element    ${WORKFLOWTEMPLATEPENCILICON}

editWorkflowTemplateName
    [Arguments]    ${workflowName}
    Wait until element is enabled    ${WORKFLOWTEMPLATETITLEEDITTEXTFIELD}    timeout=${timeout}
    Input Text    ${WORKFLOWTEMPLATETITLEEDITTEXTFIELD}    ${workflowName}

editWorkflowTemplateDescription
    [Arguments]    ${workflowDescription}
    Wait until element is enabled    ${WORKFLOWTEMPLATEDESCRIPTIONEDITTEXTFIELD}    timeout=${timeout}
    Input Text    ${WORKFLOWTEMPLATEDESCRIPTIONEDITTEXTFIELD}    ${workflowDescription}

clickConfirmEditWorkflowTemplateTitle
    Wait until element is enabled    ${WORKFLOWTEMPLATEPENCILICON}    timeout=${timeout}
    Click element    ${WORKFLOWTEMPLATEPENCILICON}

clickSaveTheWorkflowTemplate
    Wait until element is enabled    ${SAVEWORKFLOWBTN}    timeout=${timeout}
    Click element    ${SAVEWORKFLOWBTN}
    Wait until page does not contain element    ${SAVEWORKFLOWLOADING}    timeout=${timeout}

backToWorkflowHomePage
    Wait until element is enabled    ${WORKFLOWBACKARROW}    timeout=${timeout}
    Click element    ${WORKFLOWBACKARROW}
    Wait until element is enabled    ${CONFIRMBACKTOWIRKFLOWHOMEPAGEBTN}    timeout=${timeout}
    Click element    ${CONFIRMBACKTOWIRKFLOWHOMEPAGEBTN}

selectWorkflowBuilderMenu
    Wait until element is enabled    ${WORKFLOWBUILDERMENU}    timeout=${timeout}
    Click element    ${WORKFLOWBUILDERMENU}

clickEditStageDetail
    Wait until element is enabled    ${EDITSTAGEDETAILBTN}    timeout=${timeout}
    Click element    ${EDITSTAGEDETAILBTN}

editStageNameAndDescription
    [Arguments]    ${stageNumber}    ${stageName}
    selectStageNumber  ${stageNumber}
    clickEditStageName
    inputStageTitle  ${stageName}
    inputStageDescription  The stage name is edited by robot
    clickEditStageName

editParallelStageName
    [Arguments]    ${stageNumber}    ${parallelName}
    selectStageNumber  ${stageNumber}
    clickEditStageName
    inputParallelStageTitle    ${parallelName}
    clickEditStageName

editStageNameInParallelStage
    [Arguments]    ${stageNumber}    ${subStageNumber}    ${subStageName}
    selectStageInParallelFlow  ${stageNumber}  ${subStageNumber}
    clickEditStageName
    inputStageTitle  ${subStageName}
    inputStageDescription  The stage name is edited by robot
    clickEditStageName

selectStageNumber
    [Arguments]    ${stageNumber}
    Wait until element is enabled    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]    timeout=${timeout}
    Click element    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]

selectParallelStage
    [Arguments]    ${stageNumber}    ${subStageNumber}
    ${click}    Run Keyword And Return Status    Element should be visible    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]//div[@class="sc-tilXH jxSiES"][${subStageNumber}]
    Run Keyword Unless    ${click}    Click element    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]
    Click element    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]//div[@class="sc-tilXH jxSiES"][${subStageNumber}]

clickEditStageName
    Wait until element is enabled    //div[@class="sc-kgAjT hKyyPb"]/*[@data-icon="pen"]    timeout=${timeout}
    Click element    //div[@class="sc-kgAjT hKyyPb"]/*[@data-icon="pen"]

inputStageTitle
    [Arguments]    ${stageName}
    Wait until element is enabled    ${STAGETITLETEXTEDITFIELD}    timeout=${timeout}
    Input text    ${STAGETITLETEXTEDITFIELD}    ${stageName}

inputStageDescription
    [Arguments]    ${stageDescription}
    Wait until element is enabled    ${STAGEDESCRIPTIONEDITTEXTFIELD}    timeout=${timeout}
    Input text    ${STAGEDESCRIPTIONEDITTEXTFIELD}    ${stageDescription}

inputParallelStageTitle
    [Arguments]    ${parallelName}
    Wait until element is enabled    ${PARALLELSTAGEEDITTEXTFIELD}    timeout=${timeout}
    Input text    ${PARALLELSTAGEEDITTEXTFIELD}    ${parallelName}

selectStageInParallelFlow
    [Arguments]    ${stageNumber}    ${subStageNumber}
    ${click}    Run Keyword And Return Status    Element should be visible    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]//div[@class="sc-tilXH jxSiES"][${subStageNumber}]
    Run Keyword Unless    ${click}    Click element    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]
    Click element    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]//div[@class="sc-tilXH jxSiES"][${subStageNumber}]

clickDropdownListInParalleStage
    [Arguments]    ${stageNumber}
    ${click}    Run Keyword And Return Status    Element should be visible    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]//div[@class="sc-tilXH jxSiES"][1]
    Run Keyword Unless    ${click}    Click element    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]
    
editLabelNameInAField
    [Arguments]    ${oldLabel}    ${newLabel}
    Input text    //input[@value="${oldLabel}"]    ${newLabel}

clickMoreSettingInFieldName
    [Arguments]    ${fieldName}
    Wait until element is enabled    //input[@value="${fieldName}"]/parent::*/parent::*/following-sibling::div//div[@role="button"]    timeout=${timeout}
    Click element    //input[@value="${fieldName}"]/parent::*/parent::*/following-sibling::div//div[@role="button"]

selectRestrictOnlyToTheseUser
    [Arguments]    ${fieldName}
    Wait until element is enabled    //input[@value="${fieldName}"]/parent::*/parent::*/following-sibling::div//div[@role="button"]/following-sibling::div//input[@type="radio"][@value="true"]    timeout=${timeout}
    Click element        //input[@value="${fieldName}"]/parent::*/parent::*/following-sibling::div//div[@role="button"]/following-sibling::div//input[@type="radio"][@value="true"]

addSelectedUserInFieldName
    [Arguments]    ${fieldName}    ${user}
    Wait until element is visible        //input[@value="${fieldName}"]/parent::*/parent::*/following-sibling::div//div[@role="button"]/following-sibling::div/div/div[2]//div[contains(@class, "selection")][@role="combobox"]
    Input text    //input[@value="${fieldName}"]/parent::*/parent::*/following-sibling::div//div[@role="button"]/following-sibling::div//div[text()="Add users and groups"]/following-sibling::ul//input    ${user}
    Wait until element is visible    //ul[@role="listbox"]/li[@role="option"][text()="${user}"]
    Click element    //ul[@role="listbox"]/li[@role="option"][text()="${user}"]
    Wait until page contains element    //div[contains(@class, "selection__choice")][text()="${user}"]
    
unselectRequiredInput
    [Arguments]    ${fieldNumber}
    ${check}    Run Keyword And Return Status     Page should not contain element    //div[@class="sc-bnXvFD fJDTPl"]/div[@class="sc-gHboQg gdrMwE"][${fieldNumber}]//div[text()="Required"]/button[@aria-checked="false"]
    Wait until page contains element    //div[@class="sc-bnXvFD fJDTPl"]/div[@class="sc-gHboQg gdrMwE"][${fieldNumber}]//div[text()="Required"]/button    timeout=${timeout}
    Run Keyword If    ${check}    Click element    //div[@class="sc-bnXvFD fJDTPl"]/div[@class="sc-gHboQg gdrMwE"][${fieldNumber}]//div[text()="Required"]/button

selectSettingTab
    Wait until element is enabled    ${SETTINGTABMENU}    timeout=${timeout}
    Click element    ${SETTINGTABMENU}
    
selectRecipientsTabMenu
    Wait until element is enabled    ${RECIPIENTSTABMENU}    timeout=${timeout}
    Click element    ${RECIPIENTSTABMENU} 

selectFormDesignTabMenu
    Wait until element is enabled    ${FORMDESIGNSTABMENU}    timeout=${timeout}
    Click element    ${FORMDESIGNSTABMENU}

selectActionsTabMenu
    Wait until element is enabled    ${ACTIONSTABMENU}    timeout=${timeout}
    Click element    ${ACTIONSTABMENU}

selectAdminDefinedForReciepientsType
    Wait until element is enabled    ${ADMINDEFINERADIOTYPE}    timeout=${timeout}
    Click element    ${ADMINDEFINERADIOTYPE}

selectUserDefinedForReciepientsType
    Wait until element is enabled    ${USERDEFINERADIOTYPE}    timeout=${timeout}
    Click element    ${USERDEFINERADIOTYPE}

inputReciepientSourceForWorkflow
    [Arguments]    ${source}
    Wait until element is enabled    //input[@autocomplete]    timeout=${timeout}
    Input text    //input[@autocomplete]    ${source}
    Wait until element is enabled    //ul[@role="listbox"]/li[@role="option"][text()="${source}"]    timeout=${timeout}
    Click element    //ul[@role="listbox"]/li[@role="option"][text()="${source}"]
    Wait until page contains element    //div[contains(@class, "selection__choice")][text()="${source}"]    timeout=${timeout}

editLabelForResponseButton
    [Arguments]    ${actionNumber}    ${oldLabel}    ${newLabel}
    Click element    //div[@class="sc-dliRfk kaEmoH"]/div[@draggable="true"][${actionNumber}]//*[@data-icon="pen"]
    Input text    //input[@value="${oldLabel}"]    ${newLabel}
    Click element    //div[@class="sc-dliRfk kaEmoH"]/div[@draggable="true"][${actionNumber}]//*[@data-icon="pen"]

# User define recipient source by select stage
selectStageForUserDefineSource
    [Arguments]    ${stage}
    Click element    //div[text()="Source"]/following-sibling::div[1]//div[@role="combobox"]
    Click element    //ul[@role="listbox"]/li[@role="option"][text()="${stage}"]

selectInputFieldForUserDefineSource
    [Arguments]    ${input}
    Click element    //div[text()="Source"]/following-sibling::div[2]//div[@role="combobox"]
    Click element    //ul[@role="listbox"]/li[@role="option"][text()="${input}"]

clickRemoveTheInputFieldInFormDesign
    [Arguments]    ${fieldName}
    Wait until element is enabled    //input[@value="${fieldName}"]/parent::*/parent::*/preceding-sibling::div//*[@data-icon="times"]    timeout=${timeout}
    Click element    //input[@value="${fieldName}"]/parent::*/parent::*/preceding-sibling::div//*[@data-icon="times"]

selectEveryonePrivacy
    Wait until element is enabled    ${PRIVACYBOX}    timeout=${timeout}
    Click element    ${PRIVACYBOX}
    Wait until element is enabled    ${EVERYONEPRIVACYSELECTION}    timeout=${timeout}
    Click element    ${EVERYONEPRIVACYSELECTION}

selectOnlyCreatorPrivacy
    Wait until element is enabled    ${PRIVACYBOX}    timeout=${timeout}
    Click element    ${PRIVACYBOX}
    Wait until element is enabled    ${CREATORPRIVACYSELECTION}    timeout=${timeout}
    Click element    ${CREATORPRIVACYSELECTION}

selectNoOnePrivacy
    Wait until element is enabled    ${PRIVACYBOX}    timeout=${timeout}
    Click element    ${PRIVACYBOX}
    Wait until element is enabled    ${NOONEPRIVACYSELECTION}    timeout=${timeout}
    Click element    ${NOONEPRIVACYSELECTION}

clickRemoveRecipientSource
    [Arguments]    ${source}
    Wait until element is enabled    //div[text()="Source"]/following-sibling::div//div[text()="${source}"]/following-sibling::span//*[@data-icon="close"]    timeout=${timeout}
    Click element    //div[text()="Source"]/following-sibling::div//div[text()="${source}"]/following-sibling::span//*[@data-icon="close"]

setNumberOfAssigneesInRecipients
    [Arguments]    ${number}
    Wait until element is enabled    ${ASSIGNEENUMBEREDITTEXT}    timeout=${timeout}
    Input text    ${ASSIGNEENUMBEREDITTEXT}    ${number}

setNumberOfApprovalsInStage
    [Arguments]    ${number}
    Wait until element is enabled    ${APPROVALNUMBEREDITTEXT}    timeout=${timeout}
    Input text    ${APPROVALNUMBEREDITTEXT}    ${number}

editTitleForCustomField
    [Arguments]    ${fieldNumber}    ${oldLabel}    ${newLabel}
    Wait until element is enabled    //div[@class="sc-gHboQg gdrMwE"][${fieldNumber}]//input[@value="${oldLabel}"]
    Input text    //div[@class="sc-gHboQg gdrMwE"][${fieldNumber}]//input[@value="${oldLabel}"]    ${newLabel}

# Add more choice in the field
addMoreChoiceForCustomField
    [Arguments]    ${fieldNumber}
    Wait until element is enabled    //div[@class="sc-gHboQg gdrMwE"][${fieldNumber}]//div[text()="Options"]//div[text()="Add Option"]    timeout=${timeout}
    Click element    //div[@class="sc-gHboQg gdrMwE"][${fieldNumber}]//div[text()="Options"]//div[text()="Add Option"]

editChoiceLabel
    [Arguments]    ${fieldNumber}    ${choice}    ${newLabel}
    ${fieldNumber}=  Evaluate    ${fieldNumber}*2
    Clear element text    //div[@class="sc-bnXvFD fJDTPl"]/div[${fieldNumber}]//div[text()="Options"]/div[${choice}]//input
    Input text    //div[@class="sc-bnXvFD fJDTPl"]/div[${fieldNumber}]//div[text()="Options"]/div[${choice}]//input    ${newLabel}

addEndActioButtonInWorkflow
    Wait until element is enabled    ${ADDANOTHERACTIONBTN}    timeout=${timeout}
    Click element    ${ADDANOTHERACTIONBTN}
    Wait until element is enabled    ${ENDACTIONLISTBTN}    timeout=${timeout}
    Click element    ${ENDACTIONLISTBTN}

editLabelActionButton
    [Arguments]    ${actionNumber}    ${oldLabel}    ${newLabel}
    Wait until element is enabled    //div[@class="sc-dliRfk kaEmoH"]/div[@draggable="true"][${actionNumber}]//*[@data-icon="pen"]    timeout=${timeout}
    Click element    //div[@class="sc-dliRfk kaEmoH"]/div[@draggable="true"][${actionNumber}]//*[@data-icon="pen"]
    Input text    //input[@value="${oldLabel}"]    ${newLabel}
    Click element    //div[@class="sc-dliRfk kaEmoH"]/div[@draggable="true"][${actionNumber}]//*[@data-icon="pen"]

setActionButtonColor
    [Arguments]    ${actionNumber}    ${color}    ${colorCode}
    Click element    //div[@draggable="true"][${actionNumber}]//h3[text()="Response Button"]/parent::*/*[@data-icon="pen"]
    Wait until element is visible    //div[contains(@style, "position: absolute; top: 0px")][${actionNumber}+1]//div[contains(@class, "circle")]    timeout=${timeout}
    Click element    //div[contains(@style, "position: absolute; top: 0px")][${actionNumber}+1]//div[contains(@class, "circle")]/span[${color}]/div
    Wait until element is visible    //div[@draggable="true"][${actionNumber}]//h3[text()="Response Button"]/following-sibling::button[@color="${colorCode}"]
    Click element    //div[@draggable="true"][${actionNumber}]//h3[text()="Response Button"]/parent::*/*[@data-icon="pen"]
    Wait until element is not visible    //div[contains(@style, "position: absolute; top: 0px")][${actionNumber}+1]//div[contains(@class, "circle")]    timeout=${timeout}

clickDiabledWorkflowNotification
    Wait until element is enabled    ${WORKFLOWNOTIFICATIONCHECKBOX}    timeout=${timeout}
    Click element    ${WORKFLOWNOTIFICATIONCHECKBOX}

clickDisabledWorkflowDueDateField
    Wait until element is enabled    ${WORKFLOWDUEDATECHECKBOX}    timeout=${timeout}
    Click element    ${WORKFLOWDUEDATECHECKBOX}
#--------------------------- verify --------------------------------------------

# Page should contains workflow template with correct status
isWorkflowDisplayCorrectNameAndStatus
    [Arguments]    ${templateName}    ${status}
    Wait until element is visible    //div[@title][text()="${templateName}"]/parent::*/following-sibling::td[3]/div/button[@aria-checked="${status}"]    timeout=${timeout}

isStageNameDisplayCorrect
    [Arguments]    ${stageNumber}    ${stageName}
    Wait until element is visible     //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]/div//*[text()="${stageName}"]    timeout=${timeout}

isSubStageNameDisplayCorrect
    [Arguments]    ${stageNumber}    ${subStageNumber}    ${subStageName}
    Wait until element is visible    //p[text()="STAGES"]/following-sibling::div[1]/div[${stageNumber}]//div[@class="sc-tilXH jxSiES"][${subStageNumber}]/div/a/div[text()="${subStageName}"]    timeout=${timeout}

isRestricUserAdded
    [Arguments]    ${name} 
    Wait until element is visible    //div[contains(@class, "selection__choice")][text()="${name}"]    timeout=${timeout}

isRestricDirectoryAdded
    [Arguments]    ${name} 
    Wait until element is visible    //div[contains(@class, "selection__choice")][text()="${name}"]    timeout=${timeout}

isUnRequiredInput
    [Arguments]    ${fieldNumber}
    Wait until page contains element    //div[@class="sc-bnXvFD fJDTPl"]/div[@class="sc-gHboQg gdrMwE"][${fieldNumber}]//div[text()="Required"]/button[@aria-checked="false"]    timeout=${timeout}

isAdminDefinedTypeDisplayCorrect
    [Arguments]    ${source}
    Wait until page contains element    //div[contains(@class, "selection__choice")][text()="${source}"]    timeout=${timeout}

isResponseButtonDisplayCorrect
    [Arguments]    ${actionNumber}    ${text}
    Wait until element is visible    //div[@class="sc-dliRfk kaEmoH"]/div[@draggable="true"][${actionNumber}]//*[text()="${text}"]    timeout=${timeout}

isUserDefineTypeDisplayCorrect
    [Arguments]    ${text}
    Wait until element is visible    //*[text()="Select input"]/following-sibling::div[text()="${text}"]    timeout=${timeout}

isInputFieldRemoved
    [Arguments]    ${text}
    Wait until element is not visible    //input[@value="Select multiple user for final step"]    timeout=${timeout}

isWorkflowPrivacyStageDisplayCorrect
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "selected-value")][@title="${text}"]    timeout=${timeout}

isReciepientSourceRemove
    [Arguments]    ${text}
    Wait until element is not visible    //div[contains(@class, "selection")]//*[text()="${text}"]    timeout=${timeout}
    
isReciepientSourceAdd
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "selection")]//*[text()="${text}"]    timeout=${timeout}
    
isNumberOfAssigneesDisplayCorrect
    [Arguments]    ${index}
    Wait until element is visible    //div[text()="Assignees"]/following-sibling::div[1]/input[@value="${index}"]    timeout=${timeout}

isNumberOfApprovalsDisplayCorrect
    [Arguments]    ${index}
    Wait until element is visible    //div[text()="Approvals"]/following-sibling::div[1]/input[@value="${index}"]    timeout=${timeout}

isTitleForCustomFieldDisplayCorrect
    [Arguments]    ${fieldNumber}    ${text}
    Wait until element is visible    //div[@class="sc-gHboQg gdrMwE"][${fieldNumber}]//input[@value="${text}"]    timeout=${timeout}

isChoiceLabelDisplayCorrect
    [Arguments]    ${fieldNumber}    ${choice}    ${text}
    ${fieldNumber}=  Evaluate    ${fieldNumber}*2
    Wait until element is visible    //div[@class="sc-bnXvFD fJDTPl"]/div[${fieldNumber}]//div[text()="Options"]/div[${choice}]//input[@value="${text}"]    timeout=${timeout}

isActionButtonDisplayCorrect
    [Arguments]    ${color}    ${label}
    Wait until element is visible    //button[@color="${color}"]/*[text()="${label}"]    timeout=${timeout}