*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***











*** Keywords ***

clickDuplicateWorkflowtemplate
    [Arguments]    ${templateName}
    Wait until element is enabled    //div[@title][text()="${templateName}"]/parent::*/following-sibling::td[3]/div/div[1]/*[@data-icon="copy"]    timeout=${timeout}
    Click element    //div[@title][text()="${templateName}"]/parent::*/following-sibling::td[3]/div/div[1]/*[@data-icon="copy"]

selectWorkflowToEdit
    [Arguments]    ${templateName}
    Wait until element is enabled    //div[contains(@title, "id")][text()="${templateName}"]    timeout=${timeout}
    Click element    //div[contains(@title, "id")][text()="${templateName}"]

publishTheWorkflowTemplate
    [Arguments]    ${workflowName}
    ${turnOff}    Run Keyword And Return Status    Page should contain element    //div[@title][text()="${workflowName}"]/parent::*/following-sibling::td[3]//button[@aria-checked="false"]
    Run keyword if    ${turnOff}    Click element    //div[@title][text()="${workflowName}"]/parent::*/following-sibling::td[3]//button 

unPublishTheWorkflowTemplate
    [Arguments]    ${workflowName}
    ${turnOn}    Run Keyword And Return Status    Page should contain element    //div[@title][text()="${workflowName}"]/parent::*/following-sibling::td[3]//button[@aria-checked="true"]
    Run keyword if    ${turnOn}    Click element    //div[@title][text()="${workflowName}"]/parent::*/following-sibling::td[3]//button

clickDeleteWorkflowTemplate
    [Arguments]    ${workflowName}
    Wait until element is enabled    //div[@title][text()="${workflowName}"]/parent::*/following-sibling::td[3]//*[@data-icon="trash-alt"]    timeout=${timeout}
    Click element    //div[@title][text()="${workflowName}"]/parent::*/following-sibling::td[3]//*[@data-icon="trash-alt"]
    Wait until element is visible    //div[@class="ant-popover-message-title"]
    Click element    //div[@class="ant-popover-message-title"]/parent::*/following-sibling::div/button[2]

searchWorkflowInAdminPanel
    [Arguments]    ${text}
    Wait until element is enabled    //span[contains(@class, "ant-input-search")]/input    timeout=${timeout}
    Input text    //span[contains(@class, "ant-input-search")]/input    ${text}

#----------------- verify-----------------------

isWorkflowTemplateDeleted
    [Arguments]    ${workflowName}
    Wait until element is not visible    //div[@title][text()="${workflowName}"]    timeout=${timeout}