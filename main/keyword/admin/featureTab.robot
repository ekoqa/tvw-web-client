*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${SKIPBTN}    //a[contains(@class, "skipbutton")]
${BANNERMENUINADMIN}    //a[@href="/banner"]    
${PORTALMENUINADMIN}    //a[@href="/webview"]    
${CREATEUSERMENU}    //a[@href="/users/create"]
${COMPANYMENUINADMIN}    //a[@href="/company"]
${WORKFLOWMENUINADMIN}    //a[@href="/workflow"]
${WORKFLOWIFRMAEINADMIN}    //iframe[contains(@src, ${AdminWorkflowiframe})]
${USERMENUINADMIN}    //a[@href="/users"]
${CHATGROUPMENUINADMIN}    //a[@href="/workspaces"]


*** Keywords ***

skipTheAdminTutorial
    ${skip}    Run Keyword And Return Status    Wait until element is enabled    ${SKIPBTN}    timeout=${timeout}
    Run keyword If    ${skip}    Click element    ${SKIPBTN}
    
clickBannerMenuInAdmin
    Wait until element is enabled   ${BANNERMENUINADMIN}    timeout=${timeout}
    Click element    ${BANNERMENUINADMIN}

clickPortalMenuInAdmin
    Wait until element is enabled   ${PORTALMENUINADMIN}    timeout=${timeout}
    Click element    ${PORTALMENUINADMIN}

clickCreateUserMenuInAdmin
    Wait until element is enabled   ${CREATEUSERMENU}    timeout=${timeout}
    Click element    ${CREATEUSERMENU}

clickCompanyProfileMenuInAdmin
    Wait until element is enabled   ${COMPANYMENUINADMIN}    timeout=${timeout}
    Click element    ${COMPANYMENUINADMIN}

clickWorkflowMenuInAdmin
    Wait until element is enabled   ${WORKFLOWMENUINADMIN}    timeout=${timeout}
    Click element    ${WORKFLOWMENUINADMIN}
    Select Frame    ${WORKFLOWIFRMAEINADMIN}

clickUserMenuInAdmin
    Wait until element is enabled   ${USERMENUINADMIN}    timeout=${timeout}
    Click element    ${USERMENUINADMIN}

clickChatGroupMenuInAdmin
    Wait until element is enabled    ${CHATGROUPMENUINADMIN}    timeout=${timeout}
    Click element    ${CHATGROUPMENUINADMIN}

