*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${GDPRBTN}    //input[@name="enabled"]
${DISABLEGDPR}    //input[@name="enabled"][@value="false"]
${ENABLEDGDPR}    //input[@name="enabled"][@value="true"]
${CONFIRMCHANGEGDPRSETTINBTN}    changeGdprSettingConfirm


*** Keywords ***

enabledGDPR
    Wait until element is enabled    ${GDPRBTN}    timeout=${timeout}
    ${turnOn}    Run Keyword And Return Status    Wait until page contains element    ${DISABLEGDPR}    timeout=5s
    Run Keyword If    ${turnOn}    clickTurnOnGDPR

clickTurnOnGDPR
    Click element    ${DISABLEGDPR}
    Wait until element is visible    ${CONFIRMCHANGEGDPRSETTINBTN}    timeout=${timeout}
    Click element    ${CONFIRMCHANGEGDPRSETTINBTN}
    Wait until page contains element    ${ENABLEDGDPR}    timeout=${timeout}

disabledGDPR
    Wait until element is enabled    ${GDPRBTN}    timeout=${timeout}
    ${turnOn}    Run Keyword And Return Status    Wait until page contains element    ${ENABLEDGDPR}    timeout=5s
    Run Keyword If    ${turnOn}    clickTurnOffGDPR

clickTurnOffGDPR
    Click element    ${ENABLEDGDPR}
    Wait until element is visible    ${CONFIRMCHANGEGDPRSETTINBTN}    timeout=${timeout}
    Click element    ${CONFIRMCHANGEGDPRSETTINBTN}
    Wait until page contains element    ${DISABLEGDPR}    timeout=${timeout}