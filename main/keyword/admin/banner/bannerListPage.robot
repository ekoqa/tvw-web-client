*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${CREATEBANNERBTN}    //a[@href="/banner/create"]
${PUBLISHBANNERBTN}    bannerPublishButton
${PUBLISHSUCCESSMESSAGE}    //span[text()="All banners in the queue are published."]
${BANNEREDITHEADER}    //div[@class="col-md-12 index-bar"]/h3[text()="Banner edit"]
${CONFIRMREMOVEBANNERBTN}   removeBannerConfirm

*** Keywords ***

clickCreateNewBannerBroadcast
    Wait until element is enabled    ${CREATEBANNERBTN}    timeout=${timeout}
    Click element    ${CREATEBANNERBTN}
    
addDefaultColorBannerToTheQueue
    [Arguments]    ${text}
    Wait until page contains element    //div[@class="title"][contains(text(), "${text}")]    timeout=${timeout}
    ${bannerID}    get element attribute    //div[@class="title"][contains(text(), "${text}")]/parent::*/parent::*/parent::*/preceding-sibling::td//input    id
    Click element    //div[@class="title"][contains(text(), "${text}")]/parent::*/parent::*/parent::*/preceding-sibling::td//div[contains(@class, "ToggleSwitch")]
    Wait until element is visible    //div[@class="title"][contains(text(), "${text}")]/parent::*/parent::*/parent::*/preceding-sibling::td//span[@class="on-label"]    timeout=${timeout}    error=App display wrong banner status    
    Wait until page contains element    //div[@id="dragAndDropContainer"]/div[@data-id="${bannerID}"]    timeout=${timeout}    error=Cannot add to the queue

removeDefaultColorBannerToTheQueue
    [Arguments]    ${text}
    Wait until page contains element    //div[@class="title"][contains(text(), "${text}")]    timeout=${timeout}
    ${bannerID}    get element attribute    //div[@class="title"][contains(text(), "${text}")]/parent::*/parent::*/parent::*/preceding-sibling::td//input    id
    Click element    //div[@class="title"][contains(text(), "${text}")]/parent::*/parent::*/parent::*/preceding-sibling::td//div[contains(@class, "ToggleSwitch")]

clickEditDefaultColorBanner
    [Arguments]    ${text}
    Wait until page contains element    //div[@class="title"][contains(text(), "${text}")]    timeout=${timeout}
    ${bannerID}    get element attribute    //div[@class="title"][contains(text(), "${text}")]/parent::*/parent::*/parent::*/preceding-sibling::td//input    id
    Click element    //a[contains(@href, "edit/${bannerID}")]
    Wait until element is visible    ${BANNEREDITHEADER}    timeout=${timeout}
    Location should contain    edit/${bannerID}    message=Cannot open edit page 

clickDeleteDefaultColorBanner
    [Arguments]    ${text}
    Wait until page contains element    //div[@class="title"][contains(text(), "${text}")]    timeout=${timeout}
    ${bannerID}    get element attribute    //div[@class="title"][contains(text(), "${text}")]/parent::*/parent::*/parent::*/preceding-sibling::td//input    id
    Click element    //button[@data="${bannerID}"][contains(@class, "removeBannerButton")]

confirmToDeleteBannerBroadcast
    Wait until element is enabled    ${CONFIRMREMOVEBANNERBTN}    timeout=${timeout}
    Click element    ${CONFIRMREMOVEBANNERBTN}

publishTheBannerQueue
    Wait until element is enabled    ${PUBLISHBANNERBTN}    timeout=${timeout}
    Click element    ${PUBLISHBANNERBTN}
    Wait until element is visible    ${PUBLISHSUCCESSMESSAGE}    timeout=${timeout}