*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${BANNERTITLEFIELD}    title
${BANNERURLFIELD}    url
${CONFIRMCREATEBANNERBTN}    create-banner-button
${BANNETYPESELECTION}    type
${TEXTCONTENTFIELDINBANNER}    //div[@id="contentEditor"]/div[1][@contenteditable="true"]
${DATEPICKERSTARTDATE}    datePickerStartDate
${STARTPUBLISHHOURVALUE}    //div[@class="drp-calendar left"]//select[@class="hourselect"]    
${STARTPUBLISHMINUTEVALUE}    //div[@class="drp-calendar left"]//select[@class="minuteselect"]
${STARTPUBLISHAMPM}    //div[@class="drp-calendar left"]//select[@class="ampmselect"]    
${APPLYPUBLISHTIMEBTN}    //button[contains(@class, "applyBtn")]

*** Keywords ***

setBannerName
    [Arguments]    ${text}
    Wait until element is enabled    ${BANNERTITLEFIELD}    timeout=${timeout}
    Input text    ${BANNERTITLEFIELD}    ${text}

setBannerTypeAs
    [Arguments]    ${bannerType}
    Select radio button    ${BANNETYPESELECTION}    ${bannerType}
    Wait until element is visible    ${bannerType}Container    timeout=${timeout}    error=App display wrong type of container

setUrlLink
    [Arguments]    ${text}
    Wait until element is enabled    ${BANNERURLFIELD}    timeout=${timeout}
    Input text    ${BANNERURLFIELD}    ${text}

inputTextInContent
    [Arguments]    ${text}
    Wait until element is enabled    ${TEXTCONTENTFIELDINBANNER}    timeout=${timeout}
    Input text    ${TEXTCONTENTFIELDINBANNER}    ${text}

confrimCreateBannerBroadcast
    Wait until element is enabled    ${CONFIRMCREATEBANNERBTN}    timeout=${timeout}
    Click element    ${CONFIRMCREATEBANNERBTN}

clickEditPublishTime
    Wait until element is visible    ${DATEPICKERSTARTDATE}    timeout=${timeout}
    Click element    ${DATEPICKERSTARTDATE}

changeThePublishTime
    ${currentHour}    get value    ${STARTPUBLISHHOURVALUE}
    ${currentMinute}    get value    ${STARTPUBLISHMINUTEVALUE}
    Run Keyword If    ${currentMinute}<58     changeOnlyMinuteSelected
    ...    ELSE IF    ${currentHour}<11    changeHoursAndPublicSelected
    ...    ELSE IF   ${currentHour}=11    changeHoursAndAMPM
    ...    ELSE    changeForHoursAtTwelve

changeOnlyMinuteSelected
    ${currentMinute}    get value    ${STARTPUBLISHMINUTEVALUE}
    ${expectedMinute}    Evaluate    ${currentMinute}+2
    ${publishMinute}    Convert To String    ${expectedMinute}  
    Select from list by value    ${STARTPUBLISHMINUTEVALUE}    ${publishMinute}

changeHoursAndPublicSelected
    ${currentHour}    get value    ${STARTPUBLISHHOURVALUE}
    ${currentMinute}    get value    ${STARTPUBLISHMINUTEVALUE}
    ${expectedHour}    Evaluate    ${currentHour}+1
    ${expectedMinute}    Evaluate    1
    Select from list by value    ${STARTPUBLISHHOURVALUE}    ${expectedHour}
    Select from list by value    ${STARTPUBLISHMINUTEVALUE}    ${expectedMinute}

changeHoursAndAMPM
    ${current}    get value    ${STARTPUBLISHAMPM}
    Run Keyword If    '${current}'=='AM'    Select from list by value    ${STARTPUBLISHAMPM}    PM
    ...    Else    Select from list by value    ${STARTPUBLISHAMPM}    AM
    Select from list by value    ${STARTPUBLISHHOURVALUE}    12
    Select from list by value    ${STARTPUBLISHMINUTEVALUE}    1

changeForHoursAtTwelve
    Select from list by value    ${STARTPUBLISHHOURVALUE}    1
    Select from list by value    ${STARTPUBLISHMINUTEVALUE}    1

clickApplyChangePublishTime
    Wait until element is enabled    ${APPLYPUBLISHTIMEBTN}    timeout=${timeout}
    Click element    ${APPLYPUBLISHTIMEBTN}