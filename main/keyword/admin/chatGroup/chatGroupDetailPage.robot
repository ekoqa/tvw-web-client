*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${ADDMEMBERINCLOSECHATINPUTFIELD}    //div[@class="users-master-search"]//input
${ADDMEMBERTOCLOSEDCHATBTN}    //button[@type="submit"][contains(@class, "btn-addmembers")]
${ADDGROUPMODERATORBTN}    toAdminConfirm


*** Keywords ***


addUserInClosedChat
    [Arguments]    ${name}
    searchForAddUserInClosedChat  ${name}
    selectUserListToAddInClosedChatByName  ${name}

searchForAddUserInClosedChat
    [Arguments]    ${name}
    Wait until element is enabled    ${ADDMEMBERINCLOSECHATINPUTFIELD}    timeout=${timeout}
    Input text    ${ADDMEMBERINCLOSECHATINPUTFIELD}    ${name}
    # Wait until element is visible    //ul[@class="select2-results__options"][@role="tree"]    timeout=${timeout}

selectUserListToAddInClosedChatByName
    [Arguments]    ${text}
    Wait until element is enabled    //li[contains(@class, "select2-results")]//*[text()="${text}"]    timeout=${timeout}
    click element    //li[contains(@class, "select2-results")]//*[text()="${text}"]

clickAddUserToClosedGroupChatButton
    Wait until element is enabled    ${ADDMEMBERTOCLOSEDCHATBTN}    timeout=${timeout}
    Click element    ${ADDMEMBERTOCLOSEDCHATBTN}

changeMemberRoleToModeratorInCloseGroupChatFromAdmin
    [Arguments]    ${username}
    Wait until element is visible    //div[@class="username"][@title="${username}"]/following-sibling::div/button    timeout=${timeout}
    Click element    //div[@class="username"][@title="${username}"]/following-sibling::div/button
    Wait until element is visible    //div[@class="username"][@title="${username}"]/following-sibling::div//a[@data-type="moderator"]    timeout=${timeout}
    Click element    //div[@class="username"][@title="${username}"]/following-sibling::div//a[@data-type="moderator"]
    Wait until element is visible    ${ADDGROUPMODERATORBTN}    timeout=${timeout}
    Click element    ${ADDGROUPMODERATORBTN}

#--------------------------------------- VERIFY ---------------------------------------

isUserAddedToClosedGroupChat
    [Arguments]    ${username}
    ${check}    Run Keyword And Return Status    Wait until page contains element    //form[@class="workspace-users-list"]//div[@class="username"][@title="${username}"]    timeout=${timeout}    error=Cannot find the expected user
    Run Keyword Unless    ${check}    Reload page
    Run Keyword Unless    ${check}    Wait until page contains element    //form[@class="workspace-users-list"]//div[@class="username"][@title="${username}"]    timeout=${timeout}    error=Cannot find the expected user

isAppDisplayCorrectMemberRole
    [Arguments]    ${username}    ${role}
    Wait until element is visible    //div[@class="username"][@title="${username}"]/following-sibling::div/button//span[contains(text(), "${role}")]    timeout=${timeout}