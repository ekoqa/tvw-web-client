*** Settings ***

Library    SeleniumLibrary

Resource                  ../../../config/environment.robot

*** Variable ***

${CLOSEDCHATTABMENU}    //div[contains(@class, "btn-group")]/a[contains(@href, "type=closed")]
${CLOSEDCHATTABMENUACTIVE}    //div[contains(@class, "btn-group")]/a[contains(@href, "type=closed")][contains(@class, "active")]
${REMOVESELECTEDCHATBTN}    //button[contains(@class, "remove-select")]
${ADDNEWCLOSEDCHATBTN}    //div[@class="options-group"]//a[@href="#add"]
${CLOSEDCHATNAMETEXTFIELD}    name
${CREATEDCLOSEDCHATSAVEBTN}    //div[@class="modal-footer"]/button[@type="submit"]

*** Keywords ***

selectClosedChatTab
    Wait until element is enabled    ${CLOSEDCHATTABMENU}    timeout=${timeout}
    Click element    ${CLOSEDCHATTABMENU}
    Wait until page contains element    ${CLOSEDCHATTABMENUACTIVE}    timeout=${timeout}    error=Cannot select closed chat group type

clickAddNewClosedChatButton
    Wait until element is enabled    ${ADDNEWCLOSEDCHATBTN}    timeout=${timeout}
    Click element    ${ADDNEWCLOSEDCHATBTN}

setClosedChatName
    [Arguments]    ${name}
    Wait until element is enabled    ${CLOSEDCHATNAMETEXTFIELD}    timeout=${timeout}
    Input text    ${CLOSEDCHATNAMETEXTFIELD}    ${name}

clearClosedChatName
    Wait until element is enabled    ${CLOSEDCHATNAMETEXTFIELD}    timeout=${timeout}
    Input text    ${CLOSEDCHATNAMETEXTFIELD}    ${empty}

clickSaveClosedChatCreated
    Wait until element is enabled    ${CREATEDCLOSEDCHATSAVEBTN}    timeout=${timeout}
    Click element    ${CREATEDCLOSEDCHATSAVEBTN}

selectChatGroupNameInAdminPanel
    [Arguments]    ${name}
    Wait until element is enabled    //table//a[@title="${name}"]    timeout=${timeout}
    Click element    //table//a[@title="${name}"]

selectGroupChatToDelete
    [Arguments]    ${name}
    Wait until element is enabled    //table//a[@title="${name}"]/parent::*/preceding-sibling::td/input    timeout=${timeout}
    Click element    //table//a[@title="${name}"]/parent::*/preceding-sibling::td/input

clickRemoveSelectedChatButton
    Wait until element is enabled    ${REMOVESELECTEDCHATBTN}    timeout=${timeout}
    Click element    ${REMOVESELECTEDCHATBTN}
    Alert should be present    Are you sure you want to delete groups?


#------------- verify ------------------

isClosedChatRemoved
    [Arguments]    ${name}
    Wait until element is not visible    //a[@title="${name}"]    timeout=${timeout}