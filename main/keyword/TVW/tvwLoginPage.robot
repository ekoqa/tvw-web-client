*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot
Resource                   ../../keyword/TVW/tvwRegisterPage.robot

*** Variable ***

${SIGNINBTN}    //button[contains(@class, "Button__PrimaryButton")]
${REGISTERBTN}    //a[contains(@href, "/sso/register")]
${FORGOTBTN}    //a[contains(@href, "/sso/forgot-password")]
${TVWIMAGE}    //img[contains(@src, "/asset/images/logo.png")]
${VERIFYTVWTEXTMESSAGE}    //*[contains(text(),'Don’t have True Virtual World account yet?')]
${CHOOSEUSER}    //div[contains(@class, "SigninChooser__ProfileName")]
${CHOOSEREGISTERUSER}    //a[contains(@class, "SigninChooser__CurrentSessionContainer")]




*** Keywords ***

clickRegisterButton
    Wait until element is enabled    ${REGISTERBTN}    timeout=${timeout}
    Click element    ${REGISTERBTN}

clickSignInButton
    Wait until element is enabled    ${SIGNINBTN}    timeout=${timeout}
    Click element    ${SIGNINBTN}


clickChooseUser
    Wait until element is enabled    ${CHOOSEUSER}    timeout=${timeout}
    Click element    ${CHOOSEUSER}

clickChooseUserFotRedirectToRecentPage
    Wait until element is enabled    ${CHOOSEREGISTERUSER}    timeout=${timeout}
    Click element    ${CHOOSEREGISTERUSER}

clickForgotPasswordButton
    Wait until element is enabled    ${FORGOTBTN}    timeout=${timeout}
    Click element    ${FORGOTBTN}
    
### Verify ###

isTVWLoginPageDisplay
    Wait until element is enabled    ${EMAILINPUT}    timeout=${timeout}
    Wait until element is enabled    ${PASSWORDINPUT}    timeout=${timeout}
    Wait until element is enabled    ${SIGNINBTN}    timeout=${timeout}
    Wait until element is enabled    ${FORGOTBTN}    timeout=${timeout}
    Wait until element is enabled    ${REGISTERBTN}    timeout=${timeout}
    Wait until element is visible    ${TVWIMAGE}    timeout=${timeout}
    Wait until element is visible    ${VERIFYTVWTEXTMESSAGE}    timeout=${timeout}

isForgotPasswordButtonDisplay
    Wait until element is enabled    ${FORGOTBTN}
    
