*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***
${COPYBTN}    copy_address
${EMAILTEXT}    mail_address
${MAILINCOME}    //span[contains(@class, "small_message_icon")]
${CONFIRMEMAIL}    //a[contains(text(), "Confirm this email address")]
${EMAILTOPIC}    //span[contains(text(), "Welcome to True virtual world")]

*** Keywords ***

clickCopyEmail
    Wait until element is enabled    ${COPYBTN}    timeout=${timeout}
    Click element    ${COPYBTN}

clickEmail
    Wait until element is visible    ${MAILINCOME}    timeout=${timeout}
    Click element    ${MAILINCOME}

clickEmailByName
    [Arguments]    ${text}
    Wait until element is enabled    //span[contains(text(), "${text}")]    timeout=${timeout}
    Click element    //span[contains(text(), "${text}")]

clickButtonByName
    [Arguments]    ${text}
    Wait until element is enabled    //a[text()="${text}"]     timeout=${timeout}
    Click element    //a[text()="${text}"]

copyEmailText
    ${emailtext}    Get WebElements    ${EMAILTEXT}

### Verify ###

isEmailIncome
    Wait until element is visible    ${MAILINCOME}    timeout=${timeout}