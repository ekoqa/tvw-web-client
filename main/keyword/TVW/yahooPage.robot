*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot
Resource                   ../../keyword/TVW/tvwRegisterPage.robot
Resource                   ../../keyword/TVW/tvwLoginPage.robot

*** Variable ***

${USEREMAILINPUT}    login-username
${PASSWORDINPUT}    login-passwd
# ${MAILBOXMENUBTN}    //span[text()="Mail"]
${MAILBOXMENUBTN}    ybarMailLink
${YAHOOMAILBOXBTN}    //span[contains(@class, "rtlI_dz_sSg")]
${NOTRECEIVEEMAILBTN}    //span[text()="Didn't receive an email?"]
${NEXTTOINPUTPASSWORDBTN}    //input[contains(@class, "pure-button")]
${LOGINTOYAHOOBTN}    login-signin
${FORGOTPASSWORDBTN}    //a[text()="Reset password"]
${DELETEEMAILBTN}    //span[contains(@class, "D_X")]

*** Keywords ***

inputEmail
    [Arguments]    ${text}
    Wait until element is enabled    ${USEREMAILINPUT}    timeout=${timeout}
    Press Keys    ${USEREMAILINPUT}    ${text}

clickNextButtonToInputPassword
    Wait until element is enabled    ${NEXTTOINPUTPASSWORDBTN}     timeout=${timeout}
    Click element    ${NEXTTOINPUTPASSWORDBTN}

inputYahooPassWord
    [Arguments]    ${text}
    Wait until element is enabled    ${PASSWORDINPUT}    timeout=${timeout}
    Press Keys    ${PASSWORDINPUT}    ${text}

clickLoginButton
    Wait until element is enabled    ${LOGINTOYAHOOBTN}     timeout=${timeout}
    Click element    ${LOGINTOYAHOOBTN}


clickEmailMenu
    Wait until element is enabled    ${MAILBOXMENUBTN}      timeout=${timeout}
    Click element    ${MAILBOXMENUBTN} 
    
clickIncomeEmailByName
    [Arguments]    ${text}
    Wait until element is enabled    //span[contains(text(),"${text}")]    timeout=${timeout}
    Click element    //span[contains(text(),"${text}")]

clickForgotEmailByindex
    [Arguments]    ${index}    
    Wait until element is visible    //*[contains(@class, "H_A")][${index}]//descendant::span[contains(text(),"Password Reset Request")]
    Click element    //*[contains(@class, "H_A")][${index}]//descendant::span[contains(text(),"Password Reset Request")]

clickMailBox
    Wait until element is enabled    ${YAHOOMAILBOXBTN}     timeout=${timeout}
    Click element    ${YAHOOMAILBOXBTN}

tickChooseEmailByName
    [Arguments]    ${text}
    Wait until element is enabled    //span[contains(text(),"${text}")]//ancestor::div[contains(@class, "message-list-item")]/div/div/button    timeout=${timeout}
    Click element    //span[contains(text(),"${text}")]//ancestor::div[contains(@class, "message-list-item")]/div/div/button

clickDeleteEmail
    Wait until element is enabled    ${DELETEEMAILBTN}    timeout=${timeout}
    Click element    ${DELETEEMAILBTN}

clickForgotPasswordEmail
    Wait until element is enabled    ${FORGOTPASSWORDBTN}    timeout=${timeout}
    Click element    ${FORGOTPASSWORDBTN}

inputNewPassword
    [Arguments]    ${text}
    Wait until element is enabled    ${NEWPASSWORDINPUT}     timeout=${timeout}
    Press Keys    ${NEWPASSWORDINPUT}    ${text}

tickAndDeleteForgotEmail
    Wait until element is enabled    //span[contains(text(),"Password Reset Request")]//ancestor::div[contains(@class, "message-list-item")]/div/div/button
    Click element    //span[contains(text(),"Password Reset Request")]//ancestor::div[contains(@class, "message-list-item")]/div/div/button
    Wait until element is enabled    ${DELETEEMAILBTN}    timeout=${timeout}
    Click element    ${DELETEEMAILBTN}

checkEmailisEmpty
    ${index}    get element count    //span[contains(text(),"Password Reset Request")]//ancestor::div[contains(@class, "message-list-item")]/div/div/button
    :FOR  ${index}  IN RANGE  ${index}
    \  Run Keyword If    ${index}>0    tickAndDeleteForgotEmail
    \  ${element_is_here} =  Run Keyword and return status  Page Should Not Contain Element  //span[contains(text(),"Password Reset Request")]//ancestor::div[contains(@class, "message-list-item")]/div/div/button
    \  Exit For Loop If  ${element_is_here}

# checkIsloginAlreadyHahoomailOrNot
#     # [Arguments]    ${username}
#     ${userlogin}    Run Keyword And Return Status    Wait until element contains    ${YAHOOMAILBOXBTN}    timeout=${timeout}
#     # ${userlogin} =  Run Keyword and return status  Wait until element contains  ${YAHOOMAILBOXBTN}    timeout=${timeout}
#     ${needLogin}    Run Keyword And Return Status    Element should be visible    ${USEREMAILINPUT}
#     Run Keyword If    ${userLogin}    Sleep    0.5s
#     ...    ELSE    ${needLogin}    loginYahoo
#     # ...    ELSE    reloginWithUser    ${username}  

checkIsloginAlreadyHahoomailOrNot
    [Arguments]    ${text1}    ${text2}
    ${loginalready}=    Run keyword and ignore error    Element should be visible    ${MAILBOXMENUBTN}
    Run keyword if    '${loginalready[0]}' == 'PASS'
    ...    click element    ${MAILBOXMENUBTN}
    ...    ELSE    
    ...    loginYahoo    ${text1}    ${text2}


    
# loginYahoo
#     Wait until element is enabled    ${USEREMAILINPUT}    timeout=${timeout}
#     Press Keys    ${USEREMAILINPUT}    test99email
#     Wait until element is enabled    ${NEXTTOINPUTPASSWORDBTN}     timeout=${timeout}
#     Click element    ${NEXTTOINPUTPASSWORDBTN}
#     Wait until element is enabled    ${PASSWORDINPUT}    timeout=${timeout}
#     Press Keys    ${PASSWORDINPUT}    123456789gG$

loginYahoo
    [Arguments]    ${text1}    ${text2}
    set selenium speed    ${speed}
    Wait until element is enabled    ${USEREMAILINPUT}    timeout=${timeout}
    Input Text          ${USEREMAILINPUT}        ${text1}
    Wait until element is enabled    ${NEXTTOINPUTPASSWORDBTN}     timeout=${timeout}
    Click element    ${NEXTTOINPUTPASSWORDBTN}
    Wait until element is enabled    ${PASSWORDINPUT}    timeout=${timeout}
    Input Text      ${PASSWORDINPUT}        ${text2}
    Wait until element is enabled    ${LOGINTOYAHOOBTN}     timeout=${timeout}
    Click element    ${LOGINTOYAHOOBTN}
    Wait until element is enabled    ${MAILBOXMENUBTN}      timeout=${timeout}
    Click element    ${MAILBOXMENUBTN}
    sleep    5s
  

### Verify ###
isForgotPasswordButtonDisplayInMail
    Wait until element is enabled    //span[contains(text(),"Password Reset Request")]    timeout=${timeout}

isMultipleEmail
    [Arguments]    ${index}
    :FOR  ${index}  IN RANGE  2
    \    Wait until element is visible    //span[contains(text(),"Password Reset Request")]//ancestor::*[contains(@class, "H_A")]    timeout=${timeout}

