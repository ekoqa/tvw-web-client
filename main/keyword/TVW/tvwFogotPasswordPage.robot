*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot
Resource                   ../../keyword/TVW/tvwRegisterPage.robot
Resource                   ../../keyword/TVW/tvwLoginPage.robot

*** Variable ***

${SENDEMAILBTN}    //button[contains(@class, "Button__PrimaryButton")]
${NOTRECEIVEEMAILBTN}    //span[text()="Didn't receive an email?"]
${๊์UNABLEVERIFYTOKENTEXT}    //p[contains(@class, "styles__Paragraph")][text()="Unable to verify token"]
${NEWPASSWORDINPUT}    newPassword
${CONFIRMSETPASSWORDINPUT}    confirmNewPassword
${SETNEWPASSWORDBTN}    //button[contains(@class, "Button__PrimaryButton")]
${๊์CONFIRMNEWPASSWORDNOTMATCHTEXT}    //*[contains(text(), "The two passwords that you entered do not match!")] 

*** Keywords ***

clickSendEmaiilButton
    Wait until element is visible    ${SENDEMAILBTN}    timeout=${timeout}
    Click element    ${SENDEMAILBTN}

clickNotReceiveEmaiilButton
    Wait until element is visible    ${NOTRECEIVEEMAILBTN}    timeout=${timeout}
    Click element    ${NOTRECEIVEEMAILBTN}

inputNewPasswordInSetNewPasswordPage
    [Arguments]    ${text}
    Wait until element is enabled    ${NEWPASSWORDINPUT}     timeout=${timeout}
    Press Keys    ${NEWPASSWORDINPUT}    ${text}

inputConfirmNewPasswordInSetNewPasswordPage
    [Arguments]    ${text}
    Wait until element is enabled    ${CONFIRMSETPASSWORDINPUT}    timeout=${timeout}
    Press Keys    ${CONFIRMSETPASSWORDINPUT}   ${text}

clickSetNewPassword
    Wait until element is visible    ${SETNEWPASSWORDBTN}    timeout=${timeout}
    Click element    ${SETNEWPASSWORDBTN}

### Verify ###

isInputEmailFieldDisplay
    Wait until element is visible    ${EMAILINPUT}    timeout=${timeout}

isNotInputEmail
    Wait until element is visible    //div[contains(@class, "ant-form-item-explain ant-form-item-explain-error")]/div[text()="Please input your Email"]    timeout=${timeout} 
    
isNotTickAgree
    Wait until element is visible    //div[contains(@class, "ant-form-item-explain ant-form-item-explain-error")]/div[text()="Please complete reCAPTCHA"]    timeout=${timeout}

isInputIncorrectEmail
    Wait until element is visible    //p[contains(@class, "FormErrorMessage__StyledMessage")][text()="No user found with this email"]    timeout=${timeout}

isForgotPasswordLinkSentToEmail
    Wait until element is visible    //*[contains(@class, "styles__Paragraph")][text()="The email contains a link to reset your password. The link will expire in 24 hours."]    timeout=${timeout}
    Wait until element is visible    ${NOTRECEIVEEMAILBTN}    timeout=${timeout}

isCanNotForgotPasswordTextDisplay
    Wait until element is visible    ${๊์UNABLEVERIFYTOKENTEXT}    timeout=${timeout}

isSetNewPasswordPageDisplay
    Wait until element is visible    ${NEWPASSWORDINPUT}    timeout=${timeout}
    Wait until element is visible    ${CONFIRMSETPASSWORDINPUT}    timeout=${timeout}
    Wait until element is visible    ${SETNEWPASSWORDBTN}    timeout=${timeout}

isComfirmNewPasswordNotMatchWithnewPassword
    Wait until element is visible    ${๊์CONFIRMNEWPASSWORDNOTMATCHTEXT}    timeout=${timeout}