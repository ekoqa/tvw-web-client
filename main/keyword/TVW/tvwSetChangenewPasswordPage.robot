*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot
Resource                   ../../keyword/TVW/tvwRegisterPage.robot
Resource                   ../../keyword/TVW/tvwLoginPage.robot

*** Variable ***
${TVWCURRENTPASSWORDINPUT}    currentPassword
${TVWNEWPASSWORDINPUT}    newPassword
${TVWCONFIRMNEWPASSWORDINPUT}    confirmPassword
${SUBMITCHANGEPASSWORDBTN}    //button[contains(@class, "ant-btn-primary")]
 
*** Keywords ***

inputCurrentPasswordInChangePasswordPage
    [Arguments]    ${text}
    Wait until element is enabled    ${TVWCURRENTPASSWORDINPUT}     timeout=${timeout}
    Press Keys    ${TVWCURRENTPASSWORDINPUT}    ${text}

inputNewPasswordInChangePasswordPage
    [Arguments]    ${text}
    Wait until element is enabled    ${TVWNEWPASSWORDINPUT}     timeout=${timeout}
    Press Keys    ${TVWNEWPASSWORDINPUT}    ${text}

inputConfirmNewPasswordInChangePasswordPage
    [Arguments]    ${text}
    Wait until element is enabled    ${TVWCONFIRMNEWPASSWORDINPUT}     timeout=${timeout}
    Press Keys    ${TVWCONFIRMNEWPASSWORDINPUT}    ${text}

clickSubmitChangePasswordButton
    Wait until element is enabled    ${SUBMITCHANGEPASSWORDBTN}    timeout=${timeout}
    Click element    ${SUBMITCHANGEPASSWORDBTN}

### Verify ###
