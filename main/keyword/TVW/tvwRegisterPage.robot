*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${EMAILINPUT}    email
${FIRSTNAMEINPUT}    firstname
${LASTNAMEINPUT}    lastname
${MOBILEEINPUT}    phoneNumber
${PASSWORDINPUT}    password
${CONFIRMPASSWORDINPUT}    confirmPassword
${BIRTHDATEFIELD}    birthDate
${MOBILEEINPUT}    phoneNumber
${ORGANIZATIONINPUT}    organizationName
${SCHOOLNUMBERINPUT}    schoolId
${REGISTERNUMBERINPUT}    taxId
${ORGANIZATIONURLINPUT}    websiteUrl
${URLINPUT}    //input[@placeholder="Institution website"]
${DISPLAYNAMEINPUT}    displayName
${WORKSPACENAMEINPUT}    workspaceName
${REQUESTFIRSTNAMEINPUT}    requestorFirstName
${REQUESTLASTNAMEINPUT}    requestorLastName
${CONTACTMOBILEEINPUT}    contactMobileNumber
${AGREERADIO}    //span[contains(@class, "ant-checkbox")]
${SIGNUPBTN}    //button[contains(@class, "ant-btn ant-btn-primary")]
${BACKLOGINBTN}    //a[contains(@href, "/sso/login")]
${TERMCONDITIONBTN}    //a[contains(@href, "https://truevirtualworld.com/about/")]
${VLEARNBTN}    //a[contains(text(), "Continue on VLearn")] 
${VWORKBTN}    //a[contains(text(), "Continue on VWork")] 
${OFFICIALACCOUNTBTN}    //a[contains(@href, "/create/official")]
${GENERALACCOUNTBTN}    //a[contains(@href, "/create/general")]
${JOINEXISTINGBTN}    //a[contains(@href, "/join/workspace-id")]
${DOCUMENTATTACHFILEBTN}    //button[contains(@class, "ant-btn styles__StyledButton")]
${IDCARDATTACHFILEBTN}    //button[contains(@class, "ant-btn styles__StyledButton")]    
${NEXTBTN}    //button[contains(@class, "ant-btn")]
${ATTACHDOCUMENTBTN}    //button[contains(@class, "styles__StyledButton")]//ancestor::div[contains(@class, "styles__UploadContainer")]//descendant::p[contains(@class, "styles__ItemText")][text()="Copy of Company certificate (Must be signed and/or stamped)"]
${EDITBTN}    //div[contains(@class, "styles__WrapActions")]/button[1]
${CONFIRMBTN}    //div[contains(@class, "styles__WrapActions")]/button[2]
${LETGOBTN}    //button[contains(@class, "Button__Button")]
${JOINEXITINGBTN}    //a[contains(@href, "/join/workspace-id")]
${WORKSPACEINPUT}    workspaceId
${CONFIRMWORKSPACEBTN}    //button[contains(@class, "ant-btn-primary")]

*** Keywords ***

pressKeyForPasteCopyText
    [Arguments]    ${text}
    Wait until element is enabled    email    timeout=${timeout}
    Press Keys    email    ${text}

inputTVWEmail
    [Arguments]    ${text}
    Wait until element is enabled    ${EMAILINPUT}    timeout=${timeout}
    Press Keys    ${EMAILINPUT}    ${text}

clickEmailField
    Wait until element is enabled    ${EMAILINPUT}    timeout=${timeout}
    Click element    ${EMAILINPUT}

inputFirstName
    [Arguments]    ${text}
    Wait until element is enabled    ${FIRSTNAMEINPUT}    timeout=${timeout}
    Press Keys    ${FIRSTNAMEINPUT}    ${text}

inputLastName
    [Arguments]    ${text}
    Wait until element is enabled    ${LASTNAMEINPUT}    timeout=${timeout}
    Press Keys    ${LASTNAMEINPUT}    ${text}
    
inputMobileNumber
    [Arguments]    ${text}
    Wait until element is enabled    ${MOBILEEINPUT}    timeout=${timeout}
    Press Keys    ${MOBILEEINPUT}    ${text}
    
inputPassword
    [Arguments]    ${text}
    Wait until element is enabled    ${PASSWORDINPUT}    timeout=${timeout}
    Press Keys    ${PASSWORDINPUT}    ${text}

inputConfirmPassword
    [Arguments]    ${text}
    Wait until element is enabled    ${CONFIRMPASSWORDINPUT}    timeout=${timeout}
    Press Keys    ${CONFIRMPASSWORDINPUT}    ${text}

inputBirthDate
    [Arguments]    ${text}
    Wait until element is visible    ${BIRTHDATEFIELD}    timeout=${timeout}
    Press Keys    ${BIRTHDATEFIELD}    ${text}

inputWorkspaceID
    [Arguments]    ${text}
    Wait until element is enabled    ${WORKSPACEINPUT}    timeout=${timeout}
    Press Keys    ${WORKSPACEINPUT}    ${text}

tickAgreeRadioField
    Wait until element is enabled    ${AGREERADIO}    timeout=${timeout}
    Click element    ${AGREERADIO}

selectCapchaFrame
    Wait until element is enabled    //div[contains(@class, "ant-form-item-control-input-content")]/div/div/div/div/iframe    timeout=${timeout}
    Select frame    //div[contains(@class, "ant-form-item-control-input-content")]/div/div/div/div/iframe

tickCapchaRadioField
    Wait until element is enabled    //div[@id="rc-anchor-container"]    timeout=${timeout}
    Click element    //div[@id="rc-anchor-container"]

clickSignupButton
    Wait until element is enabled    ${SIGNUPBTN}    timeout=${timeout}
    Click element    ${SIGNUPBTN}

clickVLearnButton
    Wait until element is enabled    ${VLEARNBTN}     timeout=${timeout}
    Click element    ${VLEARNBTN}

clickVWorkButton
    Wait until element is enabled    ${VWORKBTN}     timeout=${timeout}
    Click element    ${VWORKBTN}

clickBacktoLoginButton
    Wait until element is enabled    ${BACKLOGINBTN}    timeout=${timeout}
    Click element    ${BACKLOGINBTN}

clickTermAndConditionLink
    Wait until element is enabled    ${TERMCONDITIONBTN}    timeout=${timeout}
    Click element    ${TERMCONDITIONBTN}

clickOfficialAccount
    Wait until element is enabled    ${OFFICIALACCOUNTBTN}    timeout=${timeout}
    Click element    ${OFFICIALACCOUNTBTN}

clickCreateGeneralAccount
    Wait until element is enabled    ${GENERALACCOUNTBTN}    timeout=${timeout}
    Click element    ${GENERALACCOUNTBTN}

clickJoinExistingWorkspace
    Wait until element is enabled    ${JOINEXISTINGBTN}    timeout=${timeout}
    Click element    ${JOINEXISTINGBTN}

inputOrganization
    [Arguments]    ${text}
    Wait until element is enabled    ${ORGANIZATIONINPUT}    timeout=${timeout}
    Press Keys    ${ORGANIZATIONINPUT}    ${text}

inputInstitutionNumber
    [Arguments]    ${text}
    Wait until element is enabled    ${SCHOOLNUMBERINPUT}    timeout=${timeout}
    Press Keys    ${SCHOOLNUMBERINPUT}    ${text}

inputRegisterNumber
    [Arguments]    ${text}
    Wait until element is enabled    ${REGISTERNUMBERINPUT}    timeout=${timeout}
    Press Keys    ${REGISTERNUMBERINPUT}    ${text}

inputURL
    [Arguments]    ${text}
    Wait until element is enabled    ${URLINPUT}    timeout=${timeout}
    Press Keys    ${URLINPUT}    ${text}

inputOrganizationURL
    [Arguments]    ${text}
    Wait until element is enabled    ${ORGANIZATIONURLINPUT}    timeout=${timeout}
    Press Keys    ${ORGANIZATIONURLINPUT}    ${text}


inputDisplayName
    [Arguments]    ${text}
    Wait until element is enabled    ${DISPLAYNAMEINPUT}    timeout=${timeout}
    Press Keys    ${DISPLAYNAMEINPUT}    ${text}

clickStudent
    Wait until element is enabled    //div[contains(@class, "ant-select-selection__placeholder")][text()="No. of student(s)"]    timeout=${timeout}
    Click element    //div[contains(@class, "ant-select-selection__placeholder")][text()="No. of student(s)"]

clickDropDownTypeByName
    [Arguments]    ${text}
    Wait until element is enabled    //div[contains(@class, "ant-select-selection__placeholder")][contains(text(), "${text}")]    timeout=${timeout}
    Click element    //div[contains(@class, "ant-select-selection__placeholder")][contains(text(), "${text}")]

chooseDropDownByName
    [Arguments]    ${text}
    Wait until element is enabled    //span[contains(@class, "FormSelect__OptionLabel")][text()="${text}"]    timeout=${timeout}
    Click element    //span[contains(@class, "FormSelect__OptionLabel")][text()="${text}"]

chooseDropDownByindex
    Wait until element is enabled    //*[contains(@class, "ant-select-dropdown-menu-root")]/li[2]    timeout=${timeout}
    Click element    //*[contains(@class, "ant-select-dropdown-menu-root")]/li[2]

addDocumentByName
    [Arguments]    ${text}
    Wait until element is enabled    //p[contains(@class, "styles__ItemText")][text()="${text}"]/parent::*/span/div[1]/span/input    timeout=${timeout}
    Choose file    //p[contains(@class, "styles__ItemText")][text()="${text}"]/parent::*/span/div[1]/span/input    ${filePath}/${imageInChatFile}

clickNextButton
    Wait until element is enabled    ${NEXTBTN}    timeout=${timeout}
    Click element    ${NEXTBTN}

clickConfirmCreateTVWButton
    Wait until element is enabled    ${CONFIRMBTN}    timeout=${timeout}
    Click element    ${CONFIRMBTN}

clickEditCreateTVWButton
    Wait until element is enabled    ${EDITBTN}    timeout=${timeout}
    Click element    ${EDITBTN}

clickLetGoButton
    Wait until element is visible    ${LETGOBTN}    timeout=${timeout}
    Click element    ${LETGOBTN}

clickJoinExitingButton
    Wait until element is enabled    ${JOINEXITINGBTN}    timeout=${timeout}
    Click element    ${JOINEXITINGBTN}

clickComfirmWorkSpaceButton
    Wait until element is enabled    ${CONFIRMWORKSPACEBTN}    timeout=${timeout}
    Click element    ${CONFIRMWORKSPACEBTN}

inputWorkspaceName
    [Arguments]    ${text}
    Wait until element is enabled    ${WORKSPACENAMEINPUT}    timeout=${timeout}
    Press Keys    ${WORKSPACENAMEINPUT}    ${text}
    
setCTRLOrCommandKey
    ${CTRL_OR_COMMAND} =    Ctrl Or Command Key
    Set Suite Variable      ${CTRL_OR_COMMAND}  

inputRequestFirstName
    [Arguments]    ${text}
    Wait until element is enabled    ${REQUESTFIRSTNAMEINPUT}    timeout=${timeout}
    Press Keys    ${REQUESTFIRSTNAMEINPUT}    ${text}  

inputRequestLasttName
    [Arguments]    ${text}
    Wait until element is enabled    ${REQUESTLASTNAMEINPUT}    timeout=${timeout}
    Press Keys    ${REQUESTLASTNAMEINPUT}    ${text}

inputContactMobile
    [Arguments]    ${text}
    Wait until element is enabled    ${CONTACTMOBILEEINPUT}    timeout=${timeout}
    Press Keys    ${CONTACTMOBILEEINPUT}    ${text}



### Verify ###

isTVWRegisterPageDisplay
    Wait until element is enabled    ${EMAILINPUT}    timeout=${timeout}
    Wait until element is enabled    ${FIRSTNAMEINPUT}    timeout=${timeout}
    Wait until element is enabled    ${LASTNAMEINPUT}    timeout=${timeout}
    Wait until element is enabled    ${MOBILEEINPUT}    timeout=${timeout}
    Wait until element is enabled    ${PASSWORDINPUT}    timeout=${timeout}
    Wait until element is enabled    ${CONFIRMPASSWORDINPUT}    timeout=${timeout}
    Wait until element is visible    ${BIRTHDATEFIELD}    timeout=${timeout}
    Wait until element is enabled    ${AGREERADIO}    timeout=${timeout}

isVLearnButtonDisplay
    Wait until element is enabled    ${VLEARNBTN}     timeout=${timeout}

isVWorkButtonDisplay
    Wait until element is enabled    ${VWORKBTN}     timeout=${timeout}
