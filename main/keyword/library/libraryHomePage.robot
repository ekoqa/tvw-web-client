*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${FIRSTFOLDERTITLE}    //div[contains(@class, "ant-list styles__List")]/div/div/div/div[1]//div[contains(@class, "CardTitle")]
${SEARCHLIBRALYBAR}    //*[contains(@class, "styles__SearchContainer")]/span/input
${TAGS}    //*[contains(@class, 'styles__Tag')]
${SEARCHRESULTITEM}    //*[contains(@class, 'styles__ListItem')]
${TAGSTYPE}    //*[contains(@class, 'styles__RadioButton')]//*[contains(string(), 'Tags')]
${ALLTYPE}    //*[contains(@class, 'styles__RadioButton')]//*[contains(string(), 'ALL')]
${FOLDERTYPE}    //*[contains(@class, 'styles__RadioButton')]//*[contains(string(), 'Folders')]
${DOCUMENTTYPE}    //*[contains(@class, 'styles__RadioButton')]//*[contains(string(), 'Documents')]
${ATTACHMENTTYPE}    //*[contains(@class, 'styles__RadioButton')]//*[contains(string(), 'Attachments')]
${SEARCHRESULTITEMINLIBRARY}    //ul[@class="ant-list-items"]/li
${ACTIVETAGTAB}    //*[contains(@class, "styles__RadioButton")]//span[contains(@class, "checked")]/following-sibling::span[contains(string(), "Tags")]
${PLAYVIDEOBTN}    //*[contains(@class, 'play')]
${DOWNLOADBTN}    //*[contains(@class, 'arrow')]
${VERFYPAUSEBTN}    //*[contains(@class, 'pause')]
${VERFYNODATAMESSAGE}    //*[contains(string(), 'No Data')]
${FILEPREVIEWIFRAME}    //iframe[@data-qa-anchor="wrap-iframe-library"]
${PDFPREVIEWCONTAINER}    //div[contains(@class, "PdfContainer")]

*** Keywords ***

selectFolderByIndex
    [Arguments]    ${index}
    Wait until element is enabled    //div[contains(@class, "ant-list styles__List")]/div/div/div/div[${index}]    timeout=${timeout}
    Click element    //div[contains(@class, "ant-list styles__List")]/div/div/div/div[${index}]

searchTextInLibraryBarAndPressEnterButton
    [Arguments]    ${text}
    Wait until element is visible    ${SEARCHLIBRALYBAR}    timeout=${timeout}
    Input text    ${SEARCHLIBRALYBAR}    ${text}
    Press keys    ${SEARCHLIBRALYBAR}    RETURN

searchText
    [Arguments]    ${text}
    Wait until element is visible    ${SEARCHLIBRALYBAR}    timeout=${timeout}
    Input text    ${SEARCHLIBRALYBAR}    ${text}

clickTextSearchInput
    Wait until element is visible    ${SEARCHLIBRALYBAR}    timeout=${timeout}
    Click element    ${SEARCHLIBRALYBAR}

clickTagReturnToSearchPage
    Wait until element is enabled    ${TAGS}    timeout=${timeout}
    Click element    ${TAGS}

chooseResultTagType
    Wait until element is visible    ${TAGSTYPE}    timeout=${timeout}
    Click element    ${TAGSTYPE}

chooseResultFolderType
    Wait until element is visible    ${FOLDERTYPE}    timeout=${timeout}
    Click element    ${FOLDERTYPE}

chooseResultDocumentType
    Wait until element is visible    ${DOCUMENTTYPE}    timeout=${timeout}
    Click element    ${DOCUMENTTYPE}

chooseResultAttachmentType
    Wait until element is visible    ${ATTACHMENTTYPE}    timeout=${timeout}
    Click element    ${ATTACHMENTTYPE}

selectItemResultByIndex
    [Arguments]    ${index}
    Wait until element is enabled    //*[contains(@class, 'styles__ListItem')][${index}]    timeout=${timeout}
    Click element    //*[contains(@class, 'styles__ListItem')][${index}]

chooseResultAllType
    Wait until element is visible    ${TAGSALL}    timeout=${timeout}
    Click element    ${TAGSALL}

backToPreviousPageInLibrary
    Go back
    Select Frame    ${LIBRARYIFRAME}

chooseFolderByname
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "styles__CardTitle")][contains(text(), "${text}")]    timeout=${timeout}
    Click element    //div[contains(@class, "styles__CardTitle")][contains(text(), "${text}")]

chooseDocumentByname
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "styles__CardTitle")][contains(text(), "${text}")]    timeout=${timeout}
    Click element    //div[contains(@class, "styles__CardTitle")][contains(text(), "${text}")]

playVideoInDocument
    Wait until element is visible    ${PLAYVIDEOBTN}    timeout=${timeout}
    Click element    ${PLAYVIDEOBTN}

clickFileByname
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "styles__AttachmentTitle")][contains(text(), "${text}")]    timeout=${timeout}
    Click element    //div[contains(@class, "styles__AttachmentTitle")][contains(text(), "${text}")]

downloadFileInDocument
    Wait until element is visible    ${DOWNLOADBTN}    timeout=${timeout}
    Click element    ${DOWNLOADBTN}

clickSearchResult
    [Arguments]    ${text}
    Wait until element is visible    //span[contains(@class, "ElasticSearchHighlighterstyles")][text()="${text}"]    timeout=${timeout}
    Click element    //span[contains(@class, "ElasticSearchHighlighterstyles")][text()="${text}"]

clickSearchHistoryItem
    [Arguments]    ${text}
    Wait until element is visible    //span[contains(string(), '${text}')]    timeout=${timeout}
    Click element    //span[contains(string(), '${text}')]

#---------------------- verify ------------------------------------------

clickLibrarySearchResultByIndex
    [Arguments]    ${index}
    Wait until element is visible    //ul[@class="ant-list-items"]/li[${index}]    timeout=${timeout}
    Click element    //ul[@class="ant-list-items"]/li[${index}]

clickOnListItemPathByIndex
    [Arguments]    ${index}
    Wait until element is visible    //ul[@class="ant-list-items"]/li[${index}]//div[contains(@class, "ListItemPath")]    timeout=${timeout}
    Click element    //ul[@class="ant-list-items"]/li[${index}]//div[contains(@class, "ListItemPath")]


clearSearchLibraryKeyword
    ${value}=     Get Element Attribute   ${SEARCHLIBRALYBAR}      value
    ${backspacesCount}=    Get Length      ${value}
    Run Keyword If    """${value}""" != ''    
    ...     Repeat Keyword  ${backspacesCount}  Press Keys  ${SEARCHLIBRALYBAR}   BACKSPACE

#---------------------- verify ------------------------------------------

isNotFoundItem
    Wait until element is visible    ${VERFYNODATAMESSAGE}    timeout=${timeout}

isResultDisplay
    [Arguments]    ${result}
    Wait until element is visible    //*[contains(@class, "styles__ListItemTitle")]//*[contains(string(), "${result}")]    timeout=${timeout}

isListResultDisplay
    [Arguments]    ${result}
    Wait until element is visible    //span[contains(@class, "ElasticSearchHighlighterstyles")][text()="${result}"]    timeout=${timeout}

isSearchBarListResultDisplay
    [Arguments]    ${result}
    Wait until element is visible    //*[contains(string(), '${result}')]    timeout=${timeout}

isSearchResultDisplayCorrectly
    [Arguments]    @{results}
    FOR  ${element}  IN  @{results} 
        isResultDisplay    ${element}
    END

isSearchListResultDisplayCorrectly
    [Arguments]    @{results}
    FOR  ${element}  IN  @{results}
        isListResultDisplay    ${element}
    END

isSearchListResultDisplayAfterClickSearchBar
    [Arguments]    @{results}
    FOR  ${element}  IN  @{results}
        isSearchBarListResultDisplay    ${element}
    END

isHighlightKeywordDisplayCorrectlyForSingleKeyword
    [Arguments]    ${keyword}
    ${count}    Get element count    ${SEARCHRESULTITEMINLIBRARY}
    FOR  ${index}    IN RANGE    ${count}
        Wait until element is visible    //ul[@class="ant-list-items"]/li[${index}+1]//span[contains(@class, "ElasticSearchHighlight")][contains(text(), "${keyword}")]    timeout=${timeout}
    END

isTagTabActive
    Wait until element is visible    ${ACTIVETAGTAB}    timeout=${timeout}

isItemResultDisplayOnlyOne
    [Arguments]    ${index}
    Wait until element is not visible    //*[contains(@class, 'styles__ListItem')][${index}]    timeout=${timeout}

isTitleDocumentDisplay
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "styles__DocumentTitle")][contains(text(), "${text}")]    timeout=${timeout}
 

isContentDocumentDisplay
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "styles__ContentContainer")]//*[contains(text(), "${text}")]    timeout=${timeout}
  
isPauseVideoButtonDisplay
    Wait until element is visible    ${VERFYPAUSEBTN}    timeout=${timeout}
isAppCanPreviewFileInLibrary
    Unselect Frame
    Select frame    ${FILEPREVIEWIFRAME}
    Wait until element is visible    ${PDFPREVIEWCONTAINER}    timeout=${timeout}
    Unselect Frame

isDownloadFileFinish
    # File Should Exist    ${filePath}\\${pdfFile}
    File Should Exist    ${filePath}/${pdfFile}

isAttachmentFileDisplay
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "styles__AttachmentTitle")][contains(text(), "${text}")]    timeout=${timeout}

isDownloadFileButtonDisplay
    Wait until element is visible    ${DOWNLOADBTN}    timeout=${timeout}

isFileNameDisplayInFilePreview
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "styles__ToolbarFileName")][contains(text(), "${text}")]    timeout=${timeout}

isResultSortByIndexPosition
    Wait until element is visible    //li[contains(@class, "ant-list-item")][1]//*[contains(string(), "Green1 Load Folder")]    timeout=${timeout}
    Wait until element is visible    //li[contains(@class, "ant-list-item")][2]//*[contains(string(), "Green2 Load Folder")]    timeout=${timeout}
    Wait until element is visible    //li[contains(@class, "ant-list-item")][3]//*[contains(string(), "Green3 Load Folder")]    timeout=${timeout}
    Wait until element is visible    //li[contains(@class, "ant-list-item")][4]//*[contains(string(), "Green1 Load Document")]    timeout=${timeout}
    Wait until element is visible    //li[contains(@class, "ant-list-item")][5]//*[contains(string(), "Green2 Load Document")]    timeout=${timeout}
    Wait until element is visible    //li[contains(@class, "ant-list-item")][6]//*[contains(string(), "Green3 Load Document")]    timeout=${timeout}
    Wait until element is visible    //li[contains(@class, "ant-list-item")][7]//*[contains(string(), "Tesla")]    timeout=${timeout}
    Wait until element is visible    //li[contains(@class, "ant-list-item")][8]//*[contains(string(), "Toyota")]    timeout=${timeout}
    Wait until element is visible    //li[contains(@class, "ant-list-item")][9]//*[contains(string(), "Green1 Load Attachment")]    timeout=${timeout}
    Wait until element is visible    //li[contains(@class, "ant-list-item")][10]//*[contains(string(), "Green2 Load Attachment")]    timeout=${timeout}

isTitleFolderDisplay
    [Arguments]    ${text}
    Wait until element is visible    //span[contains(@class, "styles__BreadcrumbItem")][contains(text(), "${text}")]    timeout=${timeout}

isNoPermissionToAccessDucument
    Wait until element is visible    //*[contains(text(), "Permission denied")]    timeout=${timeout}