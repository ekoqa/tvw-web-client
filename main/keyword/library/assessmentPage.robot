*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot


*** Variable ***

${STARTQUIZBTN}    //button[contains(@class, "Start")]
${QUIZCHOICE}    //input[@type="radio"]
${SUBMITQUIZBTN}    //button[contains(@class, "Submit")]
${QUIZRESULTTITLE}    //div[contains(@class, "ResultTitle")]


*** Keywords ***

selectQuizByIndex
    [Arguments]    ${index}
    Wait until element is enabled    //div[contains(@class, "ListWrapper")]/div[${index}]    timeout=${timeout}
    Click element    //div[contains(@class, "ListWrapper")]/div[${index}]

clickStartQuiz
    Wait until element is enabled    ${STARTQUIZBTN}    timeout=${timeout}
    Click element    ${STARTQUIZBTN}

selectTheQuizChoice
    Wait until element is enabled    ${QUIZCHOICE}    timeout=${timeout}
    Click element    ${QUIZCHOICE}

clickSubmitQuiz
    Wait until element is enabled    ${SUBMITQUIZBTN}    timeout=${timeout}
    Click element    ${SUBMITQUIZBTN}

#--------------------------------------verify------------------------------

isQuizResultDisplay
    Wait until element is visible    ${QUIZRESULTTITLE}    timeout=${timeout}

isQuizDisplayCorrectTitle
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "Title")][text()="${text}"]    timeout=${timeout}
    Wait until element is enabled    ${STARTQUIZBTN}    timeout=${timeout}

isQuizeDisplayTime
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "styles__DetailTime")][text()="${text}"]    timeout=${timeout}