*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${FOLDERTITLE}    //div/span[2]//span[contains(@class, "styles__Breadcrumb")]
${FIRSTSUBFOLDERTITLE}    //div[contains(@class, "Content")]/div[2]//div[contains(@class, "ant-list styles__List")]/div/div/div/div[1]//div[contains(@class, "CardTitle")]
${SUBFOLDERTITLE}    //div/span[3]//span[contains(@class, "styles__Breadcrumb")]

*** Keywords ***


selectSubfolderByIndex
    [Arguments]    ${index}
    Wait until element is enabled   //div[contains(@class, "Content")]/div[2]//div[contains(@class, "ant-list styles__List")]/div/div/div/div[${index}]    timeout=${timeout}
    Click element    //div[contains(@class, "Content")]/div[2]//div[contains(@class, "ant-list styles__List")]/div/div/div/div[${index}]

selectDocumentByIndex
    [Arguments]    ${index}
    Wait until element is enabled   //div[contains(@class, "Content")]/div[3]//div[contains(@class, "ant-list styles__List")]/div/div/div/div[${index}]    timeout=${timeout}
    Click element    //div[contains(@class, "Content")]/div[3]//div[contains(@class, "ant-list styles__List")]/div/div/div/div[${index}]
    



#--------------------------- verify --------------------------------

isFolderNameCorrect
    [Arguments]    ${text}
    Wait until element is visible    ${FOLDERTITLE}    timeout=${timeout}
    Element should contain    ${FOLDERTITLE}    ${text}

isSubFolderNameCorrect
    [Arguments]    ${text}
    Wait until element is visible    ${SUBFOLDERTITLE}    timeout=${timeout}
    Element should contain    ${SUBFOLDERTITLE}    ${text}