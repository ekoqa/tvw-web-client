*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${OPENQUIZBTN}    //div[contains(@class, "PageWrapper")]//button

*** Keywords ***

clickTaskAnAssessmentButton
   Wait until element is enabled    ${OPENQUIZBTN}    timeout=${timeout}
   Click element    ${OPENQUIZBTN}


#-------------------------- verify ---------------------------------------

isDocumentDisplayCorrectTitle
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "DocumentTitle")][text()="${text}"]    timeout=${timeout}