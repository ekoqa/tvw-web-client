*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${CREATEFORMBTN}    //div[contains(@class, "FlexHeaderRight")]/button
${FIRSTFORMLIST}    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ListItem")][1]
${MUTEFORMBTN}    //div[contains(@class, "FormHeader")]/div[2]/button[1]
${CONFIRMMUTEFORMBTN}    //span[text()="Mute"]/parent::*
${FORMMUTEICON}    //div[contains(@class, "FormHeader")]/div[2]/button[1]/span[@class="lnr lnr-mute"]
${FILTERFORMBTN}    //div[@data-qa-anchor="filter-status-secondary"]
${EXPORTFORMBTN}    //div[contains(@class, "FormHeader")]/div[2]/button[2]
${FILTERFORMBYAWAITING}    //ul[contains(@class, "dropdown")]/li/span[text()="Awaiting your action"]
${FILTERFORMRESULTLIST}    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ListItem")]


*** Keywords ***

clickCreateFormButton
    Wait until element is enabled    ${CREATEFORMBTN}    timeout=${timeout}
    Click element    ${CREATEFORMBTN}

selectFirstForm
    Wait until element is enabled   ${FIRSTFORMLIST}    timeout=${timeout}
    Click element    ${FIRSTFORMLIST}
    

clickMuteForm
    Wait until element is enabled    ${MUTEFORMBTN}    timeout=${timeout}
    Click element    ${MUTEFORMBTN}
    Wait until element is enabled    ${CONFIRMMUTEFORMBTN}    timeout=${timeout}
    Click element    ${CONFIRMMUTEFORMBTN}
    Wait until element is not visible    ${CONFIRMMUTEFORMBTN}    timeout=${timeout}

clickExportForm
    Wait until element is enabled    ${EXPORTFORMBTN}    timeout=${timeout}
    Click element    ${EXPORTFORMBTN}

clickFilterForm
    Wait until element is enabled    ${FILTERFORMBTN}    timeout=${timeout}
    Click element    ${FILTERFORMBTN}

selectFilterByAwaitingForm
    Wait until element is enabled    ${FILTERFORMBYAWAITING}    timeout=${timeout}
    CLick element    ${FILTERFORMBYAWAITING}


#------------------------verify------------------------------------------------

isFormMute
    Wait until element is visible    ${FORMMUTEICON}    timeout=${timeout}

isDisplayOnlyAwaitingForm
    ${index}    Get element count    ${FILTERFORMRESULTLIST}
    :FOR  ${index}  IN RANGE  ${index}
    \  Page should contain element    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ListItem")][${index}+1]//span[text()=" Awaiting your action"]

isFormExport
    [Arguments]    ${index}
    ${new_window_list}    get window handles
    Should Not Be Equal      ${index}    ${new_window_list}    msg=Cannot open a new tab