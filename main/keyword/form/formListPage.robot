*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${FIRSTFORMTEMPLATE}    //tbody/tr[1]
${THEFIRSTFORMLIST}    //div[contains(@class, "ListItem")][1]
${FORMSUBJECTNAME}    //label[@title="Form Subject"]/parent::*/following-sibling::div/div
${FORMCANCELBTN}    //button[contains(@class, "BtnCancel")]
${CONFIRMCANCELFORMBTN}    //div[@class="ant-confirm-btns"]/button[2]
${CANCELFORMSTATUSONFIRSTFORMLIST}    //div[contains(@class, "StatusTag")][text()="Canceled"]
${APPROVEFROMSTATUSONFIRSTFORMLIST}    //div[contains(@class, "ListItem")][1]//div[contains(@class, "StatusdDot-iqabCg fcOeII")]
${REJECTFROMSTATUSONFIRSTFORMLIST}    //div[contains(@class, "ListItem")][1]//div[contains(@class, "StatusdDot-iqabCg bIidAZ")]
${CLOSESIGNATUREINFORMPOPUP}    //button[@class="ant-modal-close"]
${APPROVEFORMBTN}    //div[contains(@class, "Container")]/div/button[1]
${REJECTFORMBTN}    //div[contains(@class, "Container")]/div/button[2]
${RESONTEXTFIELD}    reason
${RESPONDFORMBTN}    //div[@class="ant-form-item-control "]/button[2]
${FORMUNREADBADGE}    //a[@data-qa-anchor="appnav-menu-forms"]//sup[@data-show="true"]//p[@class="current"]
${DUEDATETIME}    //span[contains(@class, "lnr lnr-calendar")]

*** Keywords ***

selectFirstFormTemplate
    Wait until element is enabled    ${FIRSTFORMTEMPLATE}    timeout=${timeout}
    Click element    ${FIRSTFORMTEMPLATE}

clickCancelTheForm
    Wait until element is enabled    ${FORMCANCELBTN}    timeout=${timeout}
    Click element    ${FORMCANCELBTN}

confirmCancelTheForm
    Wait until element is enabled    ${CONFIRMCANCELFORMBTN}   timeout=${timeout}
    Click element    ${CONFIRMCANCELFORMBTN}

selectTheFirstFormList
    Wait until element is enabled    ${THEFIRSTFORMLIST}    timeout=${timeout}
    Click element    ${THEFIRSTFORMLIST}

closeTheFormSignaturePopUp
    Wait until element is enabled    ${CLOSESIGNATUREINFORMPOPUP}    timeout=${timeout}
    Click element    ${CLOSESIGNATUREINFORMPOPUP}

clickApproveForm
    Wait until element is enabled    ${APPROVEFORMBTN}    timeout=${timeout}
    Click element    ${APPROVEFORMBTN}

clickRejectForm
    Wait until element is enabled    ${REJECTFORMBTN}    timeout=${timeout}
    Click element    ${REJECTFORMBTN}

addRespondReason
    [Arguments]    ${text}
    Wait until element is enabled    ${RESONTEXTFIELD}    timeout=${timeout}
    Input text    ${RESONTEXTFIELD}    ${text}

clickConfirmFormRespond
    Wait until element is enabled    ${RESPONDFORMBTN}    timeout=${timeout}
    Click element    ${RESPONDFORMBTN}


selectFormbyName
   [Arguments]   ${text}
   Wait until element is enabled    //div[contains(@class, "styles__WrapListItem")][text()="${text}"]     timeout=${timeout}
   Click element   //div[contains(@class, "styles__WrapListItem")][text()="${text}"]

#---------------------------- Verify ---------------------------------------------------------------

isFormCreated
    [Arguments]    ${text}
    Wait until element is visible    ${FORMSUBJECTNAME}    timeout=${timeout}
    Element should contain    ${FORMSUBJECTNAME}    ${text}

isFormCanceled
    Wait until element is visible    ${CANCELFORMSTATUSONFIRSTFORMLIST}    timeout=${timeout}

isFormApprove
    Wait until element is visible    ${APPROVEFROMSTATUSONFIRSTFORMLIST}    timeout=${timeout}

isFormReject
    Wait until element is visible    ${REJECTFROMSTATUSONFIRSTFORMLIST}    timeout=${timeout}

isFormUnreadBadgeDisplay
    [Arguments]    ${index}=1
    Wait until element is visible    ${FORMUNREADBADGE}    timeout=${timeout}
    Element should contain    ${FORMUNREADBADGE}  ${index}

isFormDisplayWithNoSetRespondTime
    [Arguments]    ${text}
    Wait until element is not visible    //span[contains(@class, "styles__Capitalized")][text()="${text}"]//ancestor::div[contains(@class, "styles__Clearfix")]//descendant::span[contains(@class, "styles__ContentRowInfo")]/span[@class="lnr lnr-timer2"]    timeout=${timeout}

isFormDisplayWithSetRespondTime
    [Arguments]    ${text}
    Wait until element is visible    //span[contains(@class, "styles__Capitalized")][text()="${text}"]//ancestor::div[contains(@class, "styles__Clearfix")]//descendant::span[contains(@class, "styles__ContentRowInfo")][contains(text(), "1 hour")]    timeout=${timeout}

isFormDisplayWithSetRespondTimeMoreThan1Day
    [Arguments]    ${text}
    Wait until element is visible    //span[contains(@class, "styles__Capitalized")][text()="${text}"]//ancestor::div[contains(@class, "styles__Clearfix")]//descendant::span[contains(@class, "styles__ContentRowInfo")][contains(text(), "5 days 2 hours")]    timeout=${timeout}

isDuedateTimeDisplayCorrect
    [Arguments]    ${time}
    Wait until element is visible    //time[contains(@class, "styles__DueDate")][contains(text(),"${time}")]    timeout=${timeout}