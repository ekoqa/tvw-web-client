*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${FIRSRECIPIENTDROPDOWN}    //div[text()="1st Recipient"]
${SECONDRECIPIENTDROPDOWN}    //div[text()="2nd Recipient"]
${THIRDRECIPIENTDROPDOWN}    //div[text()="3rd Recipient"]
${SELECTFIRSTUSERLIST}    //*[contains(@class, 'Select-menu-outer')]/div/div/div/div/div/div
${SUBMITFORMBTN}    //div[contains(@class, "FormTail")]//button[@type="submit"]
${FORMSUBJECTFIELD}    Subject
${SEARCHFIELDFORFORMRECIPIENT}    //input[@data-qa-anchor="select-input"]
${DUEDATEBTN}    //span[contains(@class, "ant-calendar-picker")]
${FORMOKBUTTONINDUEDATE}    //*[contains(@class, "ant-calendar-ok-btn")]


*** Keywords ***

selectFirstRecipient
    Wait until element is enabled    ${FIRSRECIPIENTDROPDOWN}    timeout=${timeout}
    Click element    ${FIRSRECIPIENTDROPDOWN}
    Wait until element is enabled    ${SELECTFIRSTUSERLIST}    timeout=${timeout}
    Click element    ${SELECTFIRSTUSERLIST}

clickSubmitForm
    Click element    ${SUBMITFORMBTN}

setFormSubjectName
    [Arguments]    ${text}
    Wait until element is enabled    ${FORMSUBJECTFIELD}    timeout=${timeout}
    Input text    ${FORMSUBJECTFIELD}    ${text}

searchForAddedRecipientInFrom
    [Arguments]    ${text}
    Wait until element is enabled    ${SEARCHFIELDFORFORMRECIPIENT}    timeout=${timeout}
    Input text    ${SEARCHFIELDFORFORMRECIPIENT}    ${text}
    sleep    3s
    Wait until element is enabled    ${SELECTFIRSTUSERLIST}    timeout=${timeout}
    Click element    ${SELECTFIRSTUSERLIST}

setDuedateCurentTime
    Wait until element is enabled    ${DUEDATEBTN}    timeout=${timeout}
    Click element    ${DUEDATEBTN}
    Wait until element is enabled    ${FORMOKBUTTONINDUEDATE}    timeout=${timeout}
    Click element    ${FORMOKBUTTONINDUEDATE}

selectFirstRecipientBySearchName
    [Arguments]    ${text}
    Wait until element is enabled    ${FIRSRECIPIENTDROPDOWN}    timeout=${timeout}
    Click element    ${FIRSRECIPIENTDROPDOWN}
    Wait until element is enabled    ${SEARCHFIELDFORFORMRECIPIENT}    timeout=${timeout}
    Input text    ${SEARCHFIELDFORFORMRECIPIENT}    ${text}
    sleep    3s
    Wait until element is enabled    ${SELECTFIRSTUSERLIST}    timeout=${timeout}
    Click element    ${SELECTFIRSTUSERLIST}

selectSecondRecipientBySearchName
    [Arguments]    ${text}
    Wait until element is enabled    ${SECONDRECIPIENTDROPDOWN}    timeout=${timeout}
    Click element    ${SECONDRECIPIENTDROPDOWN}
    Wait until element is enabled    //div[contains(@class, "Select-placeholder")][contains(text(), "2nd Recipient")]//following-sibling::div/input    timeout=${timeout}
    Input text    //div[contains(@class, "Select-placeholder")][contains(text(), "2nd Recipient")]//following-sibling::div/input   ${text}
    sleep    3s
    Wait until element is enabled    ${SELECTFIRSTUSERLIST}    timeout=${timeout}
    Click element    ${SELECTFIRSTUSERLIST}

selectThirdRecipientBySearchName
    [Arguments]    ${text}
    Wait until element is enabled    ${THIRDRECIPIENTDROPDOWN}    timeout=${timeout}
    Click element    ${THIRDRECIPIENTDROPDOWN}
    Wait until element is enabled    //div[contains(@class, "Select-placeholder")][contains(text(), "3rd Recipient")]//following-sibling::div/input    timeout=${timeout}
    Input text    //div[contains(@class, "Select-placeholder")][contains(text(), "3rd Recipient")]//following-sibling::div/input   ${text}
    sleep    3s
    Wait until element is enabled    ${SELECTFIRSTUSERLIST}    timeout=${timeout}
    Click element    ${SELECTFIRSTUSERLIST}
