*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${TASKTITLEFIELD}    //input[@data-qa-anchor="title"]
${CLOSETASKDETAILPAGEBTN}    //div[contains(@class, "CloseIcon")]
${ADDASSIGNEEINTASKFIELD}    //div[contains(@class, "SelectorHead")]
${SEARCHFIELDFORADDASSINGEEINTASK}    //span[contains(@class, "SearchBar")]//input[@data-qa-anchor="search-box"]
${FIRSTSEARCFORADDASSIGNEERESULT}    //div[contains(@class, "Scroller")]//div[contains(@class, "UserItem")][1]
${SUBMITTASKBTN}    //button[@data-qa-anchor="create-button"]
${ASSIGNEELIST}    //span[contains(@class, "UserName")]
${REMOVEFIRSTASSIGNEEBTN}    //div[contains(@class, "UserItem")][1]//button[contains(@class, "UnstyledButton")]
${REMOVESECONDASSIGNEEBTN}    //div[contains(@class, "UserItem")][2]//button[contains(@class, "UnstyledButton")]
${FIRSTASSIGNEEBTN}    //div[contains(@class, "UserItem")][1]
${TASKDETAIFIELD}    //*[@data-qa-anchor="description"]
${TASKDUAEATE}    //*[contains(@class, 'styles__DueDatePicker')]
${TASKOKBUTTONINDUEDATE}    //*[contains(@class, "ant-calendar-ok-btn")]
${TASKPRIORITY}    priority
${TASKCHOOSEPRIORITY}    //*[contains(@class, 'dropdown-menu-vertical')]/li[2]
${SUBTASKINPUT}    //input[@data-qa-anchor="add-subtasks-input"]
${INPUTTAGFIELD}    //input[@data-qa-anchor="add-tags-input"]
${ADDTAG}    //div[@data-qa-anchor="add-tags-wrapper"]
${SELECTTAG}    //*[contains(@class, 'Select-menu-outer')]
${TASKATTACHPHOTO}    //*[contains(@class,"ant-upload")]/span/input
${TASKATTACHFILE}    //*[contains(@class,"antd3-upload")]/input
${SEARCHTASK}    //*[@data-qa-anchor="search-box"]
${REMOVEPHOTO}    //*[contains(@class, 'styles__AttachmentBadge')]
${REMOVEFILE}    //*[contains(@data-icon, 'close')]
${REMOVETASK}    //*[contains(@class, 'styles__DeleteTask')]
${CONFIRMREMOVETASK}    //*[contains(@class, 'antd3-popover-buttons')]/button[2]
${TASKCOMPLETECHECKBOX}    //*[contains(@class, 'antd3-checkbox')]
${TASKUNCOMPLETECHECKBOX}    //*[contains(@class, 'antd3-checkbox-checked')]
${ALLTASKBTN}    //span[@data-qa-anchor="all-task-tab"]
${VERIFYTASKDUEDATE}    //*[contains(text(), 'Add due date')]
${VERIYTASKPRIORITY}    //*[contains(@class,"styles__PriorityBadge")]
${VERIYSUBTASK}    //li[@data-qa-anchor="check-list-item"]
${VERIYTAGS}    //*[contains(@class,"Select-value-label")]
${VERIYPHOTO}    //*[contains(@class,"styles__ImageItem")]
${VERIFYFILE}    //*[contains(@class,"styles__UploadItem")]
${VERIFYFILE2}   //*[contains(@class,"styles__UploadItem")][2]
${ADDFILEINTASKBTN}    //input[@type="file"]/following-sibling::button

*** Keywords ***

setTaskTiTle
    [Arguments]    ${title}
    Wait until element is visible    ${TASKTITLEFIELD}    timeout=${timeout}
    Input text    ${TASKTITLEFIELD}    ${title}

setTaskDetail
    [Arguments]    ${title}
    Wait until element is visible    ${TASKDETAIFIELD}   timeout=${timeout}
    Input text    ${TASKDETAIFIELD}   ${title}

setSubTask
    [Arguments]    ${title}
    Wait until element is enabled    ${SUBTASKINPUT}    timeout=${timeout}
    Input Text    ${SUBTASKINPUT}    ${title}
    Press keys    ${SUBTASKINPUT}    RETURN

setDueDate
    Wait until element is enabled    ${TASKDUAEATE}    timeout=${timeout}
    Click element    ${TASKDUAEATE}
    Wait until element is enabled    ${TASKOKBUTTONINDUEDATE}    timeout=${timeout}
    Click element    ${TASKOKBUTTONINDUEDATE}

setPriority
    [Arguments]    ${index}=2
    Wait until element is enabled    ${TASKPRIORITY}    timeout=${timeout}
    Click element    ${TASKPRIORITY}
    Wait until element is enabled    //*[contains(@class, 'dropdown-menu-vertical')]/li[${index}]     timeout=${timeout}
    Click element    //*[contains(@class, 'dropdown-menu-vertical')]/li[${index}]

setTagsTask
    Wait until element is enabled    ${ADDTAG}    timeout=${timeout}
    Click element    ${ADDTAG}
    Wait until element is enabled    ${SELECTTAG}     timeout=${timeout}
    Click element    ${SELECTTAG} 

taskAttachPhoto
    Wait until element is enabled    ${TASKATTACHPHOTO}    timeout=${timeout}
    Choose file    ${TASKATTACHPHOTO}    ${filePath}/${imageInChatFile}

taskAttachfile
    Wait until element is enabled    ${TASKATTACHFILE}    timeout=${timeout}
    Choose file    ${TASKATTACHFILE}    ${filePath}/${pdfFile}

clickAddAssigneeInTask
    Click element    ${ADDASSIGNEEINTASKFIELD}

searchForAddAssigneeInTask
    [Arguments]   ${text}
    Wait until element is enabled    ${SEARCHFIELDFORADDASSINGEEINTASK}    timeout=${timeout}
    Input text    ${SEARCHFIELDFORADDASSINGEEINTASK}    ${text}

searchTaskByName
    [Arguments]   ${text}
    Wait until element is enabled    ${SEARCHTASK}    timeout=${timeout}
    Input text    ${SEARCHTASK}    ${text}

selectFirstSearchResult
    Wait until element is enabled    ${FIRSTSEARCFORADDASSIGNEERESULT}    timeout=${timeout}
    Click element    ${FIRSTSEARCFORADDASSIGNEERESULT}

selectTaskAssigneeByName
    [Arguments]    ${title}
    Wait until element is enabled    //div[contains(@class, "UserItem")]//span[@title="${title}"]    timeout=${timeout}
    Click element    //div[contains(@class, "UserItem")]//span[@title="${title}"]

selectAssigneeByIndex
    [Arguments]    ${index}
    Wait until element is enabled    //div[contains(@class, "Scroller")]//div[contains(@class, "UserItem")][${index}]    timeout=${timeout}
    Click element    //div[contains(@class, "Scroller")]//div[contains(@class, "UserItem")][${index}]

closeTaskDetailPage
    Click element    ${CLOSETASKDETAILPAGEBTN}

confirmCreateTask
    [Arguments]    ${text}
    Wait until element is enabled    ${SUBMITTASKBTN}    timeout=${timeout}
    Click element    ${SUBMITTASKBTN}
    Wait until element is enabled    //div[contains(@class, "Scroller")]/div/li//span[text()="${text}"]    timeout=${timeout}

removeFirstAssigneeFromTask
    Wait until element is enabled    ${REMOVEFIRSTASSIGNEEBTN}    timeout=${timeout}
    Click element    ${REMOVEFIRSTASSIGNEEBTN}

removeSecondAssigneeFromTask
    Wait until element is enabled    ${REMOVESECONDASSIGNEEBTN}    timeout=${timeout}
    Click element    ${REMOVESECONDASSIGNEEBTN}

removePhotoFromTask
    Wait until element is enabled    ${REMOVEPHOTO}    timeout=${timeout}
    Click element    ${REMOVEPHOTO}

removeFileFromTask
    Wait until element is enabled    ${REMOVEFILE}    timeout=${timeout}
    Click element    ${REMOVEFILE}
    Click element    ${REMOVEFILE}

removeFileFromTaskByClickCloseOneTime
    Wait until element is enabled    ${REMOVEFILE}    timeout=${timeout}
    Click element    ${REMOVEFILE}

removeTask
    Wait until element is enabled    ${REMOVETASK}    timeout=${timeout}
    Click element    ${REMOVETASK}
    Wait until element is enabled    ${CONFIRMREMOVETASK}    timeout=${timeout}
    Click element    ${CONFIRMREMOVETASK}

clickFirstAssigneeFromTask
    Wait until element is enabled    ${FIRSTASSIGNEEBTN}    timeout=${timeout}
    Click element    ${FIRSTASSIGNEEBTN}

clickTaskComplete
    Wait until element is enabled    ${TASKCOMPLETECHECKBOX}    timeout=${timeout}
    Click element    ${TASKCOMPLETECHECKBOX}

clickTaskUncomplete
    Wait until element is enabled    ${TASKUNCOMPLETECHECKBOX}    timeout=${timeout}
    Click element    ${TASKUNCOMPLETECHECKBOX}

clickCreateTask
    Wait until element is enabled    ${SUBMITTASKBTN}    timeout=${timeout}
    Click element    ${SUBMITTASKBTN}

clickAllTaskTab
    Wait until element is enabled    ${ALLTASKBTN}    timeout=${timeout}
    Click element    ${ALLTASKBTN}

#----------------------------- verify---------------------------------------------

isTaskAssignedToUser
    [Arguments]    @{text}
    FOR  ${element}  IN  @{text}
        Wait until element is visible  //div[contains(@class, "UserItem")]//*[text()="${element}"]  timeout=${timeout}
    END

isAssignedNotShow
    [Arguments]    @{text}
    FOR  ${element}  IN  @{text}
        Wait until element is not visible  //div[contains(@class, "UserItem")]//*[text()="${element}"]  timeout=${timeout}
    END

isUserCompleteTask
    [Arguments]    @{text}
    FOR  ${element}  IN  @{text}
        Wait until element is visible  //div[contains(@class, "UserItem")]/*[@data-icon="check"]/following-sibling::span[text()="${element}"]  timeout=${timeout}
    END

isUserNotCompleteTask
    [Arguments]    @{text}
    FOR  ${element}  IN  @{text}
        Wait until element is not visible  //div[contains(@class, "UserItem")]/*[@data-icon="check"]/following-sibling::span[text()="${element}"]  timeout=${timeout}
    END

isTaskSentWithAllField
    Wait until element is not visible    ${VERIFYTASKDUEDATE}    timeout=${timeout}
    Wait until element is visible    ${VERIYTASKPRIORITY}    timeout=${timeout}
    Wait until element is visible    ${VERIYSUBTASK}    timeout=${timeout}
    Wait until element is visible    ${VERIYTAGS}    timeout=${timeout}
    Wait until element is visible    ${VERIYPHOTO}    timeout=${timeout}

isUpdateTagInTask
    Wait until element is visible    ${VERIYTAGS}    timeout=${timeout}

isTaskSentWithPhoto
    Wait until element is visible    ${VERIYPHOTO}    timeout=${timeout}

isDeletePhotoFromTask
    Wait until element is not visible    ${VERIYPHOTO}    timeout=${timeout}

isTaskSentWithFile
    Wait until element is visible    ${VERIFYFILE}    timeout=${timeout}

isDeleteFileFromTask
    Wait until element is not visible    ${VERIFYFILE}    timeout=${timeout}

isAssigneeNotSeeDeleteTaskIcon
    Wait until element is not visible   ${REMOVETASK}    timeout=${timeout}

isTaskSentWithNoFile
    Wait until element is not visible    ${VERIFYFILE}    timeout=${timeout}

isTaskDisplayCorrectFileNumber
    [Arguments]    ${index}
    ${count}    Get element count    ${VERIFYFILE} 
    Run Keyword If    '${index}'!='${count}'    fail

isAddFileButtonInTaskDisappear
    Wait until element is not visible    ${ADDFILEINTASKBTN}    timeout=${timeout}
