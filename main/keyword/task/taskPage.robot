*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${CREATETASKFROMTASKPAGEBTN}    //button[contains(@class, "CreateTaskButton")]
${ALLTASKSTABBTN}    //div[contains(@class, "tabs-nav")]/div/div[@role="tab"][2]
${FIRSTTASK}    //*[contains(@class,"styles__TaskItemContainer-")][1]
${TASKCOMMENTBTN}    //*[contains(@class,"styles__CommentsIconWrapper")]
${SORTTASKBTN}    //div[contains(@class, "Filters")]/div[1]/button
${LASTUPDATEBTN}    //*[text()="Last updated"]
${TASKSTATUSFILTERBTN}    //button[@data-qa-anchor="tasks-dropdown-filter"]
${CLOSEDBTN}    //*[text()="Closed"]

*** Keywords ***

clickCreateTaskFromTaskPage
    Wait until element is enabled    ${CREATETASKFROMTASKPAGEBTN}    timeout=${timeout}
    Click element    ${CREATETASKFROMTASKPAGEBTN}

selectAllTaskTab
    Wait until element is enabled    ${ALLTASKSTABBTN}    timeout=${timeout}
    Click element    ${ALLTASKSTABBTN}

selectTaskName
    [Arguments]    ${text}
    Wait until element is enabled    //div[contains(@class, "Scroller")]/div/li//span[text()="${text}"]    timeout=${timeout}
    Click element    //div[contains(@class, "Scroller")]/div/li//span[text()="${text}"]

clickFirstTask
    Wait until element is enabled    ${FIRSTTASK}    timeout=${timeout}
    Click element    ${FIRSTTASK}

clickTaskComment
    Wait until element is enabled    ${TASKCOMMENTBTN}    timeout=${timeout}
    Click element    ${TASKCOMMENTBTN}

sortTaskByLastUpdate
    Click element    ${SORTTASKBTN}
    Wait until element is enabled    ${LASTUPDATEBTN}    timeout=${timeout}
    Click element    ${LASTUPDATEBTN}

sortTaskByClosed
    Click element    ${TASKSTATUSFILTERBTN}
    Wait until element is enabled    ${CLOSEDBTN}    timeout=${timeout}
    Click element    ${CLOSEDBTN}

#------------------- Varify -------------------------------------

isFirstTaskDisplayCorrectName
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "Scroller")]/div/li[1]//span[text()="${text}"]    timeout=${timeout}

isTaskDisplayInTaskPage
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "Scroller")]/div/li//span[text()="${text}"]    timeout=${timeout}