*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${HUBTITLEHEADER}    //div[contains(@class, "TopSection")]//h4
${PENCILICONINSIDEDISCOVERY}    //button[@data-qa-anchor="discovery-create-thread-btn"]
${CREATEQUESTIONINHUBBTN}    //a[@data-qa-anchor="discovery-create-question"]
${CREATEQPOLLINHUBBTN}    //a[@data-qa-anchor="discovery-create-vote"]
${CREATESHAREINHUBBTN}    //a[@data-qa-anchor="discovery-create-share"]
${HUBQUESTIONTITLEFIELD}    //div[@class="ant-form-item-control "]/input
${HUBQUESTIONTITLEEDITFIELD}    //input[@id="name"]
${HUBPOLLTITLEFIELD}    //form/div[1]//div[@class="ant-form-item-control "]/input
${POLLLISTFIELD}    //form/div[3]/div[contains(@class, "FormItemSuffix")]
${NEXTBTNFORCREATEQUESTIONINHUB}    //div[@data-qa-anchor="discovery-modal-create-form-button-next"]/button[2]
${CONFIRMFORCREATEQUESTIONINHUB}    //div[@data-qa-anchor="discovery-modal-create-form-button-submit"]/button[2]
${FIRSTTHREADINHUBLOCATOR}    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ThreadItem")][1]
${FIRSTTHREADTITLEINHUB}    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ThreadItem")][1]//h4
${HUBTITLELIST}    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ThreadItem")][1]//h4
${INPUTIMAGEINSIDEHUBTOPICLOCATOR}    //div[@data-qa-anchor="discovery-modal-create-form"]//input[@type="file"]
${INPUTIMAGEINHUBTOPICCOMMENT}    //div[contains(@class, "ComposerContainer")]//input
${IMGPREVIEWINSIDETOPICHUBCREATION}    //div[contains(@class, "Preview")]/img
${IMGPREVIEWINHUBTOPICCOMMENT}    //div[contains(@class, "ComposerContainer")]//div[contains(@class, "Preview")]/img
${REMOVEIMGICONINHUBTOPIC}    //div[contains(@class, "AttachmentPreview")]//*[contains(@class, "RemoveBtnIcon")]
${THREEDOTINHUBTOPIC}    //span[contains(@class, "Option")]
${EDITHUBTOPICBTN}    //span//ul[contains(@class, "dropdown")]/li/span[text()=" Edit"]
${DELETEHUBTOPICBTN}    //span//ul[contains(@class, "dropdown")]/li/span[text()=" Delete"]
${EDITCOMMENTINHUBTOPICBTN}    //ul[contains(@class, "dropdown")]/li[text()="Edit"]
${DELETECOMMENTINHUBTOPICBTN}    //ul[contains(@class, "dropdown")]/li[text()="Delete"]
${CONFIRMDELEATETOPICINHUBBTN}    //div[@class="ant-confirm-btns"]/button[2]
${FIRSTPOLLCHOICE}    //div[contains(@class, "PollRadio")]/label[1]
${VOTEBUTTON}    //button[contains(@class, "UpvoteButton")]
${SUBMITPOLLCHOICEBTN}    //div[contains(@class, "ButtonSubmitPoll")]
${VOTERESULTFORFIRSTCHOICE}    //div[contains(@class, "PollContent")][1]//span[contains(@class, "PollVotes")]
${COMMENTFIELDINTOPICHUB}    //div[@data-qa-anchor="composer"]//div[@contenteditable="true"]
${COMMENTLOCATORINHUBTOPIC}    //div[contains(@class, "CommentContainer")]//span[contains(@class, "TextBody")]/span
${SINGLEIMGINHUBTOPICDETAIL}    //div[contains(@class, "DetailsContainer")]//div[contains(@class, "SingleImage")]
${THREEDOTFORFIRSTCOMMENTINHUB}    //div/div[contains(@class, "CommentContainer")][1]//*[@data-icon="ellipsis-v"]
${IMGINFIRSTCOMMENT}    //div[contains(@class, "CommentContainer")][1]//img
${EDITCOMMENTFIELDINHUB}    //textarea[@id="data"]
${OKBTNTOEDITCOMMENT}    //div[@class="antd3-modal-content"]/div[2]/button[2]
${ADDPOLLCHOICEBTN}    //div[@class="ant-form-item-control "]/button
${POLLCHOICETEXTFIELD}    //div[@class="ant-form-item-control "]/input
${MULTIPLEIMAGECONTAINER}    //div[contains(@class, "Attachment")]//div[contains(@class, "FewImage")]
${ALERTBANNER}    //div[contains(@class, "AlertContainer")]
${CLOSEICONONALERTBANNER}    //div[contains(@class, "AlertContainer")]//*[@data-icon="close"]
${HUBCOMMENTCONTAINER}    //*[contains(@class, "CommentContainer")]
${VERIFYNOPERMISSIONCREATEPOST}    //*[contains(@class,"ant-confirm-title")][text()="You're not allowed to post in this hub"]
${OKPOPUPBTN}    //div[@class="ant-confirm-btns"]/button
${PENCElNOPERMISSIONUSER}    //*[contains(@class,"styles__OnlyAdminCanPostContainer")]
${PUBLICHUBICON}    //*[contains(@class, "PublicHubIcon")]
${PRIVATEHUBICON}    //*[contains(@class, "PrivateHubIcon")]
${HUBSETTING}    //*[contains(@class,"styles__HubDetailsContainer")]
${PRIVATERADIO}    //*[contains(@class,"antd3-radio-group")]/label[2]/span/input
${PUBLICRADIO}    //*[contains(@class,"antd3-radio-group")]/label[1]/span/input
${CONFIRMCHANGEPUBLICTOPRIVATE}    //div[@class="ant-confirm-btns"]/button[2]
${CREATENEWHUBBTN}    //div[@data-qa-anchor="discovery-modal-create-hub-form-button-create"]/button[2]
${UNFOLLOWINSIDEHUBBTN}    //*[contains(@class,"styles__RightNodeWrapper")]/button[2]
${CONFIRMEDITHUBBTN}    //div[@class="ant-confirm-btns"]/button[2]
${ADDMOREUSERTOHUBBTN}    //*[contains(@class, "Selector")]/div[contains(@class, "Hidden")]
${NOTIFYBANNERCLOSEBTN}    //div[contains(@class, "Alert")]//button
${NOTIFYBANNERMESSAGE}    //div[contains(@class, "Alert")]/div/span[1]

*** Keywords ***

clickCreateQuestionInHub
    Wait until element is enabled    ${PENCILICONINSIDEDISCOVERY}    timeout=${timeout}
    Click element    ${PENCILICONINSIDEDISCOVERY}
    Wait until element is enabled    ${CREATEQUESTIONINHUBBTN}    timeout=${timeout}
    Click element    ${CREATEQUESTIONINHUBBTN}

clickCreatePollInHub
    Wait until element is enabled    ${PENCILICONINSIDEDISCOVERY}    timeout=${timeout}
    Click element    ${PENCILICONINSIDEDISCOVERY}
    Wait until element is enabled    ${CREATEQPOLLINHUBBTN}    timeout=${timeout}
    Click element    ${CREATEQPOLLINHUBBTN}

inputQuestionTitle
    [Arguments]    ${text}
    Wait until element is enabled    ${HUBQUESTIONTITLEFIELD}    timeout=${timeout}
    Input text    ${HUBQUESTIONTITLEFIELD}    ${text}

editHubTopicTitle
    [Arguments]    ${text}
    ${value}=     Get Element Attribute   ${HUBQUESTIONTITLEEDITFIELD}      value
    ${backspacesCount}=    Get Length      ${value}
    Run Keyword If    """${value}""" != ''    
    ...     Repeat Keyword  ${backspacesCount}  Press Keys  ${HUBQUESTIONTITLEEDITFIELD}   BACKSPACE
    Input text    ${HUBQUESTIONTITLEEDITFIELD}    ${text}

inputPollTitle
    [Arguments]    ${text}
    Wait until element is enabled    ${HUBPOLLTITLEFIELD}    timeout=${timeout}
    Input text    ${HUBPOLLTITLEFIELD}    ${text}

inputPollList
    ${index}    Get element count    ${POLLLISTFIELD}
    :FOR  ${index}  IN RANGE  ${index}
    \    Input text    //form/div[3]/div[contains(@class, "FormItemSuffix")][${index}+1]//input    ${index}

inputShareTitle
    [Arguments]    ${text}
    Wait until element is enabled    ${HUBQUESTIONTITLEFIELD}    timeout=${timeout}
    Input text    ${HUBQUESTIONTITLEFIELD}    ${text}

clickCreateShareInHub
    Wait until element is enabled    ${PENCILICONINSIDEDISCOVERY}    timeout=${timeout}
    Click element    ${PENCILICONINSIDEDISCOVERY}
    Wait until element is enabled    ${CREATESHAREINHUBBTN}    timeout=${timeout}
    Click element    ${CREATESHAREINHUBBTN}

addImageIntoHubTopic
    Choose file    ${INPUTIMAGEINSIDEHUBTOPICLOCATOR}    ${filepath}/${imageInChatFile}
    Wait until element is visible    ${IMGPREVIEWINSIDETOPICHUBCREATION}    timeout=${timeout}

commentInHubByImage
    Wait until element is enabled    ${INPUTIMAGEINHUBTOPICCOMMENT}    timeout=${timeout}
    Choose file    ${INPUTIMAGEINHUBTOPICCOMMENT}    ${filepath}/${imageInChatFile}
    Wait until element is visible    ${IMGPREVIEWINHUBTOPICCOMMENT}

confirmToCreateTopicInHub
    Wait until element is enabled    ${NEXTBTNFORCREATEQUESTIONINHUB}    timeout=${timeout}
    Click element    ${NEXTBTNFORCREATEQUESTIONINHUB}
    Sleep    3s
    Wait until element is enabled    ${CONFIRMFORCREATEQUESTIONINHUB}    timeout=${timeout}
    Click element    ${CONFIRMFORCREATEQUESTIONINHUB}

selectFirstThreadInHub
    Wait until element is enabled    ${FIRSTTHREADINHUBLOCATOR}    timeout=${timeout}
    Click element    ${FIRSTTHREADINHUBLOCATOR}

clickThreeDotInsideHubTopic
    Wait until element is enabled    ${THREEDOTINHUBTOPIC}    timeout=${timeout}
    Click element    ${THREEDOTINHUBTOPIC}

clickThreeDotForCommentInHubTopic
    Wait until element is enabled    ${THREEDOTFORFIRSTCOMMENTINHUB}    timeout=${timeout}
    Click element    ${THREEDOTFORFIRSTCOMMENTINHUB}

editHubTopic
    Wait until element is enabled    ${EDITHUBTOPICBTN}    timeout=${timeout}
    Click element    ${EDITHUBTOPICBTN}    

clickEditCommentInHubTopic
    Wait until element is enabled   ${EDITCOMMENTINHUBTOPICBTN}   timeout=${timeout}
    Click element    ${EDITCOMMENTINHUBTOPICBTN}

editCommentTextInHub
    [Arguments]   ${text}
    Wait until element is enabled    ${EDITCOMMENTFIELDINHUB}    timeout=${timeout}
    Clear element text    ${EDITCOMMENTFIELDINHUB}
    Input text    ${EDITCOMMENTFIELDINHUB}    ${text}

clickConfirmEditCommentInHub
    Wait until element is enabled    ${OKBTNTOEDITCOMMENT}    timeout=${timeout}
    Click element    ${OKBTNTOEDITCOMMENT}

deleteHubTopic
    Wait until element is enabled    ${DELETEHUBTOPICBTN}    timeout=${timeout}
    Click element    ${DELETEHUBTOPICBTN}
    Wait until element is enabled    ${CONFIRMDELEATETOPICINHUBBTN}    timeout=${timeout}
    Click element    ${CONFIRMDELEATETOPICINHUBBTN}

deleteCommentInHubTopic
    Wait until element is enabled    ${DELETECOMMENTINHUBTOPICBTN}    timeout=${timeout}
    Click element    ${DELETECOMMENTINHUBTOPICBTN}
    Wait until element is enabled    ${CONFIRMDELEATETOPICINHUBBTN}    timeout=${timeout}
    Click element    ${CONFIRMDELEATETOPICINHUBBTN}

selectHubTopicName
    [Arguments]    ${text}
    Wait until element is enabled    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ThreadItem")]//h4[text()="${text}"]    timeout=${timeout}
    Click element    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ThreadItem")][1]//h4[text()="${text}"]

voteFirstPollChoice
    Wait until element is enabled    ${FIRSTPOLLCHOICE}    timeout=${timeout}
    Click element    ${FIRSTPOLLCHOICE}

voteQuestionInsideTopic
    Wait until element is enabled    ${VOTEBUTTON}    timeout=${timeout}
    Click element    ${VOTEBUTTON}  

clickSubmitVote
    Wait until element is enabled    ${SUBMITPOLLCHOICEBTN}    timeout=${timeout}
    Click element    ${SUBMITPOLLCHOICEBTN}

commentInHubTopic
    [Arguments]    ${text}=comment
    Wait until element is enabled    ${COMMENTFIELDINTOPICHUB}    timeout=${timeout}
    Input text    ${COMMENTFIELDINTOPICHUB}    ${text}
    Press Keys    ${COMMENTFIELDINTOPICHUB}    RETURN

clickRemoveImageInHUbTopic
    Wait until element is enabled    ${REMOVEIMGICONINHUBTOPIC}    timeout=${timeout}
    Click element    ${REMOVEIMGICONINHUBTOPIC}

clickAddMoreChoiceInPoll
    Wait until element is enabled    ${ADDPOLLCHOICEBTN}    timeout=${timeout}
    Click element    ${ADDPOLLCHOICEBTN}

addPollChoice
    [Arguments]    ${text}=extra
    Wait until element is enabled    ${POLLCHOICETEXTFIELD}    timeout=${timeout}
    Input text    ${POLLCHOICETEXTFIELD}    ${text}

clickCloseAlertBanner
    Wait until element is enabled    ${CLOSEICONONALERTBANNER}    timeout=${timeout}
    Click element    ${CLOSEICONONALERTBANNER}

clickPencilIconInHub
    Wait until element is enabled    ${PENCElNOPERMISSIONUSER}    timeout=${timeout}
    Click element    ${PENCElNOPERMISSIONUSER}

clickOKButtonInNoPermissionPopup
    Wait until element is enabled    ${OKPOPUPBTN}    timeout=${timeout}
    Click element    ${OKPOPUPBTN}

isHubDisplayPublicIcon
    Wait until element is visible    ${PUBLICHUBICON}    timeout=${timeout}

isHubDisplayPrivateIcon
    Wait until element is visible    ${PRIVATEHUBICON}    timeout=${timeout}

clickHubSetting
    Wait until element is enabled    ${HUBSETTING}    timeout=${timeout}
    Click element    ${HUBSETTING}

clickPublicRadio
    Wait until element is enabled    ${PUBLICRADIO}    timeout=${timeout}
    Click element    ${PUBLICRADIO}

clickPrivateRadio
    Wait until element is enabled    ${PRIVATERADIO}    timeout=${timeout}
    Click element    ${PRIVATERADIO}

clickConfirmChangeHubPermission
    Wait until element is enabled    ${CONFIRMCHANGEPUBLICTOPRIVATE}    timeout=${timeout}
    Click element    ${CONFIRMCHANGEPUBLICTOPRIVATE}

clickUpdateHub
    Wait until element is enabled    ${CREATENEWHUBBTN}    timeout=${timeout}
    Click element    ${CREATENEWHUBBTN}

clickUnFollowInsideHub
    Wait until element is enabled    ${UNFOLLOWINSIDEHUBBTN}    timeout=${timeout}
    Click element    ${UNFOLLOWINSIDEHUBBTN}
    Wait until element is enabled    ${CONFIRMEDITHUBBTN}    timeout=${timeout}
    Click element    ${CONFIRMEDITHUBBTN}

clickAddMoreUserToHub
    Wait until element is enabled    ${ADDMOREUSERTOHUBBTN}    timeout=${timeout}
    click element    ${ADDMOREUSERTOHUBBTN}

clickRemoveUserFromHub
    [Arguments]    ${text}
    Wait until element is enabled    //*[contains(@class, "UserName")][text()="${text}"]/following-sibling::button    timeout=${timeout}
    Click element    //*[contains(@class, "UserName")][text()="${text}"]/following-sibling::button

clearNotifyBannerInDiscoverPage
    ${clear}    Run Keyword And Return Status     Wait until element is visible    ${NOTIFYBANNERCLOSEBTN}    timeout=15s
    Run keyword If    ${clear}    Click element    ${NOTIFYBANNERCLOSEBTN}

    
#------------- verify -----------------------

isUserCanAccessFollowingHub
    [Arguments]    ${text}
    Wait until element is visible    ${HUBTITLEHEADER}    timeout=${timeout}
    Element should contain    ${HUBTITLEHEADER}    ${text}

isThreadCreatedInHub
    [Arguments]    ${text}
    Wait until element is visible    ${HUBTITLELIST}    timeout=${timeout}
    Element should contain    ${HUBTITLELIST}    ${text}

isThreadDeletedInHub
    [Arguments]    ${text}
    Wait until element is visible    ${FIRSTTHREADTITLEINHUB}    timeout=${timeout}
    Wait until element is not visible    //div[contains(@class, "Scroller")]/div/div[contains(@class, "ThreadItem")][1]//h4[text()="${text}"]    timeout=${timeout}
    # Element should not contain    ${FIRSTTHREADTITLEINHUB}    ${text}

isVoteResultDisplayCorrectly
    [Arguments]    ${index}=1
    Wait until element is visible    ${VOTERESULTFORFIRSTCHOICE}    timeout=${timeout}
    Element should contain    ${VOTERESULTFORFIRSTCHOICE}    ${index}

isCommentInHubDisplay
    [Arguments]    ${text}
    Wait until element is visible    ${COMMENTLOCATORINHUBTOPIC}    timeout=${timeout}
    Element should contain    ${COMMENTLOCATORINHUBTOPIC}    ${text}

isCommentInHubDeleted
    [Arguments]    ${text}
    Wait until element is not visible    //div[contains(@class, "CommentContainer")]//span[contains(@class, "TextBody")]/span[text()="${text}"]    timeout=${timeout}

isSingleImageInHubTopicRemoved
    Wait until element is not visible    ${SINGLEIMGINHUBTOPICDETAIL}    timeout=${timeout}

isCommentByImageDisplayInFirstComment
    Wait until element is visible    ${IMGINFIRSTCOMMENT}    timeout=${timeout}

isPollChoiceDisplay
    [Arguments]    ${text}=extra
    Wait until element is visible    //label[contains(@class, "PollRadio")]//span[text()="${text}"]    timeout=${timeout}

isMulitpleImageDisplayInTopic
    Wait until element is visible    ${MULTIPLEIMAGECONTAINER}    timeout=${timeout}

isAlertBannerDisplay
    Wait until element is visible    ${ALERTBANNER}    timeout=${timeout}

isNoPermissionToCreateTopicDisplay
    Wait until element is visible    ${VERIFYNOPERMISSIONCREATEPOST}    timeout=${timeout}

isVoteInsideTopicIDisplayNumber
    [Arguments]    ${text}
    Wait until element is visible    //*[contains(@class, "styles__DetailsContainer")]/button[contains(@class, "UpvoteButton")][text()="${text}"]

isVoteOutsideTopicIDisplayNumber
    [Arguments]    ${text}    ${vote}=1
    Wait until element is visible    //div[@title="${text}"]//button[text()="${vote}"]    timeout=${timeout}

isNotifyBannerDisplayCorrectForDeleteTopic
   [Arguments]    ${name}
   Element should contain    ${NOTIFYBANNERMESSAGE}    ${name}
   Element should contain    ${NOTIFYBANNERMESSAGE}     that you participated in has been removed
