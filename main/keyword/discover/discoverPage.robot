*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${HUBFOLLOWBTN}    //div[contains(@class, "HubContainer")]/div/div[1]//a[contains(@class, "HubFollowButton")]
${FOLLOWINGHUBSTATUS}    //div[contains(@class, "HubContainer")]/div/div[1]//*[@data-icon="check"]
${UNFOLLOWHUBSTATUS}    //div[contains(@class, "HubContainer")]/div/div[contains(@class, "Item")][1]//*[@data-icon="plus"]
${HUBTITLE}    //div[contains(@class, "HubContainer")]/div/div[1]//h2
${HUBITEMLIST}    //*[contains(@class, "HubContainer")]//div[contains(@class, "Item")]
${FIRSTHUBLOCATOR}    //div[contains(@class, "HubContainer")]/div/div[1]/a
${THIRDHUBLOCATOR}    //div[contains(@class, "HubContainer")]/div/div[3]/div/div[2]/a
${REQUIREFOLLOWHUBPOPUP}    //div[text()="Would you like to follow this hub in order to view it?"]
${CANCELPOPUPBTN}    //div[@class="ant-confirm-btns"]/button[1]
${CONFIRMPOPUPBTN}    //*[contains(@class,"ant-confirm-btns")]/button[2]
${TOPRANKUSER1ST}    //*[contains(@class,"styles__Item-")][1]
${LEADERBOARDCONTAINER}    //div[contains(@class, "Leaderboard")]/a
${NEWHUBBTN}    //*[@data-qa-anchor="new-hub-button"]
${AUTOFOLLOW}    isAutoManagedMember
${HUBNAMEINPUT}    name
${DESCRIPTIONINPUT}    description
${NOPERMISSIONHUB}    //*[contains(@class,"styles__Item")]//*[contains(text(),"secondHub")]
${DELETEHUB}    //*[contains(@class,"style__DeleteHub")]
${CONFIRMDELETEHUB}    //*[contains(@class,"ant-confirm-btns")]/button[2]
${PRIVATETAB}    //*[contains(@class,"antd3-tabs-nav-animated")]/div/div[2]
${SAVESETTINGBTN}    //div[@data-qa-anchor="discovery-modal-create-hub-form-button-create"]/button[2]
${VERIFYRANKDISPLAY}    //*[contains(@class,"styles__UserRankDetail")][1][text()=" Rank 1"]
${VERIFYPOINTDISPLAY}    //*[contains(text(),"points")]
${REQUIREDFOLLOWMESSAGE}    //div[@class="ant-confirm-content"][text()="Would you like to follow this hub in order to view it?"]
${CANCELREQUIREDFOLLOWBTN}    //div[@class="ant-confirm-btns"]/button[1]
${OKAYREQUIREDFOLLOWBTN}    //div[@class="ant-confirm-btns"]/button[2]
${ADDUSERINHUBBTN}    //*[contains(@class, "DefaultButton")]
${SEARCHUSERHUBTEXTFIELD}    //*[@data-qa-anchor="discovery-modal-create-hub-form-add-members"]//input
${CONFIRMADDUSERBTN}    //*[@data-qa-anchor="discovery-modal-create-hub-form-add-user-buttons"]/button[2]
${ADDMEMBERSBTNINDISCOVER}    //button[contains(@class, "AddMembers")]
${CREATENEWHUBBTNINMAINPAGE}    //*[@data-qa-anchor="new-hub-button"]

*** Keywords ***

clickFollowTheFirstHub
    Wait until element is enabled    ${HUBITEMLIST}    timeout=${timeout}
    ${follow}    Run Keyword And Return Status    Wait until page contains element    ${UNFOLLOWHUBSTATUS}    timeout=${timeout}
    Run Keyword If    ${follow}    click element    ${HUBFOLLOWBTN}

selectHubName
    [Arguments]    ${text}
    Wait until element is enabled    //*[contains(@class, "HubTitle")][text()="${text}"]    timeout=${timeout}
    Click element    //*[contains(@class, "HubTitle")][text()="${text}"]

clickFollowHubName
    [Arguments]    ${text}
    Wait until element is enabled    ${HUBITEMLIST}    timeout=${timeout}
    ${follow}    Run Keyword And Return Status    Wait until page contains element    //*[contains(@class, "HubTitle")][text()="${text}"]/parent::*/parent::*/following-sibling::div//*[@data-icon="plus"]    timeout=${timeout}
    Run Keyword If    ${follow}    click element    //*[contains(@class, "HubTitle")][text()="${text}"]/parent::*/parent::*/following-sibling::div//a[contains(@class, "Follow")]


clickOnTheFirstHub
    Wait until element is enabled    ${FIRSTHUBLOCATOR}    timeout=${timeout}
    Click element    ${FIRSTHUBLOCATOR}

clickUnFollowTheFirstHub
    Wait until element is enabled    ${FIRSTHUBLOCATOR}    timeout=${timeout}
    ${follow}    Run Keyword And Return Status    Wait until page contains element    ${FOLLOWINGHUBSTATUS}    timeout=${timeout}
    Run Keyword If    ${follow}    click element    ${HUBFOLLOWBTN}

clickUnFollowHubName
    [Arguments]    ${text}
    Wait until element is enabled    ${HUBITEMLIST}    timeout=${timeout}
    ${follow}    Run Keyword And Return Status    Wait until page contains element    //*[contains(@class, "HubTitle")][text()="${text}"]/parent::*/parent::*/following-sibling::div//*[@data-icon="check"]    timeout=${timeout}
    Run Keyword If    ${follow}    click element  //*[contains(@class, "HubTitle")][text()="${text}"]/parent::*/parent::*/following-sibling::div//a[contains(@class, "Follow")]

clickConfirmUnFollowHub
    Wait until element is enabled    ${OKAYREQUIREDFOLLOWBTN}    timeout=${timeout}
    Click element    ${OKAYREQUIREDFOLLOWBTN}

clickAddUserInPrivateHub
    Wait until element is enabled    ${ADDUSERINHUBBTN}    timeout=${timeout}
    Click element    ${ADDUSERINHUBBTN}

isGoInsideHub
    Wait until element is enabled    ${UNFOLLOWINSIDEHUBBTN}    timeout=${timeout}

clickCancelFollowHub
    Wait until element is enabled    ${CANCELPOPUPBTN}    timeout=${timeout}
    Click element    ${CANCELPOPUPBTN}

clickConfirmFollowHub
    Wait until element is enabled    ${CONFIRMPOPUPBTN}    timeout=${timeout}
    Click element    ${CONFIRMPOPUPBTN}

clickTopRankUser
    Wait until element is enabled    ${TOPRANKUSER1ST}    timeout=${timeout}
    Click element    ${TOPRANKUSER1ST}

clickAddNewHub
    Wait until element is enabled    ${NEWHUBBTN}    timeout=${timeout}
    Click element    ${NEWHUBBTN}

clickAutoFollow
    Wait until element is enabled    ${AUTOFOLLOW}    timeout=${timeout}
    Click element    ${AUTOFOLLOW}

clickCreatenewHub
    Wait until element is enabled    ${CREATENEWHUBBTN}    timeout=${timeout}
    Click element    ${CREATENEWHUBBTN}

inputHubName
    [Arguments]    ${text}
    Wait until element is enabled    ${HUBNAMEINPUT}
    Input text    ${HUBNAMEINPUT}    ${text}

inputDescriptionName
    [Arguments]    ${text}
    Wait until element is enabled    ${DESCRIPTIONINPUT}
    Input text    ${DESCRIPTIONINPUT}    ${text}

clickDeleteHub
    Wait until element is enabled    ${DELETEHUB}    timeout=${timeout}
    Click element    ${DELETEHUB}

clickConfirmDeleteHub
    Wait until element is enabled    ${CONFIRMDELETEHUB}    timeout=${timeout}
    Click element    ${CONFIRMDELETEHUB}

clickPrivateTab
    Wait until element is enabled    ${PRIVATETAB}    timeout=${timeout}
    Click element    ${PRIVATETAB}

clickNoPermissionHub
    Wait until element is enabled    ${NOPERMISSIONHUB}    timeout=${timeout}
    Click element    ${NOPERMISSIONHUB}
 
clickSaveSetting
    Wait until element is enabled    ${SAVESETTINGBTN}    timeout=${timeout}
    Click element    ${SAVESETTINGBTN}

clickCancelRequiredFollowPopUp
    Wait until element is enabled    ${CANCELREQUIREDFOLLOWBTN}    timeout=${timeout}
    Click element    ${CANCELREQUIREDFOLLOWBTN}

clickOpenAddMemberListPopupInDiscover
    Wait until element is enabled    ${ADDMEMBERSBTNINDISCOVER}    timeout=${timeout}
    Click element    ${ADDMEMBERSBTNINDISCOVER}

addUserToHub
    [Arguments]    ${text}
    inputKeywordForSearchUserToAddInHub  ${text}
    addUserToHubFromSearch

inputKeywordForSearchUserToAddInHub
    [Arguments]    ${text}
    Wait until element is enabled    ${SEARCHUSERHUBTEXTFIELD}    timeout=${timeout}
    Input text    ${SEARCHUSERHUBTEXTFIELD}    ${text}

addUserToHubFromSearch
    [Arguments]    ${text}=1
    :FOR  ${index}  IN RANGE  ${text}
    \    Click element    //*[contains(@class, "Scroller")]/div/div[contains(@class, "Picker")][${index}+1]

clickConfirmAddUserToHub
    Wait until element is enabled    ${CONFIRMADDUSERBTN}    timeout=${timeout}
    Click element    ${CONFIRMADDUSERBTN}

editHubnameByClearOldText
    [Arguments]    ${text}
    Wait until element is visible     ${HUBNAMEINPUT}    timeout=${timeout}  
    Input Text    ${HUBNAMEINPUT}    ${EMPTY}
    Wait until element is enabled    ${HUBNAMEINPUT}    timeout=${timeout}
    Press keys    ${HUBNAMEINPUT}    ${text}

editDescriptionByClearOldText
    [Arguments]    ${text}
    Wait until element is visible     ${DESCRIPTIONINPUT}    timeout=${timeout}  
    Input Text    ${DESCRIPTIONINPUT}    ${EMPTY}
    Wait until element is enabled    ${DESCRIPTIONINPUT}    timeout=${timeout}
    Press keys    ${DESCRIPTIONINPUT}    ${text}

#---------------------------------- Verify -------------------------------------------

isRequireFollowPopUpDisplay
    Wait until element is visible    ${REQUIREFOLLOWHUBPOPUP}    timeout=${timeout}

isLeaderBoardDisplay
    ${count}    get element count    ${LEADERBOARDCONTAINER}
    :FOR  ${index}  IN RANGE  ${count}
    \    Wait until element is visible    //div[contains(@class, "Leaderboard")]/a[${index}+1]    timeout=${timeout}

isPointUserDisplay
    Wait until element is visible    ${VERIFYRANKDISPLAY}    timeout=${timeout}
    Wait until element is visible    ${VERIFYPOINTDISPLAY}    timeout=${timeout}

isHubNameUpdated
    [Arguments]    ${new}    ${old}
    Wait until element is visible    //*[contains(@class,"HubContainer")]//*[contains(text(),"${new}")]    timeout=${timeout}
    Wait until element is not visible   //*[contains(@class,"HubContainer")]//*[contains(text(),"${old}")]    timeout=${timeout}

isHubDisplay
    [Arguments]    ${new}
    Wait until element is visible    //*[contains(@class,"HubContainer")]//*[contains(text(),"${new}")]    timeout=${timeout}

isHubNotDisplay
    [Arguments]    ${new}
    Wait until element is not visible    //*[contains(@class,"HubContainer")]//*[contains(text(),"${new}")]    timeout=${timeout}

isHubDeleted
    [Arguments]    ${text}
    Wait until element is not visible   //*[contains(@class,"HubContainer")]//*[contains(text(),"${text}")]    timeout=${timeout}

isUserNotAbleToOpenHub
    Wait until element is visible    ${REQUIREDFOLLOWMESSAGE}    timeout=${timeout}
    
isEditHubName
    [Arguments]    ${text}
    Wait until element is visible    //*[contains(@class,"HubContainer")]//*[contains(text(),"${text}")]    timeout=${timeout}
    
isEditDescription
    [Arguments]    ${text}
    Wait until element is visible    //*[contains(@class,"HubContainer")]//*[contains(text(),"${text}")]    timeout=${timeout}

isHubTitleDisplay
    [Arguments]    ${text}
    Wait until element is visible    //*[contains(@class,"styles__FlexHeaderLeft")]//*[contains(text(),"${text}")]    timeout=${timeout}

isCreteNewHubNotDisplay
    Wait until element is not visible    ${CREATENEWHUBBTNINMAINPAGE}    timeout=${timeout}
