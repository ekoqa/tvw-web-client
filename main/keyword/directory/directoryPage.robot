*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***
${INVITETEAMMATINPEOPLEEBTN}    //*[contains(@class,"styles__WrapContent")]/button
${VERIFYFAVORITEICON}    //*[contains(@class,"is-favorite")]
${FIRSTUSERINDIRECTORY}    //*[contains(@class,"styles__ItemNav")][1]
${VERIFYACTIVEBTN}    //*[contains(@class,"ant-select-selection")]//*[contains(text(),'Active')]
${VERIFALLBTN}    //*[contains(@class,"ant-select-selection")]//*[contains(text(),'All')]
${VERIFFAVORITEBTN}    //*[contains(@class,"ant-select-selection")]//*[contains(text(),'Favorites')]
${FIRSTONLINEUSER}    //*[contains(@class,"styles__ItemNav")][1]//*[contains(@class,"styles__OnlineIndicator")]
${USERDIRECTORYDROPDOWN}    //div[@data-qa-anchor="dropdown-filter"]
${ALLUSERSELECTTIONINDIRECTORY}    //span[@data-qa-anchor="dropdown-filter-item-all"][text()="All"]


*** Keywords ***

clickinviteTeamMateInPeoplePage           
    Wait until element is enabled    ${INVITETEAMMATINPEOPLEEBTN}    timeout=${timeout}
    Click element    ${INVITETEAMMATINPEOPLEEBTN}

selectFirstUser
    Wait until element is enabled    ${FIRSTUSERINDIRECTORY}    timeout=${timeout}
    Click element    ${FIRSTUSERINDIRECTORY}

clickOpenAllUserInDirectory
    Wait until element is enabled    ${USERDIRECTORYDROPDOWN}    timeout=${timeout}
    Click element    ${USERDIRECTORYDROPDOWN}
    Wait until element is enabled    ${ALLUSERSELECTTIONINDIRECTORY}    timeout=${timeout}
    Click element    ${ALLUSERSELECTTIONINDIRECTORY}

#---------------------------------------------------------------------

isContactDisplay
    [Arguments]    ${text}
    Wait until element is enabled    //div[contains(@data-qa-anchor, "user")]//h4[contains(text(), "${text}")]

isContactNotDisplay
    [Arguments]    ${text}
    Wait until element is not visible    //div[contains(@data-qa-anchor, "user")]//h4[contains(text(), "${text}")]

isDirectoryShowOnlineUser
    Wait until element is visible    ${VERIFYACTIVEBTN}    timeout=${timeout} 
    Wait until element is not visible    ${VERIFALLBTN}    timeout=${timeout} 
    Wait until element is not visible    ${VERIFFAVORITEBTN}    timeout=${timeout} 

isHaveOnlineUser   
    Wait until element is visible    ${FIRSTONLINEUSER}    timeout=${timeout}

isUserDirectoryDisplayRightSequence
    ${index}    evaluate    1
    FOR  ${element}  IN  banner1 web  banner2 web  banner3 web  banner4 web  banner5 web
        Wait until page contains element    //div[@data-qa-anchor="directory-separator-B"]/following-sibling::div[${index}+1][@title="${element}"]    timeout=${timeout}
        ${index}    evaluate    ${index}+1
    END
