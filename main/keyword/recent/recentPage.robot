*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot
Resource                   ../../keyword/directory/directoryPage.robot

*** Variable ***

${CREATEFROMRECENTBTN}    //button[@data-qa-anchor="recent-dropdown-create-chat"]
${CREATEDM}    //ul[contains(@class, "Option")]/li[1]/a
${CREATEGC}    //ul[contains(@class, "Option")]/li[2]/a
${CREATEFORM}    //ul[contains(@class, "Option")]/li[5]/a
${CREATECARDFROMRECENT}   //ul[contains(@class, "Option")]/li[3]/a
${CREATETASKBTN}    //ul[contains(@class, "Option")]/li[6]/a
${CREATECONFERENCEBTN}    //ul[contains(@class, "Option")]/li[7]/a
${USERSELECTION}    //div[text()="Contacts"]
${NEXTBTN}    //span[text()="Next"]/parent::*
${CREATEBTN}    //span[text()="Create"]/parent::*
${SEARCHBOX}    //input[@data-qa-anchor="search-box"]
${SEARCHBYMESSAGERESULT}    //div[contains(@class, "SearchResult")][2]/following-sibling::div[1]/a
${VIEWTARGET}    //div[contains(@class, "ItemNav")][10]
${SEARCHUSERFIELDINPOPUP}    //div[contains(@class, "SearchBar")]//input[@data-qa-anchor="search-box"]
${CREATECHATSEARCHRESULT}    //div[contains(@class, "ListItem")][1]
${GROUPTYPESWITCHBTN}    //div[contains(@class, "WrapGroupInfo")]//span[@class="ant-switch-inner"]
${GCNAMEFIELD}    //input[@id="name"]
${CLOSEBANNERBTN}    //button[@class="antd3-modal-close"]
${BANNERLOCATION}    //div[@class="slick-slide slick-active slick-current"]
${NEXTARROWBANNERBTN}    //button[@class="slick-arrow\ slick-next"]
${USERPROFILEIMAGE}    //header//a
${TASKUNREADBADGE}    //a[@data-qa-anchor="appnav-menu-tasks"]//sup[@data-show="true"]//p[@class="current"]
${CONFERENCEROOMNAMETITLEFIELD}    roomName
${CONFIRMCREATECONFERENCEBTN}    //div[contains(@class, "WrapAction")]/button[2]
${FILTERCHATTYPEICON}    //button[contains(@class, "IconButton")]
${ARCHIVEDCHATMENU}    //li[text()="Archived Chats"]
${ACTIVECHATMENU}    //li[text()="Active Chats"]
${EMPTYCHATICON}    //p[contains(text(), "Please start a chat")]
${INVITETEAMMATINRECENTBTN}    //*[contains(@class,"styles__HighlightedText")]
${VERIFYINVITETEAMMATINRECENTBTN}    //*[contains(@class,"styles__HighlightedText")]
${EMILYBOT}    //span[contains(@class, "styles__WrapTitle")][text()="Emily"]
${EDDIEBOT}    //span[contains(@class, "styles__WrapTitle")][text()="Eddie"]
${RECENTUNREADBADGE}    //a[@data-qa-anchor="appnav-menu-recents"]//sup[@data-show="true"]//p[@class="current"]
${VERIFYSEEUSER}    //div[contains(@class, "ItemNav")][1]
${VERIFYONLYUSERMESSAGE}    //*[contains(text(),'Say Hi')]
${CLOSCONTENTBANNERBTN}    //*[@data-icon="close"]
${DIRECTORYDROPDOWNBTN}    //div[contains(@class, "ant-select-selection-selected-value")]
${ALLUSERBTN}    //span[@data-qa-anchor="dropdown-filter-item-all"]

*** Keywords ***

createDCFromRecent
    Wait until element is enabled    ${CREATEFROMRECENTBTN}    timeout=${timeout}
    Click element    ${CREATEFROMRECENTBTN}
    Click element    ${CREATEDM}

createGCFromRecent
    Wait until element is enabled    ${CREATEFROMRECENTBTN}    timeout=${timeout}
    Click element    ${CREATEFROMRECENTBTN}
    Click element    ${CREATEGC}

createTaskFromRecent
    Wait until element is enabled    ${CREATEFROMRECENTBTN}    timeout=${timeout}
    Click element    ${CREATEFROMRECENTBTN}
    Click element    ${CREATETASKBTN}

createFormInRecent
    Wait until element is enabled    ${CREATEFROMRECENTBTN}    timeout=${timeout}
    Click element    ${CREATEFROMRECENTBTN}
    Click element    ${CREATEFORM}

createConferenceFromRecent
    Wait until element is enabled    ${CREATEFROMRECENTBTN}    timeout=${timeout}
    Click element    ${CREATEFROMRECENTBTN}
    Click element    ${CREATECONFERENCEBTN}

selectUserByIndex
    [Arguments]    ${index}
    Wait until element is visible    ${USERSELECTION}    timeout=${timeout}
    Click element    //div[text()="Contacts"]/following-sibling::div[${index}]

selectUserFromSearchToCreateChat
    Wait until element is enabled    ${CREATECHATSEARCHRESULT}    timeout=${timeout}
    Click element    ${CREATECHATSEARCHRESULT}

confirmSelectUser
    Wait until element is visible    ${NEXTBTN}    timeout=${timeout}
    Click element    ${NEXTBTN}

confirmCreateChat
    Wait until element is enabled    ${CREATEBTN}    timeout=${timeout}
    sleep    2s
    Click element    ${CREATEBTN}
    Wait until element is not visible    ${CREATEBTN}    timeout=${timeout}

# selectChatroomByTitle
#    [Arguments]   ${title}
#    Wait until element is enabled    //div[@title="${title}"]    timeout=${timeout}
#    Click element    //div[@title="${title}"]

selectChatroomByTitle
   [Arguments]   ${text}
   Wait until element is enabled    //span[contains(@class, "styles__WrapTitle")][text()="${text}"]     timeout=${timeout}
   Click element    //span[contains(@class, "styles__WrapTitle")][text()="${text}"]

selectChatroomByIndex
    [Arguments]    ${index}
    Wait until element is enabled    //div[contains(@class, "Container")]//div[contains(@class, "ItemNav")][${index}]/a    timeout=${timeout}
    Click element    //div[contains(@class, "Container")]//div[contains(@class, "ItemNav")][${index}]/a
    ${pass}  Run Keyword And Return Status  Wait until element is not visible    ${EMPTYCHATICON}    timeout=${timeout}
    Run Keyword Unless    ${pass}    Click element    //div[contains(@class, "Container")]//div[contains(@class, "ItemNav")][${index}]/a

selectTopicByIndex
    [Arguments]    ${index}
    Wait until element is enabled    //div[contains(@class, "styles__TopicItem")][${index}]/a    timeout=${timeout}
    Click element    //div[contains(@class, "styles__TopicItem")][${index}]/a
    
selectChatroomFromSearchByMSG
    Sleep    5s
    Wait until element is enabled    ${SEARCHBYMESSAGERESULT}    timeout=${timeout}
    Click element    ${SEARCHBYMESSAGERESULT}

searchUserInCreateChatSearchBox
    [Arguments]    ${text}
    Wait until element is enabled    ${SEARCHUSERFIELDINPOPUP}
    Input text    ${SEARCHUSERFIELDINPOPUP}    ${text}

inputElasticSearch
    [Arguments]    ${text}
    Wait until element is enabled    ${SEARCHBOX}    timeout=${timeout}
    Input text    ${SEARCHBOX}    ${text}

scrolldownInSearchResult
    Scroll element into view    ${VIEWTARGET}

scrolldownToLastContact
    Scroll element into view    //div[contains(@class, "ItemNav")][last()]

changeGCtype
    Click element    ${GROUPTYPESWITCHBTN}

setGCname
    [Arguments]    ${text}
    Input text    ${GCNAMEFIELD}    ${text}

clickOpenBannerBroadcast
    [Arguments]    ${text}
    :FOR  ${index}  IN RANGE  3
    \    Wait until element is visible    //div[contains(@class, "slick-active")]//h3[@class="banner_title"][text()="${text}"]    timeout=60s   error=Cannot see the banner
    \    ${status}    Run keyword and Return Status    Click element    //div[contains(@class, "slick-active")]//h3[@class="banner_title"][text()="${text}"]
    \    ${open}    Run Keyword And Return Status    Wait until element is visible    //div[contains(@id, "rcDialogTitle")][text()="${text}"]    timeout=${timeout}
    \    Run Keyword If    ${open}    Exit For Loop

clickCloseBannerBroadcast
    Wait until element is enabled    ${CLOSEBANNERBTN}    timeout=${timeout}
    Click element    ${CLOSEBANNERBTN}
    Wait until element is not visible    ${CLOSEBANNERBTN}    timeout=${timeout}    error=Cannot close the content banner

createCardFromRecent
    Wait until element is enabled    ${CREATEFROMRECENTBTN}    timeout=${timeout}
    Click element    ${CREATEFROMRECENTBTN}
    Wait until element is enabled    ${CREATECARDFROMRECENT}    timeout=${timeout}
    Click element    ${CREATECARDFROMRECENT}

clickToSeeUserProfile
    Wait until element is enabled    ${USERPROFILEIMAGE}    timeout=${timeout}
    Click element    ${USERPROFILEIMAGE}

setConferenceRoomName
    [Arguments]    ${text}
    Wait until element is enabled    ${CONFERENCEROOMNAMETITLEFIELD}    timeout=${timeout}
    Input text    ${CONFERENCEROOMNAMETITLEFIELD}    ${text}

confirmCreateConferenceRoom
    Wait until element is enabled    ${CONFIRMCREATECONFERENCEBTN}    timeout=${timeout}
    Click element    ${CONFIRMCREATECONFERENCEBTN}

clickFilterChatStatus
    Wait until element is enabled    ${FILTERCHATTYPEICON}    timeout=${timeout}
    Click element    ${FILTERCHATTYPEICON}

selectArchivedChatStatus
    Wait until element is enabled    ${ARCHIVEDCHATMENU}    timeout=${timeout}
    Click element    ${ARCHIVEDCHATMENU}

selectActiveChatStatus
    Wait until element is enabled    ${ACTIVECHATMENU}    timeout=${timeout}
    Click element    ${ACTIVECHATMENU}

clickinviteTeamMateInRecentPage
    Wait until element is enabled    ${INVITETEAMMATINRECENTBTN}    timeout=${timeout}
    Click element    ${INVITETEAMMATINRECENTBTN}

clickOnSelectedBanner
    [Arguments]    ${text}
    Wait until page contains element    //div[contains(@class, "slick-active")]//h3[@class="banner_title"][text()="${text}"]    timeout=60s    error=Cannot see the banner
    Click element    //div[contains(@class, "slick-active")]//h3[@class="banner_title"][text()="${text}"]

selectTabURL
    [Arguments]    ${URL}
    @{list}    get window handles
    FOR  ${window}  IN  @{list}
       Select window    ${window}
       ${location}    get location    
       Exit For Loop If     '${location}'=='${URL}'
    END

clickCloseBannerContentPopUp    
    Click element    ${CLOSCONTENTBANNERBTN}

filterAllUser  
    Wait until element is enabled    ${DIRECTORYDROPDOWNBTN}    timeout=${timeout}
    Click element    ${DIRECTORYDROPDOWNBTN}
    Wait until element is enabled    ${ALLUSERBTN}    timeout=${timeout}
    Click element    ${ALLUSERBTN}
    
#----------------------------------------------------------------------------------------------

isSearchForChatDisplay
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "ItemNav")][1]//mark[text()="${text}"]    timeout=${timeout}

isSearchForMessageDisplay
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "ItemNav")][1]//mark[text()="${text}"]    timeout=${timeout}

isSearchUserDisplay
    Wait until element is visible    ${VERIFYSEEUSER}    timeout=${timeout}

isSearchFavoriteUserDisplay
    [Arguments]    ${text}
    Wait until element is visible    ${VERIFYSEEUSER}    timeout=${timeout}
    Wait until element is visible    ${VERIFYFAVORITEICON}    timeout=${timeout}

isAbleToScrollDown
    Wait until element is visible    ${VIEWTARGET}    timeout=${timeout}

isChatroomUnArchive
    [Arguments]    ${text}
    Wait until element is visible    //span[contains(@class, "Title")][text()="${text}"]    timeout=${timeout}

isChatroomDisappear
    [Arguments]    ${text}
    Page should not contain element    //span[contains(@class, "Title")][text()="${text}"]

isChatroomStillAppear
    [Arguments]    ${text}
    Page should contain element    //span[contains(@class, "Title")][text()="${text}"]

isBotIconDisplayOnUserAvatar
    [Arguments]    ${text}
    Page should contain element    //div[@title="${text}"]//div[contains(@class, "BadgeIcon")]

isBannerDisappear
    [Arguments]    ${text}
    ${reload}  Run Keyword And Return Status  Wait until element is not visible    //div[contains(@class, "slick-slide")]//h3[@class="banner_title"][text()="${text}"]    timeout=${timeout}
    Run Keyword Unless    ${reload}    Reload Page
    Wait until element is not visible    //div[contains(@class, "slick-slide")]//h3[@class="banner_title"][text()="${text}"]    timeout=${timeout}

isBannerDisplayOnRecentPage
    [Arguments]    ${text}
    ${reload}  Run Keyword And Return Status  Wait until page contains element    //div[contains(@class, "slick-active")]//h3[@class="banner_title"][text()="${text}"]    timeout=60s    error=Cannot see the banner
    Run Keyword Unless    ${reload}    Reload Page
    Wait until page contains element    //div[contains(@class, "slick-active")]//h3[@class="banner_title"][text()="${text}"]    timeout=60s    error=Cannot see the banner

isContentBannerDisplayCorrectly
    [Arguments]    ${text}
    Wait until element is visible    //div[@class="ql-editor"]/p[contains(text(), "${text}")]    timeout=${timeout}    error=The content is wrong

isBannerDisplayCorrectOrder
    [Arguments]    @{name}
    FOR  ${element}  IN  @{name}
        Wait until page contains element    //div[contains(@class, "slick-active")]//h3[@class="banner_title"][text()="${element}"]    timeout=30s    error=Cannot see the banner
    END
    
isTaskUnreadBadgeDisplay
    [Arguments]    ${index}=1
    Wait until element is visible    ${TASKUNREADBADGE}    timeout=${timeout}
    Element should contain    ${TASKUNREADBADGE}  ${index}

isNewTabOpen
    [Arguments]    ${current_window}    ${new_window_list}
    Should Not Be Equal      ${current_window}    ${new_window_list}    msg=Cannot open a new tab

isUserIconAsDeletedUSer
    [Arguments]    ${firstname}
    ${pass}    Run Keyword And Return Status    Wait until element is visible    //span[text()="${firstname}"]/parent::*/parent::*/preceding-sibling::div[1]//div[contains(@style, "${deleteUserIcon}")]    timeout=${timeout}
    Run Keyword unless    ${pass}    Reload Page
    Wait until element is visible    //span[text()="${firstname}"]/parent::*/parent::*/preceding-sibling::div[1]//div[contains(@style, "${deleteUserIcon}")]    timeout=${timeout}

isLogInSuccess
    Wait until element is enabled    ${RECENTMENU}    timeout=${timeout}

isChatroomDisplayOnRecent
    [Arguments]    ${text}
    Wait until element is visible    //span[contains(@class, "styles__WrapTitle")][text()="${text}"]    timeout=${timeout}

isNetworkNotHaveOnlyOne
    Wait until element is not visible    ${VERIFYINVITETEAMMATINRECENTBTN}    timeout=${timeout}

isOnlyEmilyBotDisplayed
    Wait until element is visible    ${EMILYBOT}    timeout=${timeout}
    Page should not contain element    ${EDDIEBOT}

isRecentPageUnreadBadgeDisplay
    [Arguments]    ${index}=1
    Wait until element is visible    ${RECENTUNREADBADGE}    timeout=${timeout}
    Element should contain    ${RECENTUNREADBADGE}  ${index}

isShowUserOnlineMessage
    Wait until element is visible    ${VERIFYONLYUSERMESSAGE}    timeout=${timeout}

isChatroomUnreadBadgeDisappear
    [Arguments]    ${text}
    Wait until element is not visible    //span[text()="${text}]"]/parent::*/following-sibling::div[1]//*[@class="ant-scroll-number ant-badge-count"]    timeout=${timeout}

isAppReDirectToCorrectURLWhenClickOnBanner
    [Arguments]    ${URL}
    @{list}    get window handles
    FOR  ${window}  IN  @{list}
       Select window    ${window}
    END
    ${location}    get location    
    Run Keyword if     '${location}'!='${URL}'    fail

isContentBannerPopUpDisplayCorrectly
    [Arguments]    ${text}
    Wait until element is visible    //p[text()="${text}"]    timeout=${timeout}

isUserDisplayInRecentPage
    [Arguments]    ${text}
    # Wait until element is visible    //div[contains(@class, "styles__UserAvatar")]//*[contains(text(), "${text}")]    timeout=${timeout}
    Wait until element is visible    //a[@data-qa-anchor="appbar-user-profile"]//*[contains(text(), "${text}")] 
    
isSearchUserDisplaybyName
    [Arguments]    ${text}
    Wait until element is visible    //h4[contains(@class, "styles__Title")][text()="${text}"]