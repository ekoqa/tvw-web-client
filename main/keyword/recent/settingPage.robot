*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${LANGUAGEANDTEXTSETTINGFORADMINUSER}    //ul[contains(@class, "SettingMenu")]/li[4]
${LANGUAGEANDTEXTSETTINGFORNORMALUSER}    //ul[contains(@class, "SettingMenu")]/li[3]
${LANGUAGEDROPDOWN}    //div[@class="ant-modal-body"]/div[1]//div[contains(@class, "selection--single")]
${CHANGETOTHAILANGUAGEBTN}    //ul[contains(@class, "dropdown")]/li[text()="Thai"]
${SAVEGLOBALSETTINGBTN}    //div[@class="ant-modal-footer"]/button
${MARKALLASREADBTN}    //ul[contains(@class, "dropdown")]/li[text()="Mark all as read"]
${CHANGEPASSWORDBTN}    //*[contains(@class,"styles__SettingMenu-")]//*[contains(@class,"styles__MenuItem-")]/*[contains(text(), "Password")]  #//*[contains(@class,"styles__SettingMenu-")]//*[contains(@class,"styles__MenuItem-")][5]
${SAVEEDITPASSWORDBTN}    //div[@class="antd3-modal-footer"]/button[2]
${CURRENTPASSWORDINPUT}    currentPassword
${NEWPASSWORDINPUT}    //*[@id="newPassword"]
${CONFIRMNEWPASSWORDINPUT}    //*[@id="confirmPassword"]
${ADMINPANELBTN}    //*[contains(@class,"styles__SettingMenu-")]//*[contains(@class,"styles__MenuItem-")]/*[contains(text(), "Admin panel")]

*** Keywords ***

clickSettingButton
    Wait until element is enabled    ${SETTINGBTN}    timeout=${timeout}
    Click element    ${SETTINGBTN}

selectLanguageAndTextForAdmin
    Wait until element is enabled    ${LANGUAGEANDTEXTSETTINGFORADMINUSER}    timeout=${timeout}
    Click element    ${LANGUAGEANDTEXTSETTINGFORADMINUSER}

selectLanguageAndTextForUser
    Wait until element is enabled    ${LANGUAGEANDTEXTSETTINGFORNORMALUSER}    timeout=${timeout}
    Click element    ${LANGUAGEANDTEXTSETTINGFORNORMALUSER}

changeToThaiLanguage
    Click element    ${LANGUAGEDROPDOWN}
    Wait until element is enabled    ${CHANGETOTHAILANGUAGEBTN}    timeout=${timeout}
    Click element    ${CHANGETOTHAILANGUAGEBTN}

confirmSettingChange
    Wait until element is enabled    ${SAVEGLOBALSETTINGBTN}    timeout=${timeout}
    Click element    ${SAVEGLOBALSETTINGBTN}

clickMarkAllAsRead
    ${unread}    Run Keyword And Return Status    Wait until element is visible    ${MARKALLASREADBTN}
    Run Keyword If  ${unread}  Click element  ${MARKALLASREADBTN}   

clickChangePasswordButton
    Wait until element is enabled   ${CHANGEPASSWORDBTN}    timeout=${timeout}
    Click element    ${CHANGEPASSWORDBTN}

clickSaveEditPasswordButton
    Wait until element is enabled   ${SAVEEDITPASSWORDBTN}    timeout=${timeout}
    Click element    ${SAVEEDITPASSWORDBTN}
    
inputCurrentPassword
    [Arguments]    ${text}
    Wait until element is enabled   ${CURRENTPASSWORDINPUT}    timeout=${timeout}
    Click element    ${CURRENTPASSWORDINPUT}
    Input Text    ${CURRENTPASSWORDINPUT}    ${text} 

inputNewPassword
    [Arguments]    ${text}
    Wait until element is enabled   ${NEWPASSWORDINPUT}    timeout=${timeout}
    Click element    ${NEWPASSWORDINPUT}
    Input Text    ${NEWPASSWORDINPUT}    ${text}

inputConfirmNewPassword
    [Arguments]    ${text}
    Wait until element is enabled   ${CONFIRMNEWPASSWORDINPUT}    timeout=${timeout}
    Click element    ${CONFIRMNEWPASSWORDINPUT}
    Input Text    ${CONFIRMNEWPASSWORDINPUT}    ${text}

isAdminpanelDisplay
    Wait until element is enabled   ${ADMINPANELBTN}    timeout=${timeout}