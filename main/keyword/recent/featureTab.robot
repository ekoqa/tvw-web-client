*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${RECENTMENU}    //li[@role="menuitem"]/a[@data-qa-anchor="appnav-menu-recents"]
${DIRECTTORYMENU}    //a[@data-qa-anchor="appnav-menu-directory"]
${CARDMENU}    //li[@role="menuitem"]/a[@href="/cards"]
${DISCOVERMENU}    //li[@role="menuitem"]/a[@data-qa-anchor="appnav-menu-hubs"]
${FORMMENU}    //li[@role="menuitem"]/a[@data-qa-anchor="appnav-menu-forms"]
${TASKMENU}    //li[@role="menuitem"]/a[@data-qa-anchor="appnav-menu-tasks"]
${PORTALMENU}    //a[@data-qa-anchor="appnav-menu-portal"]
${PORTALIFRAME}    //iframe[@title="Webview"]
${LIBRARYMENU}    //a[@data-qa-anchor="appnav-menu-library"]
${LIBRARYIFRAME}  //iframe[@title="Knowledge Base"]
${CONFERENCEMENU}    //li[@role="menuitem"]/a[@data-qa-anchor="appnav-menu-conferences"]
${VROOMIFRAME}    //iframe[@src="https://vroom.truevirtualworld.com"]
${JITSIIFRMAE}    //iframe[@src="https://meet.jit.si/"]
${CONFERENCETITLEIFRAME}    //iframe[@title="Conferences"]
${WORKFLOWMENU}    //li[@role="menuitem"]/a[@data-qa-anchor="appnav-menu-workflows"]

*** Keywords ***

selectRecentMenu
    Wait until element is enabled    ${RECENTMENU}    timeout=${timeout}
    Click element    ${RECENTMENU}

selectDirectoryMenu
    Wait until element is enabled    ${DIRECTTORYMENU}    timeout=${timeout}
    Click element    ${DIRECTTORYMENU}    

selectCardMenu
    Wait until element is enabled    ${CARDMENU}    timeout=${timeout}
    Click element    ${CARDMENU}

selectDiscoverMenu
    Wait until element is enabled    ${DISCOVERMENU}    timeout=${timeout}
    Click element    ${DISCOVERMENU}

selectFormMenu
    Wait until element is enabled    ${FORMMENU}    timeout=${timeout}
    Click element    ${FORMMENU}

selectTaskMenu
    Wait until element is enabled    ${TASKMENU}    timeout=${timeout}
    Click element    ${TASKMENU}

selectPortalMenu
    Wait until element is enabled    ${PORTALMENU}    timeout=${timeout}
    Click element    ${PORTALMENU}
    Select Frame    ${PORTALIFRAME}

selectLibraryMenu
    Wait until element is enabled    ${LIBRARYMENU}    timeout=${timeout}
    Click element    ${LIBRARYMENU}
    Select Frame    ${LIBRARYIFRAME}

selectWorkflowMenu
    Wait until element is enabled    ${WORKFLOWMENU}    timeout=${timeout}
    Click element    ${WORKFLOWMENU}
    
selectConferenceMenu
    # [Arguments]    ${iframe}=${VROOMIFRAME}
    Wait until element is enabled    ${CONFERENCEMENU}    timeout=${timeout}
    Click element    ${CONFERENCEMENU}
    Select Frame    ${CONFERENCETITLEIFRAME}

selectLibraryFrame
    Wait until element is enabled    ${LIBRARYIFRAME}    timeout=${timeout}
    Select Frame    ${LIBRARYIFRAME}

#--------------------------- verify ----------------------------------

isConferenceMenuDisplayOnSideBar
    Wait until element is visible    ${CONFERENCEMENU}    timeout=${timeout}