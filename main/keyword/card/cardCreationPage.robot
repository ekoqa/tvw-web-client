*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***
${CARDTEXTFIELD}    //div[contains(@class, "EditorArea")]//div[@class="DraftEditor-editorContainer"]    #/div/div/div/div/span
${SENDCARDINCHATBTN}    //div[contains(@class, "WrapToolbar")]//button[@class="ant-btn ant-btn-primary"]
${SETCARDPRIORITYBTN}    //span[@id="cardDatePicker"]/preceding-sibling::button[1]
${ADDUSERINCARDBTN}    //span[@id="cardDatePicker"]/following-sibling::button[1]
${CARDHISTORYLOGBTN}    //span[@id="cardDatePicker"]/following-sibling::button[2]
${COMMENTTABBTNONEDITORVIEW}    //span[@id="cardDatePicker"]/following-sibling::button[3]
${COMMENTTABBTNONVIEWERVIEW}    //span[@id="cardDatePicker"]/following-sibling::button[2]
${SHARECARDTOFAVORITELIST}    //div[text()="Favorites"]/following-sibling::div[@role="listbox"][1]
${SHARECARDTOUSERCONTACTLIST}    //div[contains(@class, "Scroller")]/div/section[2]/div[@role="listbox"][1]
${ADDSHARECARDUSERBTN}    //div[contains(@class, "UserContainer")]//button[2]
${ADDMOREUSERINCARDBTN}    //div[contains(@class, "SharedContainer")]//button[contains(@class, "Add")]
${CANEDITROLEBTN}    //ul[@role="menu"]/li[text()="Can edit"]
${CANVIEWROLEBTN}    //ul[@role="menu"]/li[text()="Can view"]
${REMOVEUSERFROMCARDBTN}    //ul[@role="menu"]/li[text()="Remove"]
${CONFIRMEDITUSERINCARDBTN}    //div[contains(@class, "SharedContainer")]/div[3]/button
${CARDROLEDROPDOWNBTN}    //span[@class="Select-arrow-zone"]
${CANEDITCARDROLEBTN}    //ul[contains(@class, "dropdown")]/li[1]
${CANVIEWCARDONLYBTN}    //ul[contains(@class, "dropdown")]/li[2]
${CREATEDCARDLOG}    //p[contains(@class, "Content")][text()="created a card"]
${ADDCARDTOFAVORITE}    //ul[@role="menu"]/li[text()="Add to Favorites"]
${REMOVECARDFROMFAVORITE}    //ul[@role="menu"]/li[text()="Remove from Favorites"]
${OPENCARDBTN}    //ul[@role="menu"]/li[text()="Open card"]
${CLOSECARDBTN}    //ul[@role="menu"]/li[text()="Close card"]
${DELETECARDBTN}    //ul[@role="menu"]/li[text()="Delete"]
${SHAREUSERINCARDPOPUPCLOSEBTN}    //*[contains(@class,"antd3-modal-content")]/button
${CLOSEHISTORYLOGINCARDBTN}    //button[contains(@class, "CloseButton")]
${CARDCONTENTISNOTEDITABLE}    //div[contains(@class, "EditorArea")]//div[@class="DraftEditor-editorContainer"]/div[@contenteditable="false"]
${CONFIRMOPENCARDBTN}    //div[@class="ant-confirm-btns"]/button[2]
${INVITETEAMMATEBTN}    //*[contains(@class,"styles__Content")]/button
${VERIFYINVITEPAGE}    //*[contains(text(), 'Invite new teammates')]
${SHAREUSERINCARDICON}    //button/span/div[contains(@class, "UserAvatar")][1]
${THREEDOTINCARDCREATIONPAGE}    //*[@data-icon="ellipsis-h"]
${CONFIRMDELETECARDBTN}    //div[@class="ant-confirm-btns"]/button[2]
${EDITCARDOFFLINEBAR}    //div[@contenteditable="false"]/div[contains(@class, "Offline")]
${TEXTINCARD}    //span[@data-text="true"]

*** Keywords ***

addTextInCard
    [Arguments]    ${text}
    Wait until element is enabled    ${CARDTEXTFIELD}    timeout=${timeout}
    # Press keys    ${CARDTEXTFIELD}    RETURN
    Press keys    ${CARDTEXTFIELD}    ${text}
    Press keys    ${CARDTEXTFIELD}    RETURN

editTextInCard
    [Arguments]    ${text}
    Wait until element is enabled    ${CARDTEXTFIELD}    timeout=${timeout}
    Press keys    ${CARDTEXTFIELD}    BACKSPACE
    Press keys    ${CARDTEXTFIELD}    ${text}
    Press keys    ${CARDTEXTFIELD}    RETURN

backToCardMenuPage
    Wait until element is enabled    //div[contains(@class, "FlexHeaderLeft")]/div/button    timeout=${timeout}
    Click element    //div[contains(@class, "FlexHeaderLeft")]/div/button  

sendCardInChat
    Wait until element is enabled    ${SENDCARDINCHATBTN}    timeout=${timeout}
    Click element    ${SENDCARDINCHATBTN}
    Wait until element is not visible    ${CARDTEXTFIELD}    timeout=${timeout}

clickAddShareUserIcon
    Wait until element is enabled    ${ADDUSERINCARDBTN}    timeout=${timeout}
    Click element    ${ADDUSERINCARDBTN}

clickinviteTeamMate
    Wait until element is enabled    ${INVITETEAMMATEBTN}    timeout=${timeout}
    Click element    ${INVITETEAMMATEBTN}

clickAddMoreUserInCard
    Wait until element is enabled    ${ADDMOREUSERINCARDBTN}    timeout=${timeout}
    Click element    ${ADDMOREUSERINCARDBTN}

selectUserFromFavoriteContact
    Wait until element is enabled    ${SHARECARDTOFAVORITELIST}    timeout=${timeout}
    Click element    ${SHARECARDTOFAVORITELIST}

selectAddedUserInCardFromContact
    Wait until element is enabled    ${SHARECARDTOUSERCONTACTLIST}    timeout=${timeout}
    Click element    ${SHARECARDTOUSERCONTACTLIST}

confirmToAddUserInCard
    Wait until element is enabled    ${ADDSHARECARDUSERBTN}    timeout=${timeout}
    Click element    ${ADDSHARECARDUSERBTN}
    Wait until element is visible    ${SHAREUSERINCARDICON}    timeout=${timeout}

removeUserFromCard
    [Arguments]    ${text}
    Wait until element is enabled    //span[text()="${text}"]/parent::*/parent::*/preceding-sibling::div//div[@role="combobox"]    timeout=${timeout}
    Click element    //span[text()="${text}"]/parent::*/parent::*/preceding-sibling::div//div[@role="combobox"]
    Wait until element is enabled    ${REMOVEUSERFROMCARDBTN}    timeout=${timeout}
    Click element    ${REMOVEUSERFROMCARDBTN}

confirmEditUserInCard
    Click element    ${CONFIRMEDITUSERINCARDBTN}
    Wait until element is not visible    ${CONFIRMEDITUSERINCARDBTN}    timeout=${timeout}

setCardPriority
    [Arguments]    ${index}
    Wait until element is enabled    ${SETCARDPRIORITYBTN}    timeout=${timeout}
    Click element    ${SETCARDPRIORITYBTN}
    Wait until element is enabled    //ul[contains(@class, "dropdown")]/li[${index}]    timeout=${timeout}
    Click element    //ul[contains(@class, "dropdown")]/li[${index}]

setUserRoleInCardToEditor
    Wait until element is enabled    ${CARDROLEDROPDOWNBTN}    timeout=${timeout}
    Click element    ${CARDROLEDROPDOWNBTN}
    Wait until element is enabled    ${CANEDITCARDROLEBTN}    timeout=${timeout}
    Click element    ${CANEDITCARDROLEBTN}

changeUserRoleFromEditorToViewer
    [Arguments]    ${text}
    Wait until element is enabled    //span[text()="${text}"]/parent::*/parent::*/preceding-sibling::div//div[@title="Can edit"]    timeout=${timeout}
    Click element    //span[text()="${text}"]/parent::*/parent::*/preceding-sibling::div//div[@title="Can edit"]
    Wait until element is enabled    ${CANVIEWROLEBTN}    timeout=${timeout}
    Click element    ${CANVIEWROLEBTN}

changeUserRoleFromViewerToEditor
    [Arguments]    ${text}
    Wait until element is enabled    //span[text()="${text}"]/parent::*/parent::*/preceding-sibling::div//div[@title="Can view"]    timeout=${timeout}
    Click element    //span[text()="${text}"]/parent::*/parent::*/preceding-sibling::div//div[@title="Can view"]
    Wait until element is enabled    ${CANEDITROLEBTN}    timeout=${timeout}
    click element    ${CANEDITROLEBTN}

clickOpenCardHistoryLog
    Wait until element is enabled    ${CARDHISTORYLOGBTN}    timeout=${timeout}
    Click element    ${CARDHISTORYLOGBTN}

clickThreeDotOnCardCreationPage
    # ${index}    get element count    //div[contains(@class, "WrapRight")]/button[contains(@class, "tool-button")]
    Wait until element is enabled    ${THREEDOTINCARDCREATIONPAGE}    timeout=${timeout}
    Click element    ${THREEDOTINCARDCREATIONPAGE}

addCardToFavorite
    Wait until element is enabled    ${ADDCARDTOFAVORITE}    timeout=${timeout}
    click element    ${ADDCARDTOFAVORITE}

removeCardFromFavorite
    Wait until element is enabled    ${REMOVECARDFROMFAVORITE}    timeout=${timeout}
    click element    ${REMOVECARDFROMFAVORITE}

openCard
    Wait until element is enabled    ${OPENCARDBTN}    timeout=${timeout}
    Click element    ${OPENCARDBTN}

confirmToOpenCard
    Wait until element is enabled    ${CONFIRMOPENCARDBTN}    timeout=${timeout}
    Click element    ${CONFIRMOPENCARDBTN}

closeCard
    Wait until element is enabled    ${CLOSECARDBTN}    timeout=${timeout}
    Click element    ${CLOSECARDBTN}

clickCloseButtonInShareCardUserList
    CLick element    ${SHAREUSERINCARDPOPUPCLOSEBTN}

clickCloseHistoryLogInCardButton
    Click element    ${CLOSEHISTORYLOGINCARDBTN}

clickCloseCommentInCardButton
    Click element    ${CLOSEHISTORYLOGINCARDBTN}

clickOpenCommentChatInCardForEditor
    Wait until element is enabled    ${COMMENTTABBTNONEDITORVIEW}    timeout=${timeout}
    Click element    ${COMMENTTABBTNONEDITORVIEW}

clearTextInCard
    Clear Element Text    ${CARDTEXTFIELD}

deleteCard
    clickThreeDotOnCardCreationPage
    Wait until element is enabled    ${DELETECARDBTN}    timeout=${timeout}
    Click element    ${DELETECARDBTN}
    Wait until element is enabled    ${CONFIRMDELETECARDBTN}    timeout=${timeout}
    Click element    ${CONFIRMDELETECARDBTN}

waitOtherToEditCard
    Wait until element is not visible    ${EDITCARDOFFLINEBAR}    timeout=${timeout}

#--------------------------------------------------------------------------

isUserIsCardCreator
    [Arguments]    ${text}=${mainUserDisplayName}
    Wait until element is visible    //span[text()="${text}"]/parent::*/parent::*/preceding-sibling::div/span[text()="Creator"]    timeout=${timeout}

isAddedUserIsEditorRole
    [Arguments]    ${text}
    Wait until element is visible    //span[text()="${text}"]/parent::*/parent::*/preceding-sibling::div//div[text()="Can edit"]    timeout=${timeout}

isAddedUserIsViewerRole
    [Arguments]    ${text}
    Wait until element is visible    //span[text()="${text}"]/parent::*/parent::*/preceding-sibling::div//div[text()="Can view"]    timeout=${timeout}

isUserRemoveFromCard
    [Arguments]    ${text}
    Page should not contain element    //span[text()="${text}"]

isHistoryLogOpen
    Wait until element is visible    ${CREATEDCARDLOG}    timeout=${timeout}

isContentIsNotEditable
    Page should contain element    ${CARDCONTENTISNOTEDITABLE}

isOpenInviteTeammatePage
    Wait until element is visible    ${VERIFYINVITEPAGE}    timeout=${timeout}

isAddShareUserIconDisable
    Element Should Be Disabled    ${ADDUSERINCARDBTN}
    