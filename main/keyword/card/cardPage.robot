*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${CREATENEWCARDBTN}    //a[@href="/cards/new"]
${FIRSTPINCARD}    //div[contains(@class, "SectionSeparator")]/preceding-sibling::div/div[1]
${FIRSTUNPINCARD}    //div[contains(@class, "SectionSeparator")]/following-sibling::div/div[1]
${FIRSTPINCARDNAME}    //div[contains(@class, "Separator")]/preceding-sibling::div/div[1]//div[contains(@class, "CardTitle")]/div
${FIRSUNPINCARDNAME}    //div[contains(@class, "Separator")]/following-sibling::div/div[1]//div[contains(@class, "CardTitle")]/div
${FIRSTCARDNAME}    //a[contains(@class, "CardItem")]//div[contains(@class, "CardTitle")]/div
${SORTCARDBTN}    //div[contains(@class, "Filter")]/div[1]/button
${SORTBYLASTUPDATEBTN}    //ul[contains(@class,"dropdown")]/li[text()="Last Updated"]
${SORTBYDUEDATEBTN}    //ul[contains(@class,"dropdown")]/li[text()="Due Date"]
${SORTBYPRIORITYBTN}    //ul[contains(@class,"dropdown")]/li[text()="Priority"]
${NONESORTINGBTN}    //ul[contains(@class,"dropdown")]/li[text()="None"]
${FILTERCARDBTN}    //div[contains(@class, "Filter")]/div[2]/button
${FILTERALLCARDSBTN}    //ul[contains(@class,"dropdown")]/li[text()="All Cards"]
${FILTERACTIVECARDSBTN}    //ul[contains(@class,"dropdown")]/li[text()="Active"]
${FILTERCLOSECARDSBTN}    //ul[contains(@class,"dropdown")]/li[text()="Closed"]
${NONEFILTERCARDSBTN}    //ul[contains(@class,"dropdown")]/li[text()="None"]
${SEARCHFIELDINCARD}    //input[@data-qa-anchor="search-box"]
${CARDSEARCHRESULT}    //div[@class="antd3-row"]/div
${ARCHIVECARDTABBTN}    //a[@href="/cards/archived"]
${ARCHIVECARDBREADCRUMB}    //span[contains(@class, "breadcrumb")][text()="Archived"]
${PINCARDOPTION}    //*[text()="Pin"]
${UNPINCARDOPTION}    //*[text()="Remove from Pin"]
${UNARCHIVECARDOPTION}    //*[text()="Unarchive"]
${ARCHIVECARDOPTION}    //*[text()="Archive"]
${FIRSTCARD}    //a[contains(@class, "CardItem")]
${CARDCOMMENTBTN}    //*[contains(@class,"styles__WrapRight")]/button[4]
${CARDUNREADBADGE}    //a[@data-qa-anchor="appnav-menu-cards"]//sup[@data-show="true"]//p[@class="current"]
${CARDCOMMENTCLOSEBTN}    //*[contains(@class, 'styles__CloseButton')]
${CARDATTACHFILEBTN}    //*[contains(@class,"styles__WrapItem")][4]/button
${CARDCHOOSEFILEBTN}      //input[@type="file"]

*** Keywords ***

clickCreateNewCard
    Wait until element is enabled    ${CREATENEWCARDBTN}    timeout=${timeout}
    Click element    ${CREATENEWCARDBTN}

selectFirstPinCard
    Wait until element is enabled    ${FIRSTPINCARD}    timeout=${timeout}
    Click element    ${FIRSTPINCARD}

selectFirstUnpinCard
    Wait until element is enabled    ${FIRSTUNPINCARD}    timeout=${timeout}
    Click element    ${FIRSTUNPINCARD}

selectUnpinCardName
    [Arguments]    ${text}
    Wait until element is enabled    //div[contains(@class, "Separator")]/following-sibling::div//div[contains(@class, "CardTitle")]/div[contains(text(), "${text}")]    timeout=${timeout}
    Click element    //div[contains(@class, "Separator")]/following-sibling::div//div[contains(@class, "CardTitle")]/div[contains(text(), "${text}")]

SelectCardName
    [Arguments]    ${text}
    Wait until element is enabled    //div[@class="antd3-row"]/div//div[text()="${text}"]    timeout=${timeout}
    Click element    //div[@class="antd3-row"]/div//div[text()="${text}"]

clickSortCardButton
    Wait until element is enabled    ${SORTCARDBTN}    timeout=${timeout}
    Click element    ${SORTCARDBTN}

sortCardByLastUpdate
    Wait until element is enabled    ${SORTCARDBTN}    timeout=${timeout}
    Click element    ${SORTCARDBTN}
    Wait until element is enabled    ${SORTBYLASTUPDATEBTN}    timeout=${timeout}
    Click element    ${SORTBYLASTUPDATEBTN}

sortCardByDueDate
    Wait until element is enabled    ${SORTCARDBTN}    timeout=${timeout}
    Click element    ${SORTCARDBTN}
    Wait until element is enabled    ${SORTBYDUEDATEBTN}    timeout=${timeout}
    Click element    ${SORTBYDUEDATEBTN}

sortByCardPriority
    Wait until element is enabled    ${SORTCARDBTN}    timeout=${timeout}
    Click element    ${SORTCARDBTN}
    Wait until element is enabled    ${SORTBYPRIORITYBTN}    timeout=${timeout}
    Click element    ${SORTBYPRIORITYBTN}

clickFilterCardButton
    Wait until element is enabled    ${FILTERCARDBTN}    timeout=${timeout}
    Click element    ${FILTERCARDBTN}

filterCardByActiveCard
    Wait until element is enabled    ${FILTERCARDBTN}    timeout=${timeout}
    Click element    ${FILTERCARDBTN}
    Wait until element is enabled    ${FILTERACTIVECARDSBTN}    timeout=${timeout}
    Click element    ${FILTERACTIVECARDSBTN}

filterCardByClosedCard
    Wait until element is enabled    ${FILTERCARDBTN}    timeout=${timeout}
    Click element    ${FILTERCARDBTN}
    Wait until element is enabled    ${FILTERCLOSECARDSBTN}    timeout=${timeout}
    Click element    ${FILTERCLOSECARDSBTN}

filterCardByAllCard
    Wait until element is enabled    ${FILTERCARDBTN}    timeout=${timeout}
    Click element    ${FILTERCARDBTN}
    Wait until element is enabled    ${FILTERALLCARDSBTN}    timeout=${timeout}
    Click element    ${FILTERALLCARDSBTN}

searchForCard
    [Arguments]    ${text}
    Wait until element is enabled    ${SEARCHFIELDINCARD}    timeout=${timeout}
    Input text    ${SEARCHFIELDINCARD}    ${text}

clickArchivedCardMenu
    Wait until element is enabled    ${ARCHIVECARDTABBTN}    timeout=${timeout}
    Click element    ${ARCHIVECARDTABBTN}

righClickOnFirstUnPinCardItem
    Wait until element is enabled    ${FIRSTUNPINCARD}    timeout=${timeout}
    Open context menu    ${FIRSTUNPINCARD}    

rightClickOnFirstCard
    Wait until element is enabled    ${FIRSTCARD}    timeout=${timeout}
    Open context menu    ${FIRSTCARD}

rightClickOnCardName
    [Arguments]    ${text}
    Wait until element is enabled    //div[@class="antd3-row"]/div//div[text()="${text}"]    timeout=${timeout}
    Open context menu    //div[@class="antd3-row"]/div//div[text()="${text}"]

clickPinCardFromCardPage
    Wait until element is enabled   ${PINCARDOPTION}    timeout=${timeout}
    Click element    ${PINCARDOPTION}

clickUnpinCardFromCardPage
    Wait until element is enabled    ${UNPINCARDOPTION}    timeout=${timeout}
    Click element    ${UNPINCARDOPTION}

clickArchiveCardFromCardPage
    Wait until element is enabled    ${ARCHIVECARDOPTION}    timeout=${timeout}
    Click element    ${ARCHIVECARDOPTION}

clickUnArchiveCardFromCardPage
    Wait until element is enabled    ${UNARCHIVECARDOPTION}    timeout=${timeout}
    Click element    ${UNARCHIVECARDOPTION}

clickCardComment
    Wait until element is enabled    ${CARDCOMMENTBTN}    timeout=${timeout}
    Click element    ${CARDCOMMENTBTN}

closeCardComment
    Wait until element is enabled    ${CARDCOMMENTCLOSEBTN}    timeout=${timeout}
    Click element    ${CARDCOMMENTCLOSEBTN}

addFileInCard
    Wait until element is enabled    ${CARDATTACHFILEBTN}     timeout=${timeout}
    Click Element    ${CARDATTACHFILEBTN}
    Wait until element is enabled    ${CARDCHOOSEFILEBTN}     timeout=${timeout}
    Choose file    ${CARDCHOOSEFILEBTN}     ${filePath}/${pdfFile}

#-----------------------------------------------------------------------------------------

isCardDisplayCorrectPriority
    [Arguments]    ${text}    ${priority}=High
    Wait until element is visible    //div[text()="${text}"]/parent::*/parent::*/parent::*/following-sibling::div//span[text()="${priority}"]    timeout=${timeout}

isCardDisappear
    [Arguments]    ${text}
    Page should not contain element    /div[contains(@class, "CardTitle")]/div[text()="${text}"]

isCardPin
    [Arguments]    ${text}
    Wait until element is visible    //div[text()="${text}"]/parent::*/preceding-sibling::span//*[@data-icon="thumbtack"]    timeout=${timeout}

isCardUnPin
    [Arguments]    ${text}
    Page should not contain element    //div[contains(@class, "Separator")]/preceding-sibling::div//div[contains(@class, "CardTitle")]/div[text()="${text}"]

isCardArchived
    [Arguments]    ${text}
    Wait until element is visible    //*[text()="${text}"]/parent::*/preceding-sibling::span/span[text()="Archived"]    timeout=${timeout}

isCardUnArchive
    [Arguments]    ${text}
    Page should not contain element    /div[contains(@class, "CardTitle")]/div[text()="${text}"]

isPinIconDisappear
    [Arguments]    ${text}
    Wait until element is not visible    //*[text()="${text}"]/parent::*/preceding-sibling::span//*[@data-icon="thumbtack"]    timeout=${timeout}

isUnpinCardDisplayCorrectly
    [Arguments]    ${text}
    Page should contain element    //div[@class="antd3-row"]/div//div[text()="${text}"]
    Wait until element is not visible    //div[text()="${text}"]/parent::*/preceding-sibling::span//*[@data-icon="thumbtack"]    timeout=${timeout}

isCardDisPlayCorrectlyAfterSortAndFilter
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "List")]//div[contains(@class, "antd3-col")][1]//div[contains(@class, "CardTitle")]/div[text()="${text}"]    timeout=${timeout}

isCardResultContainsKeyword
    [Arguments]    ${text}
    ${index}    get element count    ${CARDSEARCHRESULT}
    :FOR  ${index}  IN RANGE  ${index}
    \    Wait until element is visible    //div[@class="antd3-row"]/div[${index}+1]//span[contains(@class, "Highlighted")][text()="${text}"]    timeout=${timeout}
    
isArchivedCardPageDisplay
    Wait until element is visible    ${ARCHIVECARDBREADCRUMB}    timeout=${timeout}

isAllSortFunctionalityDisplay
    Wait until element is visible    ${SORTBYLASTUPDATEBTN}    timeout=${timeout}
    Wait until element is visible    ${SORTBYDUEDATEBTN}    timeout=${timeout}
    Wait until element is visible    ${SORTBYPRIORITYBTN}    timeout=${timeout}

isAllFilterCardFunctionalityDisplay
    Wait until element is visible    ${FILTERACTIVECARDSBTN}    timeout=${timeout}
    Wait until element is visible    ${FILTERCLOSECARDSBTN}    timeout=${timeout}

isUnreadBadgeDisplayOnEditedCard
    [Arguments]    ${text}
    Wait until element is visible    //*[text()="${text}"]/parent::*/parent::*/parent::*/parent::*/following-sibling::sup[@data-show="true"]    timeout=${timeout}
    
isUnArchiveCardOptionDisplay
    Wait until element is visible    ${UNARCHIVECARDOPTION}   timeout=${timeout}

isCardUnreadBadgeDisplay
    [Arguments]    ${index}=1
    Wait until element is visible    ${CARDUNREADBADGE}    timeout=${timeout}
    Element should contain    ${CARDUNREADBADGE}  ${index}

isCardDisplay
    [Arguments]    ${text}
    Wait until element is visible    /div[contains(@class, "CardTitle")]/div[text()="${text}"]    timeout=${timeout}
    
isFileNameDisplayInCard
    [Arguments]    ${text}
    Wait until element is visible    //*[contains(@class, "styles__FileHref")]/span[text()="${text}"]    timeout=${timeout}