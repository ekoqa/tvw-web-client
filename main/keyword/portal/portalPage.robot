*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${INAPPPORTALID}    //*[@alt="Tripadvisor"]
${PORTALSEARCHFIELD}    //input[@id="searchBox"]
${TARGETPORTAL}    //div[contains(@class, "portalItem")][21]

*** Keywords ***

selectPortalNumber
    [Arguments]    ${index}
    Wait until element is enabled    //div[@id="itemContainer"]/div[@id="favoriteEndingLine"]/following-sibling::div[${index}]    timeout=${timeout}
    Click element    //div[@id="itemContainer"]/div[@id="favoriteEndingLine"]/following-sibling::div[${index}]

addPortalFavoriteByIndex
    [Arguments]    ${index}
    Mouse over    //div[@id="itemContainer"]/div[@id="favoriteEndingLine"]/following-sibling::div[${index}]
    Wait until element is visible    //div[@id="itemContainer"]/div[@id="favoriteEndingLine"]/following-sibling::div[${index}]//i[2]    timeout=${timeout}
    Click element    //div[@id="itemContainer"]/div[@id="favoriteEndingLine"]/following-sibling::div[${index}]//i[2]

removePortalFavoriteByIndex
    [Arguments]    ${index}
    Wait until element is enabled    //div[@id="itemContainer"]/div[@data-id][${index}]//i[1]    timeout=${timeout}
    Click element    //div[@id="itemContainer"]/div[@data-id][${index}]//i[1]
    
inputSearchForPortal
    [Arguments]    ${text}
    Wait until element is enabled    ${PORTALSEARCHFIELD}    timeout=${timeout}
    Input text    ${PORTALSEARCHFIELD}    ${text}
    Press keys    ${PORTALSEARCHFIELD}    RETURN

scrolldownInPortalPage
    Scroll element into view    ${TARGETPORTAL}

#--------------- verify ----------------------------------

isNewTabPortalOpen
    [Arguments]    ${current_window}    ${new_window_list}
    Should Not Be Equal      ${current_window}    ${new_window_list}    msg=Cannot open a new tab

isInAppPortalOpen
    Wait until element is visible    ${INAPPPORTALID}    timeout=${timeout}

isPortalAddToFavorite
    [Arguments]    ${text}
    :FOR  ${index}  IN RANGE  10
    \  ${type}  get element attribute  //div[@id="itemContainer"]/div[${index}+1]  id
    \  Exit For Loop If  '${type}'=='favoriteEndingLine'
    \  ${status}  Run Keyword And Return Status  Element should contain  //div[@id="itemContainer"]/div[@data-id][${index+1}]//h6/span  ${text}
    Run Keyword If  '${status}'=='fail'  fail

isPortalRemoveFromFavorite
    [Arguments]    ${text}
    :FOR  ${index}  IN RANGE  10
    \  ${type}  get element attribute  //div[@id="itemContainer"]/div[${index}+1]  id
    \  Exit For Loop If  '${type}'=='favoriteEndingLine'
    \  Wait until element is not visible  //div[@id="itemContainer"]/div[@data-id][${index}]//h6/span[text()="${text}"]

isSearchResultDisplayCorrect
    [Arguments]    ${text}
    Wait until element is visible    //div[@id="searchResult"]/div[@data-id]//mark[text()="${text}"]

isPortalDisplayCorrect
    [Arguments]    ${text}
    Wait until element is visible    //div[@id="itemContainer"]/div[@data-id]//h6/span[text()="${text}"]    timeout=${timeout}
    Scroll Element Into View    //div[@id="itemContainer"]/div[@data-id]//h6/span[text()="${text}"]

isPortalDisplayCorrectDescription
    [Arguments]    ${text}    ${description}
    Wait until element is visible    //*[text()="${text}"]/parent::*/parent::*/following-sibling::p[text()="${description}"]    timeout=${timeout}

isPortalDisappear
    [Arguments]    ${text}
    Wait until element is not visible    //div[@id="itemContainer"]/div[@data-id]//h6/span[text()="${text}"]    timeout=${timeout}

isPortalDisplayAllDetailCorrect
    [Arguments]    ${name}    ${description}
    isPortalDisplayCorrect    ${name}
    isPortalDisplayCorrectDescription  ${name}    ${description}

isAppReDirectToCorrectURL
    [Arguments]    ${URL}
    @{list}    get window handles
    FOR  ${window}  IN  @{list}
       Select window    ${window}
    END
    ${location}    get location    
    Run Keyword if     '${location}'!='${URL}'    fail

isAbleToScrollDownInPortalPage
    Wait until element is visible    ${TARGETPORTAL}    timeout=${timeout}
