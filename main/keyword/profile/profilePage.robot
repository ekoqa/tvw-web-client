*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${UPLOADCOVERPHOTOLOCATOR}    //div[contains(@class, "CoverPhoto")]//input
${UPLOADCOVERPHOTOLOCATORINGROUPCHAT}    //div[contains(@class, "ant-upload ant-upload-drag")]/span/input
${UPLOADPROFILEPHOTOLOCATOR}    //*[contains(@class,"styles__AvatarEdit-")]/span/div/span/input
# ${UPLOADCOVERPHOTOLOCATOR}    //*[contains(@class,"styles__CoverPhotoEdit-")]/span/div/span/input
${COVERIMAGECONTAINER}    //div[@class="ant-modal-body"]/div/div[contains(@class, "styles__Cover")]
${CHATICONONPROFILE}    //div[contains(@class, "ActionContainer")]/div[2]/div[4]/button
${EDITPROFILEBTN}    //div[contains(@class, "Details")]/button
${FIRSTNAMETEXTFIELD}    firstname
${LASTNAMETEXTFIELD}    lastname
${TITLETEXTFIELD}    position
${SAVEEDITPROFILEBTN}    //div[contains(@class, "Edit")]/button[2]
${FULLNAMETITLE}   //h1[contains(@class, "Name")]
${CLOSEPROFILEPOPUPICON}    //button[@class="antd3-modal-close"]
${SETTINGBTN}    //button[@data-qa-anchor="appbar-settings"]
${VERIFYOWNEMAILDISPLAY}    //*[contains(@class,"styles__Detail")]//*[contains(text(),'qwee@gmail.com')]
${VERIFYOWNMOBILEDISPLAY}    //*[contains(@class,"styles__Detail")]//*[contains(text(),'9999')]
${VERIFYOTHEREMAILDISPLAY}    //*[contains(@class,"styles__Detail")]//*[contains(text(),'ggg@gmail.com')]
${VERIFYOTHERMOBILEDISPLAY}    //*[contains(@class,"styles__Detail")]//*[contains(text(),'1234')]
${VERIFYEMAILDISPLAY}    //*[text()="Email"]/following-sibling::div
${VERIFYMOBILEDISPLAY}    //*[text()="Phone Number"]/following-sibling::div
${VERIFYTITLEDISPLAY}    //*[text()="Title"]/following-sibling::div
${JITSIICONINPROFILEPAGE}    //button//*[@data-icon="video-plus"]
${VERIFYWRONGPASSWORD}    //*[contains(text(),'The current password you entered is incorrect, please try again.')]


*** Keywords ***

uploadNewCoverPhoto
    Wait until element is enabled    ${UPLOADCOVERPHOTOLOCATOR}    timeout=${timeout}
    Choose file    ${UPLOADCOVERPHOTOLOCATOR}    ${filePath}/${coverProfile}

uploadNewCoverPhotoInGroupChat
    Wait until element is enabled    ${UPLOADCOVERPHOTOLOCATORINGROUPCHAT}    timeout=${timeout}
    Choose file    ${UPLOADCOVERPHOTOLOCATORINGROUPCHAT}    ${filePath}/${coverProfile}

uploadNewProfilePhoto
    Wait until element is enabled    ${UPLOADPROFILEPHOTOLOCATOR}    timeout=${timeout}
    Choose file    ${UPLOADPROFILEPHOTOLOCATOR}    ${filePath}/${coverProfile}

uploadOldCoverPhoto
    Wait until element is enabled    ${UPLOADCOVERPHOTOLOCATOR}    timeout=${timeout}
    Choose file    ${UPLOADCOVERPHOTOLOCATOR}    ${filePath}/${imageInChatFile}

uploadOldProfilePhoto
    Wait until element is enabled    ${UPLOADPROFILEPHOTOLOCATOR}    timeout=${timeout}
    Choose file    ${UPLOADPROFILEPHOTOLOCATOR}    ${filePath}/${imageInChatFile}

createDcChatFromProfile
    Wait until element is enabled    ${CHATICONONPROFILE}    timeout=${timeout}
    Click element    ${CHATICONONPROFILE}

clickEditProfilIcon
    Wait until element is enabled    ${EDITPROFILEBTN}    timeout=${timeout}
    Click element    ${EDITPROFILEBTN}

clearFirstnameValue
    ${value}=     Get Element Attribute   ${FIRSTNAMETEXTFIELD}      value
    ${backspacesCount}=    Get Length      ${value}
    Run Keyword If    """${value}""" != ''    
    ...     Repeat Keyword  ${backspacesCount}  Press Keys  ${FIRSTNAMETEXTFIELD}   BACKSPACE

clearTitle
    ${value}=     Get Element Attribute   ${TITLETEXTFIELD}      value
    ${backspacesCount}=    Get Length      ${value}
    Run Keyword If    """${value}""" != ''    
    ...     Repeat Keyword  ${backspacesCount}  Press Keys  ${TITLETEXTFIELD}   BACKSPACE

editFirstName
    [Arguments]    ${text}
    Wait until element is enabled   ${FIRSTNAMETEXTFIELD}    timeout=${timeout}
    Input text    ${FIRSTNAMETEXTFIELD}    ${text}

clearLastnameValue
    ${value}=     Get Element Attribute   ${LASTNAMETEXTFIELD}      value
    ${backspacesCount}=    Get Length      ${value}
    Run Keyword If    """${value}""" != ''    
    ...     Repeat Keyword  ${backspacesCount}  Press Keys  ${LASTNAMETEXTFIELD}   BACKSPACE

editLastName
    [Arguments]    ${text}
    Wait until element is enabled   ${LASTNAMETEXTFIELD}    timeout=${timeout}
    Input text    ${LASTNAMETEXTFIELD}    ${text}

clickSaveEditProfileButton
    Wait until element is enabled    ${SAVEEDITPROFILEBTN}    timeout=${timeout}
    Click element    ${SAVEEDITPROFILEBTN}

clickCloseProfilePopup
    Wait until element is enabled   ${CLOSEPROFILEPOPUPICON}    timeout=${timeout}
    Click element    ${CLOSEPROFILEPOPUPICON}
    
clickStartJitsiCallFromProfilePage
    Wait until element is enabled    ${JITSIICONINPROFILEPAGE}    timeout=${timeout}
    Click element    ${JITSIICONINPROFILEPAGE}

editTitle
    [Arguments]    ${text}
    Wait until element is enabled   ${TITLETEXTFIELD}    timeout=${timeout}
    Input text    ${TITLETEXTFIELD}    ${text}
#-------------------- verify ------------------------------

isNewCoverImageUploadSuccess
    [Arguments]    ${text}
    Sleep    2s
    ${new}    Get element attribute    ${COVERIMAGECONTAINER}    style
    Run Keyword If  '${new}'=='${text}'    fail    msg=The cover photo doesnot change

isProfileUserDisplayCorrect
    [Arguments]    ${text}
    Wait until element is visible    //div[contains(@class, "HeaderContainer")]//h1[contains(text(), "${text}")]    timeout=${timeout}

isProfileUserNotDisplay
    [Arguments]    ${text}
    Wait until element is not visible    //div[contains(@class, "HeaderContainer")]//h1[contains(text(), "${text}")]    timeout=${timeout}

isUserFullNameDisplayCorrect
    [Arguments]    ${text}
    Element should contain    ${FULLNAMETITLE}    ${text}

isUserFullNameNotDisplayCorrect
    [Arguments]    ${text}
    Wait until element is not visible    //h1[contains(@class, "Name")][text()="${text}"]
    
isEmailDisplay
    [Arguments]    ${text}
    Wait until element is visible    ${VERIFYEMAILDISPLAY}    timeout=${timeout}
    Element should contain    ${VERIFYEMAILDISPLAY}    ${text}

isMobileDisplay
    [Arguments]    ${text}
    Wait until element is visible    ${VERIFYMOBILEDISPLAY}    timeout=${timeout}
    Element should contain    ${VERIFYMOBILEDISPLAY}    ${text}

isEmailNotDisplay
    Wait until element is not visible    ${VERIFYEMAILDISPLAY}    timeout=${timeout}

isMobileNotDisplay
    Wait until element is not visible    ${VERIFYMOBILEDISPLAY}    timeout=${timeout}

isTitleDisplay
    [Arguments]    ${text}
    Wait until element is visible    //input[@id="position"][@value="${text}"]    timeout=${timeout}

isChangeWrongPassword
    Wait until element is visible    ${VERIFYWRONGPASSWORD}    timeout=${timeout}