*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${RES_DYNAMICCONFIGTAB}    //a[contains(text(), "Dynamic config")]
${RES_EDITJITSIICON}    //input[@id="dc-jitsi"]/parent::*/following-sibling::a
${RES_JITSIFORMATTEXTFIELD}    //textarea[@id="ta-jitsi"]
${RES_SAVEJSONJITSIFORMATBTN}    //div[@id="modal-jitsi"]/a[text()="Save"]
${RES_SAVEEDITNETWORKBTN}    //button[@type="submit"]


*** Keywords ***

clickDynamicConfigTab
    Wait until element is enabled    ${RES_DYNAMICCONFIGTAB}    timeout=${timeout}
    Click element    ${RES_DYNAMICCONFIGTAB}

clickEditJitsiJSONFormat
    Wait until element is enabled    ${RES_EDITJITSIICON}    timeout=${timeout}
    Click element    ${RES_EDITJITSIICON}

editJSONFormat
    [Arguments]    ${text}
    Wait until element is enabled    ${RES_JITSIFORMATTEXTFIELD}    timeout=${timeout}
    Input text    ${RES_JITSIFORMATTEXTFIELD}    ${text}

saveEditJSONFormat
    Wait until element is enabled    ${RES_SAVEJSONJITSIFORMATBTN}    timeout=${timeout}
    Click element    ${RES_SAVEJSONJITSIFORMATBTN}
    Wait until element is not visible    ${RES_SAVEJSONJITSIFORMATBTN}    timeout=${timeout}

clickSaveEditNetwork
    Wait until element is enabled    ${RES_SAVEEDITNETWORKBTN}    timeout=${timeout}
    Click element    ${RES_SAVEEDITNETWORKBTN}
    Wait until element is not visible    ${RES_SAVEEDITNETWORKBTN}    timeout=${timeout}

