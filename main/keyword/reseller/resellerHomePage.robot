*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${RES_SERCHNETWORKFIELD}    //form[@id="searchNetwork"]//input
${RES_DROPDOWNBTNINHOMEPAGE}    //button[contains(@class, "btn-circle dropdown-toggle")]
${RES_EDITNETWORKBTN}    //div[contains(@class, "dropdown-menu-right show")]/a[2]



*** Keywords ***

searchNetworkInReseller
    [Arguments]    ${text}
    Wait until element is enabled    ${RES_SERCHNETWORKFIELD}    timeout=${timeout}
    Input text    ${RES_SERCHNETWORKFIELD}    ${text}
    Press keys    ${RES_SERCHNETWORKFIELD}    RETURN

clickEditNetwork
    Wait until element is enabled    ${RES_DROPDOWNBTNINHOMEPAGE}    timeout=${timeout}
    Click element    ${RES_DROPDOWNBTNINHOMEPAGE}
    Wait until element is enabled    ${RES_EDITNETWORKBTN}    timeout=${timeout}
    Click element    ${RES_EDITNETWORKBTN}
