*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***
${EPSMENU}    //*[@data-qa-anchor="appnav-menu-eps"]
${EPSIFRAME}    xpath=//iframe[contains(@class, 'styles__EpsFrame')]
${360FEEDBACKBTN}    //*[contains(@class,"styles__MenuListWrapper-")]/div[2]
${VERIFYEPSNODATA}    //*[contains(text(), "Please submit a 360 workflow for your manager and peers to evaluate you")]

*** Keywords ***

selectEPSMenu
    Wait until element is enabled    ${EPSMENU}    timeout=${timeout}
    Click element    ${EPSMENU}

click360Feedback
    Select frame    ${EPSIFRAME}
    Wait until element is enabled    ${360FEEDBACKBTN}    timeout=${timeout}
    Click element    ${360FEEDBACKBTN}

#------------- verify -----------------------

isEPSPageNoData
    Wait until element is visible    ${VERIFYEPSNODATA}    timeout=${timeout}


















#---------------------------------------------------------------------

# isContactDisplay
#     [Arguments]    ${text}
#     Wait until element is enabled    //div[contains(@data-qa-anchor, "user")]//h4[contains(text(), "${text}")]

