*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${SHAREEMAILINPUTFIELD}    //div[contains(@class, "WrapInputEmail")]//input
${SENDINVITATIONEMAILBTN}    //button[@type="submit"]
${SENDINVITATIONSUCCESSTEXT}    //*[text()="We notified members by email that you’re waiting them to join the team."]
${OKAYINVITATIONBTN}    //*[contains(@class, "ButtonContainer")]/button

*** Keywords ***

inputInviteEmail
    [Arguments]    ${text}
    Wait until element is enabled    ${SHAREEMAILINPUTFIELD}    timeout=${timeout}
    Input text    ${SHAREEMAILINPUTFIELD}    ${text}
    Press keys    ${SHAREEMAILINPUTFIELD}    RETURN

clickSendEmailInvitation
    Wait until element is enabled    ${SENDINVITATIONEMAILBTN}    timeout=${timeout}
    Click element    ${SENDINVITATIONEMAILBTN}

clickOkThanksButton
    Wait until element is enabled    ${OKAYINVITATIONBTN}    timeout=${timeout}
    Click element    ${OKAYINVITATIONBTN}


#-------------------------- verify ----------------------------------------

isSendInvitationSuccessDisplay
    Wait until element is visible    ${SENDINVITATIONSUCCESSTEXT}    timeout=${timeout}