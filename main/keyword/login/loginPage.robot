*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${USERNAMEFIELD}    username
${PASSWORDFIELD}    password
${SIGNINBTN}    //button[@type="submit"]
${RECENTMENU}    //li[@role="menuitem"]/a[@data-qa-anchor="appnav-menu-recents"]
${TUTORIALPOPUP}    //p[contains(@class, "StepParagraph")][contains(text(), "Welcome to Eko")]
${SKIPTUTORIAL}    //a[contains(@class, "StepLink")]
${SETTINGBTN}    //button[contains(@class, "SettingWrapper")]
${LOGOUTBTN}        //li[text()="Log out"]
${LOGINBTNFROMLANDINGPAGE}    //a[contains(@class, "LinkRedirect")]
${EXCEEDDEVICEERROR}    //div[@class="ant-notification-notice-message"][text()="You have exceeded the number of devices to access this app."]
${LOGINRESELLERIDFIELD}    loginUserEmail
${RESELLERPASSWORDFIELD}    loginUserPassword
${LOGINRESELLERBTN}    //input[@value="Login"]
${SSOBTN}    //button[contains(@class, "styles__GoogleLoginButton")]
${SERVERDROPDOWNBTN}    //span[contains(@class, "styles__RegionItemLabel")]
${EUROPEBTN}    //span[contains(@class, "styles__RegionItemLabel")][contains(text(), "Europe")]
${AMERICAEBTN}    //span[contains(@class, "styles__RegionItemLabel")][contains(text(), "North America")]
# ${SSONEXTBTN}    //span[contains(@class, "VfPpkd")]
${SSONEXTBTN}    //div[contains(@class, "VfPpkd")]
${EMAILFIELD}    //input[contains(@class, "whsOnd")]
${EMAILPASSWORDFIELD}    //input[contains(@autocomplete, "current-password")]
# ${JOINNETWORKBTN}    //button[contains(@class, "styles__Submit")]
${JOINNETWORKBTN}    //a[contains(@href, "https://sea-staging-register.ekoapp.com/join?lang=en")]
${EMAILUNPUT}    //a[@href="/create"]

*** Keywords ***

openEkoAppAndLogIn
    [Arguments]    ${USERNAME}=${mainUsername}
    openWindow
    set window size    1920  1080
    loginWebClient    ${USERNAME}

openAdminPanelAndLogIn
    [Arguments]    ${USERNAME}=${mainUsername}
    openWindow    ${AdminURL}
    set window size    1920  1080
    Maximize browser window
    loginAdminPanel  ${USERNAME}

handleFailcaseWithUser
    [Arguments]    ${USERNAME}=${mainUsername}
    Run Keyword If Test Failed    setUpForNextTestWithUser    ${USERNAME}

setUpForNextTestWithUser
    [Arguments]    ${USERNAME}=${mainUsername}
    Go to    ${ClientURL}
    loginPage.logoutWebClient
    loginPage.loginWebClient    ${USERNAME}     

openWindow
    [Arguments]    ${URL}=${ClientURL}    ${BROWSER}=${BROWSER}
    # [Arguments]    ${URL}=${EkogreenURL}    ${BROWSER}=${BROWSER}
    Open Browser    ${URL}    ${BROWSER}    options=add_argument("--disable-dev-shm-usage")
    # Open Browser    ${URL}    ${BROWSER}    options=add_argument("--incognito")

openWindowToTVW
    [Arguments]    ${URL}=${TVWLoginURL}    ${BROWSER}=${BROWSER}
    Open Browser    ${URL}    ${BROWSER}    options=add_argument("--disable-dev-shm-usage")
    
loginWebClient
    [Arguments]    ${USERNAME}=${mainUsername}    ${PASSWORD}=${mainPassword}
    set selenium speed    ${speed}
    maximize browser window
    Wait until element is enabled    ${USERNAMEFIELD}    timeout=${timeout}
    Input Text          ${USERNAMEFIELD}        ${USERNAME}
    Input Password      ${PASSWORDFIELD}        ${PASSWORD}
    Wait until element is enabled    ${SIGNINBTN}    timeout=${timeout}
    Click element    ${SIGNINBTN}
    Wait until element is visible    ${RECENTMENU}    timeout=${timeout}
    ${skip}    Run Keyword And Return Status    Page should not contain element    ${TUTORIALPOPUP}
    Run Keyword Unless    ${skip}    Click element    ${SKIPTUTORIAL}

logoutWebClient
    ${check}    Run Keyword And Return Status    Wait until element is not visible    ${LOGOUTBTN}
    Run Keyword If  ${check}  clickSettingButton
    Wait until element is enabled    ${LOGOUTBTN}    timeout=${timeout}
    Click Element    ${LOGOUTBTN}
    Wait until page contains element    ${USERNAMEFIELD}    timeout=${timeout}
    # Reload page

clickLoginButtonFromLandingPage
    Wait until element is enabled    ${LOGINBTNFROMLANDINGPAGE}    timeout=${timeout}
    Click element    ${LOGINBTNFROMLANDINGPAGE}

goToWebClientWith
    [Arguments]    ${username}
    Go to    ${ClientURL}
    ${userlogin}    Run Keyword And Return Status    Wait until element contains    //div[contains(@class, "UserAvatar")]/following-sibling::span    ${username}    timeout=10s
    ${needLogin}    Run Keyword And Return Status    Element should be visible    ${USERNAMEFIELD}
    Run Keyword If    ${userLogin}    Sleep    0.5s
    ...    ELSE IF    ${needLogin}    loginWebClient  ${username}    password
    ...    ELSE    reloginWithUser    ${username}  

reloginWithUser
    [Arguments]    ${USERNAME}=${mainUsername}    ${PASSWORD}=${mainPassword}
    logoutWebClient
    loginWebClient  ${USERNAME}  ${PASSWORD}

logInAndCheckDeviceLimit
    [Arguments]    ${USERNAME}=${mainUsername}    ${PASSWORD}=${mainPassword}
    set selenium speed    ${speed}
    maximize browser window
    Wait until element is enabled    ${USERNAMEFIELD}    timeout=${timeout}
    Input Text          ${USERNAMEFIELD}        ${USERNAME}
    Input Password      ${PASSWORDFIELD}        ${PASSWORD}
    Wait until element is enabled    ${SIGNINBTN}    timeout=${timeout}
    Click element    ${SIGNINBTN}

closeAndOpenTVW
    [Arguments]    ${URL}=${TVWLoginURL}    ${BROWSER}=${BROWSER}
    Close Browser
    Open Browser    ${URL}    ${BROWSER}    options=add_argument("--disable-dev-shm-usage")

#----------------------------------------------------------------------------------------------
loginAdminPanel
    [Arguments]    ${USERNAME}=${mainUsername}    ${PASSWORD}=${mainPassword}
    Wait until element is enabled    username    timeout=${timeout}
    Input Text          username        ${USERNAME}
    Input Password      password        ${PASSWORD}
    Wait until element is enabled    loginButton    timeout=${timeout}
    Click element       loginButton
    ${skip}    Run Keyword And Return Status    Page should not contain element    //div[contains(@class, "h1")][contains(text(), "Welcome to eko")]
    Run Keyword Unless    ${skip}    Click element    //a[contains(@class, "skipbutton")]   

loginAdminPanelWithGDPRUser
    [Arguments]    ${USERNAME}=${gdprUsername}    ${PASSWORD}=${mainPassword}
    Wait until element is enabled    username    timeout=${timeout}
    Input Text          username        ${USERNAME}
    Input Password      password        ${PASSWORD}
    Wait until element is enabled    loginButton    timeout=${timeout}
    Click element       loginButton
    ${skip}    Run Keyword And Return Status    Page should not contain element    //div[contains(@class, "h1")][contains(text(), "Welcome to eko")]
    Run Keyword Unless    ${skip}    Click element    //a[contains(@class, "skipbutton")] 


logoutAdminPanel
    Click element    logoutDropdown
    Click element    logout
    Wait until page contains element    passwordExpireNotificationMessage    timeout=${timeout}
    Click element    logoutButton
    Wait until page contains element    username    timeout=${timeout}

chooseEuropeServer
    Wait until element is visible    ${SERVERDROPDOWNBTN}    timeout=${timeout}
    Click element    ${SERVERDROPDOWNBTN}
    Wait until element is visible    ${EUROPEBTN}    timeout=${timeout}
    Click element    ${EUROPEBTN}

chooseAmericaServer
    Wait until element is visible    ${SERVERDROPDOWNBTN}    timeout=${timeout}
    Click element    ${SERVERDROPDOWNBTN}
    Wait until element is visible    ${AMERICAEBTN}    timeout=${timeout}
    Click element    ${AMERICAEBTN}
    
clickSSOButton
    Wait until element is visible    ${SSOBTN}    timeout=${timeout}
    Click element    ${SSOBTN}

loginSSOWithEmail
    [Arguments]    ${USERNAME}=${Email}    ${PASSWORD}=${emailPassword}
    set selenium speed    ${speed}
    Wait until element is enabled    ${EMAILFIELD}    timeout=${timeout}
    Input Text          ${EMAILFIELD}        ${Email}
    Wait until element is enabled    ${SSONEXTBTN}    timeout=${timeout}
    Click element    ${SSONEXTBTN}
    Wait until element is enabled    ${EMAILPASSWORDFIELD}    timeout=${timeout}
    Input Password      ${EMAILPASSWORDFIELD}        ${emailPassword}
    Wait until element is enabled    ${SSONEXTBTN}    timeout=${timeout}
    Click element    ${SSONEXTBTN}
    Wait until element is visible    ${RECENTMENU}    timeout=${timeout}

clickJoinNetwork
    Wait until element is visible    ${JOINNETWORKBTN}    timeout=${timeout}
    Click element    ${JOINNETWORKBTN}

inputEmailField
    [Arguments]    ${text}
    Wait until element is visible    ${EMAILUNPUT}    timeout=${timeout}
    Input text    ${EMAILUNPUT}    ${text}

chooseNewPopup
    @{list}    get window handles
    FOR  ${window}  IN  @{list}
       Select window    ${window}
    END

OpenWebInNewWindow
    [Arguments]    ${text}
    Open Browser    ${text}    chrome

switchWindowByIndex
    [Arguments]    ${index}
    switch browser    ${index}
    ${title1}=  get title
    log to console  ${title1} 

OpenWebInNewWindowTab
    [Arguments]    ${text}
    Execute Javascript    window.open('${text}')

#------------------------ reseller -------------------------------------

loginReseller
    [Arguments]    ${USERNAME}=admin    ${PASSWORD}=${mainPassword}
    Wait until element is enabled    ${LOGINRESELLERIDFIELD}    timeout=${timeout}
    Input text    ${LOGINRESELLERIDFIELD}    ${USERNAME}
    Input text    ${RESELLERPASSWORDFIELD}    ${PASSWORD}
    Wait until element is enabled    ${LOGINRESELLERBTN}    timeout=${timeout}
    Click element    ${LOGINRESELLERBTN}

#------------------------- verify ----------------------------------

isAppAutoSwipContent
    :FOR  ${index}  IN RANGE  3
    \    Wait until element is visible    //ul/li[${index}+1][@class="slick-active"]    timeout=60s

isUserCannotLogin
    Wait until element is visible    ${EXCEEDDEVICEERROR}    timeout=${timeout}

isSSOButtonDisplay
    Wait until element is visible    ${SSOBTN}    timeout=${timeout}

isSSOButtonNotDisplay
    Wait until element is not visible    ${SSOBTN}    timeout=${timeout} 