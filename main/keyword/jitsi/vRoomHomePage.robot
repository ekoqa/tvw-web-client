*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${VROOMLANDINGPAGE}    input-room
# ${ENTERCONFERENCEROOMFIELD}    enter_room_field
${ENTERCONFERENCEROOMFIELD}    //span[contains(@class, "ant-select-selection-search")]/input
# ${ENTERROOMBTN}    enter_room_button
${ENTERROOMBTN}    //div[contains(@class, "box-input-room")]/button
# ${CONFERENCEROOMNAME}    //div[@id="videoconference_page"]/div/span
${CONFERENCEROOMNAME}    //p[contains(@class, "first-line-history")]
${JITSIHOMEPAGETEXT}    //h1[@class="header-text-title"][text()="Jitsi Meet"]
${VWORLDHOMEPAGETEXT}    //*[text()="เริ่มต้นใช้งานง่าย ๆ โดยการสร้างห้องประชุมของคุณ แล้วส่งลิงค์ให้ผู้อื่น คุณสามารถตั้งรหัสผ่านเพื่อเพิ่มความปลอดภัยได้"]

*** Keywords ***

setConferenceRoomName
   [Arguments]    ${text}
   Wait until element is enabled   ${ENTERCONFERENCEROOMFIELD}    timeout=${timeout}
   Input text    ${ENTERCONFERENCEROOMFIELD}    ${text}

startConferenceRoom
    Wait until element is enabled   ${ENTERROOMBTN}    timeout=${timeout}
    Click element    ${ENTERROOMBTN}


#--------------------------- verify -----------------------------------------

isConferenePageDisplayJitsiUI
    Wait until element is visible    ${JITSIHOMEPAGETEXT}    timeout=${timeout}

isConferenePageDisplayVWorldUI
    Wait until element is visible    ${VWORLDHOMEPAGETEXT}    timeout=${timeout}

isVroomPageDisplayCorrect
    Wait until element is visible    ${VROOMLANDINGPAGE}    timeout=${timeout}

isConferenceNameDisplayCorrect
    [Arguments]    ${text}
    # Wait until element is visible    ${CONFERENCEROOMNAME}    timeout=${timeout}
    # Element should contain    ${CONFERENCEROOMNAME}    ${text}
    
    Wait until element is visible    //*[contains(@class, "first-line-history")][contains(text(), "${text}")]    timeout=${timeout}
   
