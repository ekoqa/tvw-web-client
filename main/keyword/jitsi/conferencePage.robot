*** Settings ***

Library    SeleniumLibrary

Resource                  ../../config/environment.robot

*** Variable ***

${JITSIIFRAME}    //iframe[contains(@src, "meet.jit.si")]



*** Keywords ***


selectJitsiIframe
    Select frame    ${JITSIIFRAME}