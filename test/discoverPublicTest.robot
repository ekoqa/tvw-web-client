*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/discover/discoverPage.robot
Resource                  ../main/keyword/discover/insideDiscoverPage.robot
Resource                  ../main/keyword/recent/settingPage.robot

Suite Setup    openEkoAppAndLogIn  wdpublic1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wdpublic1
Suite Teardown    Close browser
Force Tags    privatePublicDis

*** Test Case ***

UpdatePublicHubName 
    [Tags]    testrailid=16371    critical
    ${key}    Generate random string    12    [LETTERS]
    ${newkey}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickAddNewHub
    inputHubName  ${key}
    clickCreatenewHub
    selectHubName  ${key}
    clickHubSetting
    inputHubName  ${newkey}
    clickUpdateHub
    selectDiscoverMenu
    isHubNameUpdated  ${newkey}  ${key}
    selectHubName  ${newkey}
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub

changePublicToPrivate
    [Tags]    testrailid=16373    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickAddNewHub
    inputHubName  ${key}
    clickCreatenewHub
    selectHubName  ${key}
    clickHubSetting
    clickPrivateRadio
    clickConfirmChangeHubPermission
    clickUpdateHub
    selectDiscoverMenu
    clickPrivateTab
    selectHubName  ${key}
    isHubDisplayPrivateIcon
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub

DeleteHubFromHubSettingPage 
    [Tags]    testrailid=16378    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickAddNewHub
    inputHubName  ${key}
    clickCreatenewHub
    selectHubName  ${key}
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub
    selectDiscoverMenu
    isHubDeleted  ${key}

everyoneCanFollowAndUnfollowPublicHub
    [Tags]    testrailid=16383    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickAddNewHub
    inputHubName  ${key}
    clickCreatenewHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdpublic2
    selectDiscoverMenu
    clickFollowHubName  ${key}
    selectHubName  ${key}
    clickUnFollowInsideHub
    selectDiscoverMenu
    selectHubName  ${key}
    isUserNotAbleToOpenHub
    clickCancelRequiredFollowPopUp
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdpublic1
    selectDiscoverMenu
    selectHubName  ${key}
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub

adminCanPostTopicForPublicHub
    [Tags]    testrailid=16362    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  16364
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    addImageIntoHubTopic
    addImageIntoHubTopic
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${key}

standardUserCannotPostTopicForPublicHub
    [Tags]    testrailid=16364    critical
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdpublic2
    selectDiscoverMenu
    selectHubName  16364
    clickPencilIconInHub
    isNoPermissionToCreateTopicDisplay
    clickOKButtonInNoPermissionPopup
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdpublic1



# createPublicHubWithAutoFollow 
#     [Tags]    testrailid=16358
#     selectDiscoverMenu
#     clickAddNewHub
#     inputHubName  Public
#     clickAutoFollow
#     clickCreatenewHub
#     isAutoFollowPlublicHubFinish
#     loginPage.logoutWebClient
#     loginPage.loginWebClient  wdpublic2
#     selectDiscoverMenu
#     isAutoFollowPlublicHubDisplay
#     clickAutoFollowPublicHub
#     isGoInsideHub

# EnableAutoFollowForPublicHub
#     [Tags]    testrailid=16375
#     selectDiscoverMenu
#     clickAddNewHub
#     inputHubName  Public
#     clickCreatenewHub
#     clickAutoFollowPublicHub
#     clickHubSetting
#     clickAutoFollow
#     clickSaveSetting
#     loginPage.logoutWebClient
#     loginPage.loginWebClient  wdpublic2
#     selectDiscoverMenu
#     isAutoFollowPlublicHubDisplay
#     loginPage.logoutWebClient
#     loginPage.loginWebClient  wdpublic1
#     selectDiscoverMenu
#     isAutoFollowPlublicHubDisplay

adminUserCannPostTopicForPublicHub
    [Tags]    testrailid=18486    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  Standart2
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${key}

standartUserCanPostTopicForPublicHub
    [Tags]    testrailid=18487    sanity
    ${key}    Generate random string    12    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdpublic2
    selectDiscoverMenu
    selectHubName  Standart2
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${key}

canOpenPlublicHubFromRecentPage
    [Tags]    testrailid=20140    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickAddNewHub
    inputHubName  ${key}
    clickSaveSetting
    selectRecentMenu
    selectChatroomByTitle  ${key}
    isHubTitleDisplay  ${key}