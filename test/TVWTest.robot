*** Settings ***

Library    SeleniumLibrary
Library    String
Library    ../main/resources/testlibs/ctrl_or_command.py


Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/discover/discoverPage.robot
Resource                  ../main/keyword/discover/insideDiscoverPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/task/taskPage.robot
Resource                  ../main/keyword/TVW/tvwLoginPage.robot
Resource                  ../main/keyword/TVW/tvwRegisterPage.robot
Resource                  ../main/keyword/TVW/10MinPage.robot
Resource                  ../main/keyword/portal/portalPage.robot
Resource                  ../main/keyword/TVW/tvwFogotPasswordPage.robot
Resource                  ../main/keyword/TVW/yahooPage.robot
Resource                  ../main/keyword/TVW/tvwSetChangenewPasswordPage.robot

Suite Setup    openWindowToTVW
Test Setup    closeAndOpenTVW
Suite Teardown    Close browser
Force Tags    TVW

*** Test Case ***

loginPageShowCorrectDetail
    [Tags]    testrailid=51627    sanity  AUTOQA-300
    isTVWLoginPageDisplay

loginPageShowCorrectDetailAfterCloseWebAndOpenAgain
    [Tags]    testrailid=51628    sanity  AUTOQA-300
    sleep    3s
    Go to    ${ClientURL}
    Go to    ${TVWLoginURL}
    isTVWLoginPageDisplay

registerPageShowCorrectDetail
    [Tags]    testrailid=51629    sanity  AUTOQA-300
    clickRegisterButton
    isTVWRegisterPageDisplay

loginPageDisplayCorrectDetailAfterBackFromRegisterpage
    [Tags]    testrailid=51630    sanity  AUTOQA-300
    clickRegisterButton
    clickBacktoLoginButton
    isTVWLoginPageDisplay

termAndConditionPageDisplayCorrectDetail
    [Tags]    testrailid=51631    sanity  AUTOQA-300
    clickRegisterButton
    clickTermAndConditionLink
    isAppReDirectToCorrectURL    https://truevirtualworld.com/about/
    
verifyEmailIncomeAfterRegisterTVW
    [Tags]    testrailid=51632    sanity  AUTOQA-341
    Go to    ${10MinURL}
    maximize browser window
    clickCopyEmail
    OpenWebInNewWindowTab  ${TVWLoginURL}
    sleep    3s
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    sleep    3s
    clickRegisterButton
    sleep    2s
    Click Element    ${EMAILINPUT}
    setCTRLOrCommandKey
    Press Keys       None    ${CTRL_OR_COMMAND}+v
    inputFirstName  Firstname
    inputLastName  Lastname
    inputMobileNumber  0853645879
    inputPassword  123456789gG$
    inputConfirmPassword  123456789gG$
    inputBirthDate  2000-12-19
    clickEmailField
    tickAgreeRadioField
    sleep    3s
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickSignupButton
    sleep    3s
    Switch Window  @{Windowhandles}[0]
    sleep    10s
    isEmailIncome

landingPageDisplayAfterClickVerifyEmail
    [Tags]    testrailid=51633    sanity  AUTOQA-341
    Go to    ${10MinURL}
    maximize browser window
    clickCopyEmail
    OpenWebInNewWindowTab  ${TVWLoginURL}
    sleep    3s
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    clickRegisterButton
    sleep    2s
    Click Element    ${EMAILINPUT}
    setCTRLOrCommandKey
    Press Keys       None    ${CTRL_OR_COMMAND}+v
    inputFirstName  Firstname
    inputLastName  Lastname
    inputMobileNumber  0853645879
    inputPassword  123456789gG$
    inputConfirmPassword  123456789gG$
    inputBirthDate  2000-12-19
    clickEmailField
    tickAgreeRadioField
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickSignupButton
    sleep    3s
    Switch Window  @{Windowhandles}[0]
    sleep    10s
    clickEmailByName  Confirm your email address on True virtual world
    clickButtonByName  Confirm this email address
    chooseNewPopup
    isVLearnButtonDisplay
    isVWorkButtonDisplay

webLandToVLearnAndVWorkAfterLoginInCaseUserRegisterFinishButNotHaveNetwork
    [Tags]    testrailid=51634    sanity  AUTOQA-341
    clickSignInButton
    inputTVWEmail  peemawkonkuneng1@gmail.com
    inputPassword  123456789gG$
    clickSignInButton
    isVLearnButtonDisplay
    isVWorkButtonDisplay

canLoginInCaseHaveNetworkAlready
    [Tags]    testrailid=51635    sanity  AUTOQA-341
    clickSignInButton
    inputTVWEmail  chaddanai@amity.co
    inputPassword  123456789gG$
    clickSignInButton
    clickVLearnButton
    clickChooseUser
    isLogInSuccess
    isUserDisplayInRecentPage  Tokyo

forgotPasswordDisplayInLoginPage
    [Tags]    testrailid=41077    sanity  AUTOQA-341
    isForgotPasswordButtonDisplay

forgotPasswordPageDisplayInputEmailField
    [Tags]    testrailid=41078    sanity  AUTOQA-341
    clickForgotPasswordButton
    isForgotPasswordButtonDisplay

errorDisplayInCaseNotInputEmailAndNotTickAgreeInForgotPasswordPage
    [Tags]    testrailid=41079    sanity  AUTOQA-343
    clickForgotPasswordButton
    sleep    3s
    clickSendEmaiilButton
    isNotInputEmail
    isNotTickAgree

errorDisplayInCaseInputCorrectEmailButNotTickAgreeInForgotPasswordPage
    [Tags]    testrailid=41080    sanity  AUTOQA-343
    clickForgotPasswordButton
    sleep    3s
    inputTVWEmail  chaddanai@amity.co
    sleep    3s
    clickSendEmaiilButton
    isNotTickAgree

errorDisplayInCaseNotInputEmailButTickAgreeInForgotPasswordPage
    [Tags]    testrailid=41081    sanity  AUTOQA-343
    clickForgotPasswordButton
    sleep    3s
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    sleep    3s
    clickSendEmaiilButton
    isNotInputEmail

errorDisplayInCaseInputIncorrectEmailAndTickAgreeInForgotPasswordPage
    [Tags]    testrailid=41082    sanity  AUTOQA-343
    clickForgotPasswordButton
    sleep    3s
    inputTVWEmail  bademail@gmail.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    isInputIncorrectEmail

errorDisplayInCaseInputEmailThatNotHaveIDPAccountAndTickAgreeInForgotPasswordPage
    [Tags]    testrailid=41083    sanity  AUTOQA-343
    clickForgotPasswordButton
    sleep    3s
    inputTVWEmail  vudafon@gmail.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    isInputIncorrectEmail

messageDisplayAfterSendEmailSuccessInForgotPasswordPage
    [Tags]    testrailid=41084    sanity  AUTOQA-343
    clickForgotPasswordButton
    sleep    3s
    inputTVWEmail  peemawkonkuneng1@gmail.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    isForgotPasswordLinkSentToEmail

getEmailAfterClickForgotEmail
    [Tags]    testrailid=41085    sanity  AUTOQA-344
    ## Check yahoo mail is empty
    Go to    ${YahooURL}
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    checkEmailisEmpty
    Go to    ${TVWLoginURL}
    clickForgotPasswordButton
    sleep    3s
    inputTVWEmail  test99email@yahoo.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    Go to    ${YahooURL}
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    clickIncomeEmailByName  Password Reset Request
    isForgotPasswordButtonDisplayInMail
    

getMoreEmailAfterClickDidNotReceiveEmail
    [Tags]    testrailid=41086    sanity  AUTOQA-344
    ## Check yahoo mail is empty
    Go to    ${YahooURL}
    maximize browser window
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    checkEmailisEmpty

    Go to    ${TVWLoginURL}
    clickForgotPasswordButton
    sleep    3s
    maximize browser window
    inputTVWEmail  test99email@yahoo.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    clickNotReceiveEmaiilButton
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    Go to    ${YahooURL}
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    isMultipleEmail  2
    
canNotForgotPasswordByPreviousEmail
    [Tags]    testrailid=41087    sanity  AUTOQA-344
    ## Check yahoo mail is empty
    Go to    ${YahooURL}
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$  
    checkEmailisEmpty

    Go to    ${TVWLoginURL}
    clickForgotPasswordButton
    sleep    3s
    maximize browser window
    inputTVWEmail  test99email@yahoo.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    sleep    3s
    Go to    ${TVWLoginURL}
    clickForgotPasswordButton
    sleep    3s
    inputTVWEmail  test99email@yahoo.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    sleep    3s
    Go to    ${YahooURL}
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    clickForgotEmailByindex  3
    clickForgotPasswordEmail
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    isCanNotForgotPasswordTextDisplay
    
canLandToInputEmailPageAfterClickStartAgain
    [Tags]    testrailid=41088    sanity  AUTOQA-344
    ## Check yahoo mail is empty
    Go to    ${YahooURL}
    maximize browser window
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    checkEmailisEmpty
    Go to    ${TVWLoginURL}
    clickForgotPasswordButton
    sleep    3s
    maximize browser window
    inputTVWEmail  test99email@yahoo.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    sleep    3s
    clickNotReceiveEmaiilButton
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    Go to    ${YahooURL}
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    clickForgotEmailByindex  3
    clickForgotPasswordEmail
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    clickForgotPasswordButton
    isInputEmailFieldDisplay
    sleep    3s

canLandToSetNewPasswordPageAfterClickLatestEmail
    [Tags]    testrailid=41090    sanity  AUTOQA-345
    ## Check yahoo mail is empty
    Go to    ${YahooURL}
    maximize browser window
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    checkEmailisEmpty

    Go to    ${TVWLoginURL}
    clickForgotPasswordButton
    sleep    3s
    maximize browser window
    inputTVWEmail  test99email@yahoo.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    sleep    3s
    Go to    ${YahooURL}
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    clickForgotEmailByindex  2
    clickForgotPasswordEmail
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    isSetNewPasswordPageDisplay
    sleep    3s

errorDisplayInCaseNewpasswordNotMatchConfirmNewPasswordInSetNewPasswordPage
    [Tags]    testrailid=41091    sanity  AUTOQA-345
    ## Check yahoo mail is empty
    Go to    ${YahooURL}
    maximize browser window
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    checkEmailisEmpty

    Go to    ${TVWLoginURL}
    clickForgotPasswordButton
    sleep    3s
    maximize browser window
    inputTVWEmail  test99email@yahoo.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    sleep    3s
    Go to    ${YahooURL}
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    clickForgotEmailByindex  2
    clickForgotPasswordEmail
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    inputNewPasswordInSetNewPasswordPage  987654321gG$
    inputConfirmNewPasswordInSetNewPasswordPage  777654321gG$
    clickSetNewPassword
    isComfirmNewPasswordNotMatchWithnewPassword
    sleep    3s

CanSetNewPasswordFromForgotPassword
    [Tags]    testrailid=41092    sanity  AUTOQA-345
    ## Check yahoo mail is empty
    Go to    ${YahooURL}
    maximize browser window
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    checkEmailisEmpty

    Go to    ${TVWLoginURL}
    clickForgotPasswordButton
    sleep    3s
    maximize browser window
    inputTVWEmail  test99email@yahoo.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    sleep    3s
    Go to    ${YahooURL}
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    clickForgotEmailByindex  2
    clickForgotPasswordEmail
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    inputNewPasswordInSetNewPasswordPage  987654321gG$
    inputConfirmNewPasswordInSetNewPasswordPage  987654321gG$
    clickSetNewPassword
    clickVWorkButton
    inputTVWEmail  test99email@yahoo.com
    inputPassword  987654321gG$
    clickSignInButton
    isLogInSuccess
    isUserDisplayInRecentPage  EKO
    clickSettingButton
    clickChangePasswordButton
    chooseNewPopup
    inputCurrentPasswordInChangePasswordPage  987654321gG$
    inputNewPasswordInChangePasswordPage  123456789gG$
    inputConfirmNewPasswordInChangePasswordPage  123456789gG$ 
    clickSubmitChangePasswordButton
    Sleep    3s

CanLandToLoginPageAfterClickTVWorkOrTVLearnInForgotPasswordFlow
    [Tags]    testrailid=41093    sanity  AUTOQA-345
    ## Check yahoo mail is empty
    Go to    ${YahooURL}
    maximize browser window
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    checkEmailisEmpty

    Go to    ${TVWLoginURL}
    clickForgotPasswordButton
    sleep    3s
    maximize browser window
    inputTVWEmail  test99email@yahoo.com
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    sleep    3s
    Go to    ${YahooURL}
    checkIsloginAlreadyHahoomailOrNot  test99email  123456789gG$
    clickForgotEmailByindex  2
    clickForgotPasswordEmail
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    inputNewPasswordInSetNewPasswordPage  987654321gG$
    inputConfirmNewPasswordInSetNewPasswordPage  987654321gG$
    clickSetNewPassword
    clickVWorkButton
    isTVWLoginPageDisplay
    inputTVWEmail  test99email@yahoo.com
    inputPassword  987654321gG$
    clickSignInButton
    clickSettingButton
    clickChangePasswordButton
    chooseNewPopup
    inputCurrentPasswordInChangePasswordPage  987654321gG$
    inputNewPasswordInChangePasswordPage  123456789gG$
    inputConfirmNewPasswordInChangePasswordPage  123456789gG$ 
    clickSubmitChangePasswordButton
    
canJoinExitingWorkspaceInRegisterFlow
    [Tags]    testrailid=51640    sanity  AUTOQA-342
    ${key1}    Generate random string    8    [LETTERS]
    ${key2}    Generate random string    8    [LETTERS]
    ${key3}    Generate random string    8    [LETTERS]
    Go to    ${10MinURL}    
    maximize browser window
    clickCopyEmail
    OpenWebInNewWindowTab  ${TVWLoginURL}
    sleep    3s
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    clickRegisterButton
    sleep    2s
    Click Element    ${EMAILINPUT}
    setCTRLOrCommandKey
    Press Keys       None    ${CTRL_OR_COMMAND}+v
    inputFirstName  ${key1}
    inputLastName  ${key2}
    inputMobileNumber  0853645879
    inputPassword  123456789gG$
    inputConfirmPassword  123456789gG$
    inputBirthDate  2000-12-19
    clickEmailField
    tickAgreeRadioField
    sleep    2s
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickSignupButton
    sleep    3s
    Switch Window  @{Windowhandles}[0]
    sleep    10s
    clickEmailByName  Confirm your email address on True virtual world
    clickButtonByName  Confirm this email address 
    chooseNewPopup
    sleep    3s
    clickVLearnButton
    clickJoinExitingButton
    inputWorkspaceID  tvlea
    tickAgreeRadioField
    clickComfirmWorkSpaceButton
    clickChooseUser
    isLogInSuccess
    isUserDisplayInRecentPage  ${key1}
    selectDirectoryMenu
    inputElasticSearch  Tokyo
    isSearchUserDisplaybyName  Tokyo Osaka

# canCreateOfficialAccountOnVLearn
#     [Tags]    testrailid=51638    sanity  AUTOQA-342
    # ${key1}    Generate random string    8    [LETTERS]
    # ${key2}    Generate random string    8    [LETTERS]
    # ${key3}    Generate random string    8    [LETTERS]
    # Go to    ${10MinURL}    
    # maximize browser window
    # clickCopyEmail
    # OpenWebInNewWindowTab  ${TVWLoginURL}
    # sleep    3s
    # @{Windowhandles}    get window handles
    # Switch Window  @{Windowhandles}[1]
    # clickRegisterButton
    # sleep    2s
    # Click Element    ${EMAILINPUT}
    # setCTRLOrCommandKey
    # Press Keys       None    ${CTRL_OR_COMMAND}+v
    # inputFirstName  ${key1}
    # inputLastName  ${key2}
    # inputMobileNumber  0853645879
    # inputPassword  123456789gG$
    # inputConfirmPassword  123456789gG$
    # inputBirthDate  2000-12-19
    # clickEmailField
    # tickAgreeRadioField
    # selectCapchaFrame
    # tickCapchaRadioField
    # Unselect frame
    # clickSignupButton
    # sleep    3s
    # Switch Window  @{Windowhandles}[0]
    # sleep    10s
    # clickEmailByName  Confirm your email address on True virtual world
    # clickButtonByName  Confirm this email address 
    # chooseNewPopup
    # clickVLearnButton

    # Go to    https://review-registration-staging.ekoapp.com/create/account?registrationToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImdnYnZuanF4dm11ZnF3bHhhdUB1cGl2ZWQub25saW5lIiwiZ2l2ZW5fbmFtZSI6IktUU21xWXlCIiwiZmFtaWx5X25hbWUiOiJjT2tIQlVGZyIsImZpcnN0bmFtZSI6IktUU21xWXlCIiwibGFzdG5hbWUiOiJjT2tIQlVGZyIsInBob25lX251bWJlciI6Iis2Njg1MzY0NTg3OSIsInVzZXJuYW1lIjoiZ2didm5qcXh2bXVmcXdseGF1QHVwaXZlZC5vbmxpbmUiLCJzdWIiOiI2MDM1ZjVkMDY0ODA3NDAwMjBlYjVhMTIiLCJhdWQiOiIxOTllY2YyOS01ZGU3LTRhMWItYmM0Mi1jYTcwYWZhMzY1M2IiLCJpc3MiOiJodHRwczovL3N0YWdpbmdla29zLWgxLmRldi5la29hcHAuY29tIiwiaWF0IjoxNjE0MTQ5MDgyLCJleHAiOjE2MTQxNTI2ODJ9.wTLefOsKxD_lRJXINNspNF2p7VrKm1mLvykR9WOZpBE&type=vlearn
    # clickOfficialAccount
    
#     Sleep    2s
    # clickDropDownTypeByName  No. of student(s)
    # chooseDropDownByName  1 - 10
#     inputOrganization  Organization
#     Sleep    2s
#     clickDropDownTypeByName  Type of institution
#     chooseDropDownByName  International school
#     inputInstitutionNumber  999
#     Sleep    2s
    #   clickDropDownTypeByName  No. of teachers and education personnel
    #   chooseDropDownByName2
#     inputDisplayName  ${key3}
#     clickDropDownTypeByName  Authorization
#     chooseDropDownByName  Authorized representative and/or Institution owner
#     addDocumentByName  Copy of Educational institution license or a Letter of appointment (Must be signed and/or stamped)
#     addDocumentByName  Copy of ID card of authorized person (Must be signed)
#     tickAgreeRadioField
#     clickNextButton
#     clickConfirmCreateTVWButton
#     sleep    3s
#     clickConfirmCreateTVWButton
#     sleep    5s
#     @{Windowhandles}    get window handles
#     Switch Window  @{Windowhandles}[0]
#     clickEmailByName  Welcome to True virtual world
#     clickButtonByName  Login to True virtual world
#     sleep    3s
#     chooseNewPopup
#     clickChooseUserFotRedirectToRecentPage
#     sleep    5s
#     isLogInSuccess
#     isUserDisplayInRecentPage  ${key1}

canCreateGeneralAccountOnVLearn
    [Tags]    testrailid=51639    sanity  AUTOQA-342
    ${key1}    Generate random string    8    [LETTERS]
    ${key2}    Generate random string    8    [LETTERS]
    ${key3}    Generate random string    8    [LETTERS]
    ${key4}    Generate random string    8    [LETTERS]
    Go to    ${10MinURL}    
    maximize browser window
    clickCopyEmail
    OpenWebInNewWindowTab  ${TVWLoginURL}
    sleep    3s
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    clickRegisterButton
    sleep    2s
    Click Element    ${EMAILINPUT}
    setCTRLOrCommandKey
    Press Keys       None    ${CTRL_OR_COMMAND}+v
    inputFirstName  ${key1}
    inputLastName  ${key2}
    inputMobileNumber  0853645879
    inputPassword  123456789gG$
    inputConfirmPassword  123456789gG$
    inputBirthDate  2000-12-19
    clickEmailField
    tickAgreeRadioField
    sleep    2s
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickSignupButton
    sleep    3s
    sleep    3s
    Switch Window  @{Windowhandles}[0]
    sleep    10s
    clickEmailByName  Confirm your email address on True virtual world
    clickButtonByName  Confirm this email address 
    chooseNewPopup
    clickVLearnButton
    clickCreateGeneralAccount
    inputWorkspaceName  ${key4}
    clickDropDownTypeByName  Type of institution
    chooseDropDownByName  International school
    tickAgreeRadioField
    Sleep  3s
    clickComfirmWorkSpaceButton
    clickChooseUserFotRedirectToRecentPage
    sleep    5s
    isLogInSuccess
    isUserDisplayInRecentPage  ${key1}

canResetPasswordAfterRegisterRegister
    [Tags]    testrailid=43380    sanity  AUTOQA-342
    ${key1}    Generate random string    8    [LETTERS]
    ${key2}    Generate random string    8    [LETTERS]
    ${key3}    Generate random string    8    [LETTERS]
    ${key4}    Generate random string    8    [LETTERS]
    Go to    ${10MinURL}    
    maximize browser window
    clickCopyEmail
    OpenWebInNewWindowTab  ${TVWLoginURL}
    sleep    3s
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    clickRegisterButton
    sleep    2s
    Click Element    ${EMAILINPUT}
    setCTRLOrCommandKey
    Press Keys       None    ${CTRL_OR_COMMAND}+v
    inputFirstName  ${key1}
    inputLastName  ${key2}
    inputMobileNumber  0853645879
    inputPassword  123456789gG$
    inputConfirmPassword  123456789gG$
    inputBirthDate  2000-12-19
    clickEmailField
    tickAgreeRadioField
    sleep    2s
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickSignupButton
    sleep    3s
    Switch Window  @{Windowhandles}[0]
    sleep    10s
    clickEmailByName  Confirm your email address on True virtual world
    clickButtonByName  Confirm this email address 
    chooseNewPopup
    clickVLearnButton
    clickCreateGeneralAccount
    inputWorkspaceName  ${key4}
    clickDropDownTypeByName  Type of institution
    chooseDropDownByName  International school
    tickAgreeRadioField
    Sleep  3s
    clickComfirmWorkSpaceButton
    clickChooseUserFotRedirectToRecentPage
    sleep    5s
    isLogInSuccess
    isUserDisplayInRecentPage  ${key1}
    Go to    ${TVWLoginURL}
    clickForgotPasswordButton
    sleep    3s
    maximize browser window
    sleep    2s
    Click Element    ${EMAILINPUT}
    setCTRLOrCommandKey
    Press Keys       None    ${CTRL_OR_COMMAND}+v
    sleep    2s
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickEmailField
    sleep    3s
    clickSendEmaiilButton
    sleep    3s
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[0]
    clickEmailByName  Password Reset Request
    clickButtonByName  Reset password
    chooseNewPopup
    inputNewPasswordInSetNewPasswordPage  987654321gG$
    inputConfirmNewPasswordInSetNewPasswordPage  987654321gG$
    clickSetNewPassword
    clickVWorkButton
    isLogInSuccess
    isUserDisplayInRecentPage  ${key1}

canCreateOfficialAccountOnVWork
    [Tags]    testrailid=51636    sanity  AUTOQA-355
    ${key1}    Generate random string    8    [LETTERS]
    ${key2}    Generate random string    8    [LETTERS]
    ${key3}    Generate random string    8    [LETTERS]
    ${key4}    Generate random string    8    [LETTERS]
    ${key5}    Generate random string    8    [LETTERS]
    Go to    ${10MinURL}    
    maximize browser window
    clickCopyEmail
    OpenWebInNewWindowTab  ${TVWLoginURL}
    sleep    3s
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    clickRegisterButton
    sleep    2s
    Click Element    ${EMAILINPUT}
    setCTRLOrCommandKey
    Press Keys       None    ${CTRL_OR_COMMAND}+v
    inputFirstName  ${key1}
    inputLastName  ${key2}
    inputMobileNumber  0853645879
    inputPassword  123456789gG$
    inputConfirmPassword  123456789gG$
    inputBirthDate  2000-12-19
    clickEmailField
    tickAgreeRadioField
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickSignupButton
    sleep    3s
    Switch Window  @{Windowhandles}[0]
    sleep    10s
    clickEmailByName  Confirm your email address on True virtual world
    clickButtonByName  Confirm this email address 
    chooseNewPopup
    clickVWorkButton
    clickOfficialAccount
    inputOrganization  Organization
    clickDropDownTypeByName  Industry type
    chooseDropDownByName  Accounting
    inputRegisterNumber  999
    clickDropDownTypeByName  No. of employee(s)
    chooseDropDownByName  1 - 10
    inputOrganizationURL  www.good.com
    inputDisplayName  ${key3}
    clickDropDownTypeByName  Authorization
    chooseDropDownByName  Authorized representative / Business owner
    inputRequestFirstName  ${key4}
    inputRequestLasttName  ${key5}
    inputContactMobile  0853645879
    addDocumentByName  Copy of Company certificate (Must be signed and/or stamped)
    addDocumentByName  Copy of ID card of an Authorized representative (Must be signed)
    tickAgreeRadioField
    sleep    3s
    clickNextButton
    clickConfirmCreateTVWButton
    sleep    3s
    clickConfirmCreateTVWButton
    sleep    5s
    clickLetGoButton
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[0]
    clickEmailByName  Welcome to True virtual world
    clickButtonByName  Login to True virtual world
    chooseNewPopup
    clickChooseUser
    sleep    5s
    isLogInSuccess
    isUserDisplayInRecentPage  ${key1}

canCreateGeneralAccountOnVWork
    [Tags]    testrailid=51637    sanity  AUTOQA-342
    ${key1}    Generate random string    8    [LETTERS]
    ${key2}    Generate random string    8    [LETTERS]
    ${key3}    Generate random string    8    [LETTERS]
    ${key4}    Generate random string    8    [LETTERS]
    Go to    ${10MinURL}    
    maximize browser window
    clickCopyEmail
    OpenWebInNewWindowTab  ${TVWLoginURL}
    sleep    3s
    @{Windowhandles}    get window handles
    Switch Window  @{Windowhandles}[1]
    sleep    2s
    Click Element    ${EMAILINPUT}
    setCTRLOrCommandKey
    Press Keys       None    ${CTRL_OR_COMMAND}+v
    inputFirstName  ${key1}
    inputLastName  ${key2}
    inputMobileNumber  0853645879
    inputPassword  123456789gG$
    inputConfirmPassword  123456789gG$
    inputBirthDate  2000-12-19
    clickEmailField
    tickAgreeRadioField
    selectCapchaFrame
    tickCapchaRadioField
    Unselect frame
    clickSignupButton
    sleep    3s
    Switch Window  @{Windowhandles}[0]
    sleep    10s
    clickEmailByName  Confirm your email address on True virtual world
    clickButtonByName  Confirm this email address 
    chooseNewPopup
    clickVWorkButton
    clickCreateGeneralAccount
    inputWorkspaceName  ${key4}
    clickDropDownTypeByName  Industry type
    chooseDropDownByName  Accounting
    tickAgreeRadioField
    Sleep  3s
    clickComfirmWorkSpaceButton
    clickChooseUserFotRedirectToRecentPage
    sleep    5s
    isLogInSuccess
    isUserDisplayInRecentPage  ${key1}