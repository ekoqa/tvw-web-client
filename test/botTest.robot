*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/task/taskPage.robot
Resource                  ../main/keyword/task/taskDetailPage.robot
Resource                  ../main/keyword/form/formPage.robot
Resource                  ../main/keyword/workflow/workflowHomePage.robot
Resource                  ../main/keyword/workflow/workflowDetailPage.robot
Resource                  ../main/keyword/card/cardPage.robot
Resource                  ../main/keyword/card/cardCreationPage.robot
Resource                  ../main/keyword/profile/profilePage.robot


Suite Setup    openEkoAppAndLogIn    bottest
Test Setup     Go to    ${ClientURL}
Suite Teardown    Close browser
Force Tags    chat

*** Test Case ***

canChatWithBotInDMChat
    [Tags]    testrailid=4065    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  Bot3 Bot
    selectUserFromSearchToCreateChat
    sendMessage  hello
    isUserReply

canChatWithBotInGCChat
    [Tags]    testrailid=4067    critical
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  Bot3 Bot
    selectUserFromSearchToCreateChat
    confirmSelectUser
    confirmCreateChat
    sendMessage  hello
    isUserReply

showIconBotOnProfile
    [Tags]    testrailid=4074    critical
    selectDirectoryMenu
    inputElasticSearch  Bot3 Bot
    isBotIconDisplayOnUserAvatar  Bot3 Bot

canChatWithBotInDMTopic
    [Tags]    testrailid=4066    sanity
    inputElasticSearch  Bot3 Bot
    selectChatroomByIndex  1
    selectTopicTab
    selectTopic  Bot
    sendMessage  hello
    isUserReply    


canChatWithBotInGCTopic
    [Tags]    testrailid=4068    sanity
    inputElasticSearch  Bot
    selectChatroomByIndex  1
    selectTopicTab
    selectTopic  Bot
    sendMessage  hello
    isUserReply

canChatWithBotInTaskComment
    [Tags]    testrailid=4069    sanity
    selectTaskMenu
    searchTaskByName  Bot
    selectTaskName  Bot
    clickTaskComment
    sendMessage  hello
    isUserReply

canChatWithBotInFormComment
    [Tags]    testrailid=4070    sanity
    selectFormMenu
    selectFirstForm
    sendMessage  hello
    isUserReply

canChatWithBotInWorkflowComment
    [Tags]    testrailid=4071    sanity
    selectWorkflowMenu
    searchWorkflowByKeyword  Bot
    selectWorkflowByIndex  1
    openCommentInWorkflow
    sendMessage  hello
    isUserReply

canSendCommentInCard
    [Tags]    testrailid=4072    sanity
    selectCardMenu
    SelectCardName  Bot
    clickOpenCommentChatInCardForEditor
    sendMessage  hello
    isUserReply

canSendCommentInBotProfile
    [Tags]    testrailid=4073    sanity
    selectDirectoryMenu
    inputElasticSearch  Bot3
    selectFirstUser
    createDcChatFromProfile
    sendMessage  hello
    isUserReply