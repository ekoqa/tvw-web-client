*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/portal/portalPage.robot
Resource                  ../main/keyword/workflow/workflowCreationPage.robot
Resource                  ../main/keyword/workflow/workflowHomePage.robot


Suite Setup    openEkoAppAndLogIn  webbot1
Test Setup     Go to    ${ClientURL}
Test Teardown   handleFailcaseWithUser  webbot1
Suite Teardown    Close browser
Force Tags    others

*** Test Case ***

appDisplayQuickReplyButton
    [Tags]    testrailid=41128    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  botQuickReply
    isQuickReplyListDisplay
    isIconDisplayOnQuickReplyList
    sendMessage  clear

userCanScrollToSeeMoreQuickReplyList
    [Tags]    testrailid=41133    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  botQuickReply
    clickToSeeMoreQuickReplyList
    isQucikReplyButtonDisplay  convoLab
    sendMessage  clear

appReturnMessageWhenUserTapOnQuickReplyButton
    [Tags]    testrailid=41134    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  botQuickReply
    tapOnQuickReplyButton  Bangkok
    isMessageSent  True Digital Park

appRedirectToCorrectURLWhenUserTapOnQuickReplyButton
    [Tags]    testrailid=41135    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  botQuickReply
    tapOnQuickReplyButton  amity
    isAppReDirectToCorrectURL  https://www.amity.co/
    Close browser
    openEkoAppAndLogIn  webbot1

quickReplyListDisaapearWhenUserSendMessage
    [Tags]    testrailid=41138    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  botQuickReply
    isIconDisplayOnQuickReplyList
    sendMessage  clear
    isQuickReplyButtonDisappear

userCanReceiveSameMessageWhenTypingCorrectMessage
    [Tags]    testrailid=41139    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  botQuickReply
    isIconDisplayOnQuickReplyList
    sendMessage  True Digital Park
    isBotReplyCorrect  HeadQuarter

appWillGenerateMessageWhenUserTapOnMapImage
    [Tags]    testrailid=38845    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  imageMap
    tapOnCenterLeftOfImageMap
    isMessageSent  You're my favorite Deputy!


appWillOpenCorrectURLWhenUserTapOnImageMap
    [Tags]    testrailid=38856    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  imageMap
    tapOnCenterButtomOfImageMap
    isAppReDirectToCorrectURL  https://www.youtube.com/watch?v=wmiIUN-7qhE

canForwardAudioMessageThatReceiveFromBot
    [Tags]    testrailid=43413    critical
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  iosbot1
    selectChatroomByIndex  1
    sendMessage  ${key}
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  audio
    isVoiceSent
    clickThreeDotOnLatestMessage
    clickForwardButton
    selectRecentChatroomToForwardMessage  iosbot1 ios
    selectRecentTopicToForwardMessage  General Topic
    clickConfirmForwardMessage
    selectChatroomByTitle  iosbot1 ios
    isVoiceSent

canForwardVideoMessageThatReceiveFromBot
    [Tags]    testrailid=43425    critical
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  iosbot1
    selectChatroomByIndex  1
    sendMessage  ${key}
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  video
    isVideoSent
    clickThreeDotOnLatestMessage
    clickForwardButton
    selectRecentChatroomToForwardMessage  iosbot1 ios
    selectRecentTopicToForwardMessage  General Topic
    clickConfirmForwardMessage
    selectChatroomByTitle  iosbot1 ios
    isVideoSent

userCanReceiveAllResultButtonFromWorkflow
    [Tags]    testrailid=45906    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  /searchWorkflowTemplate Pri
    isAllWorkflowTemplateResultContainKeyword  Pri
    sendMessage  clear

userCanRedirectToCorrectWorkflowTemplate
    [Tags]    testrailid=45907    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  /searchWorkflowTemplate Advance Workflow
    tapOnQuickReplyButton  Advance Workflow
    isAppRedirectToCorrectWorkflowTemplate  Advance Workflow
    clickCloseWorkflowCreationPage
    sendMessage  clear

userCanSubmitWorkflowAfterClickFromBotReply
    [Tags]    testrailid=45908    critical
    ${key}    Generate random string    5    [LETTERS]
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  /searchWorkflowTemplate Normal Workflow
    tapOnQuickReplyButton  Normal Workflow
    inputTextInCustomField  Question  ${key}
    clickOpenUserListInField  Co-worker
    searchForWorkflowReceiver  bot1
    selectUserToAddInWorkflow  bot1
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    isWorkflowSent  ${key}

appWillRedirectUserToWorkflowDetailPageCorrectly
    [Tags]    testrailid=45909    critical
    ${key}    Generate random string    9    [LETTERS]
    ${search}  Catenate  /searchWorkflow  ${key}
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  /searchWorkflowTemplate Normal Workflow
    tapOnQuickReplyButton  Normal Workflow
    inputTextInCustomField  Question  ${key}
    clickOpenUserListInField  Co-worker
    searchForWorkflowReceiver  bot1
    selectUserToAddInWorkflow  bot1
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    selectRecentMenu
    selectChatroomByTitle  bossbot
    sendMessage  ${search}
    tapOnQuickReplyButton  ${key}
    isWorkflowSent  ${key}

canGetAudioMessageThatReceiveFromBot
    [Tags]    testrailid=43405    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  audio
    isVoiceSentFromBot

canSeeForwadButtonInThreeDotInAudioMessage
    [Tags]    testrailid=43412    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  audio
    clickThreeDotOnLatestMessage
    isOnlyForwardButtonDisplay
      
canGetVideoMessageThatReceiveFromBot
    [Tags]    testrailid=43415    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  video
    isVideoSent

canSeeForwadButtonInThreeDotInVideoMessage
    [Tags]    testrailid=43424    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  video
    clickThreeDotOnLatestMessage
    isOnlyForwardButtonDisplay

workflowButtonStilShowAfterUserGoToOtherPage
    [Tags]    testrailid=45911    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  /searchWorkflowTemplate Normal Workflow
    tapOnQuickReplyButton  Normal Workflow
    clickOpenUserListInField  Co-worker
    searchForWorkflowReceiver  bot1
    selectUserToAddInWorkflow  bot1
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    selectRecentMenu
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    isAllWorkflowTemplateResultContainKeyword  Normal Workflow
    sendMessage  clear

workflowButtonDisappearAfterUserSendAnyMessage
    [Tags]    testrailid=45912    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  /searchWorkflowTemplate Normal Workflow
    sendMessage  ${key}
    isResultContainKeywordNotDisplay  Normal Workflow

buttonStillShowAfterClosePageThatOpenFromBotReply
    [Tags]    testrailid=41140    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  botQuickReply
    selectChatroomByTitle  iosbot1 ios
    selectChatroomByTitle  bossbot
    isQuickReplyListDisplay

buttonChangeFollowKeywordThatSendToBot
    [Tags]    testrailid=41141    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  /searchWorkflowTemplate Normal Workflow
    isQuickReplyButtonDisplay  Normal Workflow
    sendMessage  /searchWorkflowTemplate Advance Workflow
    isQuickReplyButtonDisplay  Advance Workflow
    sendMessage  clear

appWillShow10MaximumCarousel
    [Tags]    testrailid=51626    sanity  94
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  carousel
    isAppCanDisplayAll10Carousel

appWillShowButtonCorrectly
    [Tags]    testrailid=51514    sanity  94
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  button
    isBotButtonDisplayCorrect

appWillGenerateMessageActionFromCarousel
    [Tags]    testrailid=51516   sanity  94
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  carousel
    clickOnBotButtonByText  Accept 
    isAppReturnTextMessageWhenClickOnBotButton  accept

appWillOpenURLWhenUserClickOnOpenUrlButton
    [Tags]    testrailid=51517   sanity  94
    inputElasticSearch  bossbot
    selectChatroomByIndex  1
    sendMessage  carousel
    clickOnBotButtonByText  Link
    isAppRedirectToCorrectURLFromChatPage  https://www.blognone.com/sites/default/files/externals/d2d9b8a7411f58f5c8ecbbda52048fe9.jpg
    selectTabURL  https://eko-dev.ekoapp.com/recents/group/5f71760aa23189d8d6d92e68/chat/t/5f71760aa23189abc6d92e69

appDisplayQuickReplyButtonInCaseButtonLessThan13
    [Tags]    testrailid=41127    sanity  Autoqa_303
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  quickReply2
    isQuickReplyListDisplay
    sendMessage  clear

appReturnPostBackMessageWhenUserTapOnQuickReplyButton
    [Tags]    testrailid=41137    sanity  Autoqa_303
    createDCFromRecent
    searchUserInCreateChatSearchBox  bossbot
    selectUserFromSearchToCreateChat
    sendMessage  greenday
    tapOnQuickReplyButton  Ping
    isMessageSent  pong
