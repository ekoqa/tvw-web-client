*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/jitsi/vRoomHomePage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/chat/chatMemberPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/profile/profilePage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/admin/user/userDetailPage.robot
Resource                  ../main/keyword/admin/user/userListHomePage.robot
Resource                  ../main/keyword/admin/adminLandingPage.robot


Suite Setup    openEkoAppAndLogIn  wjitsi1
Test Setup     Go to    ${ClientURL}
Suite Teardown    Close browser
Force Tags    others

*** Test Case ***

vRoomWebviewOpenAfterSelectConferenceMenu
    [Tags]    testrailid=10730    critical
    selectConferenceMenu
    isVroomPageDisplayCorrect

canCreateConferenceFromRecentPage
    [Tags]    testrailid=10732    critical
    ${key}    Generate random string    12    [LETTERS]
    ${current_window}    get window handles
    createConferenceFromRecent
    recentPage.setConferenceRoomName  ${key}
    confirmCreateConferenceRoom
    ${new_window_list}    get window handles
    isNewTabOpen  ${current_window}  ${new_window_list}
    Close browser
    openEkoAppAndLogIn  wjitsi1

jitsiButtonIsDisplayInDirectChat
    [Tags]    testrailid=8075    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  jitsi2
    selectUserFromSearchToCreateChat
    isJitsiIconDisplay

canCreateConferenceFromDM
    [Tags]    testrailid=8080    critical
    ${key}    Generate random string    4    [LETTERS]
    ${current_window}    get window handles
    createDCFromRecent
    searchUserInCreateChatSearchBox  jitsi2
    selectUserFromSearchToCreateChat
    sendMessage  ${key}
    clickCreateJitsiInChat
    isUserAbleToGenerateJitsiCall
    clickJoinConferenceFromLatestMessage
    ${new_window_list}    get window handles
    isNewTabOpen  ${current_window}  ${new_window_list}
    Close browser
    openEkoAppAndLogIn  wjitsi1

canCreateConferenceFromGC
    [Tags]    testrailid=8083    critical
    ${key}    Generate random string    4    [LETTERS]
    ${current_window}    get window handles
    createGCFromRecent
    searchUserInCreateChatSearchBox  jitsi2
    selectUserFromSearchToCreateChat
    searchUserInCreateChatSearchBox  jitsi3
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  ${key}
    confirmCreateChat
    clickCreateJitsiInChat
    isUserAbleToGenerateJitsiCall
    clickJoinConferenceFromLatestMessage
    ${new_window_list}    get window handles
    isNewTabOpen  ${current_window}  ${new_window_list}
    Close browser
    openEkoAppAndLogIn  wjitsi1

newAddedUserCanSeePreviousJitsiCallButton
    [Tags]    testrailid=8086    critical
    ${key}    Generate random string    4    [LETTERS]
    ${current_window}    get window handles
    createGCFromRecent
    searchUserInCreateChatSearchBox  jitsi2
    selectUserFromSearchToCreateChat
    searchUserInCreateChatSearchBox  jitsi3
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  ${key}
    confirmCreateChat
    clickCreateJitsiInChat
    selectMemberTab
    addMemberToGC
    searchUserInCreateChatSearchBox  jitsi4
    selectUserFromSearchToCreateChat
    loginPage.logoutWebClient
    loginPage.loginWebClient  wjitsi4
    selectChatroomByTitle  ${key}
    isNewAddedUserCanSeeJitsiJoinButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wjitsi1

canGenerateJitsiMessageForNewExistingDCFromProfilePage
    [Tags]    testrailid=8088    critical
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    selectDirectoryMenu
    inputElasticSearch  ${firstName}
    selectFirstUser
    clickStartJitsiCallFromProfilePage
    isUserAbleToGenerateJitsiCall
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}

jitsiMenuDisplayOnSideMenu
    [Tags]    testrailid=10731    sanity
    isConferenceMenuDisplayOnSideBar
    

# canCreateConferenceFromConferenceMenu    
#     [Tags]    testrailid=10733
#     ${key}    Generate random string    12    [LETTERS]
#     selectConferenceMenu
#     vRoomHomePage.setConferenceRoomName  ${key}
#     startConferenceRoom
#     sleep    2s
#     Go to  ${ConfelenceURL}
#     selectConferenceMenu
#     sleep    1s
#     isConferenceNameDisplayCorrect  ${key}

# canCreateConferenceWithUseOldRoomName    
#     [Tags]    testrailid=10736
#     ${key}    Generate random string    12    [LETTERS]
#     selectConferenceMenu
#     vRoomHomePage.setConferenceRoomName  ${key}
#     startConferenceRoom
#     sleep    2s
#     Go to  ${ConfelenceURL}
#     selectConferenceMenu
#     sleep    1s
#     vRoomHomePage.setConferenceRoomName  ${key}
#     startConferenceRoom
#     sleep    2s
#     Go to  ${ConfelenceURL}
#     selectConferenceMenu
#     sleep    1s
#     isConferenceNameDisplayCorrect  ${key}

otherUserCanJoinCallInDMChat
    [Tags]    testrailid=8081    sanity  Autoqa_307
    ${key}    Generate random string    4    [LETTERS]
    ${current_window}    get window handles
    createDCFromRecent
    searchUserInCreateChatSearchBox  jitsi2
    selectUserFromSearchToCreateChat
    clickCreateJitsiInChat
    loginPage.logoutWebClient
    loginPage.loginWebClient  wjitsi2
    createDCFromRecent
    searchUserInCreateChatSearchBox  jitsi1
    selectUserFromSearchToCreateChat
    isUserAbleToGenerateJitsiCall
    clickJoinConferenceFromLatestMessage
    ${new_window_list}    get window handles
    isNewTabOpen  ${current_window}  ${new_window_list}
    Close browser
    openEkoAppAndLogIn  wjitsi1
