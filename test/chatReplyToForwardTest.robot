*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/directory/directoryPage.robot
Resource                  ../main/keyword/chat/chatMemberPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/chat/mediaAndFilePage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/profile/profilePage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/admin/user/userDetailPage.robot
Resource                  ../main/keyword/admin/user/userListHomePage.robot
Resource                  ../main/keyword/admin/adminLandingPage.robot



Suite Setup    openEkoAppAndLogIn  wchatreply1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wchatreply1
Suite Teardown    Close browser
Force Tags    chat

*** Test Case ***

canReplyTextByTextMessage
    [Tags]    testrailid=3924    critical
    ${key}    Generate random string    8    [LETTERS]
    ${reply}  Generate random string    8    [LETTERS]
    inputElasticSearch  chatreply2
    selectChatroomByIndex  1
    sendMessage  ${key}
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply2
    selectChatroomByTitle  chatreply1 web
    clickThreeDotOnSpecificMessage  ${key}
    clickReplyToButton
    sendMessage  ${reply}
    isReplyToTextSuccess  ${key}
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply1

CanReplyMentionyTextMessage
    [Tags]    testrailid=3925    critical
    ${reply}  Generate random string    8    [LETTERS]
    inputElasticSearch  mention
    selectChatroomByIndex  1
    sendMentionMessage  chatreply2
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply2
    selectChatroomByTitle  mentionGroup
    clickThreeDotOnLatestMessage
    clickReplyToButton
    sendMessage  ${reply}
    isReplyToTextSuccess  @chatreply2
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply1

canReplyStickerByTextMessage
    [Tags]    testrailid=3926    critical
    ${reply}  Generate random string    8    [LETTERS]
    inputElasticSearch  chatreply2
    selectChatroomByIndex  1
    clickOpenSticker
    selectStickerToSend
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply2
    selectChatroomByTitle  chatreply1 web
    clickThreeDotOnLatestMessage
    clickReplyToButton
    sendMessage  ${reply}
    isReplyToStickerSuccess
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply1

canReplyToImageByTextMessage
    [Tags]    testrailid=3927    critical
    ${reply}  Generate random string    8    [LETTERS]
    inputElasticSearch  chatreply2
    selectChatroomByIndex  1
    sendImageFile
    clickConfirmSendMedia
    waitForMediaUpload
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply2
    selectChatroomByTitle  chatreply1 web
    clickThreeDotOnLatestMessage
    clickReplyToButton
    sendMessage  ${reply}
    isReplyToImageSuccess
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply1

canReplyToVideoByTextMessage
    [Tags]    testrailid=3928    critical
    ${reply}  Generate random string    8    [LETTERS]
    inputElasticSearch  chatreply2
    selectChatroomByIndex  1
    sendVideoInChat
    clickConfirmSendMedia
    waitForMediaUpload
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply2
    selectChatroomByTitle  chatreply1 web
    clickThreeDotOnLatestMessage
    clickReplyToButton
    sendMessage  ${reply}
    isReplyToVideoSuccess
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply1

canReplyToFileByTextMessage
    [Tags]    testrailid=3930    critical
    ${reply}  Generate random string    8    [LETTERS]
    inputElasticSearch  chatreply2
    selectChatroomByIndex  1
    sendFileInChat
    clickConfirmSendMedia
    waitForMediaUpload
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply2
    selectChatroomByTitle  chatreply1 web
    clickThreeDotOnLatestMessage
    clickReplyToButton
    sendMessage  ${reply}
    isReplyToFileSuccess
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatreply1

canForwardVoiceToDMChat
    [Tags]    testrailid=3970    critical
    ${reply}  Generate random string    8    [LETTERS]
    inputElasticSearch  chatreply2
    selectChatroomByIndex  1
    sendMessage  hi
    inputElasticSearch  voiceChat
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickForwardButton
    selectRecentChatroomToForwardMessage  chatreply2 web
    selectRecentTopicToForwardMessage  General Topic
    clickConfirmForwardMessage
    selectChatroomByTitle  chatreply2 web
    isVoiceSent

canForwardVoiceToGCChat
    [Tags]    testrailid=3971    critical
    inputElasticSearch  receive
    selectChatroomByIndex  1
    sendMessage  hi
    inputElasticSearch  voiceChat
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickForwardButton
    selectRecentChatroomToForwardMessage  receiveVoice
    selectRecentTopicToForwardMessage  General Topic
    clickConfirmForwardMessage
    selectChatroomByTitle  receiveVoice
    isVoiceSent

canForwardVDOToGCChat
    [Tags]    testrailid=8301    sanity
    inputElasticSearch  receiveVDO
    selectChatroomByIndex  1
    sendMessage  test
    inputElasticSearch  vdoChat
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickForwardButton
    selectRecentChatroomToForwardMessage  receiveVDO
    selectRecentTopicToForwardMessage  General Topic
    clickConfirmForwardMessage
    selectChatroomByTitle  receiveVDO
    isVideoSent

canForwardVDOToDMChat
    [Tags]    testrailid=8302    sanity
    ${reply}  Generate random string    8    [LETTERS]
    inputElasticSearch  chatreply2
    selectChatroomByIndex  1
    sendMessage  test
    inputElasticSearch  vdoChat
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickForwardButton
    selectRecentChatroomToForwardMessage  chatreply2 web
    selectRecentTopicToForwardMessage  General Topic
    clickConfirmForwardMessage
    selectChatroomByTitle  chatreply2 web
    isVideoSent