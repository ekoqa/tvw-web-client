*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/card/cardPage.robot
Resource                  ../main/keyword/card/cardCreationPage.robot
Resource                  ../main/keyword/recent/settingPage.robot


Suite Setup    openEkoAppAndLogIn  wcard1
Test Setup     Go to    ${ClientURL}
Test Teardown   handleFailcaseWithUser  wcard1
Suite Teardown    Close browser
Force Tags  card

*** Test Case ***

createNewCardFromDC
    [Tags]    testrailid=3748    critical
    ${key}    Generate random string    4    [LETTERS]
    inputElasticSearch  card2
    selectChatroomByIndex  1
    sendMessage  ${key}
    clickSendCard
    addTextInCard  ${key}
    sendCardInChat
    isCardSent
    selectCardMenu
    isUnpinCardDisplayCorrectly  ${key}

createNewCardFromTopic
    [Tags]    testrailid=3750    critical
    ${key}    Generate random string    4    [LETTERS]
    inputElasticSearch  card2
    selectChatroomByIndex  1
    selectTopicTab
    selectTopic  cardSent
    sendMessage  ${key}
    clickSendCard
    addTextInCard  ${key}
    sendCardInChat
    isCardSent
    selectCardMenu
    isUnpinCardDisplayCorrectly  ${key}

createNewCardFromRecentPage
    [Tags]    testrailid=3751    critical
    ${key}    Generate random string    6    [LETTERS]
    createCardFromRecent
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    selectCardMenu
    isUnpinCardDisplayCorrectly  ${key}

createNewCardFromCardPage
    [Tags]    testrailid=3752    critical
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    # selectCardMenu
    backToCardMenuPage
    isUnpinCardDisplayCorrectly  ${key}

checkCreatorPermission
    [Tags]    cat
    [Tags]    testrailid=3753    critical
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    selectCardMenu
    SelectCardName  ${key}
    clickAddShareUserIcon
    isUserIsCardCreator  card1 web
    clickCloseButtonInShareCardUserList

editorCanShareCardToOther
    [Tags]    cat 
    [Tags]    testrailid=3754    critical
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    setUserRoleInCardToEditor
    confirmToAddUserInCard
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard3
    selectCardMenu
    SelectCardName  ${key}
    clickAddShareUserIcon
    clickAddMoreUserInCard
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    clickAddShareUserIcon
    isAddedUserIsViewerRole  card2 web
    clickCloseButtonInShareCardUserList
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

editorCanRemoveOtherCardPermission
    [Tags]    
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    selectAddedUserInCardFromContact
    setUserRoleInCardToEditor
    confirmToAddUserInCard
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard3
    selectCardMenu
    SelectCardName    ${key}
    clickAddShareUserIcon
    removeUserFromCard  admin
    confirmEditUserInCard
    selectCardMenu
    SelectCardName    ${key}
    clickAddShareUserIcon
    isUserRemoveFromCard  admin
    clickCloseButtonInShareCardUserList
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

editorCanSetCardPriority
    [Tags]    
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    selectAddedUserInCardFromContact
    setUserRoleInCardToEditor
    confirmToAddUserInCard
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard3
    selectCardMenu
    SelectCardName  ${key}
    setCardPriority  1
    selectCardMenu
    isCardDisplayCorrectPriority  ${key}
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

editorCanSeeHistoryLog
    [Tags]   
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    selectAddedUserInCardFromContact
    setUserRoleInCardToEditor
    confirmToAddUserInCard
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard3
    selectCardMenu
    SelectCardName  ${key}
    clickOpenCardHistoryLog
    isHistoryLogOpen
    clickCloseHistoryLogInCardButton
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

viewerCannotEditContentInCard
    [Tags]    testrailid=3755    critical
    ${key}    Generate random string    6    [LETTERS]
    ${newKey}    Generate random string    6    [LETTERS]
    ${fullKey}    Catenate  SEPARATOR=  ${key}  ${newKey}
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard3
    selectCardMenu
    SelectCardName  ${key}
    isContentIsNotEditable
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

canSendCommentInCard
    [Tags]    testrailid=3756    critical
    ${key}    Generate random string    6    [LETTERS]
    ${name}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${name}
    selectCardMenu
    SelectCardName  ${name}
    clickOpenCommentChatInCardForEditor
    sendMessage  ${key}
    isMessageSent  ${Key}
    clickCloseCommentInCardButton

editorCanEditContentinCard
    [Tags]    testrailid=3757    critical
    ${key}    Generate random string    6    [LETTERS]
    ${newKey}    Generate random string    6    [LETTERS]
    ${fullKey}    Catenate  SEPARATOR=  ${key}  ${newKey}
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    setUserRoleInCardToEditor
    confirmToAddUserInCard
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard3
    selectCardMenu
    SelectCardName  ${key}
    waitOtherToEditCard
    editTextInCard  ${newKey}
    sleep    5s
    selectCardMenu
    isUnpinCardDisplayCorrectly  ${fullKey}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

creatorCanChangeUserPermissionFromEditorToViewer
    [Tags]    testrailid=3758    critical
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUser Icon
    selectUserFromFavoriteContact
    setUserRoleInCardToEditor
    confirmToAddUserInCard
    clickAddShareUserIcon
    changeUserRoleFromEditorToViewer  card3 web
    confirmEditUserInCard
    clickAddShareUserIcon
    isAddedUserIsViewerRole  card3 web
    clickCloseButtonInShareCardUserList

creatorCanChangeUserPermissionFromViewerToEditor
    [Tags]    testrailid=3759    critical    
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    clickAddShareUserIcon
    changeUserRoleFromViewerToEditor  card3 web
    confirmEditUserInCard
    clickAddShareUserIcon
    isAddedUserIsEditorRole  card3 web
    clickCloseButtonInShareCardUserList

creatorCanRemoveUserInCard
    [Tags]    testrailid=3760    critical
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectAddedUserInCardFromContact
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    selectCardMenu
    SelectCardName  ${key}
    clickAddShareUserIcon
    removeUserFromCard  card3 web
    confirmEditUserInCard
    selectCardMenu
    SelectCardName  ${key}
    clickAddShareUserIcon
    isUserRemoveFromCard  card3 web
    clickCloseButtonInShareCardUserList

creatorCanSeeHistoryLog
    [Tags]    testrailid=3761    critical
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    clickOpenCardHistoryLog
    isHistoryLogOpen
    clickCloseHistoryLogInCardButton

canPinCard
    [Tags]    testrailid=3762    critical
    selectCardMenu
    ${name}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${name}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    selectCardMenu
    SelectCardName  ${name}
    clickThreeDotOnCardCreationPage
    addCardToFavorite
    selectCardMenu
    isCardPin  ${name}
    SelectCardName  ${name}
    deleteCard
    isCardDisappear  ${name}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

canUnpinCard
    [Tags]    testrailid=3763    critical
    selectCardMenu
    ${name}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${name}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    selectCardMenu
    SelectCardName  ${name}
    clickThreeDotOnCardCreationPage
    addCardToFavorite
    selectCardMenu
    isCardPin  ${name}
    SelectCardName  ${name}
    clickThreeDotOnCardCreationPage
    removeCardFromFavorite
    selectCardMenu
    isCardUnPin  ${name}

canCloseCard
    [Tags]    testrailid=3764    critical
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectAddedUserInCardFromContact
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    selectCardMenu
    SelectCardName  ${key}
    clickThreeDotOnCardCreationPage
    closeCard
    selectCardMenu
    isCardDisappear  ${key}
    filterCardByClosedCard
    SelectCardName  ${key}
    deleteCard
    isCardDisappear  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

canOpenCard
    [Tags]    testrailid=3765    critical
    selectCardMenu
    ${name}    Generate random string    6    [LETTERS]
    clickCreateNewCard
    addTextInCard  ${name}
    selectCardMenu
    SelectCardName  ${name}
    clickThreeDotOnCardCreationPage
    closeCard
    selectCardMenu
    filterCardByClosedCard
    SelectCardName  ${name}
    clickThreeDotOnCardCreationPage
    openCard
    confirmToOpenCard
    selectCardMenu
    isUnpinCardDisplayCorrectly  ${name}

canSearchCard
    [Tags]    testrailid=3766
    selectCardMenu
    searchForCard    create
    isCardResultContainsKeyword  create

sortAndFilterLastUpdateAndActiveCard
    [Tags]    testrailid=3767    critical
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    selectCardMenu
    sortCardByLastUpdate
    filterCardByActiveCard
    isCardDisPlayCorrectlyAfterSortAndFilter  ${key}

sortAndFilterLastUpdateAndClosedCard
    [Tags]    testrailid=3768    sanity
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickThreeDotOnCardCreationPage
    closeCard
    selectCardMenu
    sortCardByLastUpdate
    filterCardByClosedCard
    isCardDisPlayCorrectlyAfterSortAndFilter  ${key}
    SelectCardName  ${key}
    deleteCard
    isCardDisappear  ${key}	

sortAndFilterLastUpdateAndAllCard
    [Tags]    testrailid=3769    sanity
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    selectCardMenu
    sortCardByLastUpdate
    filterCardByAllCard
    isCardDisPlayCorrectlyAfterSortAndFilter  ${key}
    SelectCardName  ${key}
    deleteCard
    isCardDisappear  ${key}	

sortAndFilterDueDatedAndClosed
    [Tags]    testrailid=3771    critical
    selectCardMenu
    sortCardByDueDate
    filterCardByClosedCard
    isCardDisPlayCorrectlyAfterSortAndFilter  firstDueDateCard

sortAndFilterDueDatedAndActive
    [Tags]    testrailid=3770    sanity
    selectCardMenu
    sortCardByDueDate
    filterCardByActiveCard
    isCardDisPlayCorrectlyAfterSortAndFilter  openDueDate

sortAndFilterDueDatedAndAll
    [Tags]    testrailid=3772    sanity
    selectCardMenu
    sortCardByDueDate
    filterCardByAllCard
    isCardDisPlayCorrectlyAfterSortAndFilter  firstDueDateCard

sortAndFilterPriorityAndActive
    [Tags]    testrailid=3773    sanity
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcardsort1
    selectCardMenu
    sortByCardPriority
    filterCardByActiveCard
    isCardDisPlayCorrectlyAfterSortAndFilter  highPriorityActiveCard
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

sortAndFilterPriorityAndClosed
    [Tags]    testrailid=3774    critical
    selectCardMenu
    sortByCardPriority
    filterCardByClosedCard
    isCardDisPlayCorrectlyAfterSortAndFilter  highPriority

sortAndFilterPriorityAndAll
    [Tags]    testrailid=3775    sanity
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcardsort1
    selectCardMenu
    sortByCardPriority
    filterCardByAllCard
    isCardDisPlayCorrectlyAfterSortAndFilter  highPriority
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

canGoToArchiveCardPageByArchiveButton
    [Tags]    testrailid=7953    critical
    selectCardMenu
    clickArchivedCardMenu
    isArchivedCardPageDisplay

canPinCardWhenThereNoPinCardBefore
    [Tags]    testrailid=7955    critical
    ${name}    Generate random string    6    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard5
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${name}
    selectCardMenu
    rightClickOnFirstCard
    clickPinCardFromCardPage
    Reload page
    isCardPin  ${name}
    SelectCardName  ${name}
    deleteCard
    isCardDisappear  ${name}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

checkSortButton
    [Tags]    testrailid=7850    critical
    selectCardMenu
    clickSortCardButton
    isAllSortFunctionalityDisplay

sortByDueDateOnly
    [Tags]    testrailid=7853    critical
    selectCardMenu
    sortCardByDueDate
    isCardDisPlayCorrectlyAfterSortAndFilter  openDueDate

checkFilterButton
    [Tags]    testrailid=7843    critical
    selectCardMenu
    clickFilterCardButton
    isAllFilterCardFunctionalityDisplay

sortFilterClosedCard
    [Tags]    testrailid=7846    critical
    selectCardMenu
    filterCardByClosedCard
    isCardDisPlayCorrectlyAfterSortAndFilter  highPriority

canPinCardFromCardHomepage
    [Tags]    testrailid=3646    critical
    ${name}    Generate random string    6    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard2
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${name}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    sleep    3s
    selectCardMenu
    rightClickOnCardName  ${name}
    clickPinCardFromCardPage
    Reload page
    isCardPin  ${name}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

canUnpinCardFromHomepage
    [Tags]    testrailid=3647    critical
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard2
    selectCardMenu
    ${key}    get text    ${FIRSTPINCARDNAME}
    rightClickOnFirstCard
    clickUnpinCardFromCardPage
    Reload page
    isPinIconDisappear    ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

checkEditOptionOnArchiveCardPage
    [Tags]    testrailid=3650    critical
    selectCardMenu
    rightClickOnFirstCard
    clickArchiveCardFromCardPage
    clickArchivedCardMenu
    rightClickOnFirstCard
    isUnArchiveCardOptionDisplay

canArchivePinCard
    [Tags]    testrailid=3654    critical
    selectCardMenu
    ${name}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${name}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    selectCardMenu
    SelectCardName  ${name}
    clickThreeDotOnCardCreationPage
    addCardToFavorite
    selectCardMenu
    rightClickOnCardName  ${name}
    clickArchiveCardFromCardPage
    clickArchivedCardMenu
    isCardArchived  ${name}

canUnArchiveCard
    [Tags]    testrailid=3656    critical
    selectCardMenu
    clickArchivedCardMenu
    ${key}    get text    ${FIRSTCARDNAME}
    rightClickOnFirstCard
    clickUnArchiveCardFromCardPage
    isCardUnArchive  ${key}

userGetNotificationBadgeWhenOtherUpdateCard
    [Tags]    testrailid=7870    critical
    ${key}    Generate random string    6    [LETTERS]
    ${newKey}    Generate random string    6    [LETTERS]
    ${fullKey}    Catenate  SEPARATOR=  ${key}  ${newKey}
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard3
    selectCardMenu
    SelectCardName  ${key}
    # clickThreeDotOnCardCreationPage
    # addCardToFavorite
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1
    selectCardMenu
    SelectCardName  ${key}
    editTextInCard    ${newKey}
    setCardPriority  2
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard3
    selectCardMenu
    isUnreadBadgeDisplayOnEditedCard  ${fullKey}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1
    

userGetNotificationBadgeWhenOtherAddImageInCard
    [Tags]    testrailid=7871    critical
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard3
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1
    selectCardMenu
    SelectCardName  ${key}
    addFileInCard
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard3
    selectCardMenu
    isUnreadBadgeDisplayOnEditedCard  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcard1

createNewCardFromGC
    [Tags]    testrailid=3749    sanity
    ${key}    Generate random string    4    [LETTERS]
    inputElasticSearch  cardGroup
    selectChatroomByIndex  1
    sendMessage  ${key}
    clickSendCard
    addTextInCard  ${key}
    sendCardInChat
    isCardSent
    selectCardMenu
    isUnpinCardDisplayCorrectly  ${key}

createNewCardFromTopic
    [Tags]    testrailid=4240    sanity
    ${key}    Generate random string    4    [LETTERS]
    inputElasticSearch  cardGroup
    selectChatroomByIndex  1
    selectTopicTab
    selectTopic  cardSent
    sendMessage  ${key}
    clickSendCard
    addTextInCard  ${key}
    sendCardInChat
    isCardSent
    selectCardMenu
    isUnpinCardDisplayCorrectly  ${key}

sharingUserButtonDisableByDefault
    [Tags]    testrailid=4192    sanity                       
    selectCardMenu
    clickCreateNewCard
    isAddShareUserIconDisable

createNewCardWithAttachFileFromCardPage
    [Tags]    testrailid=4190    sanity  Autoqa_303
    ${key}    Generate random string    6    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    addFileInCard
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    backToCardMenuPage
    isUnpinCardDisplayCorrectly  ${key}
    SelectCardName  ${key}
    isFileNameDisplayInCard  workflow_file.pdf
    