*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/workflow/workflowHomePage.robot
Resource                  ../main/keyword/workflow/workflowCreationPage.robot
Resource                  ../main/keyword/workflow/workflowDetailPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/workflow/workflowHomePage.robot
Resource                  ../main/keyword/admin/workflow/workflowCreationPage.robot


Suite Setup    openEkoAppAndLogIn  wworkflow1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wworkflow1
Suite Teardown    Close browser
Force Tags    workflow

*** Test Case ***

canCompleteSimpleFlow
    [Tags]    testrailid=3794    critical
    ${key}    Generate random string    8    [LETTERS]
    selectWorkflowMenu
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  simple
    selectFirstWorkflowTemplateInList
    inputTextInCustomField  Title  ${key}
    clickOpenUserListInField  User for next stage
    searchForWorkflowReceiver  workflow2 
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow2
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickOpenUserListInField  Multiple Users (Restrict)
    selectFirstDirectoryGroupReceiver
    clickConfirmToSelectReceiver
    clickOpenUserListInField  Single User (Restrict)
    selectWorkflowReceiverByIndex  1
    clickConfirmToSelectReceiver
    clickOpenUserListInField  User for next stage
    searchForWorkflowReceiver  workflow1
    selectUserToAddInWorkflow  workflow1
    clickConfirmToSelectReceiver
    inputTextInCustomField  Number  2
    selectMultipleChoiceInCustomField  Multiple Choice  1  2
    uploadImageInCustomField  Images
    uploadAttachmentInCustomField  Attachments
    clickOpenCalendarDate  Date & Time
    clickConfirmSelectedDate
    clickOpenTimeSetUpInWorkflow  Time
    selectCurrentHourInTimeField
    closeHourSelectionInWorkflow  Number
    inputTextInCustomField  Text  ${key}
    inputTextInTextAreaField  Text Area  ${key}
    selectSingleChoiceInCustomField  Yes/No  1
    selectSingleChoiceInCustomField  Single Choice  1
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow1
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    isWorkflowStatusComplete  ${key}

canCompleteParalleWorkflow
    [Tags]    testrailid=3795    critical
    ${key}    Generate random string    8    [LETTERS]
    selectWorkflowMenu
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  parallel test - e2e
    selectFirstWorkflowTemplateInList
    inputTextInCustomField  Title  ${key}
    clickOpenUserListInField  User for next stage
    searchForWorkflowReceiver  workflow2
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow2
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickOpenUserListInField  UserA
    searchForWorkflowReceiver  workflow1
    selectUserToAddInWorkflow  workflow1
    clickConfirmToSelectReceiver
    clickOpenUserListInField  UserB
    searchForWorkflowReceiver  workflow3
    selectUserToAddInWorkflow  workflow3
    clickConfirmToSelectReceiver
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow1
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow3
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickOpenUserListInField  Parallel
    searchForWorkflowReceiver  workflow2
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow5
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    isWorkflowStatusComplete  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow1

canCompleteRestartWorkflow
    [Tags]    testrailid=3796    critical
    ${key}    Generate random string    8    [LETTERS]
    ${newText}    Generate random string    8    [LETTERS]
    selectWorkflowMenu
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  restart stage test
    selectFirstWorkflowTemplateInList
    inputTextInCustomField  Title  ${key}
    clickOpenUserListInField  User for next stage
    searchForWorkflowReceiver  workflow2
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow2
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    inputTextInCustomField  Text  ${key}
    inputTextInTextAreaField  Text Area  ${key}
    uploadAttachmentInCustomField  Attachments
    selectSingleChoiceInCustomField  Single Choice  1
    uploadImageInCustomField  Images
    selectMultipleChoiceInCustomField  Multiple Choice  1  2
    clickOpenUserListInField  Single User
    searchForWorkflowReceiver  workflow2
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    clickOpenUserListInField  Users for next stage
    searchForWorkflowReceiver  workflow2
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    inputTextInCustomField  Number  2
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickOpenUserListInField  User for third stage
    searchForWorkflowReceiver  workflow3
    selectUserToAddInWorkflow  workflow3
    clickConfirmToSelectReceiver
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clearTextInCustomField  Text
    inputTextInCustomField  Text  ${newText}
    selectSingleChoiceInCustomField  Single Choice  2
    removeUserFromCustomField  Users for next stage  workflow2
    clickOpenUserListInField  Users for next stage
    searchForWorkflowReceiver  workflow1
    selectUserToAddInWorkflow  workflow1
    clickConfirmToSelectReceiver
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow1
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickOpenUserListInField  User for third stage
    searchForWorkflowReceiver  workflow3
    selectUserToAddInWorkflow  workflow3
    clickConfirmToSelectReceiver
    clickActionButtonInWorkflow  2
    clickConfirmActionInWorkflow
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow3
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickActionButtonInWorkflow  2
    clickConfirmActionInWorkflow
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow1
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    removeUserFromCustomField  User for third stage  workflow3
    clickOpenUserListInField  User for third stage
    searchForWorkflowReceiver  workflow2
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    clickActionButtonInWorkflow  2
    clickConfirmActionInWorkflow
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow2
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    isWorkflowStatusComplete  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow1

canCompletePrivacyWorkflow
    [Tags]    testrailid=3798    critical
    ${key}    Generate random string    8    [LETTERS]
    selectWorkflowMenu
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  privacy
    selectFirstWorkflowTemplateInList
    inputTextInCustomField  Title1 (Creator)  ${key}
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow5
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickOpenUserListInField  User A (Creator only) for Second stage
    searchForWorkflowReceiver  workflow2
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow2
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickOpenUserListInField  User C (Everyone)
    searchForWorkflowReceiver  workflow3
    selectUserToAddInWorkflow  workflow3
    clickConfirmToSelectReceiver
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow3
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    isUserCannotSeeRecipientNameInStage  First Stage (Only creator can see)
    isUserCannotSeeRecipientNameInStage  Second (No one can see)
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow2
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    isUserCanSeeTheNameWhoHasDoneTheStage  Last (Everyone can see)
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow1
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    isCreatorCanSeeTheNameWhoHasDoneTheStage  First Stage (Only creator can see)
    isCreatorCannotSeeRecipientNameInStage  Second (No one can see)
    isCreatorCanSeeTheNameWhoHasDoneTheStage  Last (Everyone can see)

canCommentInWorkflow
    [Tags]    testrailid=7900    critical
    ${key}    Generate random string    8    [LETTERS]
    ${newKey}    Generate random string    8    [LETTERS]
    selectWorkflowMenu
    selectWorkflowByIndex  1
    openCommentInWorkflow
    sendMessage  ${key}
    sendMessage  ${newKey}
    isMessageSent  ${newkey}

canFilterWorkflow
    [Tags]    testrailid=3800    critical
    selectWorkflowMenu
    filterAwaitingWorkflow
    isOnlyAwaitingWorkflowDisplay

canSearchForWorkflow
    [Tags]    testrailid=3801    critical
    selectWorkflowMenu
    searchWorkflowByKeyword  high
    isSearchResultCorrect  high

canFilterWorkflowByAdvanceSearch
    [Tags]    testrailid=3802    critical
    selectWorkflowMenu
    clickAdvanceSearchIcon
    clickOpenAdvanceSearchListInType  Workflow Name
    inputWorkflowTemplateKeyword  simple
    selectFirstWorkflowTemplateInList
    clickOpenAdvanceSearchListInType  Priority
    clickSelectFilterWorkflowPriority  High
    clickApplyAdvanceFilterWorkflow
    isAppDisplayOnlyWorkflowTemplateName  Simple Flow - E2E
    isAppDisplayOnlyHighWorkflowPriority

createNewWorkflowWithNewTemplateEnableNoti
    [Tags]    testrailid=36946    critical
    ${key}    Generate random string    8    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickWorkflowMenuInAdmin
    clickDuplicateWorkflowtemplate  WF noti Template
    sleep    1s
    clickDuplicateWorkflowtemplate  WF noti Template
    clickEditWorkflowTemplateTitle
    editWorkflowTemplateName  ${key}
    clickConfirmEditWorkflowTemplateTitle
    clickSaveTheWorkflowTemplate
    backToWorkflowHomePage
    publishTheWorkflowTemplate  ${key}
    Go to    ${ClientURL}
    selectWorkflowMenu
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  ${key}
    selectFirstWorkflowTemplateInList
    inputTextInCustomField  Title  ${key}
    clickOpenUserListInField  user
    searchForWorkflowReceiver  workflow6
    selectUserToAddInWorkflow  workflow6
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow6
    selectWorkflowMenu
    ${count}    Get element count    ${WORKFLOWBLUEHIGHLIGHT}
    isWorkflowUnreadBadgeDisplay    ${count}
    isUnreadWorkflowHasBlueHighlight  ${key}
    clickSettingButton
    clickMarkAllAsRead
    # selectWorkflowByTitle  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow1
    Go to    ${AdminURL}
    clickWorkflowMenuInAdmin
    clickDeleteWorkflowTemplate  ${key}
    isWorkflowTemplateDeleted  ${key}

createNewWorkflowWithExistingTemplateDisabledNoti
    [Tags]    testrailid=36947    critical
    ${key}    Generate random string    8    [LETTERS]
    selectWorkflowMenu
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  disabledNoti
    selectFirstWorkflowTemplateInList
    inputTextInCustomField  Title  ${key}
    clickOpenUserListInField  user
    searchForWorkflowReceiver  workflow6
    selectUserToAddInWorkflow  workflow6
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow6
    selectWorkflowMenu
    # ${count}    Get element count    ${WORKFLOWBLUEHIGHLIGHT}
    isWorkflowUnreadBadgeNotDisplay
    # isUnreadWorkflowHasBlueHighlight  ${key}
    isBlueHighlightNotDisplay  ${key}
    # selectWorkflowByTitle  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow1

createNewWorkflowWithNewTemplateDisabledNoti
    [Tags]    testrailid=36948    critical
    ${key}    Generate random string    8    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickWorkflowMenuInAdmin
    clickDuplicateWorkflowtemplate  WF noti Template
    clickEditWorkflowTemplateTitle
    editWorkflowTemplateName  ${key}
    clickConfirmEditWorkflowTemplateTitle
    clickEditStageDetail
    selectSettingTab
    clickDiabledWorkflowNotification
    clickSaveTheWorkflowTemplate
    backToWorkflowHomePage
    publishTheWorkflowTemplate  ${key}
    Go to    ${ClientURL}
    selectWorkflowMenu
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  ${key}
    selectFirstWorkflowTemplateInList
    inputTextInCustomField  Title  ${key}
    clickOpenUserListInField  user
    searchForWorkflowReceiver  workflow4
    selectUserToAddInWorkflow  workflow4
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow4
    selectWorkflowMenu
    isWorkflowUnreadBadgeNotDisplay
    isBlueHighlightNotDisplay  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow1
    Go to    ${AdminURL}
    clickWorkflowMenuInAdmin
    clickDeleteWorkflowTemplate  ${key}
    isWorkflowTemplateDeleted  ${key}

createNewTemplateWithOnlyPriorityAndScgeduleWorkflow
    [Tags]    testrailid=38880    critical
    ${key}    Generate random string    8    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickWorkflowMenuInAdmin
    clickDuplicateWorkflowtemplate  WF noti Template
    clickEditWorkflowTemplateTitle
    editWorkflowTemplateName  ${key}
    clickConfirmEditWorkflowTemplateTitle
    clickEditStageDetail
    selectSettingTab
    clickDisabledWorkflowDueDateField
    clickSaveTheWorkflowTemplate
    backToWorkflowHomePage
    publishTheWorkflowTemplate  ${key}
    Go to    ${ClientURL}
    selectWorkflowMenu
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  ${key}
    selectFirstWorkflowTemplateInList
    isPriorityFieldDisplay
    isScheduleFieldDisplay
    isDueDateFieldNotDisplay
    inputTextInCustomField  Title  ${key}
    clickOpenUserListInField  user
    searchForWorkflowReceiver  workflow2
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    isWorkflowSent    ${key}
    Go to    ${AdminURL}
    clickWorkflowMenuInAdmin
    clickDeleteWorkflowTemplate  ${key}
    isWorkflowTemplateDeleted  ${key}


existingTemplateShouldDisplayOnlyDueDateAndPriority
    [Tags]    testrailid=38881    critical
    ${key}    Generate random string    8    [LETTERS]
    selectWorkflowMenu
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  dueDateAndPriority
    selectFirstWorkflowTemplateInList
    isDueDateFieldDisplay
    isPriorityFieldDisplay
    isScheduleFieldNotDisplay
    inputTextInCustomField  Title  ${key}
    clickOpenUserListInField  user
    searchForWorkflowReceiver  workflow2
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    isWorkflowSent    ${key}

canCompleteDynamicFlow
    [Tags]    testrailid=3797    sanity  Autoqa_327
    ${key}    Generate random string    8    [LETTERS]
    ${key2}    Generate random string    8    [LETTERS]
    selectWorkflowMenu
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  Dynamic Input 
    selectFirstWorkflowTemplateInList
    inputTextInCustomField  Title  ${key}
    clickOpenUserListInField  Users
    searchForWorkflowReceiver  workflow2  
    selectUserToAddInWorkflow  workflow2 
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    clickCreateNewWorkflow
    inputWorkflowTemplateKeyword  Dynamic Input 
    selectFirstWorkflowTemplateInList
    inputTextInCustomField  Title  ${key2}
    clickOpenUserListInField  Users
    searchForWorkflowReceiver  workflow2 
    selectUserToAddInWorkflow  workflow2
    clickConfirmToSelectReceiver
    clickSubmitWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow2
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    inputTextInCustomNumberField  Number1  100
    inputTextInCustomNumberField  Number2  60
    selectSingleChoiceInButtonField  Single Choice  2
    selectSingleChoiceInButtonField  Question  2
    clickOpenUserListInField  UserA
    searchForWorkflowReceiver  workflow3
    selectUserToAddInWorkflow  workflow3
    clickConfirmToSelectReceiver
    clickOpenUserListInField  UserB
    searchForWorkflowReceiver  workflow4
    selectUserToAddInWorkflow  workflow4
    clickConfirmToSelectReceiver
    clickOpenUserListInField  UserC
    searchForWorkflowReceiver  workflow5
    selectUserToAddInWorkflow  workflow5
    clickConfirmToSelectReceiver
    selectWorkflowByTitle  ${key2}
    inputTextInCustomNumberField  Number1  90
    inputTextInCustomNumberField  Number2  30
    selectSingleChoiceInButtonField  Single Choice  1
    selectSingleChoiceInButtonField  Question  1
    clickOpenUserListInField  UserA
    searchForWorkflowReceiver  workflow3
    selectUserToAddInWorkflow  workflow3
    clickConfirmToSelectReceiver
    clickOpenUserListInField  UserB
    searchForWorkflowReceiver  workflow4
    selectUserToAddInWorkflow  workflow4
    clickConfirmToSelectReceiver
    clickOpenUserListInField  UserC
    searchForWorkflowReceiver  workflow5
    selectUserToAddInWorkflow  workflow5
    clickConfirmToSelectReceiver
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow3
    selectWorkflowMenu
    selectWorkflowByTitle  ${key}
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    isWorkflowStatusComplete  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wworkflow4
    selectWorkflowMenu
    selectWorkflowByTitle  ${key2}
    clickActionButtonInWorkflow
    clickConfirmActionInWorkflow
    isWorkflowStatusComplete  ${key2}
 
