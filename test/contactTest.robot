*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/directory/directoryPage.robot

Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot


Suite Setup    openEkoAppAndLogIn  wcontact1
Test Setup     Go to    ${ClientURL}
Suite Teardown    Close browser
Force Tags    formPortalBannerSearchProfile

*** Test Case ***

contactAvailableInRightSequence
    [Tags]    testrailid=7899    critical
    selectDirectoryMenu
    clickOpenAllUserInDirectory
    isUserDirectoryDisplayRightSequence