*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/card/cardPage.robot
Resource                  ../main/keyword/card/cardCreationPage.robot
Resource                  ../main/keyword/recent/settingPage.robot


Suite Setup    openEkoAppAndLogIn  wcardpermission1
Test Setup     Go to    ${ClientURL}
Test Teardown   handleFailcaseWithUser  wcardpermission1
Suite Teardown    Close browser

*** Test Case ***

creatorCanAddUserInCard
    [Tags]    testrailid=
    ${key}    Generate random string    8    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectAddedUserInCardFromContact
    confirmToAddUserInCard
    selectCardMenu
    selectFirstUnpinCard
    clickAddShareUserIcon
    clickAddMoreUserInCard
    selectUserFromFavoriteContact
    confirmToAddUserInCard
    clickAddShareUserIcon
    isAddedUserIsViewerRole  cardpermission3 web
    clickCloseButtonInShareCardUserList
    
creatorCanSetCardPriority
    [Tags]    test_case_id
    ${key}    Generate random string    8    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectAddedUserInCardFromContact
    confirmToAddUserInCard
    setCardPriority  1
    selectCardMenu
    isCardDisplayCorrectPriority  ${key}

#---------------- Editor -----------------------------

editorCanRemoveOtherCardPermission
    [Tags]    remove
    ${key}    Generate random string    8    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    selectAddedUserInCardFromContact
    setUserRoleInCardToEditor
    confirmToAddUserInCard
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcardpermission3
    selectCardMenu
    selectUnpinCardName  ${key}
    clickAddShareUserIcon
    removeUserFromCard  admin
    confirmEditUserInCard
    selectCardMenu
    selectFirstUnpinCard
    clickAddShareUserIcon
    isUserRemoveFromCard  admin
    clickCloseButtonInShareCardUserList
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcardpermission1

editorCanSetCardPriority
    [Tags]    
    ${key}    Generate random string    8    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    selectAddedUserInCardFromContact
    setUserRoleInCardToEditor
    confirmToAddUserInCard
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcardpermission3
    selectCardMenu
    selectUnpinCardName  ${key}
    setCardPriority  1
    selectCardMenu
    isCardDisplayCorrectPriority  ${key}
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcardpermission1

editorCanSeeHistoryLog
    [Tags]    
    ${key}    Generate random string    12    [LETTERS]
    selectCardMenu
    clickCreateNewCard
    addTextInCard  ${key}
    clickAddShareUserIcon
    selectUserFromFavoriteContact
    selectAddedUserInCardFromContact
    setUserRoleInCardToEditor
    confirmToAddUserInCard
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcardpermission3
    selectCardMenu
    selectUnpinCardName  ${key}
    clickOpenCardHistoryLog
    isHistoryLogOpen
    clickCloseHistoryLogInCardButton
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wcardpermission1

