*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/profile/profilePage.robot

Suite Setup    openEkoAppAndLogIn  wprofile1
Test Setup     Go to    ${ClientURL}
Suite Teardown    Close browser
Force Tags    formPortalBannerSearchProfile

*** Test Case ***

canEditFirstNameAndLastName
    [Tags]    testrailid=3943    critical
    ${firstname}    Generate random string    8    [LETTERS]
    ${lastname}    Generate random string    8    [LETTERS]
    ${fullname}    Catenate  ${firstname}  ${lastname}
    clickToSeeUserProfile
    clickEditProfilIcon
    clearFirstnameValue
    editFirstName  ${firstname}
    clearLastnameValue
    editLastName  ${lastname}
    clickSaveEditProfileButton
    clickCloseProfilePopup
    clickToSeeUserProfile
    isUserFullNameDisplayCorrect  ${fullname}

# canChangeCoverPicture
#     [Tags]    testrailid=3932
#     clickToSeeUserProfile
#     uploadNewCoverPhoto
#     uploadOldCoverPhoto
    

# canChangeProfilePicture
#     [Tags]    testrailid=3936
#     clickToSeeUserProfile
#     uploadNewProfilePhoto
#     uploadOldProfilePhoto

canChangeCorrectPassword
    [Tags]    testrailid=3941    critical
    clickSettingButton
    clickChangePasswordButton
    inputCurrentPassword  password
    inputNewPassword  111111         
    inputConfirmNewPassword  111111
    clickSaveEditPasswordButton
    loginPage.logoutWebClient
    loginPage.loginWebClient    wprofile1    111111
    clickSettingButton
    clickChangePasswordButton
    inputCurrentPassword  111111
    inputNewPassword  password
    inputConfirmNewPassword  password
    clickSaveEditPasswordButton
    loginPage.logoutWebClient
    loginPage.loginWebClient    wprofile1

canUpdateTitle
    [Tags]    testrailid=3942    sanity
    ${key}    Generate random string    8    [LETTERS]
    clickToSeeUserProfile
    clickEditProfilIcon
    clearTitle
    editTitle  ${key}
    clickSaveEditProfileButton
    clickCloseProfilePopup
    clickToSeeUserProfile
    clickEditProfilIcon
    isTitleDisplay  ${key}

canChangeWrongPassword
    [Tags]    testrailid=3940    sanity
    clickSettingButton
    clickChangePasswordButton
    inputCurrentPassword  wrongpass
    inputNewPassword  111111         
    inputConfirmNewPassword  111111
    clickSaveEditPasswordButton
    isChangeWrongPassword

canCanSeeAdminPanelMenu
    [Tags]    testrailid=3947    sanity
    clickSettingButton
    isAdminpanelDisplay