*** Settings ***

Library    SeleniumLibrary
Library    String
Library    DateTime

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/directory/directoryPage.robot
Resource                  ../main/keyword/chat/chatMemberPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/chat/mediaAndFilePage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/profile/profilePage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/admin/user/userDetailPage.robot
Resource                  ../main/keyword/admin/user/userListHomePage.robot
Resource                  ../main/keyword/admin/adminLandingPage.robot
Resource                  ../main/keyword/card/cardPage.robot
Resource                  ../main/keyword/card/cardCreationPage.robot
Resource                  ../main/keyword/task/taskPage.robot
Resource                  ../main/keyword/task/taskDetailPage.robot
Resource                  ../main/keyword/form/formPage.robot
Resource                  ../main/keyword/workflow/workflowHomePage.robot
Resource                  ../main/keyword/workflow/workflowDetailPage.robot



Suite Setup    openEkoAppAndLogIn  wchatmention1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wchatmention1
Suite Teardown    Close browser
Force Tags    chat

*** Test Case ***

sendShortTextInChat
    [Tags]    testrailid=4110    critical
    ${key}    Generate random string    4    [LETTERS]
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    sendMessage  ${key}
    isMessageSent  ${key}

sendLongTextInChat
    [Tags]    testrailid=4111    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    sendMessage  ${longText}
    isLongTextSent

sendLinkTextInChat
    [Tags]    testrailid=4112    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    sendMessage  ${linkText}
    isLinkTextSent

sendStickerInChat
    [Tags]    testrailid=4113    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    clickOpenSticker
    selectStickerToSend
    isStickerSent

sendImageWithCaption
    [Tags]    testrailid=4114    critical
    ${key}    Generate random string    4    [LETTERS]
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    sendImageFile
    addImageCaption  ${key}
    clickConfirmSendMedia
    waitForMediaUpload
    reload page
    isImageSent

sentNewVideo
    [Tags]    testrailid=4119    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    sendVideoInChat
    clickConfirmSendMedia
    waitForMediaUpload
    isVideoSent

sentCommendation
    [Tags]    testrailid=4120    critical
    ${key}    Generate random string    4    [LETTERS]
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    clickOpenCommendation
    selectCommendation
    addDescriptionForThumbUps  ${key}
    confirmSendCommendation
    sleep    5s
    isCommendationSent

canViewNewSentImage
    [Tags]    testrailid=5859    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    sendImageFile
    clickConfirmSendMedia
    waitForMediaUpload
    reload page
    isImageSent
    clickOnLastSentImage
    isImagePreviewOpen

mentionListDisplay10UsersAsMax
    [Tags]    testrailid=3974    critical
    inputElasticSearch  mentionG
    selectChatroomByIndex  1
    seeMentionUserList
    isMentionListDisplay  10
    sendMessage  text
    isMessageSent  @text

canSendMultipleMention
    [Tags]    testrailid=3975    critical
    inputElasticSearch  mentionG
    selectChatroomByIndex  1
    sendMentionMessage  chatmention2  chatmention3
    isMentionSent  2

cannotMentionInDCChat
    [Tags]    testrailid=3976    critical
    inputElasticSearch  mention2
    selectChatroomByIndex  1
    seeMentionUserList
    isMentionListNotDisplay
    sendMessage  text

canFilterMentionListByCharacter
    [Tags]    testrailid=3981    critical
    inputElasticSearch  mentionG
    selectChatroomByIndex  1
    seeMentionUserList
    isMentionListCanFilterByCharacter  c  a
    sendMessage  text
    isMessageSent  @catext

canViewUserProfileByClickOnMentionInMessage
    [Tags]    testrailid=3982    critical
    inputElasticSearch  mentionG
    selectChatroomByIndex  1
    sendMentionMessage  chatmention2
    isMentionSent  1
    clickOnMentionUserMessage  @chatmention2
    isProfileUserDisplayCorrect  chatmention2

canAckOnMentionMessage
    [Tags]    testrailid=3983    critical
    inputElasticSearch  mentionG
    selectChatroomByIndex  1
    sendMentionMessage  chatmention2
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatmention2
    selectChatroomByTitle  mentionGroup
    clickThreeDotOnLatestMessage
    clickAckMessageFromOptionList
    isLastMessageAcked
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchatmention1

editMentionMessageWillBeNormalMessage
    [Tags]    testrailid=3985    critical
    ${key}  Generate random string    6    [LETTERS]
    inputElasticSearch  mentionG
    selectChatroomByIndex  1
    sendMentionMessage  chatmention2
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessageByAddText  ${key}
    clickConfirmEditMessage
    isMessageDisplayAsNormalText

canDeleteMentionMessage
    [Tags]    testrailid=3986    critical
    ${key}  Generate random string    12    [LETTERS]
    inputElasticSearch  mentionG
    selectChatroomByIndex  1
    selectMentionUser  chatmention2  chatmention3
    sendMessage  ${key}
    clickThreeDotOnLatestMessage
    clickDeleteMessage
    isMessageDeleted

timeStampDisplayCorrectWhenEditMessage
    [Tags]    testrailid=4124    critical    test
    ${key}  Generate random string    12    [LETTERS]
    ${edit}  Generate random string    12    [LETTERS]
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    sendMessage  ${key}
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessagByClearOldText  ${edit}
    ${date}    Get current date    
    clickConfirmEditMessage
    ${time}    Convert Date    ${date}    result_format=%-I:%M %p
    # ${time}=    Get Substring    ${time}  1
    ${check}    Catenate  ${editString}  ${time}
    isEditedTimeStampDisplayCorrect    ${check}

timeStampDisplayCorrectWhenDeleteMessage
    [Tags]    testrailid=4125    critical    test
    ${key}  Generate random string    12    [LETTERS]
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    sendMessage  ${key}
    clickThreeDotOnLatestMessage
    ${date}    Get current date    
    clickDeleteMessage
    ${time}    Convert Date    ${date}    result_format=%-I:%M %p
    # ${time}=    Get Substring    ${time}  1
    ${check}    Catenate  ${todayString}  ${time}
    isDeletedMessageTimeStampDisplayCorrect    ${check}

canSendMentionInCardComment
    [Tags]    testrailid=3977    sanity
    selectCardMenu
    SelectCardName  Mention
    clickOpenCommentChatInCardForEditor
    sendMentionMessage  chatmention2
    isMentionSent  1

canSendMentionInTaskComment
    [Tags]    testrailid=3978    sanity
    selectTaskMenu
    searchTaskByName  Mention
    selectTaskName  Mention
    clickTaskComment
    sendMentionMessage  chatmention2
    isMentionSent  1

canSendMentionInFormComment
    [Tags]    testrailid=3979    sanity
    selectFormMenu
    selectFirstForm
    sendMentionMessage  chatmention2
    isMentionSent  1

canSendMentionInInWorkflowComment
    [Tags]    testrailid=3980    sanity
    selectWorkflowMenu
    searchWorkflowByKeyword  Bangkok
    selectWorkflowByIndex  1
    openCommentInWorkflow
    sendMentionMessage  chatmention2
    isMentionSent  1

editMessageWithMention
    [Tags]    testrailid=3984    sanity
    ${key}  Generate random string    6    [LETTERS]
    inputElasticSearch  mentionG
    selectChatroomByIndex  1
    sendMessage  ${key}
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessagByClearOldText    @chatmention2 
    clickConfirmEditMessage
    isMessageDisplayAsNormalText
    clickOnMessage  @chatmention2
    isProfileUserNotDisplay  chatmention2

timeStampDisplayCorrectWhenSendMessage
    [Tags]    testrailid=4122    sanity  Autoqa_267
    ${key}  Generate random string    12    [LETTERS]
    ${edit}  Generate random string    12    [LETTERS]
    createDCFromRecent
    searchUserInCreateChatSearchBox  mention2
    selectUserFromSearchToCreateChat
    sendMessage  ${key}
    ${date}    Get current date    
    ${time}    Convert Date    ${date}    result_format=%-I:%M %p
    isSendMessageTimeStampDisplayCorrect    ${time}

DateStampDisplayCorrectWhenSendMessage
    [Tags]    testrailid=4126    sanity  Autoqa_267
    ${key}  Generate random string    12    [LETTERS]
    ${edit}  Generate random string    12    [LETTERS]
    createGCFromRecent
    searchUserInCreateChatSearchBox  chatevent2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    confirmCreateChat
    sendMessage  ${key}
    isSendMessageDateStampDisplayCorrect
    clickLeaveChatroom
    clickConfirmLeaveChatroom

timeStampDisplayCorrectWhenSendTaskInChat
    [Tags]    testrailid=4128    sanity  Autoqa_267
    ${key}    Generate random string    12    [LETTERS]
    inputElasticSearch  mention2
    selectChatroomByIndex  1
    createTaskInChat
    setTaskTiTle  ${key}
    openTaskAssigneeListInChat
    selectTaskAssigneeInChat  task2
    openTaskAssigneeListInChat
    selectTaskAssigneeInChat  task1
    taskCreationInsideChatPage.confirmCreateTask
    ${date}    Get current date    
    ${time}    Convert Date    ${date}    result_format=%-I:%M %p
    isSendMessageTimeStampDisplayCorrect    ${time}
    