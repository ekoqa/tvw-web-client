*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/profile/profilePage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/admin/user/userDetailPage.robot
Resource                  ../main/keyword/admin/user/userListHomePage.robot
Resource                  ../main/keyword/admin/adminLandingPage.robot
Resource                  ../main/keyword/directory/directoryPage.robot
Resource                  ../main/keyword/chat/chatMemberPage.robot

Suite Setup    openEkoAppAndLogIn  wdelete1
Test Setup     Go to    ${ClientURL}
Test Teardown  setUpForNextTestWithUser  wdelete1
Suite Teardown    Close browser
Force Tags    deleteUser

*** Test Case ***

appShowDeleteAvatarForDeletedUserInRecentPage
    [Tags]    testrailid=10679    critical
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    selectDirectoryMenu
    inputElasticSearch  ${firstName}
    selectChatroomByIndex  1
    createDcChatFromProfile
    sendMessage  hi
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    Reload Page
    isUserIconAsDeletedUSer  ${firstname}

deleteAvatarDisplayInChat
    [Tags]    testrailid=10681    critical
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    Go to  ${AdminURL}
    # skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    selectDirectoryMenu
    inputElasticSearch  ${firstName}
    selectChatroomByIndex  1
    createDcChatFromProfile
    sendMessage  hi
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    Reload Page
    selectChatroomByTitle  ${firstName}
    isDeleteAvatarDisplayInChatPage

fullNameOfDeletedUserDisplayInDetailAck
    [Tags]    testrailid=10690    critical
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    ${message}    Generate random string    12    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    selectDirectoryMenu
    inputElasticSearch  ${firstName}
    selectChatroomByIndex  1
    createDcChatFromProfile
    sendMessage  ${message}
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  ${userName}
    selectChatroomByIndex  1
    clickAckMessage  ${message}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdelete1
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    Reload Page
    selectChatroomByTitle  ${firstname}
    clickThreeDotOnSpecificMessage  ${message}
    openDetailAck
    isFullnameDisplayWithDeleteTitle  ${firstname}
    clickCloseDetailAckPopup
    
deleteAvatarDisplayInDetailAck
    [Tags]    testrailid=10691    critical
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    ${message}    Generate random string    12    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    selectDirectoryMenu
    inputElasticSearch  ${firstName}
    selectChatroomByIndex  1
    createDcChatFromProfile
    sendMessage  ${message}
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  ${userName}
    selectChatroomByIndex  1
    clickAckMessage  ${message}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdelete1
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    Reload Page
    selectChatroomByTitle  ${firstname}
    clickThreeDotOnSpecificMessage  ${message}
    openDetailAck
    isDeleteAvatarDisplayInDetailAck  ${firstname}
    clickCloseDetailAckPopup

nameStillDisplayInRecentPageAfterUserIsDeleted
    [Tags]    testrailid=10680    critical
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    selectDirectoryMenu
    inputElasticSearch  ${firstName}
    selectChatroomByIndex  1
    createDcChatFromProfile
    sendMessage  hi
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    Reload Page
    isChatroomDisplayOnRecent  ${firstName}

roomNameStillDisplayCorrectAfterUserIsDeleted
    [Tags]    testrailid=10682    critical
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    selectDirectoryMenu
    inputElasticSearch  ${firstName}
    selectChatroomByIndex  1
    createDcChatFromProfile
    sendMessage  hi
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    Reload Page
    selectChatroomByTitle  ${firstName}
    isChatroomDisplay  ${firstName}

deleteAvatarDisplayInGroupChat
    [Tags]    testrailid=10687    sanity  93
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}

    # selectChatroomByTitle  DeleteGroup  
    inputElasticSearch  DeleteG
    selectChatroomByIndex  1

    selectMemberTab
    addMemberToGC
    searchUserInCreateChatSearchBox  ${firstName}
    selectUserFromSearchToCreateChat
    loginPage.logoutWebClient
    loginPage.loginWebClient  ${userName}

    # selectChatroomByTitle  DeleteGroup
    inputElasticSearch  DeleteG
    selectChatroomByIndex  1

    sendMessage  Delete
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdelete1
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdelete1
    selectChatroomByTitle  DeleteGroup
    isDeleterAvatarDisplay

chatNameNotChangeAfterDeleteUserInGroupChat
    [Tags]    testrailid=10686    sanity  93test
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    # selectChatroomByTitle  DeleteGroup  
    inputElasticSearch  DeleteG
    selectChatroomByIndex  1
    selectMemberTab
    addMemberToGC
    searchUserInCreateChatSearchBox  ${firstName}
    selectUserFromSearchToCreateChat
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    isChatroomDisplayOnRecent  DeleteGroup
    selectChatroomByTitle  DeleteGroup
    isNameDisplayOnChatroom  DeleteGroup

titleOfUserChangeAffterDeleteUserInGroupChat
    [Tags]    testrailid=10688    sanity  93
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    # selectChatroomByTitle  DeleteGroup 
    inputElasticSearch  DeleteG
    selectChatroomByIndex  1 
    selectMemberTab
    addMemberToGC
    searchUserInCreateChatSearchBox  ${firstName}
    selectUserFromSearchToCreateChat
    loginPage.logoutWebClient
    loginPage.loginWebClient  ${userName}
    # selectChatroomByTitle  DeleteGroup
    inputElasticSearch  DeleteG
    selectChatroomByIndex  1
    sendMessage  Delete
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdelete1
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdelete1
    # selectChatroomByTitle  DeleteGroup
    inputElasticSearch  DeleteG
    selectChatroomByIndex  1
    isDeleteUserTitleDisplay  ${firstName}

userProfileNotDisplayAfterClickDeleteUserProfileInDetailAct
    [Tags]    testrailid=10692    sanity  93test
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    ${message}    Generate random string    12    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    # selectChatroomByTitle  DeleteGroup
    inputElasticSearch  DeleteG
    selectChatroomByIndex  1
    sendMessage  ${message}
    selectMemberTab
    addMemberToGC
    searchUserInCreateChatSearchBox  ${firstName}
    selectUserFromSearchToCreateChat
    loginPage.logoutWebClient
    loginPage.loginWebClient  ${userName}
    selectChatroomByTitle  DeleteGroup
    clickAckMessage  ${message}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdelete1
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    # selectChatroomByTitle  DeleteGroup
    inputElasticSearch  DeleteG
    selectChatroomByIndex  1
    clickThreeDotOnSpecificMessage  ${message}
    openDetailAck
    clickUserInActDetail  ${firstName}
    isUserFullNameNotDisplayCorrect  ${firstName}

deleteUserNotDisplayWhenSearchContact
    [Tags]    testrailid=10693    sanity  93
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    ${message}    Generate random string    12    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    selectDirectoryMenu
    inputElasticSearch  ${firstName}
    isContactNotDisplay  ${firstName}
    
deleteUserNotDisplayInDirectoryPageWhenScrollThrough 
    [Tags]    testrailid=10694    sanity  Autoqa_328
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    ${message}    Generate random string    12    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}
    selectDirectoryMenu
    filterAllUser
    scrolldownToLastContact
    inputElasticSearch  ${firstName}
    isContactNotDisplay  ${firstName}