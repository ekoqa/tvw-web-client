*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/chat/taskCreationInsideChatPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/task/taskPage.robot
Resource                  ../main/keyword/task/taskDetailPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/recent/settingPage.robot



Suite Setup    openEkoAppAndLogIn  wtask1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wtask1
Suite Teardown    Close browser
Force Tags    taskUnread

*** Test Case ***

canCreateTaskByAssigneToYourselfAndOther
    [Tags]    testrailid=7960    critical
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task2
    selectFirstSearchResult
    taskDetailPage.confirmCreateTask    ${key}
    selectTaskName  ${key}
    isTaskAssignedToUser  task1  task2

canCreateTaskByAssigneToOtherWithoutYourself
    [Tags]    testrailid=7961    critical
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    removeFirstAssigneeFromTask
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectAssigneeByIndex  1
    selectAssigneeByIndex  2
    taskDetailPage.confirmCreateTask  ${key}
    selectAllTaskTab
    sortTaskByLastUpdate
    selectTaskName  ${key}
    isTaskAssignedToUser  task1  task2

assigneeCanEditTask
    [Tags]    testrailid=7963    critical
    ${key}    Generate random string    12    [LETTERS]
    ${newKey}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task2
    selectFirstSearchResult
    taskDetailPage.confirmCreateTask  ${key}
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName    ${key}
    setTaskTiTle  ${newKey}
    closeTaskDetailPage
    isTaskDisplayInTaskPage  ${newKey}
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1

canCreateTaskFromDCChat
    [Tags]    testrailid=3676    critical
    ${key}    Generate random string    12    [LETTERS]
    inputElasticSearch  task2
    selectChatroomByIndex  1
    createTaskInChat
    setTaskTiTle  ${key}
    openTaskAssigneeListInChat
    selectTaskAssigneeInChat  task2
    openTaskAssigneeListInChat
    selectTaskAssigneeInChat  task1
    taskCreationInsideChatPage.confirmCreateTask
    isTaskSent    ${key}

canCreateTaskFromGC
    [Tags]    testrailid=3686    critical
    ${key}    Generate random string    12    [LETTERS]
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  task2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  taskGroup
    confirmCreateChat
    Reload Page
    sendMessage  hi
    createTaskInChat
    setTaskTiTle  ${key}
    openTaskAssigneeListInChat
    selectTaskAssigneeInChat  task2
    openTaskAssigneeListInChat
    selectTaskAssigneeInChat  task1
    taskCreationInsideChatPage.confirmCreateTask
    isTaskSent    ${key}

canCreateTaskFromTopic
    [Tags]    testrailid=3688    critical
    ${key}    Generate random string    12    [LETTERS]
    ${topicName}  Generate random string    8    [LETTERS]
    inputElasticSearch  task2
    selectChatroomByIndex  1
    selectTopicTab
    createNewTopicInChat  ${topicName}
    selectTopic  ${topicName}
    sendMessage  hello
    createTaskInChat
    setTaskTiTle  ${key}
    openTaskAssigneeListInChat
    selectTaskAssigneeInChat  task2
    openTaskAssigneeListInChat
    selectTaskAssigneeInChat  task1
    taskCreationInsideChatPage.confirmCreateTask
    isTaskSent    ${key}

canCreateTaskFromRecentPage
    [Tags]    testrailid=3689    critical
    ${key}    Generate random string    12    [LETTERS]
    createTaskFromRecent
    setTaskTiTle  ${key}
    taskCreationInsideChatPage.searchForAddAssigneeInTask  task2
    selectTaskAssigneeInChat  task2
    taskCreationInsideChatPage.confirmCreateTask
    selectTaskMenu
    selectAllTaskTab
    sortTaskByLastUpdate
    isTaskDisplayInTaskPage  ${key}

unreadBadgeIsUpdateWhenReceieveTask
    [Tags]    testrailid=3744    critical
    ${key}    Generate random string    12    [LETTERS]
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    createTaskFromRecent
    setTaskTiTle  ${key}
    taskCreationInsideChatPage.searchForAddAssigneeInTask  task1
    selectTaskAssigneeInChat  task1
    taskCreationInsideChatPage.confirmCreateTask
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1
    isTaskUnreadBadgeDisplay

canAddAllInformationOnTaskDetailPage
    [Tags]    testrailid=7962    critical
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    setTaskDetail  ${key}
    setSubTask  ${key}
    setDueDate
    setPriority
    setTagsTask
    taskAttachPhoto
    taskDetailPage.confirmCreateTask  ${key}
    selectTaskName  ${key}
    isTaskSentWithAllField

assignerCanUploadAnyFileWhenCreateNewTaskAndSend
    [Tags]    testrailid=21989    critical
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    taskAttachfile
    taskDetailPage.confirmCreateTask  ${key}
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isTaskSentWithFile

assignerCanUploadFileWhenEditTask
    [Tags]    testrailid=21991    critical
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    taskDetailPage.confirmCreateTask  ${key}
    selectTaskName  ${key}
    taskAttachfile
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isTaskSentWithFile

assigneeCanRemoveFileWhenEditTask
    [Tags]    testrailid=21994    critical
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task2
    selectFirstSearchResult
    taskAttachfile
    taskDetailPage.confirmCreateTask  ${key}
    # clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    removeFileFromTask
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isDeleteFileFromTask
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1

canTaskFromSearchResult
    [Tags]    testrailid=3700    sanity
    selectTaskMenu
    searchTaskByName  searchTask
    selectTaskName  searchTask
    isTaskDisplayInTaskPage  searchTask

AssigneeCanNotDeleteTask
    [Tags]    testrailid=3701    sanity
    selectTaskMenu
    searchTaskByName  Can not delete task
    selectTaskName  Can not delete task
    isAssigneeNotSeeDeleteTaskIcon

assignerCanAddAssigneeAfterCtrateTaskFinish
    [Tags]    testrailid=3706    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    taskDetailPage.confirmCreateTask    ${key}
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task2
    selectFirstSearchResult
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isTaskAssignedToUser  task1  task2

assignerCanRemoveAssigneeAfterCtrateTaskFinish
    [Tags]    testrailid=3708    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task2
    selectFirstSearchResult
    taskDetailPage.confirmCreateTask    ${key}
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    removeSecondAssigneeFromTask
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isTaskAssignedToUser  task1
    isAssignedNotShow  task2

assigneeCanAddAssigneeAfterCtrateTaskFinish
    [Tags]    testrailid=3710    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task2
    selectFirstSearchResult
    taskDetailPage.confirmCreateTask    ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    clickFirstAssigneeFromTask
    taskDetailPage.searchForAddAssigneeInTask  task3
    selectFirstSearchResult
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isTaskAssignedToUser  task1  task2  task3
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1

assigneeCanRemoveAssigneesAfterCreateTaskFinish
    [Tags]    testrailid=3712    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectTaskAssigneeByName  task2
    selectTaskAssigneeByName  task3
    taskDetailPage.confirmCreateTask    ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask3
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    removeSecondAssigneeFromTask
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isTaskAssignedToUser  task1  task3
    isAssignedNotShow  task2
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1

assignerCanUpdateTagInTask
    [Tags]    testrailid=3722    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    taskDetailPage.confirmCreateTask    ${key}
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    setTagsTask
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isUpdateTagInTask

canCommentInTask
    [Tags]    testrailid=3742    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    searchTaskByName  Comment
    selectTaskName  Comment
    clickTaskComment
    sendMessage  ${key}
    isMessageSent  ${Key}

canGetNotificationWhenCommentInTask
    [Tags]    testrailid=3746    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    searchTaskByName  Notification
    selectTaskName  Notification
    clickTaskComment
    sendMessage  ${key}
    isMessageSent  ${Key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask5
    isTaskUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1

canCreateTaskFromTopicInGCChat
    [Tags]    testrailid=7959    sanity
    ${key}    Generate random string    12    [LETTERS]
    inputElasticSearch  groupTask
    selectChatroomByIndex  1
    selectTopicTab
    selectTopic  task
    createTaskInChat
    setTaskTiTle  ${key}
    openTaskAssigneeListInChat
    selectTaskAssigneeInChat  task1
    taskCreationInsideChatPage.confirmCreateTask
    isTaskSent    ${key}

assignerCanRemoveFileBeforeSendTask
    [Tags]    testrailid=21988    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    taskAttachfile
    removeFileFromTaskByClickCloseOneTime
    taskDetailPage.confirmCreateTask  ${key}
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isTaskSentWithNoFile

assigneeCanAttachFileWhenEditTask
    [Tags]    testrailid=21992    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task2
    selectFirstSearchResult
    taskAttachfile
    taskDetailPage.confirmCreateTask  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    taskAttachfile
    sleep    5s
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isTaskDisplayCorrectFileNumber  2
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1 

canShowCorrectStatusWhenAllAssigneeNotFinishTask
    [Tags]    testrailid=3736    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectTaskAssigneeByName  task2
    selectTaskAssigneeByName  task3
    taskDetailPage.confirmCreateTask    ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    clickTaskComplete
    selectCardMenu
    selectTaskMenu
    sortTaskByClosed
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isUserNotCompleteTask  task1  task3
    isUserCompleteTask  task2
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask3
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isUserNotCompleteTask  task1  task3
    isUserCompleteTask  task2

canShowCorrectStatusWhenAllAssigneeFinishTask
    [Tags]    testrailid=3737    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    removeFirstAssigneeFromTask
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectTaskAssigneeByName  task2
    selectTaskAssigneeByName  task3
    clickCreateTask
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    clickTaskComplete
    selectCardMenu
    selectTaskMenu
    sortTaskByClosed
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isUserNotCompleteTask  task1  task3
    isUserCompleteTask  task2
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask3
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    clickTaskComplete
    selectCardMenu
    selectTaskMenu
    sortTaskByClosed
    clickFirstTask
    isUserNotCompleteTask  task1
    isUserCompleteTask  task2  task3
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1
    selectTaskMenu
    clickAllTaskTab
    sortTaskByClosed
    searchTaskByName  ${key}
    selectTaskName  ${key}
    selectCardMenu
    selectTaskMenu
    sortTaskByClosed
    selectTaskName  ${key}
    # clickFirstTask
    isUserCompleteTask  task2  task3

canCompleteTaskForAllAssigneeWhenAssignerFinishTask
    [Tags]    testrailid=3738    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectTaskAssigneeByName  task2
    selectTaskAssigneeByName  task3
    taskDetailPage.confirmCreateTask    ${key}
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    clickTaskComplete
    selectCardMenu
    selectTaskMenu
    sortTaskByClosed
    searchTaskByName  ${key}
    selectTaskName  ${key}
    # clickFirstTask
    isUserCompleteTask  task1  task2  task3
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    sortTaskByClosed
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isUserCompleteTask  task1  task2  task3
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask3
    selectTaskMenu
    sortTaskByClosed
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isUserCompleteTask  task1  task2  task3
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1

canShowCorrectStatusInCreatorSideWhenSomeAssigneeNotFinishTask
    [Tags]    testrailid=3739    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectTaskAssigneeByName  task2
    selectTaskAssigneeByName  task3
    taskDetailPage.confirmCreateTask    ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    clickFirstTask
    clickTaskComplete
    selectCardMenu
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1
    selectTaskMenu
    searchTaskByName  ${key}
    clickFirstTask
    isUserNotCompleteTask  task1  task3
    isUserCompleteTask  task2

canGetNotificationWhenAssigneeFinishTask
    [Tags]    testrailid=3747    sanity
    ${key}    Generate random string    12    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask5
    clickSettingButton
    clickMarkAllAsRead
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectTaskAssigneeByName  task2
    taskDetailPage.confirmCreateTask    ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    clickFirstTask
    clickTaskComplete
    selectCardMenu
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask5
    isTaskUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1

# canGetNotificationWhenTaskWasDelete
#     [Tags]    testrailid=3745    sanity
#     ${key}    Generate random string    12    [LETTERS]
#     selectTaskMenu
#     clickCreateTaskFromTaskPage
#     setTaskTiTle  ${key}
#     clickAddAssigneeInTask
#     taskDetailPage.searchForAddAssigneeInTask  task5
#     selectFirstSearchResult
#     taskDetailPage.confirmCreateTask    ${key}
#     selectCardMenu
#     clickSettingButton
#     loginPage.logoutWebClient
#     loginPage.loginWebClient  wtask5
#     clickSettingButton
#     clickMarkAllAsRead
#     loginPage.logoutWebClient
#     loginPage.loginWebClient  wtask1
#     selectTaskMenu
#     searchTaskByName  ${key}
#     clickFirstTask
#     removeTask
#     loginPage.logoutWebClient
#     loginPage.loginWebClient  wtask5
#     isTaskUnreadBadgeDisplay
#     clickSettingButton
#     clickMarkAllAsRead
#     clickSettingButton
#     clickMarkAllAsRead
#     loginPage.logoutWebClient
#     loginPage.loginWebClient  wtask1

canGetNotificationWhenAssigneeWasUnassigned
    [Tags]    testrailid=3892    sanity  93
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectTaskAssigneeByName  task2
    selectTaskAssigneeByName  task5 web
    taskDetailPage.confirmCreateTask    ${key}
    selectCardMenu
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask5
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1
    selectTaskMenu
    searchTaskByName  ${key}
    clickFirstTask
    removeSecondAssigneeFromTask
    selectCardMenu
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask5
    isTaskUnreadBadgeDisplay

assignerCanRemoveFileWhenEditTask
    [Tags]    testrailid=21993    sanity  93
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    taskAttachfile
    taskDetailPage.confirmCreateTask  ${key}
    # selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    removeFileFromTask
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isDeleteFileFromTask

canUploadMaximum6File
    [Tags]    testrailid=22000    sanity  93
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    taskAttachfile
    taskAttachfile
    taskAttachfile
    taskAttachfile
    taskAttachfile
    taskAttachfile
    isAddFileButtonInTaskDisappear
    taskDetailPage.confirmCreateTask  ${key}
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isTaskDisplayCorrectFileNumber  6

statusOfAssigneeDisplayCorrect
    [Tags]    testrailid=3730    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectTaskAssigneeByName  task2
    selectTaskAssigneeByName  task3
    taskDetailPage.confirmCreateTask    ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    clickTaskComplete
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isUserNotCompleteTask  task1  task3
    isUserCompleteTask  task2

assigneeCanEditEverythingInTask
    [Tags]    testrailid=3715    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task2
    selectFirstSearchResult
    taskDetailPage.confirmCreateTask    ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    clickFirstAssigneeFromTask
    taskDetailPage.searchForAddAssigneeInTask  task3
    selectFirstSearchResult
    setTaskDetail  ${key}
    setSubTask  ${key}
    setDueDate
    setPriority
    setTagsTask
    taskAttachPhoto
    taskAttachfile
    sleep    5s
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isTaskSentWithAllField
    isTaskDisplayCorrectFileNumber  1

canShowCorrectStatusWhenAllAssignerCompleteAndNotCompleteTaskAgain
    [Tags]    testrailid=3740    sanity  Autoqa_303
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectTaskAssigneeByName  task2
    taskDetailPage.confirmCreateTask    ${key}
    clickTaskComplete
    clickTaskUncomplete
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isUserNotCompleteTask  task1  task2
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask2
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    isUserNotCompleteTask  task1  task2
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1
    
assigneeGetNotificationWhenAssignAndUnassign
    [Tags]    testrailid=3794    sanity  Autoqa_303
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickCreateTaskFromTaskPage
    setTaskTiTle  ${key}
    clickAddAssigneeInTask
    taskDetailPage.searchForAddAssigneeInTask  task
    selectTaskAssigneeByName  task5 web
    taskDetailPage.confirmCreateTask    ${key}
    selectCardMenu
    selectTaskMenu
    searchTaskByName  ${key}
    selectTaskName  ${key}
    removeSecondAssigneeFromTask
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask5
    isTaskUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtask1