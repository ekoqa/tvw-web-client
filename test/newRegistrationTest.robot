*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/admin/user/userDetailPage.robot
Resource                  ../main/keyword/admin/user/userListHomePage.robot
Resource                  ../main/keyword/admin/adminLandingPage.robot

Suite Setup    openEkoAppAndLogIn  wregistration1
Test Setup     Go to    ${EkogreenURL}
Test Teardown  setUpForNextTestWithUser  wregistration1
Suite Teardown    Close browser
Force Tags    others

*** Test Case ***

normalUserCanSeeOnlyEmilyBot
    [Tags]    testrailid=14511    critical
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    loginPage.logoutWebClient
    loginPage.loginWebClient    ${userName}
    isOnlyEmilyBotDisplayed
    loginPage.logoutWebClient
    loginPage.loginWebClient    wregistration1
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}