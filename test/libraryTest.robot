*** Settings ***

Library    SeleniumLibrary
Library    String
Library    OperatingSystem
Library     Collections

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/library/folderPage.robot
Resource                  ../main/keyword/library/libraryHomePage.robot
Resource                  ../main/keyword/library/documentPage.robot
Resource                  ../main/keyword/library/assessmentPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/chat/chatPage.robot


Suite Setup    openEkoAppAndLogIn  wlibrary1
Test Setup     Go to    ${ClientURL}
Suite Teardown    Close browser
Force Tags    others

*** Test Case ***

canViewFolderInLibrary
    [Tags]    testrailid=3987    critical
    selectLibraryMenu
    ${libraryName}  Get text  ${FIRSTFOLDERTITLE}
    selectFolderByIndex  1
    isFolderNameCorrect  ${libraryName}

canViewSubFolderInLibrary
    [Tags]    testrailid=3988    critical
    selectLibraryMenu
    selectFolderByIndex  1
    ${libraryName}  Get text  ${FIRSTSUBFOLDERTITLE}
    selectSubfolderByIndex  1
    isSubFolderNameCorrect  ${libraryName}

canDoQuizInLibrary
    [Tags]    testrailid=3992    critical
    selectLibraryMenu
    selectFolderByIndex  1
    selectDocumentByIndex  3
    clickTaskAnAssessmentButton
    selectQuizByIndex  1
    clickStartQuiz
    selectTheQuizChoice
    clickSubmitQuiz
    isQuizResultDisplay

canSearchSmallLetterByTag
    [Tags]    testrailid=41143    critical
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    dog
    chooseResultTagType
    isSearchResultDisplayCorrectly  London  Bangkok  Berlin
    isHighlightKeywordDisplayCorrectlyForSingleKeyword    Dog

canClickTagBackToSearchResultPage
    [Tags]    testrailid=41146    critical
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    dog
    chooseResultTagType
    selectItemResultByIndex  1
    backToPreviousPageInLibrary
    isSearchResultDisplayCorrectly  London  Bangkok  Berlin
    isTagTabActive
    

canSearchItemByMultipleTag
    [Tags]    testrailid=41147    critical
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    cat bird fog
    chooseResultTagType
    isSearchResultDisplayCorrectly  Multiple Tags  Paris  File Test

WebDisplayMessageInCaseNotFoundItem
    [Tags]    testrailid=41153    critical
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    Company
    isNotFoundItem

canSearchFolderByFolderType
    [Tags]    testrailid=41149    critical
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    Fish   
    chooseResultFolderType
    isSearchResultDisplayCorrectly  Fish Test Folder
    isItemResultDisplayOnlyOne  2

canSearchDocumentByFDocumentType
    [Tags]    testrailid=41150    critical
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    Fish   
    chooseResultDocumentType
    isSearchResultDisplayCorrectly  Fish Test Document
    isItemResultDisplayOnlyOne  2

canSearchItemByTagType
    [Tags]    testrailid=41151    critical
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    Cat   
    chooseResultTagType
    isSearchResultDisplayCorrectly  Paris
    isHighlightKeywordDisplayCorrectlyForSingleKeyword    Cat
    isItemResultDisplayOnlyOne  2
    
canSearchDocumentByAttachmentsType
    [Tags]    testrailid=41152    critical
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    Fish   
    chooseResultAttachmentType
    isSearchResultDisplayCorrectly  Fish Test File
    isItemResultDisplayOnlyOne  2

canViewDocument
    [Tags]    testrailid=3989    sanity  Autoqa_267
    selectLibraryMenu
    chooseFolderByname  mainFolder
    chooseDocumentByname  TestQuiz
    isTitleDocumentDisplay  TestQuiz
    isContentDocumentDisplay  Good

canPlayVideoInDocument
    [Tags]    testrailid=3990    sanity  Autoqa_267
    selectLibraryMenu
    chooseFolderByname  mainFolder
    chooseDocumentByname  document1
    playVideoInDocument
    isPauseVideoButtonDisplay

# canDownloadFileInDocument
#     [Tags]    testrailid=3991    sanity  Autoqa_267
#     selectLibraryMenu
#     chooseFolderByname  mainFolder
#     chooseDocumentByname  document2
#     clickFileByname  workflow_file.pdf
#     downloadFileInDocument
#     isDownloadFileFinish

SearchListCanDisplayAfterInputText
    [Tags]    testrailid=43448    sanity  Autoqa_268
    selectLibraryMenu
    searchText    document
    isSearchListResultDisplayCorrectly  document2  document1

canSearchItemByTypesEqualOrMoreThan3Letters
    [Tags]    testrailid=48822    sanity  Autoqa_268
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    Load   
    isResultSortByIndexPosition

canSearchMultipleWordatOne
    [Tags]    testrailid=51196    sanity  Autoqa_268
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    Testquiz document1  
    isSearchResultDisplayCorrectly  TestQuiz  document1

SearchBarCanDisplayHistorySearchMoreThan7Item
    [Tags]    testrailid=48818    sanity  Autoqa_268
    loginPage.logoutWebClient
    loginPage.loginWebClient  wlibrary5
    selectLibraryMenu
    clickTextSearchInput
    isSearchListResultDisplayAfterClickSearchBar  test  unp  unpu  unpub  unpubl  unpubli  unpublis

clickSearchHistoryItem
    [Tags]    testrailid=48820    sanity  Autoqa_268
    loginPage.logoutWebClient
    loginPage.loginWebClient  wlibrary5
    selectLibraryMenu
    clickTextSearchInput
    clickSearchHistoryItem  bang
    clickSearchResult  Bangkok
    isTitleDocumentDisplay  Bangkok
whenUserClickOnAttachmentAppShouldDisplayFilePreview
    [Tags]    testrailid=43450    sanity  Autoqa_268
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    ISTQB
    clickLibrarySearchResultByIndex  1
    isAppCanPreviewFileInLibrary

clickOnTheSourceOfAttachMentToGoToDocumentFile
    [Tags]    testrailid=43451    sanity  Autoqa_268
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    ISTQB
    clickOnListItemPathByIndex   1
    isDocumentDisplayCorrectTitle  DocumentFile
    
userCanSearchAssessmentOnlyPublishedStatusOnly
    [Tags]    testrailid=48550    sanity  Autoqa_268
    selectLibraryMenu
    searchTextInLibraryBarAndPressEnterButton    unpublish
    isNotFoundItem
    clearSearchLibraryKeyword
    searchTextInLibraryBarAndPressEnterButton    automate
    clickLibrarySearchResultByIndex  1
    isQuizDisplayCorrectTitle  automateQuiz

goToLibraryDocumentByClickLinkInChat
    [Tags]    testrailid=51427    sanity  Autoqa_306
    inputElasticSearch  Permission
    selectChatroomByIndex  1
    clickOnLinkPreview
    Select Window  new
    selectLibraryFrame
    isTitleDocumentDisplay  Permission

goToLibraryDocumentByClickLinkInChatInCaseNoHavePermission
    [Tags]    testrailid=51428    sanity  Autoqa_306
    inputElasticSearch  NoPer
    selectChatroomByIndex  1
    clickOnLinkPreview
    Select Window  new
    selectLibraryFrame
    isNoPermissionToAccessDucument

goToDeepLibraryDocumentByClickLinkInChat
    [Tags]    testrailid=51429    sanity  Autoqa_306
    inputElasticSearch  DeepDoc
    selectChatroomByIndex  1
    clickOnLinkPreview
    Select Window  new
    selectLibraryFrame
    isTitleDocumentDisplay  deepDocument

goToLibraryDocumentByGoToLinkInCaseloginAlready
    [Tags]    testrailid=54139    sanity  Autoqa_306
    Go to    ${LibraryURL}
    selectLibraryFrame
    isTitleDocumentDisplay  Permission

goToLibraryDocumentByGoToLinkFormOtherWeb
    [Tags]    testrailid=54140    sanity  Autoqa_306
    Go to    ${GoogleURL}
    sleep    1s
    Go to    ${LibraryURL}
    selectLibraryFrame
    isTitleDocumentDisplay  Permission

goToLibraryDocumentByGoToLinkInCaseNotloginAlready
    [Tags]    testrailid=54141    sanity  Autoqa_306
    loginPage.logoutWebClient
    Go to    ${LibraryURL}
    isLoginPageDisplay
    loginPage.loginWebClient  wlibrary1

canSeeTimeInQuizePage
    [Tags]    testrailid=55935    sanity  Autoqa_327
    selectLibraryMenu
    chooseFolderByname  Quize
    chooseDocumentByname  Assessment
    clickTaskAnAssessmentButton
    isQuizeDisplayTime  10 minutes