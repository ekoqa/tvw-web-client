*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/discover/discoverPage.robot
Resource                  ../main/keyword/discover/insideDiscoverPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/task/taskPage.robot

Suite Setup    openEkoAppAndLogIn  wdiscovery1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wdiscovery1
Suite Teardown    Close browser
Force Tags    discover

*** Test Case ***

# canFollowHub
#     [Tags]    testrailid=4016
#     selectDiscoverMenu
#     clickFollowTheFirstHub
#     ${hubName}    Get text    ${HUBTITLE}
#     Reload Page
#     selectHubName  ${hubName}
#     isUserCanAccessFollowingHub  ${hubName}

canCreateQuestionInHub
    [Tags]    testrailid=10146    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    addImageIntoHubTopic
    addImageIntoHubTopic
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${key}
    selectFirstThreadInHub
    isMulitpleImageDisplayInTopic

canCreatePollInHub
    [Tags]    testrailid=10147    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreatePollInHub
    inputPollTitle  ${key}
    inputPollList
    addImageIntoHubTopic
    addImageIntoHubTopic
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${key}
    selectFirstThreadInHub
    isMulitpleImageDisplayInTopic

canCreateShareInHub
    [Tags]    testrailid=10148    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateShareInHub
    inputShareTitle  ${key}
    addImageIntoHubTopic
    addImageIntoHubTopic
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${key}
    selectFirstThreadInHub
    isMulitpleImageDisplayInTopic

canDeleteQuestionInHub
    [Tags]    testrailid=10183    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    selectFirstThreadInHub
    clickThreeDotInsideHubTopic
    deleteHubTopic
    isThreadDeletedInHub  ${key}

canVoteOtherUserPoll
    [Tags]    testrailid=4025    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreatePollInHub
    inputPollTitle  ${key}
    inputPollList
    confirmToCreateTopicInHub
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    selectHubTopicName  ${key}
    voteFirstPollChoice
    clickSubmitVote
    isVoteResultDisplayCorrectly
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1

canCommentOwnShareInHub
    [Tags]    testrailid=4031    critical
    ${key}    Generate random string    12    [LETTERS]
    ${comment}    Generate random string    4    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateShareInHub
    inputShareTitle  ${key}
    confirmToCreateTopicInHub
    selectHubTopicName  ${key}
    commentInHubTopic    ${comment}
    isCommentInHubDisplay  ${comment}

canEditCommentInHub
    [Tags]    testrailid=10168    critical
    ${key}    Generate random string    12    [LETTERS]
    ${comment}    Generate random string    4    [LETTERS]
    ${newComment}  Generate random string    6    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateShareInHub
    inputShareTitle  ${key}
    confirmToCreateTopicInHub
    selectHubTopicName  ${key}
    commentInHubTopic    ${comment}
    clickThreeDotForCommentInHubTopic
    clickEditCommentInHubTopic
    editCommentTextInHub  ${newComment}
    clickConfirmEditCommentInHub
    isCommentInHubDisplay  ${newComment}

canCommentByImageInOwnTopic
    [Tags]    testrailid=10164    critical
    ${key}    Generate random string    12    [LETTERS]
    ${comment}    Generate random string    4    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateShareInHub
    inputShareTitle  ${key}
    confirmToCreateTopicInHub
    selectHubTopicName  ${key}
    commentInHubByImage
    commentInHubTopic  ${comment}
    isCommentByImageDisplayInFirstComment
    
canCommentOtherShareInHub
    [Tags]    testrailid=4032    critical
    ${key}    Generate random string    12    [LETTERS]
    ${comment}    Generate random string    4    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateShareInHub
    inputShareTitle  ${key}
    confirmToCreateTopicInHub
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    selectHubTopicName  ${key}
    commentInHubTopic    ${comment}
    isCommentInHubDisplay  ${comment}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1

canEditQuestionInHub
    [Tags]    testrailid=10139    critical
    ${key}    Generate random string    12    [LETTERS]
    ${edit}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    selectFirstThreadInHub
    clickThreeDotInsideHubTopic
    editHubTopic
    editHubTopicTitle  ${edit}
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${edit}
    isThreadDeletedInHub  ${key}

canEditPollInHub
    [Tags]    testrailid=10140    critical
    ${key}    Generate random string    12    [LETTERS]
    ${edit}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreatePollInHub
    inputQuestionTitle  ${key}
    inputPollList
    confirmToCreateTopicInHub
    selectFirstThreadInHub
    clickThreeDotInsideHubTopic
    editHubTopic
    editHubTopicTitle  ${edit}
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${edit}
    isThreadDeletedInHub  ${key}

canEditShareInHub
    [Tags]    testrailid=10141    critical
    ${key}    Generate random string    12    [LETTERS]
    ${edit}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateShareInHub
    inputShareTitle  ${key}
    confirmToCreateTopicInHub
    selectFirstThreadInHub    
    clickThreeDotInsideHubTopic
    editHubTopic
    editHubTopicTitle  ${edit}
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${edit}
    isThreadDeletedInHub  ${key}

canRemoveImageInQuestion
    [Tags]
    ${key}    Generate random string    12    [LETTERS]
    ${edit}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    addImageIntoHubTopic
    confirmToCreateTopicInHub
    selectFirstThreadInHub
    clickThreeDotInsideHubTopic
    editHubTopic
    clickRemoveImageInHUbTopic
    confirmToCreateTopicInHub
    isSingleImageInHubTopicRemoved

adminCanDeleteAnyQuestionTopic
    [Tags]    testrailid=10189    critical
    ${key}    Generate random string    12    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1
    selectDiscoverMenu
    selectHubName  automationHub
    selectHubTopicName  ${key}
    clickThreeDotInsideHubTopic
    deleteHubTopic
    isThreadDeletedInHub  ${key}

commentOwnerCanDeleteTheirComment
    [Tags]    testrailid=10156    critical
    ${key}    Generate random string    12    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    selectFirstThreadInHub
    commentInHubTopic  ${key}
    clickThreeDotForCommentInHubTopic
    deleteCommentInHubTopic
    isCommentInHubDeleted  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1

adminCanDeleteAnyCommentInHubTopic
    [Tags]    testrailid=14451    critical
    ${key}    Generate random string    12    [LETTERS]
    ${comment}    Generate random string    12    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    selectFirstThreadInHub
    commentInHubTopic  ${comment}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1
    selectDiscoverMenu
    selectHubName  automationHub
    selectHubTopicName  ${key}
    clickThreeDotForCommentInHubTopic
    deleteCommentInHubTopic
    isCommentInHubDeleted  ${comment}

userSeeNotifyBannerWhenAdminDeleteUserTopic
    [Tags]    testrailid=12354    critical
    ${key}    Generate random string    12    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1
    selectDiscoverMenu
    selectHubName  automationHub
    selectHubTopicName  ${key}
    clickThreeDotInsideHubTopic
    deleteHubTopic
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    isAlertBannerDisplay
    clickCloseAlertBanner
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1


posterCanDeletePollInHub
    [Tags]    testrailid=10184    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreatePollInHub
    inputPollTitle  ${key}
    inputPollList
    addImageIntoHubTopic
    confirmToCreateTopicInHub
    selectFirstThreadInHub
    clickThreeDotInsideHubTopic
    deleteHubTopic
    isThreadDeletedInHub  ${key}

posterCanDeleteShareInHub
    [Tags]    testrailid=10185    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateShareInHub
    inputShareTitle  ${key}
    addImageIntoHubTopic
    confirmToCreateTopicInHub
    selectFirstThreadInHub
    clickThreeDotInsideHubTopic
    deleteHubTopic
    isThreadDeletedInHub  ${key}

canAddMoreChoiceInPoll
    [Tags]    testrailid=12342    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreatePollInHub
    inputPollTitle  ${key}
    inputPollList
    addImageIntoHubTopic
    confirmToCreateTopicInHub
    selectFirstThreadInHub
    clickThreeDotInsideHubTopic
    editHubTopic
    clickAddMoreChoiceInPoll
    addPollChoice
    confirmToCreateTopicInHub
    isPollChoiceDisplay

# canUnFollowHub
#     [Tags]    testrailid=4017
#     selectDiscoverMenu
#     clickFollowTheFirstHub
#     clickUnFollowTheFirstHub
#     Reload page
#     selectHubName  automationHub
#     isRequireFollowPopUpDisplay
#     clickCancelFollowHub
#     clickSettingButton

allTimeRankInLeaderBoard
    [Tags]    testrailid=4037    critical
    selectDiscoverMenu
    isLeaderBoardDisplay

checkPointOfUserInDiscover
    [Tags]    testrailid=4033    critical
    selectDiscoverMenu
    clickTopRankUser
    isPointUserDisplay
    
canVoteOtherUserQuestion
    [Tags]    testrailid=4024    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    selectHubTopicName  ${key}
    voteQuestionInsideTopic
    isVoteInsideTopicIDisplayNumber  1
    isVoteOutsideTopicIDisplayNumber  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1

canVoteOtherUserShare
    [Tags]    testrailid=4026    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateShareInHub
    inputShareTitle  ${key}
    confirmToCreateTopicInHub
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    selectHubTopicName  ${key}
    voteQuestionInsideTopic
    isVoteInsideTopicIDisplayNumber  1
    isVoteOutsideTopicIDisplayNumber  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1
    
standardUserCanNotSeeCreateHubButton
    [Tags]    testrailid=16348    sanity
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    isCreteNewHubNotDisplay

adminCanDeleteOtherPollInHub
    [Tags]    testrailid=10190    sanity
    ${key}    Generate random string    12    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreatePollInHub
    inputPollTitle  ${key}
    inputPollList
    confirmToCreateTopicInHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1
    selectDiscoverMenu
    selectHubName  automationHub
    selectFirstThreadInHub
    clickThreeDotInsideHubTopic
    deleteHubTopic
    isThreadDeletedInHub  ${key}

adminCanDeleteOtherShareInHub
    [Tags]    testrailid=10191    sanity
    ${key}    Generate random string    12    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery2
    selectDiscoverMenu
    selectHubName  automationHub
    clickCreateShareInHub
    inputShareTitle  ${key}
    confirmToCreateTopicInHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1
    selectDiscoverMenu
    selectHubName  automationHub
    selectFirstThreadInHub
    clickThreeDotInsideHubTopic
    deleteHubTopic
    isThreadDeletedInHub  ${key}

canCommentOtherQuestionInHub
    [Tags]    testrailid=10165    sanity
    ${key}    Generate random string    12    [LETTERS]
    ${comment}    Generate random string    4    [LETTERS]
    selectDiscoverMenu
    selectHubName  Topic
    selectHubTopicName  otherQuestion
    commentInHubTopic    ${comment}
    isCommentInHubDisplay  ${comment}

userCanSeeNotifyBannerWhenAdminDeleteTopicThatWasComment
    [Tags]    testrailid=14452    sanity
    ${key}    Generate random string    12    [LETTERS]
    ${comment}    Generate random string    4    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery3
    selectDiscoverMenu
    selectHubName  automationHub
    clearNotifyBannerInDiscoverPage
    selectFirstThreadInHub
    commentInHubTopic    ${comment}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1
    selectDiscoverMenu
    selectHubName  automationHub
    ${name}    Get text    ${HUBTITLELIST}
    selectFirstThreadInHub
    clickThreeDotInsideHubTopic
    deleteHubTopic
    isThreadDeletedInHub  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery3
    selectDiscoverMenu
    selectHubName  automationHub
    isNotifyBannerDisplayCorrectForDeleteTopic  ${name}
    clearNotifyBannerInDiscoverPage
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdiscovery1