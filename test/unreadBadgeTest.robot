*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/directory/directoryPage.robot
Resource                  ../main/keyword/chat/chatMemberPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/chat/mediaAndFilePage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/profile/profilePage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/admin/user/userDetailPage.robot
Resource                  ../main/keyword/admin/user/userListHomePage.robot
Resource                  ../main/keyword/admin/adminLandingPage.robot
Resource                  ../main/keyword/task/taskPage.robot
Resource                  ../main/keyword/task/taskDetailPage.robot
Resource                  ../main/keyword/workflow/workflowHomePage.robot
Resource                  ../main/keyword/workflow/workflowCreationPage.robot
Resource                  ../main/keyword/workflow/workflowDetailPage.robot
Resource                  ../main/keyword/form/formListPage.robot
Resource                  ../main/keyword/form/formPage.robot
Resource                  ../main/keyword/form/formCreationPage.robot
Resource                  ../main/keyword/card/cardPage.robot
Resource                  ../main/keyword/card/cardCreationPage.robot


Suite Setup    openEkoAppAndLogIn  wunread1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wunread1
Suite Teardown    Close browser
Force Tags  taskUnread

*** Test Case ***

CheckUnreadBadgeInDMChat
    [Tags]    testrailid=18508    critical
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  unread2
    selectChatroomByIndex  1
    sendMessage  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread2
    isRecentPageUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread1

CheckUnreadBadgeInDMTopicChat
    [Tags]    testrailid=18509    critical
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  unread2
    selectChatroomByIndex  1
    selectTopicTab
    selectFirstTopic
    sendMessage  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread2
    isRecentPageUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread1

CheckUnreadBadgeInGCChat
    [Tags]    testrailid=18510    critical
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  Group
    selectChatroomByIndex  1
    sendMessage  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread2
    isRecentPageUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread1

CheckUnreadBadgeInGCTopicChat
    [Tags]    testrailid=18511    critical
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  Group
    selectChatroomByIndex  1
    selectTopicTab
    selectFirstTopic
    sendMessage  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread2
    isRecentPageUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread1

CheckUnreadBadgeInTaskComment
    [Tags]    testrailid=18512    critical
    ${key}    Generate random string    12    [LETTERS]
    selectTaskMenu
    clickFirstTask
    clickTaskComment
    sendMessage  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread2
    isTaskUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread1

CheckUnreadBadgeInFormComment
    [Tags]    testrailid=18513    critical
    ${key}    Generate random string    12    [LETTERS]
    selectFormMenu
    selectTheFirstFormList
    sendMessage  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread2
    isFormUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread1

CheckUnreadBadgeInWorkflowComment
    [Tags]    testrailid=18514    critical
    ${key}    Generate random string    12    [LETTERS]
    selectWorkflowMenu
    selectWorkflowByTitle  Unread
    clickWorkflowComment
    sendMessage  ${key}
    closeWorkflowComment
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread2
    isWorkflowUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread1

CheckUnreadBadgeInCardComment
    [Tags]    testrailid=18515    critical
    ${key}    Generate random string    12    [LETTERS]
    selectCardMenu
    SelectCardName  Unread
    clickCardComment
    sendMessage  ${key}
    closeCardComment
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread2
    isCardUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wunread1
