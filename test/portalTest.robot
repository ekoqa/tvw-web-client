*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/portal/portalPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/portal/portalHomePage.robot
Resource                  ../main/keyword/admin/portal/portalCreationPage.robot


Suite Setup    openEkoAppAndLogIn  wportal1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wportal1
Suite Teardown    Close browser
Force Tags    formPortalBannerSearchProfile

*** Test Case ***

makeSureAppShowsDetailOfPortalCorrectly
    [Tags]    testrailid=10143    critical
    selectPortalMenu
    isPortalDisplayAllDetailCorrect  netflix  ${portalDescription}

canOpenNewTabPortal
    [Tags]    testrailid=3951    critical
    selectPortalMenu
    ${current_window}    get window handles
    selectPortalNumber  1
    ${new_window_list}    get window handles
    isNewTabPortalOpen  ${current_window}  ${new_window_list}
    Close browser
    openEkoAppAndLogIn  wportal1

canOpenInAppPortal
    [Tags]    testrailid=3952    critical
    selectPortalMenu
    selectPortalNumber  2
    isInAppPortalOpen

canAddFavoritePortal
    [Tags]    testrailid=10152    critical
    ${index}=  Evaluate  1
    selectPortalMenu
    ${portalName}  Get text  //div[@id="itemContainer"]/div[@id="favoriteEndingLine"]/following-sibling::div[${index}]
    addPortalFavoriteByIndex  1
    isPortalAddToFavorite  ${portalName}

canRemoveFavoritePortal
    [Tags]    testrailid=10153    critical
    selectPortalMenu
    ${count}=  Evaluate  1
    :FOR  ${index}  IN RANGE  10
    \  ${type}  get element attribute  //div[@id="itemContainer"]/div[${count}]  id
    \  Exit For Loop If  '${type}'=='favoriteEndingLine'
    \  ${count}=  Evaluate  ${count}+1
    \  ${index}=  Evaluate  ${count}-1
    ${portalName}  Get text  //div[@id="itemContainer"]/div[@data-id][${index}]//h6/span
    removePortalFavoriteByIndex  ${index}
    isPortalRemoveFromFavorite  ${portalName}

canSearchPortal
    [Tags]    testrailid=10161    critical
    selectPortalMenu
    inputSearchForPortal  CP
    isSearchResultDisplayCorrect  CP

userCanSeePortalIfHasPermission
    [Tags]    testrailid=12262    critical
    ${key}    Generate random string    12    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickPortalMenuInAdmin
    clickAddNewPortal
    inputPortalName  ${key}
    inputPortalURL
    searchDirectoryForPortalPermission  automationPortal
    selectDirectoryToAddPortalPermission  automationPortal
    savePortalCreation
    Go to    ${ClientURL}
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wportal2
    selectPortalMenu
    isPortalDisplayCorrect  ${key}
    Unselect Frame
    loginPage.logoutWebClient
    loginPage.loginWebClient  wportal1
    Go to    ${AdminURL}
    clickPortalMenuInAdmin
    selectPortalToRemove  ${key}
    clickRemoveSelectedPortal
    confirmRemovePortal

userCannotSeePortalIfNotHasPermission
    [Tags]    testrailid=12263    critical
    ${key}    Generate random string    18    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickPortalMenuInAdmin
    clickAddNewPortal
    inputPortalName  ${key}
    inputPortalURL
    searchDirectoryForPortalPermission  automationPortal
    selectDirectoryToAddPortalPermission  automationPortal
    savePortalCreation
    Go to    ${ClientURL}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wportal3
    selectPortalMenu
    isPortalDisappear  ${key}
    Unselect Frame
    loginPage.logoutWebClient
    loginPage.loginWebClient  wportal1
    Go to    ${AdminURL}
    clickPortalMenuInAdmin
    selectPortalToRemove  ${key}
    clickRemoveSelectedPortal
    confirmRemovePortal

userCanSeePortalIfNotSetAnyPermission
    [Tags]    testrailid=12266    critical
    ${key}    Generate random string    12    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickPortalMenuInAdmin
    clickAddNewPortal
    inputPortalName  ${key}
    inputPortalURL
    savePortalCreation
    Go to    ${ClientURL}
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wportal3
    selectPortalMenu
    isPortalDisplayCorrect  ${key}
    Unselect Frame
    loginPage.logoutWebClient
    loginPage.loginWebClient  wportal1
    Go to    ${AdminURL}
    clickPortalMenuInAdmin
    selectPortalToRemove  ${key}
    clickRemoveSelectedPortal
    confirmRemovePortal

adminCanSeeAllPortalEvenItHasSetPermission
    [Tags]    testrailid=12267    critical
    ${key}    Generate random string    18    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickPortalMenuInAdmin
    clickAddNewPortal
    inputPortalName  ${key}
    inputPortalURL
    searchDirectoryForPortalPermission  automationPortal
    selectDirectoryToAddPortalPermission  automationPortal
    savePortalCreation
    Go to    ${ClientURL}
    selectPortalMenu
    isPortalDisplayCorrect  ${key}
    Unselect Frame
    Go to    ${AdminURL}
    clickPortalMenuInAdmin
    selectPortalToRemove  ${key}
    clickRemoveSelectedPortal
    confirmRemovePortal

canScrollDownToFindPortal
    [Tags]    testrailid=12264    sanity  Autoqa_327
    selectPortalMenu
    scrolldownInPortalPage
    isAbleToScrollDownInPortalPage
    isPortalDisplayCorrect  Wiki