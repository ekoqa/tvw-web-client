*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/directory/directoryPage.robot



Suite Setup    openEkoAppAndLogIn  wonline1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wonline1
Suite Teardown    Close browser
Force Tags  taskUnread

*** Test Case ***

onlineBannerShowCorrectWording
    [Tags]    testrailid=27219    critical
    isShowUserOnlineMessage

activeTabNoNumberBehideAnymore
    [Tags]    testrailid=27220    critical
    selectDirectoryMenu
    isDirectoryShowOnlineUser
    isHaveOnlineUser


