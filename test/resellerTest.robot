*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/jitsi/vRoomHomePage.robot
Resource                  ../main/keyword/reseller/resellerHomePage.robot
Resource                  ../main/keyword/reseller/editNetworkPage.robot


Suite Setup    openEkoAppAndLogIn  reseller1
Test Setup     Go to    ${ClientURL}
Suite Teardown    Close browser
Force Tags    others

*** Test Case ***

canChangeURLToJitsiServer
    [Tags]    testrailid=12245    critical
    Go to    https://sea-staging-reseller.ekoapp.com/
    loginReseller
    searchNetworkInReseller  reseller
    clickEditNetwork
    clickDynamicConfigTab
    clickEditJitsiJSONFormat
    editJSONFormat  ${JITSIJSON}
    saveEditJSONFormat
    clickSaveEditNetwork
    goToWebClientWith  reseller1
    loginPage.logoutWebClient
    loginPage.loginWebClient  reseller1
    selectConferenceMenu
    isConferenePageDisplayJitsiUI

canChangeURLToVroomServer
    [Tags]    testrailid=12246    critical
    Go to    https://sea-staging-reseller.ekoapp.com/
    # loginReseller
    searchNetworkInReseller  reseller
    clickEditNetwork
    clickDynamicConfigTab
    clickEditJitsiJSONFormat
    editJSONFormat  ${TRUEVWORLDJSON}
    saveEditJSONFormat
    clickSaveEditNetwork
    goToWebClientWith  reseller1
    loginPage.logoutWebClient
    loginPage.loginWebClient  reseller1
    selectConferenceMenu
    isConferenePageDisplayVWorldUI
