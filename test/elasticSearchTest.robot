*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/directory/directoryPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/topicListPage.robot



Suite Setup    openEkoAppAndLogIn  wsearch1
Test Setup     Go to    ${ClientURL}
Suite Teardown    Close browser
Force Tags    formPortalBannerSearchProfile

*** Test Case ***

searchForDirectChat
    inputElasticSearch  search2
    isSearchForChatDisplay  search2

selectChatFromElasticSearch
    [Tags]    testrailid=3779    critical
    inputElasticSearch  search2
    selectChatroomByIndex  1
    isSearchResultForChatDisplay  search2
    
scrollDownFromChatResult
    [Tags]    testrailid=3785    critical
    inputElasticSearch  c
    scrolldownInSearchResult
    isAbleToScrollDown

searchForMessageInDC
    [Tags]    testrailid=3782    critical
    inputElasticSearch  hi
    isSearchForMessageDisplay  hi

searchForMessageInGC
    [Tags]    testrailid=3783    critical
    ${key}    Generate random string    4    [LETTERS]
    createGCFromRecent
    searchUserInCreateChatSearchBox  search2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  searchGroup
    confirmCreateChat
    Reload page
    sendMessage  ${key}
    inputElasticSearch  ${key}
    isSearchForMessageDisplay  ${key}

scrollDownFromSearchMessageResult
    [Tags]    testrailid=3786    critical
    inputElasticSearch  hi
    scrolldownInSearchResult
    isAbleToScrollDown

searchUserFromContactPage
    [Tags]    testrailid=3787    critical
    selectDirectoryMenu
    inputElasticSearch  search2
    isContactDisplay  search2

scrollDownFromSearchContactResult
    [Tags]    testrailid=3788    critical
    selectDirectoryMenu
    inputElasticSearch  c
    scrolldownInSearchResult

searchForGCChatName
    [Tags]    testrailid=3780    sanity
    inputElasticSearch  Tokyo
    isSearchForChatDisplay  Tokyo

searchForTopicChatName
    [Tags]    testrailid=3781    sanity
    inputElasticSearch  osaka
    selectChatroomByIndex  1
    isTopicAppear  osaka

    
