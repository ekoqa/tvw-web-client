*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/directory/directoryPage.robot
Resource                  ../main/keyword/chat/chatMemberPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/chat/mediaAndFilePage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/profile/profilePage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/admin/user/userDetailPage.robot
Resource                  ../main/keyword/admin/user/userListHomePage.robot
Resource                  ../main/keyword/admin/adminLandingPage.robot



Suite Setup    openEkoAppAndLogIn  wchatevent1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wchatevent1
Suite Teardown    Close browser
Force Tags    chat

*** Test Case ***

eventMessageAfterRenameGC
    [Tags]    testrailid=4129    critical
    createGCFromRecent
    searchUserInCreateChatSearchBox  chatevent2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    confirmCreateChat
    clickSettingInChatroom
    setGCname  EditCloseGroup
    clickSaveGCSetting
    isEventMessageCorrect  ${renameGCEvent}

eventMessageAfterAddMember
    [Tags]    testrailid=4130    critical
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chatevent2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  ADDGROUP
    confirmCreateChat
    selectMemberTab
    addMemberToGC
    searchUserInCreateChatSearchBox  chatevent3
    selectUserFromSearchToCreateChat
    selectChatTab
    isEventMessageCorrect  ${addMemberEvent}

eventMessageAfterRemoveMember
    [Tags]    testrailid=4131    critical
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chatevent2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  REMOVEGROUP
    confirmCreateChat
    selectMemberTab
    removeMemberFromGC  chatevent2
    selectChatTab
    isEventMessageCorrect  ${removeMemberEvent}

eventMessageAfterCreateTopic
    [Tags]    testrailid=4133    critical
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chatevent2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  TOPICGROUP
    confirmCreateChat
    selectTopicTab
    createNewTopicInChat  newTopic
    selectChatTab
    isEventMessageCorrect  ${createTopicEvent}

eventMessageAfterChangeCoverPhotoInGroupChat
    [Tags]    testrailid=4132    sanity  Autoqa_267
    inputElasticSearch  Cover
    selectChatroomByIndex  1
    clickSettingInChatroom
    uploadNewCoverPhotoInGroupChat
    sleep  1s
    clickSaveGCSetting
    isEventMessageCorrect  ${changeCoverPhotoEvent}