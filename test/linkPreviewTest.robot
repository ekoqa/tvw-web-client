*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/directory/directoryPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/portal/portalPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/admin/user/userDetailPage.robot
Resource                  ../main/keyword/admin/user/userListHomePage.robot


Suite Setup    openEkoAppAndLogIn  wpreview1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wpreview1
Suite Teardown    Close browser
Force Tags  chat

*** Test Case ***

SendLinkPreviewInDMChat
    [Tags]    testrailid=18458    critical
    inputElasticSearch  wpreview2
    selectChatroomByIndex  1
    sendMessage  ${ekoLink}
    isLinkTextContainsDescription    ${ekoDescription}

SendLinkPreviewInTopicOfDMChat
    [Tags]    testrailid=18461    critical
    inputElasticSearch  wpreview2
    selectChatroomByIndex  1
    selectTopicTab
    selectTopicByIndex  1
    sendMessage  ${ekoLink}
    isLinkTextContainsDescription    ${ekoDescription}

EditLinkPreviewInDMChat
    [Tags]    testrailid=18465    critical
    ${key}    Generate random string    8    [LETTERS]
    ${editText}    Catenate    ${ekoLink}    ${key}    
    inputElasticSearch  wpreview2
    selectChatroomByIndex  1
    sendMessage  ${ekoLink}
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessageByAddText  ${key}
    clickConfirmEditMessage
    isMessageDisplayAfterEditLink  ${editText}

EditLinkPreviewToNewURLInDMChat
    [Tags]    testrailid=20148    critical  93
    inputElasticSearch  wpreview2
    selectChatroomByIndex  1
    sendMessage  ${ekoLink}
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessagByClearOldText    www.yahoo.com
    clickConfirmEditMessage
    isLinkTextContainsDescription    ${yahooDescription}
    
SendLinkPreviewInGCChat
    [Tags]    testrailid=18471    critical
    inputElasticSearch  Groupview
    selectChatroomByIndex  1
    sendMessage  ${ekoLink}
    isLinkTextContainsDescription    ${ekoDescription}

EditLinkPreviewInGCChat
    [Tags]    testrailid=18478    critical
    ${key}    Generate random string    8    [LETTERS]
    ${editText}    Catenate    ${ekoLink}    ${key}    
    inputElasticSearch  Groupview
    selectChatroomByIndex  1
    sendMessage  ${ekoLink}
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessageByAddText  ${key}
    clickConfirmEditMessage
    isMessageDisplayAfterEditLink    ${editText}

EditLinkPreviewToNewURLInGCChat
    [Tags]    testrailid=20147    critical
    inputElasticSearch  Groupview
    selectChatroomByIndex  1
    sendMessage  ${ekoLink}
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessagByClearOldText    www.facebook.com
    clickConfirmEditMessage
    isLinkTextContainsDescription    ${facebookDescription}

replaceNormalTextInLinkMessageGC
    [Tags]    testrailid=20150    critical
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  Groupview
    selectChatroomByIndex  1
    sendMessage  ${ekoLink}
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessagByClearOldText    ${key}
    clickConfirmEditMessage
    isMessageDisplay  ${key}

makeSureUserCanClickOnLinkPreviewAndDirectUserToTheLinkAddress
    [Tags]    testrailid=18477    critical
    inputElasticSearch  Groupview
    selectChatroomByIndex  1
    sendMessage  ${ekoLink}
    clickOnLinkPreview
    isAppReDirectToCorrectURL    https://www.ekoapp.com/

EditLinkPreviewToNormalTextInDMChat
    [Tags]    testrailid=20149    sanity  93
    inputElasticSearch  wpreview2
    selectChatroomByIndex  1
    sendMessage  ${ekoLink}
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessagByClearOldText    test123
    clickConfirmEditMessage
    isMessageDisplay  test123

SendLinkPreviewInTopicGCChat
    [Tags]    testrailid=18474    sanity  93
    inputElasticSearch  Groupview
    selectChatroomByIndex  1
    selectTopicTab
    selectTopic  1
    sendMessage  ${ekoLink}
    isLinkTextContainsDescription    ${ekoDescription}
