*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/directory/directoryPage.robot
Resource                  ../main/keyword/chat/chatMemberPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/chat/mediaAndFilePage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/profile/profilePage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/admin/user/userDetailPage.robot
Resource                  ../main/keyword/admin/user/userListHomePage.robot
Resource                  ../main/keyword/admin/chatGroup/chatGroupHomePage.robot
Resource                  ../main/keyword/admin/chatGroup/chatGroupDetailPage.robot
Resource                  ../main/keyword/admin/adminLandingPage.robot



Suite Setup    openEkoAppAndLogIn  wchat1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wchat1
Suite Teardown    Close browser
Force Tags    createChat

*** Test Case ***
    
createExistingDConRecentPage
    [Tags]    testrailid=4103    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    sendMessage  hi
    isChatroomDisplay  chat2 web

createOpenGConRecentPage
    [Tags]    testrailid=4105    critical
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  OPENROUP
    confirmCreateChat
    selectMemberTab
    isUserNotBeModerator  chat1 web

createCloseGConRecentPage
    [Tags]    testrailid=8179    critical
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    changeGCtype
    setGCname  CLOSEGROUP
    confirmCreateChat
    isChatroomDisplay  CLOSEGROUP

closeChatrCreatorIsModerator
    [Tags]    testrailid=8181    critical
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    changeGCtype
    setGCname  CLOSEGROUP
    confirmCreateChat
    selectMemberTab
    isUserIsModerator  chat1 web

checkGroupMemberInGroupChat
    [Tags]    testrailid=5860    critical
    createGCFromRecent
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    searchUserInCreateChatSearchBox  chat3
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  memberGroup
    confirmCreateChat
    selectMemberTab
    isUserInGroupChat  chat2  chat3

cancelBeforeArchiveDMChat
    [Tags]    testrailid=32    critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  chat4
    selectUserFromSearchToCreateChat
    sendMessage  hi
    clickArchiveInChatroom
    cancelArchiveChat
    selectChatroomByTitle  chat4 web
    isChatroomUnArchive  chat4 web
    
canArchiveDMChat
    [Tags]    testrailid=4137    critical
    clickFilterChatStatus
    selectArchivedChatStatus
    selectChatroomByTitle  chat5 web
    sendMessage  hi
    clickFilterChatStatus
    selectActiveChatStatus
    selectChatroomByTitle  chat5 web
    clickArchiveInChatroom
    isChatroomDisappear  chat5 web

canArchiveTopic
    [Tags]    testrailid=4142    critical
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  TOPICGROUP
    selectChatroomByIndex  1
    selectTopicTab
    createNewTopicInChat  ${key}
    selectFirstTopic
    clickArchiveInTopicChat
    isTopicDisappear  ${key}

canGoToMediaAndFileInDC
    [Tags]    testrailid=5862    critical
    inputElasticSearch  chat2
    selectChatroomByIndex  1
    selectMediaAndFileTabInDC
    isMediaDisplayCorrectly

memberCannotSeeModeratorElementInCloseGC
    [Tags]    testrailid=8182    critical
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchat2
    inputElasticSearch  id8182
    selectChatroomByIndex  1
    selectMemberTab
    isThreeDotDisappear
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchat1

moderatorCanAddMemberToCloseGC
    [Tags]    testrailid=8185    critical
    createGCFromRecent
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    changeGCtype
    setGCname  CLOSEGROUP
    confirmCreateChat
    selectMemberTab
    addMemberToGC
    searchUserInCreateChatSearchBox  chat3
    selectUserFromSearchToCreateChat
    isUserInGroupChat  chat2  chat3

moderatorCanRemoveMemberFromCloseGC
    [Tags]    testrailid=8189    critical
    ${key}    Generate random string    8    [LETTERS]
    createGCFromRecent
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    searchUserInCreateChatSearchBox  chat3
    selectUserFromSearchToCreateChat
    confirmSelectUser
    changeGCtype
    setGCname  ${key}
    confirmCreateChat
    selectMemberTab
    removeMemberFromGC  chat2
    selectChatTab
    isEventMessageCorrect  ${removeMemberEventInChat}

moderatorCanRemoveAnotherModerator
    [Tags]    testrailid=8193    critical
    ${key}    Generate random string    8    [LETTERS]
    createGCFromRecent
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    searchUserInCreateChatSearchBox  chat3
    selectUserFromSearchToCreateChat
    confirmSelectUser
    changeGCtype
    setGCname  ${key}
    confirmCreateChat
    selectMemberTab
    assignMemberTobeModerator  chat2
    isUserIsModerator  chat2 web
    removeMemberFromGC  chat2
    selectChatTab
    isEventMessageCorrect  ${removeMemberEventInChat}
   
canAssignModeratorRoleToAnotherMember
    [Tags]    testrailid=8195    critical
    ${key}    Generate random string    8    [LETTERS]
    createGCFromRecent
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    searchUserInCreateChatSearchBox  chat3
    selectUserFromSearchToCreateChat
    confirmSelectUser
    changeGCtype
    setGCname  ${key}
    confirmCreateChat
    selectMemberTab
    assignMemberTobeModerator  chat2
    isUserIsModerator  chat2 web

createNewDCChat
    [Tags]    testrailid=4102    critical
    ${userName}    Generate random string    8    [LOWER]
    ${firstName}    Generate random string    8    [LETTERS]
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickCreateUserMenuInAdmin
    inputUsername  ${userName}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${firstName}
    confirmToCreateUser  ${firstName}
    Go to    ${ClientURL}
    selectDirectoryMenu
    inputElasticSearch  ${firstName}
    selectChatroomByIndex  1
    createDcChatFromProfile
    sendMessage  hi
    sendMessage  hello
    isMessageSent  hello
    Go to  ${AdminURL}
    searchUserInAdminFromMasterSearch  ${firstName}
    selectTheSearchUser  ${firstName}
    deleteUserFromTheNetwork
    Go to    ${ClientURL}

canLeaveGroupChat
    [Tags]    testrailid=4140    critical
    ${key}    Generate random string    12    [LETTERS]
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  ${key}
    confirmCreateChat
    clickLeaveChatroom
    clickConfirmLeaveChatroom
    isChatroomDisappear  ${key}

canChangeOpenToCloseGC
    [Tags]    testrailid=8201    critical
    ${key}    Generate random string    12    [LETTERS]
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  ${key}
    confirmCreateChat
    clickSettingInChatroom
    clickChangeToCloseGC
    clickSaveGCSetting
    clickSettingInChatroom
    isCloseChatType

userWhoChangeOpenChatToCloseChatBeModerator
    [Tags]    testrailid=8202    critical
    ${key}    Generate random string    12    [LETTERS]
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  ${key}
    confirmCreateChat
    clickSettingInChatroom
    clickChangeToCloseGC
    clickSaveGCSetting
    selectMemberTab
    isUserIsModerator  chat1 web
    isUserNotBeModerator  chat2 web

changeCloseChatToOpenChat
    [Tags]    testrailid=8208    critical
    ${key}    Generate random string    8    [LETTERS]
    createGCFromRecent
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    searchUserInCreateChatSearchBox  chat3
    selectUserFromSearchToCreateChat
    confirmSelectUser
    changeGCtype
    setGCname  ${key}
    confirmCreateChat
    clickSettingInChatroom
    clickChangeToOpenGC
    clickSaveGCSetting
    clickSettingInChatroom
    isOpenChatType

noModeratorInOpenGC
    [Tags]    testrailid=8211    critical
    ${key}    Generate random string    8    [LETTERS]
    createGCFromRecent
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    searchUserInCreateChatSearchBox  chat3
    selectUserFromSearchToCreateChat
    confirmSelectUser
    changeGCtype
    setGCname  ${key}
    confirmCreateChat
    clickSettingInChatroom
    clickChangeToOpenGC
    clickSaveGCSetting
    selectMemberTab
    isUserNotBeModerator  chat1 web
    isUserNotBeModerator  chat2 web

canSeeOldTextMessageInGCChat
    [Tags]    testrailid=7965    sanity
    inputElasticSearch  Old message
    selectChatroomByIndex  1
    isOldTextMessageDisplay

canSeeOldEditedTextMessageDisplay
    [Tags]    testrailid=7966    sanity
    inputElasticSearch  Old message
    selectChatroomByIndex  1
    isOldTextMessageDisplay

canSeeOldDeletedTextMessageDisplay
    [Tags]    testrailid=7967    sanity
    inputElasticSearch  Old message
    selectChatroomByIndex  1
    isOldDeletedTextMessageDisplay

canSeeOldstickerDisplay
    [Tags]    testrailid=7968    sanity
    inputElasticSearch  Old message
    selectChatroomByIndex  1
    isOldStickerDisplay

canSeeOldFileDisplay
    [Tags]    testrailid=7969    sanity
    inputElasticSearch  Old message
    selectChatroomByIndex  1
    isOldFileDisplay

canSeeOldPhotoDisplay
    [Tags]    testrailid=7970    sanity
    inputElasticSearch  Old message
    selectChatroomByIndex  1
    isOldPhotoDisplay

canSeeOldVideoDisplay
    [Tags]    testrailid=7971    sanity
    inputElasticSearch  Old message
    selectChatroomByIndex  1
    isOldVideoDisplay

markAsReadDisabledIfNoUnreadInTopicDM
    [Tags]    testrailid=43435  critical
    createDCFromRecent
    searchUserInCreateChatSearchBox  chat3
    selectUserFromSearchToCreateChat
    clickMoreButtonInChatroom
    isMarkAsReadInChatroomIsDisabled

markAsReadInTopicChatDM
    [Tags]    testrailid=43436  critical
    ${key1}    Generate random string    8    [LETTERS]
    ${key2}    Generate random string    8    [LETTERS]
    createDCFromRecent
    searchUserInCreateChatSearchBox  chat3
    selectUserFromSearchToCreateChat
    selectTopicTab
    createNewTopicInChat  ${key1}
    selectTopic  ${key1}
    sendMessage  ${key1}
    # selectTopicTab
    createNewTopicInChat  ${key2}
    selectTopic  ${key2}
    sendMessage  ${key2}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchat3
    selectChatroomByTitle  chat1 web
    selectTopicTab
    clickMarkAsReadInChatroom
    isUnreadBadgeInTopicTabDisappear
    isChatroomUnreadBadgeDisappear  chat1 web
    clickMoreButtonInChatroom
    isMarkAsReadInChatroomIsDisabled
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchat1

markAsReadDisabledIfNoUnreadInTopicGC
    [Tags]    testrailid=43439  critical
    inputElasticSearch  markAsRead
    selectChatroomByIndex  1
    clickMoreButtonInChatroom
    isMarkAsReadInChatroomIsDisabled

markAsReadInTopicChatGC
    [Tags]    testrailid=43440  critical
    ${key1}    Generate random string    8    [LETTERS]
    ${key2}    Generate random string    8    [LETTERS]
    inputElasticSearch  markAsRead
    selectChatroomByIndex  1
    selectTopicTab
    createNewTopicInChat  ${key1}
    selectTopic  ${key1}
    sendMessage  ${key1}
    createNewTopicInChat  ${key2}
    selectTopic  ${key2}
    sendMessage  ${key2}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchat3
    inputElasticSearch  markAsRead
    selectChatroomByIndex  1
    selectTopicTab
    clickMarkAsReadInChatroom
    isUnreadBadgeInTopicTabDisappear
    isChatroomUnreadBadgeDisappear  markAsRead
    clickMoreButtonInChatroom
    isMarkAsReadInChatroomIsDisabled
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchat1

canCancelBeforeLeaveGroupChat
    [Tags]    testrailid=4138    sanity
    ${key}    Generate random string    12    [LETTERS]
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  ${key}
    confirmCreateChat
    clickLeaveChatroom
    clickCancelLeaveChatroom
    isChatroomStillAppear  ${key}

CreatPrivateGC
    [Tags]    testrailid=4106    sanity
    createGCFromRecent
    confirmSelectUser
    confirmCreateChat
    selectMemberTab
    isUserInGroupChat  chat1

userShowInAdminpanelAfterAdduserInCloseChat
    [Tags]    testrailid=8188    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    createGCFromRecent
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    changeGCtype
    setGCname  ${key}
    confirmCreateChat
    selectMemberTab
    addMemberToGC
    searchUserInCreateChatSearchBox  chat3
    selectUserFromSearchToCreateChat
    Go to  ${AdminURL}
    skipTheAdminTutorial
    clickChatGroupMenuInAdmin
    selectClosedChatTab
    selectChatGroupNameInAdminPanel  ${key}
    isUserAddedToClosedGroupChat  wchat3
    clickChatGroupMenuInAdmin
    selectGroupChatToDelete  ${key}
    clickRemoveSelectedChatButton
    isClosedChatRemoved  ${key}

moderatorUserCanSeeAddMemberUserButtonAfterChangeGroupChatFromOpenToClose
    [Tags]    testrailid=8207    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  ${key}
    confirmCreateChat
    clickSettingInChatroom
    clickChangeToCloseGC
    clickSaveGCSetting
    selectMemberTab
    isAddMemberDisplay
    isUserIsModerator  chat1 web
    isThreeDotDisplayCorrect  chat2 web

memberUserNotSeeAddMemberUserButtonAfterChangeGroupChatFromOpenToClose
    [Tags]    testrailid=8206    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    createGCFromRecent
    selectUserByIndex  2
    searchUserInCreateChatSearchBox  chat2
    selectUserFromSearchToCreateChat
    confirmSelectUser
    setGCname  ${key}
    confirmCreateChat
    clickSettingInChatroom
    clickChangeToCloseGC
    clickSaveGCSetting
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchat2
    selectChatroomByTitle  ${key}
    selectMemberTab
    isAddMemberNotDisplay
    isThreeDotDisappear
    clickMoreButtonInChatroom
    isLeaveChatButtonDisappear
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchat1

closeChatCreatedFromAdminPanel
    [Tags]    testrailid=4108    sanity
    ${key}    Generate random string    8    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickChatGroupMenuInAdmin
    selectClosedChatTab
    clickAddNewClosedChatButton
    setClosedChatName  ${key}
    clickSaveClosedChatCreated
    # selectChatGroupNameInAdminPanel  ${key}
    addUserInClosedChat    chat1 web
    addUserInClosedChat    chat2 web
    addUserInClosedChat    chat3 web
    clickAddUserToClosedGroupChatButton
    changeMemberRoleToModeratorInCloseGroupChatFromAdmin  wchat2
    isAppDisplayCorrectMemberRole  wchat2  Moderator
    Go to  ${ClientURL}
    selectChatroomByTitle  ${key}
    selectMemberTab
    isAddMemberNotDisplay
    isThreeDotDisappear
    clickMoreButtonInChatroom
    isLeaveChatButtonDisappear
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchat2
    selectChatroomByTitle  ${key}
    selectMemberTab
    isAddMemberDisplay
    isUserIsModerator  chat2 web
    isThreeDotDisplayCorrect  chat1 web  chat3 web
    loginPage.logoutWebClient
    loginPage.loginWebClient  wchat1
    
moderatorCanUnassigOtherModerator
    [Tags]    testrailid=8198    sanity  Autoqa_303
    ${key}    Generate random string    8    [LETTERS]
    inputElasticSearch  Permission
    selectChatroomByIndex  1
    selectMemberTab
    unassignMemberTobeModerator  chat5 web
    isUserNotBeModerator  chat5 web
    assignMemberTobeModerator  chat5 web