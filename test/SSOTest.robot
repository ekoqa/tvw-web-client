*** Settings ***

Library    SeleniumLibrary
Library    String
Library  ExtendedSelenium2Library

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/discover/discoverPage.robot
Resource                  ../main/keyword/discover/insideDiscoverPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/task/taskPage.robot

Suite Setup    openWindow
Suite Teardown    Close browser
Force Tags    others

*** Test Case ***

ssoButtonDisplayInAsiaServer
    [Tags]    testrailid=8169    sanity
    Sleep  2s
    isSSOButtonDisplay

ssoButtonNotDisplayInEuropeServer
    [Tags]    testrailid=8167    sanity
    chooseEuropeServer
    Sleep  2s
    isSSOButtonNotDisplay

ssoButtonNotDisplayInAmericaServer
    [Tags]    testrailid=8168    sanity
    chooseAmericaServer
    Sleep  2s
    isSSOButtonNotDisplay

# loginWithSSO
#     [Tags]    testrailid=1    sanity
#     clickSSOButton
#     clickSSOButton
#     chooseNewPopup
#     loginSSOWithEmail
#     loginHotmailEmail
