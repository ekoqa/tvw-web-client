*** Settings ***

Library    SeleniumLibrary
Library    String
Library    DateTime

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/form/formListPage.robot
Resource                  ../main/keyword/form/formPage.robot
Resource                  ../main/keyword/form/formCreationPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/task/taskDetailPage.robot


Suite Setup    openEkoAppAndLogIn  wform1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wform1
Suite Teardown    Close browser
Force Tags    formPortalBannerSearchProfile

*** Test Case ***

sentFormInChatRoom
    [Tags]    testrailid=4001    critical
    selectChatroomByTitle  form2 web
    sendMessage  hi
    clickSendFormInChat
    selectFirstFormTemplate
    selectFirstRecipient
    clickSubmitForm
    selectRecentMenu
    inputElasticSearch  form2
    selectChatroomByIndex  1
    isFormSent

sentFormToContactListFromFormMenu
    [Tags]    testrailid=3995    critical
    ${key}    Generate random string    8    [LETTERS]
    selectFormMenu
    clickCreateFormButton
    selectFirstFormTemplate
    selectFirstRecipient
    setFormSubjectName  ${key}
    clickSubmitForm
    isFormCreated  ${key}

sendFormBySearchForRecipient
    [Tags]    testrailid=3996    critical
    ${key}    Generate random string    8    [LETTERS]
    selectFormMenu
    clickCreateFormButton
    selectFirstFormTemplate
    searchForAddedRecipientInFrom  form2
    setFormSubjectName  ${key}
    clickSubmitForm
    isFormCreated  ${key}

userCanMuteForm
    [Tags]    testrailid=4002    critical
    selectFormMenu
    clickCreateFormButton
    selectFirstFormTemplate
    selectFirstRecipient
    clickSubmitForm
    clickMuteForm
    isFormMute

filterFormByAwaitingAction
    [Tags]    testrailid=4009    critical
    selectFormMenu
    clickFilterForm
    selectFilterByAwaitingForm
    isDisplayOnlyAwaitingForm

senderCanCancelForm
    [Tags]    testrailid=4011    critical
    ${key}    Generate random string    8    [LETTERS]
    selectFormMenu
    clickCreateFormButton
    selectFirstFormTemplate
    searchForAddedRecipientInFrom  form2
    setFormSubjectName  ${key}
    clickSubmitForm
    clickCancelTheForm
    confirmCancelTheForm
    isFormCanceled

reCeiverCanApproveForm
    [Tags]    testrailid=4012    critical
    ${key}    Generate random string    8    [LETTERS]
    selectFormMenu
    clickCreateFormButton
    selectFirstFormTemplate
    searchForAddedRecipientInFrom  form2
    setFormSubjectName  ${key}
    clickSubmitForm
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wform2
    selectFormMenu
    selectTheFirstFormList
    clickApproveForm
    clickConfirmFormRespond
    isFormApprove
    loginPage.logoutWebClient
    loginPage.loginWebClient  wform1

reCeiverCanRejectForm
    [Tags]    testrailid=4013    critical
    ${key}    Generate random string    8    [LETTERS]
    selectFormMenu
    clickCreateFormButton
    selectFirstFormTemplate
    searchForAddedRecipientInFrom  form2
    setFormSubjectName  ${key}
    clickSubmitForm
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wform2
    selectFormMenu
    selectTheFirstFormList
    clickRejectForm
    clickConfirmFormRespond
    isFormReject
    loginPage.logoutWebClient
    loginPage.loginWebClient  wform1

canExportPdfFile
    [Tags]    testrailid=4015    critical
    ${current_window}    get window handles
    selectFormMenu
    selectFirstForm
    clickExportForm
    isFormExport  ${current_window}

canSendFormToFavoriteContact
    [Tags]    testrailid=3994    critical
    ${key}    Generate random string    8    [LETTERS]
    selectFormMenu
    clickCreateFormButton
    selectFirstFormTemplate
    searchForAddedRecipientInFrom  form3
    setFormSubjectName  ${key}
    clickSubmitForm
    isFormCreated  ${key}

getNotificationWhenReceiveForm
    [Tags]    testrailid=4003    critical
    ${key}    Generate random string    8    [LETTERS]
    clickSettingButton
    clickMarkAllAsRead
    loginPage.logoutWebClient
    loginPage.loginWebClient  wform3
    selectFormMenu
    clickCreateFormButton
    selectFirstFormTemplate
    searchForAddedRecipientInFrom  form1
    setFormSubjectName  ${key}
    clickSubmitForm
    loginPage.logoutWebClient
    loginPage.loginWebClient  wform1
    isFormUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead

getNotificationWhenreCeiverRejectForm
    [Tags]    testrailid=4006    sanity
    ${key}    Generate random string    8    [LETTERS]
    clickSettingButton
    clickMarkAllAsRead
    selectFormMenu
    clickCreateFormButton
    selectFirstFormTemplate
    searchForAddedRecipientInFrom  form3
    setFormSubjectName  ${key}
    clickSubmitForm
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wform3
    selectFormMenu
    selectTheFirstFormList
    clickRejectForm
    clickConfirmFormRespond
    isFormReject
    loginPage.logoutWebClient
    loginPage.loginWebClient  wform1
    isFormUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead

getNotificationWhenreCeiverAproveForm
    [Tags]    testrailid=4005    critical
    ${key}    Generate random string    8    [LETTERS]
    clickSettingButton
    clickMarkAllAsRead
    selectFormMenu
    clickCreateFormButton
    selectFirstFormTemplate
    searchForAddedRecipientInFrom  form3
    setFormSubjectName  ${key}
    clickSubmitForm
    clickSettingButton
    loginPage.logoutWebClient
    loginPage.loginWebClient  wform3
    selectFormMenu
    selectTheFirstFormList
    clickApproveForm
    clickConfirmFormRespond
    isFormApprove
    loginPage.logoutWebClient
    loginPage.loginWebClient  wform1
    isFormUnreadBadgeDisplay
    clickSettingButton
    clickMarkAllAsRead

sentFormInRecentPage
    [Tags]    testrailid=4000    sanity  93
    ${key}    Generate random string    8    [LETTERS]
    createFormInRecent
    selectFirstFormTemplate
    searchForAddedRecipientInFrom  form3
    setFormSubjectName  ${key}
    clickSubmitForm
    isFormCreated  ${key}

canSubmitFormWithDueDate
    [Tags]    testrailid=3997   sanity  93
    ${key}    Generate random string    8    [LETTERS]
    createFormInRecent
    selectFirstFormTemplate
    searchForAddedRecipientInFrom  form3
    setFormSubjectName  ${key}
    setDuedateCurentTime
    ${date}    Get current date 
    ${convert}=    Convert Date    ${date}    result_format=%m/%d/%Y
    clickSubmitForm
    isFormCreated  ${key}
    isDuedateTimeDisplayCorrect  ${convert}

canSendHierarchyForm
    [Tags]    testrailid=3998   sanity  93
    ${key}    Generate random string    8    [LETTERS]
    selectFormMenu
    clickCreateFormButton
    selectFormbyName  Hierarchy
    selectFirstRecipient
    setFormSubjectName  ${key}
    clickSubmitForm
    isFormCreated  ${key}

canSendHierarchyFormWithDueDate
    [Tags]    testrailid=3999   sanity  93
    ${key}    Generate random string    8    [LETTERS]
    selectFormMenu
    clickCreateFormButton
    selectFormbyName  Relative Hierarchy
    setFormSubjectName  ${key}
    selectFirstRecipientBySearchName  form2
    selectSecondRecipientBySearchName  form3
    selectThirdRecipientBySearchName  form4
    clickSubmitForm
    isFormCreated  ${key}
    isFormDisplayWithNoSetRespondTime  form2 web
    isFormDisplayWithSetRespondTime  form3 web
    isFormDisplayWithSetRespondTimeMoreThan1Day  form4 web