*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/user/createUserPage.robot
Resource                  ../main/keyword/admin/user/userDetailPage.robot
Resource                  ../main/keyword/admin/user/userListHomePage.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/settingPage.robot


Suite Setup    openAdminPanelAndLogIn  wdevicelimit1
Test Setup     Go to    ${AdminURL}
Suite Teardown    Close browser
Force Tags    formPortalBannerSearchProfile

*** Test Case ***

whenUserLogoutTheDeviceDataShouldBeKept
    [Tags]    testrailid=33152
    ${key}    Generate random string    6    [LOWER]
    clickUserMenuInAdmin
    clickCreateSingleUser
    inputUsername  ${key}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${key}
    confirmToCreateUser  ${key}
    logoutAdminPanel
    goToWebClientWith  ${key}
    loginPage.logoutWebClient
    Go to    ${AdminURL}
    loginAdminPanel  wdevicelimit1
    clickUserMenuInAdmin
    orderUserByName
    openUserProfileFromUserHomePage  ${key}
    isUserLogInDeviceIsRecorded
    deleteUserFromTheNetwork

userCanLogoutAndLoginWithSameDevice
    [Tags]    testrailid=33153
    ${key}    Generate random string    6    [LOWER]
    clickUserMenuInAdmin
    clickCreateSingleUser
    inputUsername  ${key}
    selectToSpecifyPassword
    specifyUserPassword
    setFirstname  ${key}
    confirmToCreateUser  ${key}
    logoutAdminPanel
    goToWebClientWith  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  ${key}
    Go to    ${AdminURL}
    loginAdminPanel  wdevicelimit1
    clickUserMenuInAdmin
    orderUserByName
    openUserProfileFromUserHomePage  ${key}
    isUserLogInDeviceIsRecorded
    deleteUserFromTheNetwork

userCannotLogInOverDeviceLimit
    [Tags]    testrailid=33145    critical
    logoutAdminPanel
    Go to    ${ClientURL}
    logInAndCheckDeviceLimit  wdevicelimit2
    isUserCannotLogin
    Go to    ${AdminURL}
    loginAdminPanel  wdevicelimit1