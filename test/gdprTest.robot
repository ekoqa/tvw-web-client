*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/directory/directoryPage.robot
Resource                  ../main/keyword/chat/chatMemberPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/profile/profilePage.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/companyProfile/companyProfilePage.robot



Suite Setup    openEkoAppAndLogIn  wgdpr1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wgdpr1
Suite Teardown    Close browser
Force Tags    others

*** Test Case ***
    
canSeeEmailAndMobileInOwnProfileWhenTurnOnGDPR
    [Tags]    testrailid=18388    critical
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickCompanyProfileMenuInAdmin
    enabledGDPR
    Go to    ${ClientURL}
    clickToSeeUserProfile
    isEmailDisplay    qwee@gmail.com
    isMobileDisplay    9999

cannotSeeOthersEmailAndMobilePhonInDMWhenTurnOnGDPR
    [Tags]    testrailid=18389    critical
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickCompanyProfileMenuInAdmin
    enabledGDPR
    Go to    ${ClientURL}
    inputElasticSearch  wgdpr2
    selectChatroomByIndex  1
    clickUserInLastMessage
    isEmailNotDisplay
    isMobileNotDisplay

canSeeEmailAndMobileInOwnProfileWhenTurnOffGDPR
    [Tags]    testrailid=18390    critical
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickCompanyProfileMenuInAdmin
    disabledGDPR
    Go to    ${ClientURL}
    clickToSeeUserProfile
    isEmailDisplay    qwee@gmail.com
    isMobileDisplay    9999

canSeeOtherEmailAndMobileInDMChatWhenTurnOffGDPR
    [Tags]    testrailid=18391    critical
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickCompanyProfileMenuInAdmin
    disabledGDPR
    Go to    ${ClientURL}
    inputElasticSearch  wgdpr2
    selectChatroomByIndex  1
    clickUserInLastMessage
    isEmailDisplay    ggg@gmail.com
    isMobileDisplay    1234

