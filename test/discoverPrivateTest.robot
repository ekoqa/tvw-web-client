*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/discover/discoverPage.robot
Resource                  ../main/keyword/discover/insideDiscoverPage.robot
Resource                  ../main/keyword/recent/settingPage.robot

Suite Setup    openEkoAppAndLogIn  wdprivate1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wdprivate1
Suite Teardown    Close browser
Force Tags    privatePublicDis

*** Test Case ***

privateHubDisplayPrivateIcon
    [Tags]    testrailid=16361    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    clickAddNewHub
    inputHubName  ${key}
    clickAddUserInPrivateHub
    inputKeywordForSearchUserToAddInHub  dprivate
    addUserToHubFromSearch  5
    clickConfirmAddUserToHub
    clickCreatenewHub
    selectHubName  ${key}
    isHubDisplayPrivateIcon
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub

memberCanUnfollowPrivateHubFromHubPage
    [Tags]    testrailid=16385    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    clickAddNewHub
    inputHubName  ${key}
    clickAddUserInPrivateHub
    inputKeywordForSearchUserToAddInHub  dprivate
    addUserToHubFromSearch  5
    clickConfirmAddUserToHub
    clickCreatenewHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate2
    selectDiscoverMenu
    clickPrivateTab
    clickUnFollowHubName  ${key}
    clickConfirmUnFollowHub
    isHubDeleted  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate1
    selectDiscoverMenu
    clickPrivateTab
    selectHubName  ${key}
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub

memberCanUnfollowPrivateHubInsideHubPage
    [Tags]    testrailid=18617    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    clickAddNewHub
    inputHubName  ${key}
    clickAddUserInPrivateHub
    inputKeywordForSearchUserToAddInHub  dprivate
    addUserToHubFromSearch  5
    clickConfirmAddUserToHub
    clickCreatenewHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate2
    selectDiscoverMenu
    clickPrivateTab
    selectHubName  ${key}
    clickUnFollowInsideHub
    selectDiscoverMenu
    clickPrivateTab
    isHubDeleted  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate1
    selectDiscoverMenu
    clickPrivateTab
    selectHubName  ${key}
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub

canChangePrivateToPublicHub
    [Tags]    testrailid=16379    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    clickAddNewHub
    inputHubName  ${key}
    clickAddUserInPrivateHub
    inputKeywordForSearchUserToAddInHub  dprivate
    addUserToHubFromSearch  2
    clickConfirmAddUserToHub
    clickCreatenewHub
    selectHubName  ${key}
    clickHubSetting
    clickPublicRadio
    clickConfirmChangeHubPermission
    clickCreatenewHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate3
    selectDiscoverMenu
    clickFollowHubName  ${key}
    selectHubName  ${key}
    isHubDisplayPublicIcon
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate1
    selectDiscoverMenu
    selectHubName  ${key}
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub

canAddMemberFromPrivateHub
    [Tags]    testrailid=20139
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    clickAddNewHub
    inputHubName  ${key}
    clickAddUserInPrivateHub
    inputKeywordForSearchUserToAddInHub  dprivate
    addUserToHubFromSearch  2
    clickConfirmAddUserToHub
    clickCreatenewHub
    selectHubName  ${key}
    clickHubSetting
    clickAddMoreUserToHub
    clickOpenAddMemberListPopupInDiscover
    addUserToHub  dprivate5
    clickConfirmAddUserToHub
    clickCreatenewHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate5
    selectDiscoverMenu
    clickPrivateTab
    isHubDisplay  ${key}
    selectHubName  ${key}
    isHubDisplayPrivateIcon
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate1
    selectDiscoverMenu
    clickPrivateTab
    selectHubName  ${key}
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub

canRemoveMemberFromPrivateHub
    [Tags]    testrailid=16380    critical
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    clickAddNewHub
    inputHubName  ${key}
    clickAddUserInPrivateHub
    inputKeywordForSearchUserToAddInHub  dprivate
    addUserToHubFromSearch  2
    clickConfirmAddUserToHub
    clickCreatenewHub
    selectHubName  ${key}
    clickHubSetting
    clickRemoveUserFromHub  dprivate2 web
    clickCreatenewHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate2
    selectDiscoverMenu
    clickPrivateTab
    isHubDeleted  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate1
    selectDiscoverMenu
    clickPrivateTab
    selectHubName  ${key}
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub

canUpdateDetailOnPrivateHub
    [Tags]    testrailid=18606    critical
    ${key}    Generate random string    12    [LETTERS]
    ${key2}    Generate random string    12    [LETTERS]
    ${key3}    Generate random string    12    [LETTERS]
    ${key4}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    clickAddNewHub
    inputHubName  ${key}
    inputDescriptionName  ${key2}
    clickAddUserInPrivateHub
    inputKeywordForSearchUserToAddInHub  dprivate
    addUserToHubFromSearch  1
    clickConfirmAddUserToHub
    clickCreatenewHub
    selectHubName  ${key}
    clickHubSetting
    editHubnameByClearOldText  ${key3}
    editDescriptionByClearOldText  ${key4}
    clickCreatenewHub
    selectFormMenu
    selectDiscoverMenu
    clickPrivateTab
    isEditHubName  ${key3}
    isEditDescription  ${key4}
    selectHubName   ${key3}
    clickHubSetting
    clickDeleteHub
    clickConfirmDeleteHub

hubVisibleToEveryoneWhenChangeFromPrivateToPublicHub
    [Tags]    testrailid=16381    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    clickAddNewHub
    inputHubName  ${key}
    clickAddUserInPrivateHub
    inputKeywordForSearchUserToAddInHub  dprivate
    addUserToHubFromSearch  2
    clickConfirmAddUserToHub
    clickCreatenewHub
    selectHubName  ${key}
    clickHubSetting
    clickPublicRadio
    clickConfirmChangeHubPermission
    clickCreatenewHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate2
    selectDiscoverMenu
    isHubDisplay  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate1

onlyCurrentFollowerCanSeePrivateHub
    [Tags]    testrailid=16384    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    clickAddNewHub
    inputHubName  ${key}
    clickAddUserInPrivateHub
    inputKeywordForSearchUserToAddInHub  dprivate
    addUserToHubFromSearch  2
    clickConfirmAddUserToHub
    clickCreatenewHub
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate2
    selectDiscoverMenu
    clickPrivateTab
    isHubDisplay  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate3
    selectDiscoverMenu
    isHubNotDisplay  ${key}

canOpenPrivateHubFromRecentPage
    [Tags]    testrailid=20141    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    clickAddNewHub
    inputHubName  ${key}
    clickAddUserInPrivateHub
    inputKeywordForSearchUserToAddInHub  dprivate
    addUserToHubFromSearch  1
    clickConfirmAddUserToHub
    clickCreatenewHub
    selectRecentMenu
    selectChatroomByTitle  ${key}
    isHubTitleDisplay  ${key}

adminCanPostTopicForPrivateHub
    [Tags]    testrailid=107545    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    selectHubName  Private1
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${key}

adminCanPostTopicForPrivateHubInCaseOnlyAdminCanPost
    [Tags]    testrailid=18488    sanity
    ${key}    Generate random string    12    [LETTERS]
    selectDiscoverMenu
    clickPrivateTab
    selectHubName  Private2
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${key}

standartUserCanPostTopicForPrivateHubInCaseDisableOnlyAdminCanPost
    [Tags]    testrailid=18489    sanity
    ${key}    Generate random string    12    [LETTERS]
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate2
    selectDiscoverMenu
    clickPrivateTab
    selectHubName  Private1
    clickCreateQuestionInHub
    inputQuestionTitle  ${key}
    confirmToCreateTopicInHub
    isThreadCreatedInHub  ${key}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wdprivate1
