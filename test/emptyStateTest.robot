*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/chat/taskCreationInsideChatPage.robot
Resource                  ../main/keyword/chat/topicListPage.robot
Resource                  ../main/keyword/task/taskPage.robot
Resource                  ../main/keyword/task/taskDetailPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/card/cardPage.robot
Resource                  ../main/keyword/card/cardCreationPage.robot
Resource                  ../main/keyword/directory/directoryPage.robot



Suite Setup    openEkoAppAndLogIn  wempty2
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wempty2
Suite Teardown    Close browser
Force Tags    others

*** Test Case ***

cardSharingEmptyState
    [Tags]    testrailid=14455    critical
    selectCardMenu
    SelectCardName  Share
    clickAddShareUserIcon
    clickinviteTeamMate
    isOpenInviteTeammatePage
    
peoplePageEmptyState
    [Tags]    testrailid=14461    critical
    selectDirectoryMenu
    clickinviteTeamMateInPeoplePage
    isOpenInviteTeammatePage

RecentPageEmptyState
    [Tags]    testrailid=14462    critical
    clickinviteTeamMateInRecentPage
    isOpenInviteTeammatePage

TaskPageEmptyState
    [Tags]    testrailid=14458    critical
    selectTaskMenu
    clickCreateTaskFromTaskPage
    clickAddAssigneeInTask
    clickinviteTeamMate
    isOpenInviteTeammatePage

onlineEmptyStateBannerDisppearWhenHaveNewMember
    [Tags]    testrailid=14463    critical
    loginPage.logoutWebClient
    loginPage.loginWebClient  wempty1
    isNetworkNotHaveOnlyOne



