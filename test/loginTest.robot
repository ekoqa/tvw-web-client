*** Settings ***

Library    SeleniumLibrary

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot


Suite Setup    openWindow
Test Setup    Go to    ${NewLandingPageURL}
Suite Teardown    Close browser
Force Tags    others

*** Test Case ***

checkNewUIForLandingPage
    [Tags]    testrailid=10192    critical
    isAppAutoSwipContent

canLoginFromNewLandingPage
    clickLoginButtonFromLandingPage
    loginPage.loginWebClient    wlogin1
    isLogInSuccess