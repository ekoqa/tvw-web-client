*** Settings ***

Library    SeleniumLibrary

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/workflow/workflowHomePage.robot
Resource                  ../main/keyword/workflow/workflowDetailPage.robot
Resource                  ../main/keyword/task/taskPage.robot
Resource                  ../main/keyword/task/taskDetailPage.robot

Suite Setup    openEkoAppAndLogIn  wtranslate1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  wtranslate1
Suite Teardown    Close browser
Force Tags    chat

*** Test Case ***

translateInDM
    [Tags]    testrailid=3813    critical
    inputElasticSearch  translate2
    selectChatroomByIndex  1
    sendMessage  こんにちは
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    inputElasticSearch  translate1
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickTranslateButton
    isMessageTranslate  Hello
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInDMAfterEditMaessage
    [Tags]    testrailid=3814    critical
    inputElasticSearch  translate2
    selectChatroomByIndex  1
    sendMessage  こんにちは
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessageByAddText  朝
    clickConfirmEditMessage
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    inputElasticSearch  translate1
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickTranslateButton
    isMessageTranslate  Hello morning
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInDMWithLongMessageAndClickReadMore
    [Tags]    testrailid=3816    critical
    inputElasticSearch  translate2
    selectChatroomByIndex  1
    sendMessage  ${longJapanText}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    inputElasticSearch  translate1
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickTranslateButton
    clickReadMore
    isMessageTranslateInReadMorePopUp    ${longTranslate}
    clickCloseReadMorePopup
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInGC
    [Tags]    testrailid=3817    sanity
    inputElasticSearch  Berlin
    selectChatroomByIndex  1
    sendMessage  こんにちは
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    inputElasticSearch  Berlin
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickTranslateButton
    isMessageTranslate  Hello
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInGCAfterEditMaessage
    [Tags]    testrailid=3818    sanity
    inputElasticSearch  Berlin
    selectChatroomByIndex  1
    sendMessage  こんにちは
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessageByAddText  朝
    clickConfirmEditMessage
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    inputElasticSearch  Berlin
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickTranslateButton
    isMessageTranslate  Hello morning
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInGCWithLongMessageAndClickReadMore
    [Tags]    testrailid=3820    sanity    
    inputElasticSearch  Berlin
    selectChatroomByIndex  1
    sendMessage  ${longJapanText}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    inputElasticSearch  Berlin
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickTranslateButton
    clickReadMore
    isMessageTranslateInReadMorePopUp    ${longTranslate}
    Reload Page
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInWorkflowComment
    [Tags]    testrailid=3821    sanity
    selectWorkflowMenu
    searchWorkflowByKeyword  Translate
    selectWorkflowByIndex  1
    openCommentInWorkflow
    sendMessage  こんにちは
    closeWorkflowComment
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    selectWorkflowMenu
    searchWorkflowByKeyword  Translate
    selectWorkflowByIndex  1
    openCommentInWorkflow
    clickThreeDotOnLatestMessage
    clickTranslateButton
    isMessageTranslate  Hello
    closeWorkflowComment
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInWorkflowCommentAfterEditMaessage
    [Tags]    testrailid=3822    sanity
    selectWorkflowMenu
    searchWorkflowByKeyword  Translate
    selectWorkflowByIndex  1
    openCommentInWorkflow
    sendMessage  こんにちは
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessageByAddText  朝
    clickConfirmEditMessage
    closeWorkflowComment
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    selectWorkflowMenu
    searchWorkflowByKeyword  Translate
    selectWorkflowByIndex  1
    openCommentInWorkflow
    clickThreeDotOnLatestMessage
    clickTranslateButton
    isMessageTranslate  Hello morning
    closeWorkflowComment
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInWorkflowCommentWithLongMessageAndClickReadMore
    [Tags]    testrailid=3824    sanity    
    selectWorkflowMenu
    searchWorkflowByKeyword  Translate
    selectWorkflowByIndex  1
    openCommentInWorkflow
    sendMessage  ${longJapanText}
    closeWorkflowComment
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    selectWorkflowMenu
    searchWorkflowByKeyword  Translate
    selectWorkflowByIndex  1
    openCommentInWorkflow
    clickThreeDotOnLatestMessage
    clickTranslateButton
    clickReadMore
    isMessageTranslateInReadMorePopUp    ${longTranslate}
    Reload Page
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInTaskComment
    [Tags]    testrailid=3825    sanity
    selectTaskMenu
    selectTaskName    Translate
    clickTaskComment
    sendMessage  こんにちは
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    selectTaskMenu
    selectTaskName    Translate
    clickTaskComment
    clickThreeDotOnLatestMessage
    clickTranslateButton
    isMessageTranslate  Hello
    Reload Page
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInTaskCommentAfterEditMaessage
    [Tags]    testrailid=3826    sanity
    selectTaskMenu
    selectTaskName    Translate
    clickTaskComment
    sendMessage  こんにちは
    clickThreeDotOnLatestMessage
    clickEditMessageButton
    editMessageByAddText  朝
    clickConfirmEditMessage
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    selectTaskMenu
    selectTaskName    Translate
    clickTaskComment
    clickThreeDotOnLatestMessage
    clickTranslateButton
    isMessageTranslate  Hello morning
    Reload Page
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateInTaskCommentWithLongMessageAndClickReadMore
    [Tags]    testrailid=3828    sanity  
    selectTaskMenu
    selectTaskName    Translate
    clickTaskComment
    sendMessage  ${longJapanText}
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    selectWorkflowMenu
    searchWorkflowByKeyword  Translate
    selectWorkflowByIndex  1
    openCommentInWorkflow
    clickThreeDotOnLatestMessage
    clickTranslateButton
    clickReadMore
    isMessageTranslateInReadMorePopUp    ${longTranslate}
    Reload Page
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1

translateLinkAndoriginalTextInDM
    [Tags]    testrailid=3815    sanity
    inputElasticSearch  translate2
    selectChatroomByIndex  1
    sendMessage  これはリンクwww.sanook.comです
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    inputElasticSearch  translate1
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickTranslateButton
    isSpaceMessageTranslate  This is the link
    isSpaceMessageTranslate  www.sanook.com
    Reload Page
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1
    
translateMentionAndoriginalTextInGC
    [Tags]    testrailid=3819    sanity
    inputElasticSearch  Berlin
    selectChatroomByIndex  1
    keyMentionButNotSendMessage  translate2
    sendMessage  これはリンクwww.sanook.comです
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate2
    inputElasticSearch  Berlin
    selectChatroomByIndex  1
    clickThreeDotOnLatestMessage
    clickTranslateButton
    isSpaceMessageTranslate  @ translate2 This is the link 
    isSpaceMessageTranslate  www.sanook.com
    loginPage.logoutWebClient
    loginPage.loginWebClient  wtranslate1