*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot
Resource                  ../main/keyword/recent/featureTab.robot
Resource                  ../main/keyword/recent/settingPage.robot
Resource                  ../main/keyword/chat/chatPage.robot
Resource                  ../main/keyword/profile/profilePage.robot
Resource                  ../main/keyword/eps/epsPage.robot

Suite Setup    openEkoAppAndLogIn  weps1
Test Setup     Go to    ${ClientURL}
Test Teardown    handleFailcaseWithUser  weps1
Suite Teardown    Close browser
# Force Tags  others

*** Test Case ***

goToEPSMenuWithNoData
    [Tags]    testrailid=7982    critical
    selectEPSMenu
    click360Feedback
    isEPSPageNoData
