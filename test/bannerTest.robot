*** Settings ***

Library    SeleniumLibrary
Library    String

Resource                  ../main/config/environment.robot
Resource                  ../main/config/properties.robot
Resource                  ../main/keyword/admin/featureTab.robot
Resource                  ../main/keyword/admin/banner/bannerListPage.robot
Resource                  ../main/keyword/admin/banner/createBannerPage.robot
Resource                  ../main/keyword/login/loginPage.robot
Resource                  ../main/keyword/recent/recentPage.robot


Suite Setup    openEkoAppAndLogIn  qaautomation
Test Setup     Go to    ${ClientURL}
Suite Teardown    Close browser
Force Tags    formPortalBannerSearchProfile

*** Test Case ***

bannerCanAutoSlide
    [Tags]    testrailid=4079    critical
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickBannerMenuInAdmin
    clickCreateNewBannerBroadcast
    setBannerName  banner1
    setUrlLink  https://www.netflix.com/th-en/login
    confrimCreateBannerBroadcast
    clickCreateNewBannerBroadcast
    setBannerName  banner2
    setUrlLink  https://www.netflix.com/th-en/login
    confrimCreateBannerBroadcast
    addDefaultColorBannerToTheQueue  banner1
    addDefaultColorBannerToTheQueue  banner2 
    publishTheBannerQueue
    Go to    ${ClientURL}
    isBannerDisplayOnRecentPage  banner1
    isBannerDisplayOnRecentPage  banner2

editBannerCanDisplayOnClientSideCorrectly
    [Tags]    testrailid=4080    critical
    Go to    ${AdminURL}
    # skipTheAdminTutorial
    clickBannerMenuInAdmin
    clickEditDefaultColorBanner  banner1
    setBannerName  editBanner
    setBannerTypeAs  content
    inputTextInContent  This banner is created by automated
    confrimCreateBannerBroadcast
    Go to    ${ClientURL}
    clickOpenBannerBroadcast  editBanner
    isContentBannerDisplayCorrectly  This banner is created by automated
    clickCloseBannerBroadcast

deleteBannerIsNotDisplay
    [Tags]    testrailid=4081    critical
    Go to    ${AdminURL}
    clickBannerMenuInAdmin
    clickDeleteDefaultColorBanner  editBanner
    confirmToDeleteBannerBroadcast
    clickDeleteDefaultColorBanner  banner2
    confirmToDeleteBannerBroadcast
    Go to    ${ClientURL}
    isBannerDisappear  editBanner
    isBannerDisappear  banner2

bannerDisplayCorrectOrder
    [Tags]    testrailid=4084    critical
    ${banner1}    Generate random string    8    [LETTERS]
    ${banner2}    Generate random string    8    [LETTERS]
    ${banner3}    Generate random string    8    [LETTERS]
    Go to    ${AdminURL}
    # skipTheAdminTutorial
    clickBannerMenuInAdmin
    clickCreateNewBannerBroadcast
    setBannerName  ${banner1}
    setUrlLink  https://www.netflix.com/th-en/login
    confrimCreateBannerBroadcast
    clickCreateNewBannerBroadcast
    setBannerName  ${banner2}
    setUrlLink  https://www.netflix.com/th-en/login
    confrimCreateBannerBroadcast
    clickCreateNewBannerBroadcast
    setBannerName  ${banner3}
    setUrlLink  https://www.netflix.com/th-en/login
    confrimCreateBannerBroadcast
    addDefaultColorBannerToTheQueue  ${banner1}
    addDefaultColorBannerToTheQueue  ${banner2} 
    addDefaultColorBannerToTheQueue  ${banner3} 
    publishTheBannerQueue
    Go to    ${ClientURL}
    isBannerDisplayCorrectOrder    ${banner1}    ${banner2}    ${banner3}
    Go to    ${AdminURL}
    clickBannerMenuInAdmin
    clickDeleteDefaultColorBanner  ${banner1}
    confirmToDeleteBannerBroadcast
    clickDeleteDefaultColorBanner  ${banner2}
    confirmToDeleteBannerBroadcast
    clickDeleteDefaultColorBanner  ${banner3}
    confirmToDeleteBannerBroadcast

canDisplayUrlAndContentBanner
    [Tags]    testrailid=4077    sanity  93test
    ${banner1}    Generate random string    8    [LETTERS]
    ${banner2}    Generate random string    8    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickBannerMenuInAdmin
    clickCreateNewBannerBroadcast
    setBannerName  ${banner1}
    setUrlLink  https://www.youtube.com
    confrimCreateBannerBroadcast
    clickCreateNewBannerBroadcast
    setBannerName  ${banner2}
    setBannerTypeAs  content
    inputTextInContent  This banner is created by automated
    confrimCreateBannerBroadcast
    addDefaultColorBannerToTheQueue  ${banner1}
    addDefaultColorBannerToTheQueue  ${banner2}
    publishTheBannerQueue
    Go to    ${ClientURL}
    clickOnSelectedBanner  ${banner1}
    clickOnSelectedBanner  ${banner1}
    isAppReDirectToCorrectURLWhenClickOnBanner  https://www.youtube.com/
    selectTabURL  https://eko-dev.ekoapp.com/recents
    clickOnSelectedBanner  ${banner2}
    isContentBannerPopUpDisplayCorrectly  This banner is created by automated
    clickCloseBannerContentPopUp    
    Go to    ${AdminURL}
    clickBannerMenuInAdmin
    clickDeleteDefaultColorBanner  ${banner1}
    confirmToDeleteBannerBroadcast
    clickDeleteDefaultColorBanner  ${banner2}
    confirmToDeleteBannerBroadcast

canTurnOffBanner
    [Tags]    testrailid=4083    sanity  93
    ${banner1}    Generate random string    8    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickBannerMenuInAdmin
    clickCreateNewBannerBroadcast
    setBannerName  ${banner1}
    setUrlLink  https://www.netflix.com/th-en/login
    confrimCreateBannerBroadcast
    addDefaultColorBannerToTheQueue  ${banner1}
    publishTheBannerQueue
    Go to    ${ClientURL}
    isBannerDisplayCorrectOrder    ${banner1} 
    Go to    ${AdminURL}
    clickBannerMenuInAdmin
    removeDefaultColorBannerToTheQueue  ${banner1}
    Go to    ${ClientURL}
    isBannerDisappear  ${banner1}
    Go to    ${AdminURL}
    clickBannerMenuInAdmin
    clickDeleteDefaultColorBanner  ${banner1}
    confirmToDeleteBannerBroadcast
    
notSeeUnpublishedBanner
    [Tags]    testrailid=20142    sanity  93
    ${banner1}    Generate random string    8    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickBannerMenuInAdmin
    clickCreateNewBannerBroadcast
    setBannerName  ${banner1}
    setUrlLink  https://www.netflix.com/th-en/login
    confrimCreateBannerBroadcast
    addDefaultColorBannerToTheQueue  ${banner1}
    Go to    ${ClientURL}
    isBannerDisappear  ${banner1}
    Go to    ${AdminURL}
    clickBannerMenuInAdmin
    clickDeleteDefaultColorBanner  ${banner1}
    confirmToDeleteBannerBroadcast

bannerCanSlideInCaseMoreThanOneItem
    [Tags]    testrailid=4078    sanity
    ${banner1}    Generate random string    8    [LETTERS]
    ${banner2}    Generate random string    8    [LETTERS]
    ${banner3}    Generate random string    8    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickBannerMenuInAdmin
    clickCreateNewBannerBroadcast
    setBannerName  ${banner1}
    setUrlLink  https://www.netflix.com/th-en/login
    confrimCreateBannerBroadcast
    clickCreateNewBannerBroadcast
    setBannerName  ${banner2}
    setUrlLink  https://www.netflix.com/th-en/login
    confrimCreateBannerBroadcast
    addDefaultColorBannerToTheQueue  ${banner1}
    addDefaultColorBannerToTheQueue  ${banner2} 
    publishTheBannerQueue
    Go to    ${ClientURL}
    isBannerDisplayCorrectOrder    ${banner1}    ${banner2}
    Go to    ${AdminURL}
    clickBannerMenuInAdmin
    clickDeleteDefaultColorBanner  ${banner1}
    confirmToDeleteBannerBroadcast
    clickDeleteDefaultColorBanner  ${banner2}
    confirmToDeleteBannerBroadcast

bannerPublishBasedOnPublishTime
    [Tags]    testrailid=4085  sanity
    ${key}    Generate random string    12    [LETTERS]
    Go to    ${AdminURL}
    skipTheAdminTutorial
    clickBannerMenuInAdmin
    clickCreateNewBannerBroadcast
    setBannerName  ${key}
    setUrlLink  https://www.ekoapp.com/
    clickEditPublishTime
    changeThePublishTime
    clickApplyChangePublishTime
    confrimCreateBannerBroadcast
    addDefaultColorBannerToTheQueue  ${key}
    publishTheBannerQueue
    Go to    ${ClientURL}
    isBannerDisappear  ${key}
    isBannerDisplayOnRecentPage  ${key}
    Go to    ${AdminURL}
    clickBannerMenuInAdmin
    clickDeleteDefaultColorBanner  ${key}
    confirmToDeleteBannerBroadcast
